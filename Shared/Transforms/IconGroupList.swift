//
//  IconGroups.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 02.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

class IconGroupList: ObservableObject {
    var requiredIcons: [Icon] = []
    var optionalIcons: [Icon] = []
    var other: [String] = ["about", "position details"]

    var icons: [Icon] {
        var list: [Icon] = []
        list.append(contentsOf: optionalIcons)
        list.append(contentsOf: requiredIcons)

        return list
    }

    var list: [IconGroup] = []

    private var otherAttributes: [String: [String: Attribute]] = [:]

    var attributes: [String: AttributeGroup] {
        var result: [String: AttributeGroup] = [:]

        for group in list.filter({ $0.hasMany == false }) {
            result[group.name] = .group(group.attributes)
        }

        for icon in icons.filter({ $0.allowMany == true }) {
            result[icon.name] = .list(list
                .filter { $0.icon.id == icon.id && !$0.isAddButton }
                .map { $0.attributes })
        }

        for (key, value) in otherAttributes {
            result[key] = .group(value)
        }

        return result
    }

    var isValid: Bool {
        let hasInvalidGroup = list.filter { !$0.isAddButton }.first { !$0.isValid }

        return hasInvalidGroup == nil
    }

    func sync(requiredIcons: [Icon], optionalIcons: [Icon]) {
        icons.filter { !requiredIcons.contains($0) }.forEach { icon in
            self.remove(icon: icon)
        }

        icons.filter { !optionalIcons.contains($0) }.forEach { icon in
            self.remove(icon: icon)
        }

        requiredIcons.filter { !icons.contains($0) }.forEach { icon in
            self.add(icon: icon, isRequired: true)
        }

        optionalIcons.filter { !icons.contains($0) }.forEach { icon in
            self.add(icon: icon, isRequired: false)
        }
    }

    func add(icon: Icon, isRequired: Bool) {
        if icon.attributes.isEmpty {
            return
        }

        let allowMany = icon.allowMany ?? false
        let hasIcon = icons.contains(icon)

        if hasIcon, !allowMany {
            return
        }

        if hasIcon, allowMany {
            list.filter { $0.icon.id == icon.id }.forEach { $0.touch() }
            let lastIndexOfIconGroup = list.lastIndex { $0.icon === icon }!
            let countOfGroups = list.filter { $0.icon == icon && !$0.isAddButton }.count
            list.insert(IconGroup(icon: icon, index: countOfGroups, isAddButton: false), at: lastIndexOfIconGroup)
        }

        if !hasIcon {
            if isRequired {
                requiredIcons.append(icon)
            } else {
                optionalIcons.append(icon)
            }
            list.append(IconGroup(icon: icon, index: -1, isAddButton: allowMany))
        }
    }

    func remove(icon: Icon) {
        requiredIcons = requiredIcons.filter { $0.id != icon.id }
        optionalIcons = optionalIcons.filter { $0.id != icon.id }

        list = list.filter { $0.icon.id != icon.id }
    }

    func canDelete(icon: Icon) -> Bool {
        let isRequired = requiredIcons.first { $0.id == icon.id } != nil

        if !isRequired {
            return true
        }

        if !(icon.allowMany ?? false) {
            return false
        }

        if list.filter({ $0.icon.id == icon.id && !$0.isAddButton }).count < 2 {
            return false
        }

        return true
    }

    func removeGroup(icon: Icon, index: Int) {
        list = list.filter { !($0.icon.id == icon.id && $0.index == index) }
        list.filter { $0.icon.id == icon.id }.forEach { $0.touch() }

        var itemIndex = 0
        list
            .enumerated()
            .forEach { index, group in
                if group.icon.id != icon.id {
                    return
                }

                if group.isAddButton {
                    itemIndex = -1
                }

                self.list[index].index = itemIndex
                itemIndex = itemIndex + 1
            }
    }

    func updateValue(groupName: String, index: Int, key: String, value: Attribute) {
        let isOther = other.first(where: { $0 == groupName }) != nil

        if isOther {
            if otherAttributes[groupName] == nil {
                otherAttributes[groupName] = [:]
            }

            otherAttributes[groupName]?[key] = value
            return
        }

        let found = list.first { $0.name == groupName && $0.index == index }
        let attr = found?.attributesWithValues.first { $0.attribute.name == key }
        attr?.value = value
    }
}

class IconGroup: ObservableObject {
    @Published var id: String = ""

    @Published var index: Int {
        didSet {
            touch()
        }
    }

    @Published var canDelete: Bool = false {
        didSet {
            touch()
        }
    }

    @Published var attributesWithValues: [AttributeWithValue] = []

    var isValid: Bool {
        return attributesWithValues
            .filter { $0.attribute.isRequired ?? false }
            .filter { $0.value == .undefined }
            .count == 0
    }

    let icon: Icon
    let isAddButton: Bool

    var hasMany: Bool {
        return icon.allowMany ?? false
    }

    var name: String {
        return icon.name
    }

    private var clock: Int = 0

    var attributes: [String: Attribute] {
        var result: [String: Attribute] = [:]

        for attributeWithValue in attributesWithValues {
            result[attributeWithValue.attribute.name] = attributeWithValue.value
        }

        return result
    }

    init(icon: Icon, index: Int, isAddButton: Bool) {
        self.icon = icon
        self.index = index
        self.isAddButton = isAddButton

        touch()

        attributesWithValues = icon.attributes.map { AttributeWithValue(attribute: $0) }
    }

    func touch() {
        id = "\(icon.id)-\(index)"

        if !isAddButton, clock > 0 {
            id = "\(id)-\(clock)"
        }

        clock = clock + 1
    }
}

class AttributeWithValue: Hashable {
    let attribute: IconAttribute
    var value: Attribute = .undefined

    init(attribute: IconAttribute) {
        self.attribute = attribute
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(attribute.name)
    }

    static func == (lhs: AttributeWithValue, rhs: AttributeWithValue) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

extension IconAttribute {
    var withValue: AttributeWithValue {
        return AttributeWithValue(attribute: self)
    }
}
