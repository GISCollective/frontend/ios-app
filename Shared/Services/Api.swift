//
//  Api.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 29.04.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

typealias ApiResponseHandler = (Data?, ApiResponseStatus) -> Void

enum ApiResponseStatus {
    case disconnected, serverError, success
}

enum ConnectionStatus {
    case disconnected, lowSpeed, highSpeed
}

protocol ApiMethods {
    var baseUrl: String { get }
    var connectivity: ConnectionStatus { get }

    func create<MI: CrateModelItem>(modelName: String, record: MI) async throws -> Data
    func update<MI: CrateModelItem>(modelName: String, record: MI) async throws -> Data

    func getAll(modelName: String) async throws -> Data
    func getById(modelName: String, id: String) async throws -> Data
    func query(modelName: String, params: [String: String]) async throws -> Data

    func authToken(email: String, password: String) async throws -> Data
    func refreshToken(token: String) async throws -> Data
}

class Api: ApiMethods {
    static var instance: ApiMethods!

    let baseUrl: String
    var connectivity: ConnectionStatus {
        let type = getConnectionType()

        if type == "NO INTERNET" || type == "UNKNOWN" {
            return .disconnected
        }

        if type == "2G" {
            return .lowSpeed
        }

        return .highSpeed
    }

    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }

    func composeUrl(modelName: String, params: [String: String] = [:]) -> URL? {
        var url = URL(string: "\(baseUrl)\(modelName)")!

        for param in params {
            url.appendQueryItem(name: param.key, value: param.value)
        }

        return url
    }

    func composeUrl(modelName: String, id: String, params: [String: String] = [:]) -> URL? {
        var url = URL(string: "\(baseUrl)\(modelName)/\(id)")!

        for param in params {
            url.appendQueryItem(name: param.key, value: param.value)
        }

        return url
    }

    func create<MI: CrateModelItem>(modelName: String, record: MI) async throws -> Data {
        let url = composeUrl(modelName: modelName)!
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601

        let data = try? encoder.encode(record)

        return try await makeRequest(method: "POST", url: url, data: data ?? Data())
    }

    func update<MI: CrateModelItem>(modelName: String, record: MI) async throws -> Data {
        let url = composeUrl(modelName: modelName, id: record.item.id)!
        print("HTTP PUT: ", url)

        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601

        let data = try? encoder.encode(record)

        return try await makeRequest(method: "PUT", url: url, data: data ?? Data())
    }

    func getAll(modelName: String) async throws -> Data {
        let url = composeUrl(modelName: modelName)!
        print("HTTP GET: ", url)

        return try await makeRequest(url: url)
    }

    func getById(modelName: String, id: String) async throws -> Data {
        let url = composeUrl(modelName: modelName, id: id)!
        print("HTTP GET: ", url)

        return try await makeRequest(url: url)
    }

    func query(modelName: String, params: [String: String]) async throws -> Data {
        let url = composeUrl(modelName: modelName, params: params)!
        print("HTTP GET: ", url)

        return try await makeRequest(url: url)
    }

    private func makeRequest(method: String, url: URL, data: Data) async throws -> Data {
        print("\(method) \(url)")
        let authorization = try await UserSessionService.instance.authorize()

        var request = URLRequest(url: url)

        request.httpMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = data

        if let header = authorization.header, let value = authorization.value {
            request.addValue(value, forHTTPHeaderField: header)
        }

        return try await withUnsafeThrowingContinuation { continuation in
            URLSession.shared.dataTask(with: request) { data, _, _ in
                if let data = data {
                    continuation.resume(returning: data)
                    return
                }

                continuation.resume(throwing: RequestError.disconnected)
            }
            .resume()
        }
    }

    private func makeRequest(url: URL) async throws -> Data {
        let authorization = try await UserSessionService.instance.authorize()

        var request = URLRequest(url: url)

        if let header = authorization.header, let value = authorization.value {
            request.addValue(value, forHTTPHeaderField: header)
        }

        return try await withUnsafeThrowingContinuation { continuation in
            URLSession.shared.dataTask(with: request) { data, _, _ in
                if let data = data {
                    continuation.resume(returning: data)
                    return
                }

                continuation.resume(throwing: RequestError.disconnected)
            }
            .resume()
        }
    }

    private func responseHandler(_ data: Data?, _ response: URLResponse?, _ error: Error?, _ completion: @escaping ApiResponseHandler) {
        var statusCode = 200
        if let error = error {
            print(error)
            statusCode = 500
        }

        let strData = String(data: data ?? Data(), encoding: .utf8) ?? ""

        #if DEBUG
            // print("RESPONSE BODY: ", strData)
        #endif

        if strData.range(of: "Invalid token.") != nil {
            UserSessionService.instance.failureCount += 1
        }

        var status: ApiResponseStatus = .serverError

        if let response = response as? HTTPURLResponse {
            statusCode = response.statusCode
        }

        if statusCode >= 200, statusCode < 300 {
            status = .success
        }

        if let data = data {
            completion(data, status)
            return
        }

        completion(nil, .disconnected)
    }

    private func responseHandler(_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ResponseData {
        var statusCode = 200
        if let error = error {
            print(error)
            statusCode = 500
        }

        let strData = String(data: data ?? Data(), encoding: .utf8) ?? ""

        #if DEBUG
            // print("RESPONSE BODY: ", strData)
        #endif

        if strData.range(of: "Invalid token.") != nil {
            UserSessionService.instance.failureCount += 1
        }

        var status: ApiResponseStatus = .serverError

        if let response = response as? HTTPURLResponse {
            statusCode = response.statusCode
        }

        if statusCode >= 200, statusCode < 300 {
            status = .success
        }

        if let data = data {
            return ResponseData(status: status, statusCode: statusCode, data: data)
        }

        return ResponseData(status: .disconnected, statusCode: 0, data: nil)
    }

    func authToken(email: String, password: String) async throws -> Data {
        let url = URL(string: "\(baseUrl)auth/token")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = OAuthLogin(username: email, password: password).percentEncoded()
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

        return try await withUnsafeThrowingContinuation { continuation in
            URLSession.shared.dataTask(with: request) { data, _, _ in
                if let data = data {
                    continuation.resume(returning: data)
                    return
                }

                continuation.resume(throwing: RequestError.disconnected)
            }
            .resume()
        }
    }

    func refreshToken(token: String) async throws -> Data {
        let url = URL(string: "\(baseUrl)auth/token")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = OAuthRefresh(refresh_token: token).percentEncoded()
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

        return try await withUnsafeThrowingContinuation { continuation in
            URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, _, error in
                if let data = data {
                    continuation.resume(returning: data)
                }

                if let error = error {
                    continuation.resume(throwing: error)
                }
            })
            .resume()
        }
    }
}

struct OAuthLogin {
    let grant_type = "password"
    let username: String
    let password: String

    func percentEncoded() -> Data {
        var params: [String: String] = [:]
        params["grant_type"] = grant_type
        params["username"] = username
        params["password"] = password

        return params.percentEncoded() ?? Data()
    }
}

struct OAuthRefresh {
    let grant_type = "refresh_token"
    let refresh_token: String

    func percentEncoded() -> Data {
        var params: [String: String] = [:]
        params["grant_type"] = grant_type
        params["refresh_token"] = refresh_token

        return params.percentEncoded() ?? Data()
    }
}

struct ResponseData {
    let status: ApiResponseStatus
    let statusCode: Int
    let data: Data?
}

enum RequestError: Error {
    case clientError
    case serverError
    case disconnected
}
