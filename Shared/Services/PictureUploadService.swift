//
//  PictureUploadService.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 25.09.21.
//

import Combine
import Foundation

class PictureUploadService: ObservableObject {
    @Published var progress: Double = 0

    var parentId: String
    let storage: SQLitePushQueuePersistence

    var remoteIds: [String] {
        return storedPictures.map { $0.remoteId }.filter { $0 != "" }
    }

    var localPictures: [StoredPushQueueElement] {
        return storedPictures.filter { $0.remoteId == "" }
    }

    var storedPictures: [StoredPushQueueElement] = []

    private var cancellables = Set<AnyCancellable>()

    var allUploaded: Bool {
        return localPictures.count == 0
    }

    init(parentId: String, storage: SQLitePushQueuePersistence) {
        self.parentId = parentId
        self.storage = storage
    }

    func updateProgress() {
        progress = Double(remoteIds.count) / Double(storedPictures.count)
    }

    func load() async throws {
        storedPictures = try await storage.get(parentId: parentId)

        updateProgress()
    }

    func send() async throws -> [String] {
        while !allUploaded {
            let before = localPictures.count
            try await sendNext()

            if localPictures.count == before {
                throw NSError(domain: "There was an error on uploading the pictures.", code: 42)
            }
        }

        return remoteIds
    }

    func sendNext() async throws {
        guard var item = localPictures.last else {
            return
        }

        let picture = Picture(id: "")
        picture.name = "image for \(parentId)"
        picture.picture = "data:image/jpeg;base64," + item.data.base64EncodedString()

        let newPicture = try await Store.instance.pictures.create(item: picture)
        updateProgress()

        item.remoteId = newPicture.id
        try await storage.save(item: item)
        try await load()
    }
}
