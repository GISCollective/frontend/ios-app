//
//  MemoryPersistence.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 26.01.22.
//

import Foundation

class MemoryPersistence: Persistence {
    var storedData: [String: Data] = [:]

    init() {
        super.init(path: ":memory:")
    }
}
