//
//  SQLitePushQueuePersistence.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 10.06.22.
//

import Foundation
import SQLite3

actor SQLitePushQueuePersistence: PushQueuePersistence {
    var db: OpaquePointer
    var tableName: String
    var errorMessage: String { return String(cString: sqlite3_errmsg(db)) }

    private static let insertModelRecord = "INSERT INTO :table (slug, remoteId, parentId, data, time, 'order') VALUES (?, ?, ?, ?, ?, ?);"
    private static let updateModelRecord = "UPDATE :table SET slug=?, remoteId=?, parentId=?, data=?, time=?, 'order'=? WHERE id=?;"
    private static let queryModelById = "SELECT id, slug, remoteId, parentId, data, time, 'order' FROM :table WHERE remoteId = ?;"
    private static let queryModelBySlug = "SELECT id, slug, remoteId, parentId, data, time, 'order' FROM :table WHERE slug = ?;"
    private static let queryModelByParentId = "SELECT id, slug, remoteId, parentId, data, time, 'order' FROM :table WHERE parentId = ?;"
    private static let queryAll = "SELECT id, slug, remoteId, parentId, data, time, 'order' FROM :table;"
    private static let countModels = "SELECT count(id) FROM :table"
    private static let countModelsBy = "SELECT count(id) FROM :table WHERE :field = ?"
    private static let createModelTable = "CREATE TABLE IF NOT EXISTS :table (id integer primary key autoincrement, slug text, remoteId text, parentId text, data BLOB, time INTEGER, 'order' INTEGER)"
    private static let clearModel = "DELETE FROM :table;"

    private var hasSetup: Bool = false

    init(db: OpaquePointer, tableName: String) {
        self.db = db
        self.tableName = tableName
    }

    private func setupTables() throws {
        if hasSetup {
            return
        }

        if sqlite3_exec(db, SQLitePushQueuePersistence.createModelTable.replacingOccurrences(of: ":table", with: tableName), nil, nil, nil) != SQLITE_OK {
            throw PersistenceError.QueryError(errorMessage)
        }

        hasSetup = true
    }

    func count() async throws -> Int32 {
        try setupTables()

        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.countModels.replacingOccurrences(of: ":table", with: tableName), -1, &statement, nil) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        return try handleCountStatement(statement: statement)
    }

    func clear() async throws {
        try setupTables()

        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.clearModel.replacingOccurrences(of: ":table", with: tableName), -1, &statement, nil) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        guard sqlite3_step(statement) == SQLITE_DONE else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_changes(db) > 0 else {
            throw PersistenceError.QueryError(errorMessage)
        }
    }

    func save(item: StoredPushQueueElement) async throws {
        try setupTables()

        var exists = false

        if item.id == 0, item.remoteId != "" {
            exists = try await self.exists(remoteId: item.remoteId)
        }

        if item.id != 0 {
            exists = try await self.exists(id: item.id)
        }

        if exists {
            try await update(item: item)
            return
        }

        try await insert(item: item)
    }

    func insert(item: StoredPushQueueElement) async throws {
        try setupTables()

        let now = Int32(Date().timeIntervalSince1970)
        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.insertModelRecord.replacingOccurrences(of: ":table", with: tableName), -1, &statement, nil) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        guard sqlite3_bind_text(statement, 1, item.slug, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_bind_text(statement, 2, item.remoteId, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_bind_text(statement, 3, item.parentId, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard item.data.withUnsafeBytes({ bufferPointer -> Int32 in
            sqlite3_bind_blob(statement, 4, bufferPointer.baseAddress, Int32(item.data.count), SQLITE_TRANSIENT)
        }) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_bind_int(statement, 5, now) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_bind_int(statement, 6, item.order) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_step(statement) == SQLITE_DONE else {
            throw PersistenceError.QueryError(errorMessage)
        }
    }

    func update(item: StoredPushQueueElement) async throws {
        try setupTables()

        let now = Int32(Date().timeIntervalSince1970)
        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.updateModelRecord.replacingOccurrences(of: ":table", with: tableName), -1, &statement, nil) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        guard sqlite3_bind_text(statement, 1, item.slug, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_bind_text(statement, 2, item.remoteId, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_bind_text(statement, 3, item.parentId, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard item.data.withUnsafeBytes({ bufferPointer -> Int32 in
            sqlite3_bind_blob(statement, 4, bufferPointer.baseAddress, Int32(item.data.count), SQLITE_TRANSIENT)
        }) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_bind_int(statement, 5, now) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_bind_int(statement, 6, item.order) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_bind_int(statement, 7, item.id) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_step(statement) == SQLITE_DONE else {
            throw PersistenceError.QueryError(errorMessage)
        }
    }

    func get(remoteId: String) async throws -> StoredPushQueueElement {
        try setupTables()

        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.queryModelById.replacingOccurrences(of: ":table", with: tableName), -1, &statement, nil) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        guard sqlite3_bind_text(statement, 1, remoteId, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_step(statement) == SQLITE_ROW else {
            throw PersistenceError.NotFound(errorMessage)
        }

        return try handleGet(statement: statement!)
    }

    func exists(remoteId: String) async throws -> Bool {
        try setupTables()

        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.countModelsBy
            .replacingOccurrences(of: ":table", with: tableName)
            .replacingOccurrences(of: ":field", with: "remoteId"), -1, &statement, nil) == SQLITE_OK
        else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        guard sqlite3_bind_text(statement, 1, remoteId, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        return try handleCountStatement(statement: statement) > 0
    }

    func exists(slug: String) async throws -> Bool {
        try setupTables()

        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.countModelsBy
            .replacingOccurrences(of: ":table", with: tableName)
            .replacingOccurrences(of: ":field", with: "slug"), -1, &statement, nil) == SQLITE_OK
        else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        guard sqlite3_bind_text(statement, 1, slug, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        return try handleCountStatement(statement: statement) > 0
    }

    func exists(parentId: String) async throws -> Bool {
        try setupTables()

        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.countModelsBy
            .replacingOccurrences(of: ":table", with: tableName)
            .replacingOccurrences(of: ":field", with: "parentId"), -1, &statement, nil) == SQLITE_OK
        else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        guard sqlite3_bind_text(statement, 1, parentId, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        return try handleCountStatement(statement: statement) > 0
    }

    func exists(id: Int32) async throws -> Bool {
        try setupTables()

        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.countModelsBy
            .replacingOccurrences(of: ":table", with: tableName)
            .replacingOccurrences(of: ":field", with: "id"), -1, &statement, nil) == SQLITE_OK
        else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        guard sqlite3_bind_int(statement, 1, id) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        return try handleCountStatement(statement: statement) > 0
    }

    func handleCountStatement(statement: OpaquePointer?) throws -> Int32 {
        try setupTables()

        guard sqlite3_step(statement) == SQLITE_ROW else {
            throw PersistenceError.NotFound(errorMessage)
        }

        guard let result = sqlite3_column_int(statement, 0) as? Int32 else {
            throw PersistenceError.QueryError(errorMessage)
        }

        return result
    }

    func get(slug: String) async throws -> StoredPushQueueElement {
        try setupTables()

        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.queryModelBySlug.replacingOccurrences(of: ":table", with: tableName), -1, &statement, nil) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        guard sqlite3_bind_text(statement, 1, slug, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard sqlite3_step(statement) == SQLITE_ROW else {
            throw PersistenceError.NotFound(errorMessage)
        }

        return try handleGet(statement: statement!)
    }

    func get(parentId: String) async throws -> [StoredPushQueueElement] {
        try setupTables()

        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.queryModelByParentId.replacingOccurrences(of: ":table", with: tableName), -1, &statement, nil) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        guard sqlite3_bind_text(statement, 1, parentId, -1, SQLITE_TRANSIENT) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        var list: [StoredPushQueueElement] = []

        while sqlite3_step(statement) == SQLITE_ROW {
            let item = try handleGet(statement: statement!)
            list.append(item)
        }

        return list
    }

    func getAll() async throws -> [StoredPushQueueElement] {
        try setupTables()

        var statement: OpaquePointer?

        guard sqlite3_prepare_v2(db, SQLitePushQueuePersistence.queryAll.replacingOccurrences(of: ":table", with: tableName), -1, &statement, nil) == SQLITE_OK else {
            throw PersistenceError.QueryError(errorMessage)
        }

        defer { sqlite3_finalize(statement) }

        var list: [StoredPushQueueElement] = []

        while sqlite3_step(statement) == SQLITE_ROW {
            let item = try handleGet(statement: statement!)
            list.append(item)
        }

        return list
    }

    func handleGet(statement: OpaquePointer) throws -> StoredPushQueueElement {
        var item = StoredPushQueueElement(id: 0, slug: "", remoteId: "", parentId: "", data: Data(count: 0), order: 0)

        guard let id = sqlite3_column_int(statement, 0) as? Int32 else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard let slug = sqlite3_column_text(statement, 1) else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard let remoteId = sqlite3_column_text(statement, 2) else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard let parentId = sqlite3_column_text(statement, 3) else {
            throw PersistenceError.QueryError(errorMessage)
        }

        let dataCount = Int(sqlite3_column_bytes(statement, 4))
        guard dataCount > 0 else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard let unsafeData = sqlite3_column_blob(statement, 4) else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard let time = sqlite3_column_int(statement, 5) as? Int32 else {
            throw PersistenceError.QueryError(errorMessage)
        }

        guard let order = sqlite3_column_int(statement, 6) as? Int32 else {
            throw PersistenceError.QueryError(errorMessage)
        }

        item.id = id
        item.parentId = String(cString: parentId)
        item.remoteId = String(cString: remoteId)
        item.slug = String(cString: slug)
        item.data = Data(bytes: unsafeData, count: dataCount)
        item.time = time
        item.order = order

        return item
    }
}
