//
//  Persistence.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 16.07.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import CoreData
import Foundation
import SQLite3
import UIKit

enum PersistenceError: Error {
    case runtimeError(String)
    case NotImplemented(String)
    case NotFound(String)
    case QueryError(String)
}

class OldPersistence {
    func getModelStorage(name: String) -> SQLitePushQueuePersistence {
        var db: OpaquePointer!

        if sqlite3_open_v2(":memory:", &db, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX, nil) != SQLITE_OK {
            print("error opening database")
            sqlite3_close(db)
            db = nil
        }

        let store = SQLitePushQueuePersistence(db: db, tableName: name)

        return store
    }
}

protocol PushQueuePersistence {
    func save(item: StoredPushQueueElement) async throws
}

struct StoredPushQueueElement: Equatable {
    var id: Int32
    var slug: String
    var remoteId: String
    var parentId: String
    var data: Data
    var time: Int32?
    var order: Int32
}

class PicturePersistence {
    func store(_: StoredPicture) throws {
        throw PersistenceError.NotImplemented("pictures store not implemented")
    }

    func getByParent(id _: String) throws -> [StoredPicture] {
        throw PersistenceError.NotImplemented("getByParent not implemented")
    }

    func exists(id _: String) throws -> Bool {
        throw PersistenceError.NotImplemented("exists not implemented")
    }

    func getById(id _: String) throws -> StoredPicture? {
        throw PersistenceError.NotImplemented("getById not implemented")
    }
}

struct StoredPicture: Equatable {
    var id: String
    var remoteId: String
    var data: Data
    var campaignAnswerId: String
}
