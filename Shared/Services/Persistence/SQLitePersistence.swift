//
//  SQLitePersisence.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 18.10.21.
//

import Foundation
import SQLite3
import Swift
import UIKit

let SQLITE_STATIC = unsafeBitCast(0, to: sqlite3_destructor_type.self)
let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)

actor Persistence {
    static var instance: Persistence?

    var db: OpaquePointer!

    private static let createModelTable = "CREATE TABLE IF NOT EXISTS cached_models (id integer primary key autoincrement, collection text, type text, _id text, data BLOB, pos INTEGER, time INTEGER)"
    private static let queryModelById = "SELECT data FROM cached_models WHERE _id = ? and type = ?;"
    private static let queryModelList = "SELECT data FROM cached_models WHERE type = ? and collection = ?;"
    private static let existsModelById = "SELECT count(id) FROM cached_models WHERE _id = ? and type = ?;"
    private static let insertModelRecord = "INSERT INTO cached_models (collection, type, _id, data, pos, time) VALUES (?, ?, ?, ?, ?, ?);"
    private static let updateModelRecord = "UPDATE cached_models SET collection = ?, data = ?, pos = ? , time = ? WHERE _id = ? AND type = ?;"
    private static let countModelById = "SELECT count(id) FROM cached_models WHERE collection = ? and type = ?;"
    private static let deleteModelById = "DELETE FROM cached_models WHERE _id = ? and type = ?;"
    private static let clearModelCache = "DELETE FROM cached_models;"

    var errorMessage: String { return String(cString: sqlite3_errmsg(db)) }

    private var persistenceMap: [String: SQLitePushQueuePersistence] = [:]

    init(path: String) {
        guard sqlite3_open_v2(path, &db, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX, nil) == SQLITE_OK else {
            print("error opening database")
            sqlite3_close(db)
            db = nil
            return
        }
    }

    init(name: String) {
        let fileURL = try! FileManager.default
            .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            .appendingPathComponent("\(name).sqlite")

        print("db is located at ", fileURL)

        self.init(path: fileURL.path)
    }

    func getModelStorage(name: String) -> SQLitePushQueuePersistence {
        if !persistenceMap.keys.contains(name) {
            persistenceMap[name] = SQLitePushQueuePersistence(db: db, tableName: name)
        }

        return persistenceMap[name]!
    }

    func clearCache() {}
}
