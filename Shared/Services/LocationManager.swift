import Combine
import CoreLocation
import Foundation
import GEOSwift

class LocationManager: NSObject, ObservableObject {
    static var instance: LocationManager? = LocationManager()

    var onPin: (() -> Void)?

    @Published var isPinned: Bool = false
    @Published var isError: Bool = false
    @Published var shouldPin: Bool = false
    @Published var canBeUpdated: Bool = false
    @Published var userLocation: Geometry?

    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    @Published var locationStatus: CLAuthorizationStatus? {
        willSet {
            objectWillChange.send()
        }
    }

    @Published var lastLocation: CLLocation? {
        willSet {
            objectWillChange.send()
        }
    }

    @Published var statusMessage: String = "Setting up..."

    @Published var pinnedLocation: CLLocation? {
        willSet {
            objectWillChange.send()
        }

        didSet {
            shouldPin = false
            isPinned = true
            canBeUpdated = false
            onPin?()
        }
    }

    var pinnedLatitude: String {
        return "\(pinnedLocation?.coordinate.latitude ?? 0)"
    }

    var pinnedLongitude: String {
        return "\(pinnedLocation?.coordinate.longitude ?? 0)"
    }

    let objectWillChange = PassthroughSubject<Void, Never>()

    private let locationManager = CLLocationManager()

    func pinPosition() {
        if let lastLocation = lastLocation {
            pinnedLocation = lastLocation
            statusMessage = "Your location was updated"
        } else {
            shouldPin = true
        }
    }
}

extension LocationManager: CLLocationManagerDelegate {
    static let updateDistance: Double = 100

    func locationManager(_: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationStatus = status

        switch status {
        case .notDetermined:
            statusMessage = "Waiting for your approval"
            isError = false
            isPinned = false
        case .authorizedWhenInUse:
            statusMessage = "Authorized to get your location"
            isError = false
            isPinned = false
        case .authorizedAlways:
            statusMessage = "Authorized to get your location"
            isError = false
            isPinned = false
        case .restricted:
            statusMessage = "Location services are restricted"
            isError = true
            isPinned = false
        case .denied:
            statusMessage = "The app does not have access to your location"
            isError = true
            isPinned = false
            break
        @unknown default:
            statusMessage = "Unknown error"
            isError = true
            isPinned = false
        }
    }

    func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        lastLocation = location
        userLocation = Geometry.point(Point(x: location.coordinate.longitude, y: location.coordinate.latitude))

        let distance = pinnedLocation?.distance(from: location) ?? 0

        if distance > LocationManager.updateDistance, !canBeUpdated {
            canBeUpdated = true
        }

        if shouldPin {
            pinnedLocation = location
            statusMessage = "Your location was updated"
        }
    }
}
