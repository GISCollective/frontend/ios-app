//
//  RemoteConnection.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 11.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

class RemoteConnection {
//    static let name = "app"
//    static let url = "https://app.giscollective.com"
//
//    static let name = "hbw"
//    static let url = "https://app.honeybeewatch.com"

    static let name = "ogm"
    static let domain = "new.opengreenmap.org"
    static let url = "https://new.opengreenmap.org"
//
//    static let domain = "192.168.100.129:9091"
//    static let url = "http://192.168.100.129:9091"

    static var instance = RemoteConnection(name: RemoteConnection.name, url: RemoteConnection.url)

    let registrationUrl: URL
    let forgotPasswordUrl: URL

    let name: String
    let url: String

    init(name: String, url: String) {
        Api.instance = Api(baseUrl: "\(url)/api-v1/")
        Persistence.instance = Persistence(name: name)
        UserSessionService.instance = UserSessionService()

        forgotPasswordUrl = URL(string: "\(url)/login/reset")!
        registrationUrl = URL(string: "\(url)/login/register?embed=true")!
        self.name = name
        self.url = url
    }

    func getMapStyleUrl(id: String?) -> URL {
        return URL(string: "\(RemoteConnection.url)/api-v1/tiles/\(id ?? "_")/style.json")!
    }
}
