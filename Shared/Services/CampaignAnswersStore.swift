//
//  CampaignAnswersStore.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 15.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import Foundation
import UIKit

actor CampaignAnswerStoreData {
    var isSending: Bool = false

    var sentAnswers: Double = 0
    var totalAnswers: Double = 0
}

final class CampaignAnswerStore: ObservableObject, @unchecked Sendable {
    static var instance = CampaignAnswerStore()

    private let serializer = JSONEncoder()
    private let deserializer = JSONDecoder()
    private let data = CampaignAnswerStoreData()

    var localAnswers: [CampaignAnswer] = []
    private var storageAnswers: SQLitePushQueuePersistence!
    private var storagePictures: SQLitePushQueuePersistence!

    @Published var storedItemCount: Int = 0
    @Published var totalAnswers: Double = 0
    @Published var sentAnswers: Double = 0
    @Published var isSending: Bool = false
    @Published var progress: Double = 0

    init() {
        serializer.dataEncodingStrategy = .base64
        serializer.dateEncodingStrategy = .iso8601

        deserializer.dataDecodingStrategy = .base64
        deserializer.dateDecodingStrategy = .iso8601
    }

    func setupStorage() async {
        storageAnswers = await Persistence.instance?.getModelStorage(name: "CampaignAnswers")
        storagePictures = await Persistence.instance?.getModelStorage(name: "CampaignAnswersPictures")
    }

    func updateStoredItemCount() async throws {
        await setupStorage()

        let storedData = try await storageAnswers.getAll()
        let value = storedData.filter { $0.remoteId == "" }

        let group = DispatchGroup()

        group.enter()
        DispatchQueue.main.sync {
            defer { group.leave() }
            self.storedItemCount = Int(value.count)
        }

        group.wait()
    }

    func save(picture: UIImage, answerId: String, index: Int) async throws {
        await setupStorage()

        let data = picture.jpegData(compressionQuality: 0.9) ?? Data()

        let record = StoredPushQueueElement(id: 0, slug: "", remoteId: "", parentId: answerId, data: data, order: Int32(index))

        try await storagePictures.save(item: record)
        try await updateStoredItemCount()
    }

    func save(item: AnswerWithPictures) async throws {
        await setupStorage()

        item.answer.id = UUID().uuidString

        let answerData = try serializer.encode(item.answer)

        let record = StoredPushQueueElement(id: 0, slug: item.answer.id, remoteId: "", parentId: item.answer.campaignId, data: answerData, order: 0)

        try await storageAnswers.save(item: record)

        var i = 0
        for element in item.pictures {
            try await save(picture: element, answerId: item.answer.id, index: i)
            i += 1
        }

        try await updateStoredItemCount()
    }

    func loadLocalAnswers() async throws {
        await setupStorage()
        let storedData = try await storageAnswers.getAll()
        localAnswers = try storedData
            .filter { $0.remoteId == "" }
            .map { try deserializer.decode(CampaignAnswer.self, from: $0.data) }
    }

    func sendAll() async throws {
        if await data.isSending {
            return
        }

        let group = DispatchGroup()

        group.enter()
        DispatchQueue.main.async {
            defer { group.leave() }

            self.totalAnswers = Double(self.localAnswers.count)
            self.sentAnswers = 0
            self.isSending = true
        }
        group.wait()

        defer {
            group.enter()
            DispatchQueue.main.async {
                defer { group.leave() }
                self.isSending = false
            }
            group.wait()
        }

        repeat {
            try await loadLocalAnswers()
            try await sendNext()
        } while localAnswers.count > 0
    }

    func sendPicturesFor(answerId: String) async throws -> [String] {
        await setupStorage()

        let service = PictureUploadService(parentId: answerId, storage: storagePictures)
        try await service.load()

        return try await service.send()
    }

    func sendNext() async throws {
        await setupStorage()

        if localAnswers.count == 0 {
            return
        }

        guard let decodedAnswer = localAnswers.last else {
            return
        }

        var localRecord = try await storageAnswers.get(slug: decodedAnswer.id)
        let picturesId = try await sendPicturesFor(answerId: decodedAnswer.id)

        decodedAnswer.picturesId = picturesId

        let response = try await Store.instance.campaignAnswers.create(item: decodedAnswer)

        localRecord.remoteId = response.id
        try await storageAnswers.save(item: localRecord)
    }
}

struct AnswerWithPictures {
    var answer: CampaignAnswer
    var pictures: [UIImage]
}
