//
//  PictureImageCache.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 09.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import CoreLocation
import Foundation
import SwiftUI
import UIKit

class PictureImageCache {
    static var instance = PictureImageCache()

    private var cache: URLCache
    private var cancellable: AnyCancellable?

    lazy var cachedUrlsession: URLSession = {
        let config = URLSessionConfiguration.default
        config.urlCache = cache
        return URLSession(configuration: config)
    }()

    init() {
        let cachesURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let diskCacheURL = cachesURL.appendingPathComponent("PictureImageCache")
        cache = URLCache(memoryCapacity: 50_000_000, diskCapacity: 1_000_000_000, directory: diskCacheURL)
    }

    func get(id: String) async throws -> UIImage {
        let remoteURL = URL(string: "\(Api.instance.baseUrl)pictures/\(id)/picture/md")

        return try await get(remoteURL: remoteURL!)
    }

    func get(baseUrl: String, size: String) async throws -> UIImage {
        let remoteURL = URL(string: "\(baseUrl)/\(size)")

        return try await get(remoteURL: remoteURL!)
    }

    func get(remoteURL: URL) async throws -> UIImage {
        let request = URLRequest(url: remoteURL)
        let id = remoteURL.absoluteString

        let fileStorage = await Persistence.instance?.getModelStorage(name: "Files")

        if let exists = try await fileStorage?.exists(remoteId: id), exists == true {
            let fileRecord = try await fileStorage?.get(remoteId: id)

            if let data = fileRecord?.data, let image = UIImage(data: data) {
                return image
            }
        }

        let (data, response) = try await URLSession.shared.data(for: request)
        let image = UIImage(data: data)

        if cache.cachedResponse(for: request) == nil {
            cache.storeCachedResponse(CachedURLResponse(response: response, data: data), for: request)
        }

        let item = StoredPushQueueElement(id: 0, slug: id, remoteId: id, parentId: "", data: data, order: 0)
        try await fileStorage?.save(item: item)

        return image ?? ObservableUIImage.defaultValue.value!
    }
}
