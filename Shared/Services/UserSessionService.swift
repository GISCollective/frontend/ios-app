//
//  UserSessionService.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 10.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import Foundation

class UserSessionService: ObservableObject {
    static var instance = UserSessionService()

    private var accessToken: String?
    private var refreshToken: String?
    private var expiration: Date?

    @Published var isAuthenticated: Bool = false
    @Published var user: User?
    @Published var profile: UserProfile?

    var failureCount: Int = 0 {
        didSet {
            expiration = Date(timeIntervalSinceNow: 0)
        }
    }

    var willExpire: Bool {
        if let expiration = expiration {
            let diff = Date().distance(to: expiration)
            return diff < 60 * 5
        }

        return false
    }

    init(withRefresh: Bool) async {
        restoreTokens()

        if withRefresh {
            _ = try? await refresh()
        }
    }

    init() {
        restoreTokens()
    }

    func authorize() async throws -> AuthHeader {
        var header = AuthHeader(header: nil, value: nil)

        try await refresh()

        if let accessToken = accessToken {
            header = AuthHeader(header: "Authorization", value: "Bearer \(accessToken)")
        }

        return header
    }

    func clear() {
        accessToken = nil
        refreshToken = nil
        expiration = nil
        isAuthenticated = false
        user = nil
        profile = nil

        let secQuery: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
        ]

        let status = SecItemDelete(secQuery as CFDictionary)
        if status != errSecSuccess {
            print("The keychain data can not be deleted.")
        }
    }

    func loadCurrentUser() async throws -> User {
        let user = try await Store.instance.users.getById(id: "me")

        return user
    }

    func loadCurrentProfile() async throws -> UserProfile {
        let user = try await loadCurrentUser()

        return try await Store.instance.userProfiles.getById(id: user.id)
    }

    func refresh() async throws {
        if refreshToken == nil {
            return
        }

        if !willExpire {
            return
        }

        let data = try await Api.instance.refreshToken(token: refreshToken!)

        handleTokenResponse(data: data, onReady: { _, _, _ in })
    }

    func login(email: String, password: String) async throws -> String? {
        let data = try await Api.instance.authToken(email: email, password: password)

        try handleTokenResponse(data: data)

        return nil
    }

    func resetPassword(email _: String) async throws {}

    private func handleTokenResponse(data: Data?, onReady: @escaping (_ isSuccessful: Bool, _ message: String?, ApiResponseStatus) -> Void) {
        if data == nil {
            onReady(false, "Request error", .success)
            return
        }

        do {
            if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                let error = decodeOauthObject(json: json)

                if error == nil {
                    storeTokens()
                }

                onReady(error == nil, error, .success)
            }
        } catch {
            onReady(false, error.localizedDescription, .success)
        }
    }

    private func handleTokenResponse(data: Data?) throws {
        if data == nil {
            return
        }

        if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
            let error = decodeOauthObject(json: json)

            if error == nil {
                storeTokens()
            } else {
                throw RequestError.clientError
            }
        }
    }

    private func decodeOauthObject(json: [String: Any]) -> String? {
        if let error = json["error"] as? String {
            clear()
            return error
        }

        if let access_token = json["access_token"] as? String {
            accessToken = access_token
        }

        if let refresh_token = json["refresh_token"] as? String {
            refreshToken = refresh_token
        }

        if let expires_in = json["expires_in"] as? Int {
            expiration = Date(timeIntervalSinceNow: Double(expires_in))
        }

        if let expiration = json["expiration"] as? String {
            self.expiration = Date.fromISOString(expiration)
        }

        if refreshToken != nil {
            DispatchQueue.main.async {
                self.isAuthenticated = true
            }
        }

        return nil
    }

    private func storeTokens() {
        let data: [String: String] = [
            "access_token": accessToken ?? "",
            "refresh_token": refreshToken ?? "",
            "expiration": expiration?.toISOString() ?? "",
        ]

        let jsonData = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        let jsonString = String(data: jsonData ?? Data(), encoding: .utf8)

        let attributes: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: "GISCollective",
            kSecAttrService as String: jsonString ?? "",
        ]

        let status: OSStatus = SecItemAdd(attributes as CFDictionary, nil)

        if status == noErr {
            print("User saved successfully in the keychain")
        } else {
            print("Something went wrong trying to save the user in the keychain:", status)
        }
    }

    private func restoreTokens() {
        var item: CFTypeRef?
        let secQuery: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: "GISCollective",
            kSecMatchLimit as String: kSecMatchLimitOne,
            kSecReturnAttributes as String: true,
            kSecReturnData as String: true,
        ]

        let status: OSStatus = SecItemCopyMatching(secQuery as CFDictionary, &item)

        // Check if user exists in the keychain
        if status != noErr {
            print("Something went wrong trying to find the user in the keychain")
            return
        }

        let existingItem = item as? [String: Any]

        if existingItem == nil {
            return
        }

        // Extract result
        if let jsonString = existingItem![kSecAttrService as String] as? String,
           let json = try? JSONSerialization.jsonObject(with: jsonString.data(using: .utf8) ?? Data(), options: .mutableContainers) as? [String: Any]
        {
            _ = decodeOauthObject(json: json)
        }
    }
}

struct AuthHeader {
    let header: String?
    let value: String?
}
