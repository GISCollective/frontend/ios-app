//
//  Data.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 04.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//
/*
 Abstract:
 Helpers for loading data.
 */

import Combine
import CoreLocation
import Foundation
import SwiftUI
import UIKit

protocol CrateModel: Sendable, Codable, Hashable, Identifiable, ObservableObject {
    static var modelName: String { get }
    var id: String { get set }

    init(id: String)
    func decode(from decoder: Decoder) throws
    func copy(_ from: Self)
}

protocol CrateModelList: Codable, Sendable {
    associatedtype M: CrateModel

    static var name: String { get }
    var items: [M] { get }

    init(_ items: [M])
}

protocol CrateModelItem: Codable, Sendable {
    associatedtype M: CrateModel

    static var name: String { get }
    var item: M { get }

    init(_ item: M)
}

enum StoreError: Error {
    case runtimeError(String)
    case NotImplemented(String)
    case DecoderError(String)
}

class Store {
    static var instance = Store()

    lazy var features: ModelStore<FeatureList, FeatureItem> = {
        ModelStore<FeatureList, FeatureItem>()
    }()

    lazy var maps: ModelStore<MapList, MapItem> = {
        ModelStore<MapList, MapItem>()
    }()

    lazy var baseMaps: ModelStore<BaseMapList, BaseMapItem> = {
        ModelStore<BaseMapList, BaseMapItem>()
    }()

    lazy var sounds: ModelStore<SoundList, SoundItem> = {
        ModelStore<SoundList, SoundItem>()
    }()

    lazy var teams: ModelStore<TeamList, TeamItem> = {
        ModelStore<TeamList, TeamItem>()
    }()

    lazy var campaigns: ModelStore<CampaignList, CampaignItem> = {
        ModelStore<CampaignList, CampaignItem>()
    }()

    lazy var campaignAnswers: ModelStore<CampaignAnswerList, CampaignAnswerItem> = {
        ModelStore<CampaignAnswerList, CampaignAnswerItem>()
    }()

    lazy var pictures: ModelStore<PictureList, PictureItem> = {
        ModelStore<PictureList, PictureItem>()
    }()

    lazy var icons: ModelStore<IconList, IconItem> = {
        ModelStore<IconList, IconItem>()
    }()

    lazy var iconSets: ModelStore<IconSetList, IconSetItem> = {
        ModelStore<IconSetList, IconSetItem>()
    }()

    lazy var userProfiles: ModelStore<UserProfileList, UserProfileItem> = {
        ModelStore<UserProfileList, UserProfileItem>()
    }()

    lazy var users: ModelStore<UserList, UserItem> = {
        ModelStore<UserList, UserItem>()
    }()

    lazy var layouts: ModelStore<LayoutList, LayoutItem> = {
        ModelStore<LayoutList, LayoutItem>()
    }()

    lazy var pages: ModelStore<PageList, PageItem> = {
        ModelStore<PageList, PageItem>()
    }()

    lazy var spaces: ModelStore<SpaceList, SpaceItem> = {
        ModelStore<SpaceList, SpaceItem>()
    }()

    lazy var articles: ModelStore<ArticleList, ArticleItem> = {
        ModelStore<ArticleList, ArticleItem>()
    }()

    func clearCache() async {
        try? await features.clearCache()
        try? await campaigns.clearCache()
        try? await pages.clearCache()
        try? await layouts.clearCache()
        try? await pictures.clearCache()
        try? await icons.clearCache()
        try? await userProfiles.clearCache()
        try? await users.clearCache()
    }
}

class ModelStore<ML: CrateModelList, MI: CrateModelItem> where ML.M == MI.M {
    private var decoder = JSONDecoder()
    private var encoder = JSONEncoder()
    private var initTimestamp: Int32

    typealias ItemCompletion = (MI.M, ApiResponseStatus) -> Void
    var store: SQLitePushQueuePersistence!

    init() {
        decoder.dataDecodingStrategy = .base64
        decoder.dateDecodingStrategy = .formatted(DateFormatter.fullISO8601)

        encoder.dataEncodingStrategy = .base64
        encoder.dateEncodingStrategy = .formatted(DateFormatter.fullISO8601)

        initTimestamp = Int32(Date().timeIntervalSince1970)
    }

    func checkStore() async {
        if store != nil {
            return
        }

        let modelStorage = await Persistence.instance?.getModelStorage(name: MI.M.modelName)
        store = modelStorage
    }

    func clearCache() async throws {
        await checkStore()
        try await store.clear()
    }

    func create(item: MI.M) async throws -> MI.M {
        await checkStore()
        let record = MI(item)

        let data = try await Api.instance.create(modelName: MI.name, record: record)

        return try await decodeItem(data: data)
    }

    func update(item: MI.M) async throws -> MI.M {
        await checkStore()
        let record = MI(item)

        let data = try await Api.instance.update(modelName: MI.name, record: record)

        return try await decodeItem(data: data)
    }

    func getById(id: String) async throws -> MI.M {
        await checkStore()
        var storedRecord = try? await store.get(remoteId: id)

        if storedRecord == nil {
            storedRecord = try? await store.get(slug: id)
        }

        if let storedRecord = storedRecord {
            let time = storedRecord.time ?? 0
            let diff = time - initTimestamp

            print(diff, getConnectionType())

            if diff >= 0 || getConnectionType() == "NO INTERNET" {
                return try await decodeModel(data: storedRecord.data)
            }
        }

        var record = MI.M(id: id)

        if let data = try? await Api.instance.getById(modelName: MI.name, id: id) {
            record = try await decodeItem(data: data)

            try await save(item: record, otherId: id)
        }

        return record
    }

    private func save(item: MI.M, otherId: String = "") async throws {
        await checkStore()
        let data = try encoder.encode(item)
        let record = StoredPushQueueElement(id: 0, slug: otherId, remoteId: item.id, parentId: "", data: data, order: 0)

        try await store.save(item: record)
    }

    private func save(items: ML, collection: String) async throws {
        await checkStore()
        let data = try encoder.encode(items)
        let record = StoredPushQueueElement(id: 0, slug: collection, remoteId: collection, parentId: "", data: data, order: 0)

        try await store.save(item: record)
    }

    private func decodeModel(data: Data) async throws -> MI.M {
        var decodedResponse: MI.M?

        do {
            decodedResponse = try decoder.decode(MI.M.self, from: data)

            return decodedResponse!
        } catch {
            let errorMessage = "JSON DECODE ERROR for \(MI.name) by id: \(error)"
            print(String(decoding: data, as: UTF8.self))

            throw StoreError.DecoderError(errorMessage)
        }
    }

    private func decodeItem(data: Data) async throws -> MI.M {
        var decodedResponse: MI?

        do {
            decodedResponse = try decoder.decode(MI.self, from: data)

            guard let item = decodedResponse?.item else {
                throw StoreError.DecoderError("JSON DECODE ERROR for \(MI.name)")
            }

            return item
        } catch {
            let errorMessage = "JSON DECODE ERROR for \(MI.name) by id: \(error)"

            print(String(decoding: data, as: UTF8.self))

            throw StoreError.DecoderError(errorMessage)
        }
    }

    private func decodeList(data: Data?) async throws -> ML {
        guard let data = data else { return ML([]) }

        var decodedResponse = ML([])

        do {
            decodedResponse = try decoder.decode(ML.self, from: data)
        } catch {
            let errorMessage = "JSON DECODE ERROR for \(ML.name) by id: \(error)"

            throw StoreError.DecoderError(errorMessage)
        }

        return decodedResponse
    }

    func getList() async throws -> ML {
        await checkStore()
        let storedRecords = try? await store.get(slug: "all")

        if let data = storedRecords?.data {
            return try await decodeList(data: data)
        }

        let remoteData = try await Api.instance.getAll(modelName: MI.name)

        let recordList = try await decodeList(data: remoteData)

        try await save(items: recordList, collection: "all")

        for item in recordList.items {
            try await save(item: item)
        }

        return recordList
    }

    func query(params: [String: String], collection: String, force: Bool = false) async throws -> ML {
        await checkStore()

        if !force || Api.instance.connectivity == .disconnected {
            let storedRecords = try? await store.get(slug: collection)
            let time = storedRecords?.time ?? 0
            let diff = time - initTimestamp

            if let record = storedRecords {
                if diff >= 0 || getConnectionType() == "NO INTERNET" {
                    return try await decodeList(data: record.data)
                }
            }
        }

        let data = try await Api.instance.query(modelName: ML.name, params: params)
        let decodedResponse = try await decodeList(data: data)

        try await save(items: decodedResponse, collection: collection)

        let items = decodedResponse.items
        for item in items {
            try await save(item: item)
        }

        return decodedResponse
    }

    func peekList(collection: String) async throws -> ML {
        await checkStore()
        let modelStore = await Persistence.instance?.getModelStorage(name: ML.M.modelName)
        let record = try await modelStore?.get(slug: collection)

        guard record != nil else {
            return ML([])
        }

        let storedItems = try decoder.decode(ML.self, from: record!.data)

        return storedItems
    }
}

final class ImageStore {
    typealias _ImageDictionary = [String: CGImage]
    private var images: _ImageDictionary = [:]

    fileprivate static var scale = 2

    static var shared = ImageStore()

    func image(urlString: String) -> Image {
        let index = _guaranteeImage(urlString: urlString)

        return Image(images.values[index], scale: CGFloat(ImageStore.scale), label: Text(""))
    }

    static func loadImage(urlString: String) -> CGImage {
        guard
            let url = URL(string: urlString),
            let imageSource = CGImageSourceCreateWithURL(url as NSURL, nil),
            let image = CGImageSourceCreateImageAtIndex(imageSource, 0, nil)
        else {
            fatalError("Couldn't load image \(urlString) from main bundle.")
        }
        return image
    }

    private func _guaranteeImage(urlString: String) -> _ImageDictionary.Index {
        if let index = images.index(forKey: urlString) { return index }

        images[urlString] = ImageStore.loadImage(urlString: urlString)
        return images.index(forKey: urlString)!
    }
}

class ObservableUIImage: ObservableObject {
    @Published var value: UIImage?
    @Published var isLoading: Bool = false
    @Published var isDefault: Bool = false

    private var cancellable: AnyCancellable?
    private var startTime: CFAbsoluteTime?
    private static let imageProcessingQueue = DispatchQueue(label: "image-processing")

    static var defaultValue: ObservableUIImage = {
        let image = UIImage(blurHash: "UKO2?V%2Tw=w]~RBVZRi};RPxuwHtLOtxZ%g", size: CGSize(width: 40, height: 40))!

        return ObservableUIImage(resolved: image, isDefault: true)
    }()

    let baseUrl: String
    let size: String

    static func createDefault() -> ObservableUIImage {
        let image = UIImage(blurHash: "UKO2?V%2Tw=w]~RBVZRi};RPxuwHtLOtxZ%g", size: CGSize(width: 40, height: 40))!

        return ObservableUIImage(resolved: image, isDefault: true)
    }

    init(baseUrl: String, size: String) {
        self.baseUrl = baseUrl
        self.size = size
    }

    init(resolved: UIImage, isDefault: Bool = false) {
        value = resolved
        baseUrl = ""
        size = ""
        self.isDefault = isDefault
    }

    func load() {
        guard !isLoading else { return }
        guard value == nil else { return }

        startTime = CFAbsoluteTimeGetCurrent()

        objectWillChange.send()
        isLoading = true

        Task {
            let image = try await PictureImageCache.instance.get(baseUrl: baseUrl, size: size)

            DispatchQueue.main.async {
                self.objectWillChange.send()
                self.value = image
                self.isLoading = false
            }
        }
    }

    deinit {
        cancel()
    }

    private func onStart() {}

    private func onFinish() {}

    private func cache(_: UIImage?) {}

    func cancel() {
        cancellable?.cancel()
    }
}

class ObservableModel<M: CrateModel>: ObservableObject, Identifiable {
    @Published var value: M?
    let id: String
    var task: AppTask?

    init(id: String) {
        self.id = id
    }

    init(value: M) {
        self.value = value
        id = value.id
    }
}
