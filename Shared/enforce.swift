//
//  enforce.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 27.01.22.
//

import Foundation

func enforce(_ value: Bool, _ message: String) throws {
    if !value {
        throw NSError(domain: message, code: 42)
    }
}
