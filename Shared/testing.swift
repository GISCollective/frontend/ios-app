//
//  testing.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 18.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//
import Combine
import SwiftUI

typealias InspectionNotice = PassthroughSubject<UInt, Never>

enum InspectionNoticeInstance {
    public static let notice = InspectionNotice()
}

internal final class Inspection<V> {
    let notice = InspectionNoticeInstance.notice
    var callbacks = [UInt: (V) -> Void]()

    func visit(_ view: V, _ line: UInt) {
        if let callback = callbacks.removeValue(forKey: line) {
            callback(view)
        }
    }
}
