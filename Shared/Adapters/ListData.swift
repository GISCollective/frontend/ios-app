//
//  ListData.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 19.07.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

actor ListDataStore<M> {
    var loading: Bool = false
    var data: [M] = []

    func append(_ item: M) {
        data.append(item)
    }

    func append(contentsOf: [M]) {
        data.append(contentsOf: contentsOf)
    }

    func isLoading() -> Bool {
        return loading
    }

    func startLoading() {
        loading = true
    }

    func endLoading() {
        loading = false
    }
}

class ListData<M, ML: CrateModelList, MI: CrateModelItem>: @unchecked Sendable, ObservableObject where ML.M == M, MI.M == M {
    @Published var data: [M] = []
    @Published var isLoading: Bool = false

    let actorData = ListDataStore<M>()

    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()

    private let store: ModelStore<ML, MI>
    var preferCache: Bool = true
    var name: String

    var endReached: Bool = false
    var pageSize: Int = 15

    private let queue = DispatchQueue(label: "list_data")

    init(store: ModelStore<ML, MI>, name: String) {
        self.store = store
        self.name = name
    }

    private func copyFromActor() async -> Int {
        let loading = await actorData.loading
        let newData = await actorData.data

        DispatchQueue.main.async {
            self.isLoading = loading
            self.data = newData
        }

        return newData.count
    }

    func clear() {
        data.removeAll()
        endReached = false
    }

    func nextPageOptions() -> [String: String] {
        return [:]
    }

    func reload() async throws {
        print("[\(name)] reload list")
        endReached = false

        try await loadNextPage()
    }

    func stop() {
        print("[\(name)] stop loading list")
    }

    func loadNextPage() async throws {
        if endReached {
            return
        }

        let isLoading = await actorData.isLoading()

        if isLoading == true {
            return
        }

        await actorData.startLoading()
        let dataCount = await copyFromActor()

        var params = nextPageOptions()

        params["limit"] = "\(pageSize)"
        params["skip"] = "\(dataCount)"
        print("[\(name)] requesting list with `\(params)` items")

        guard let list = try? await store.query(params: params, collection: "\(name) \(dataCount) \(pageSize)", force: !preferCache) else {
            endReached = true

            await actorData.endLoading()
            return
        }

        let items = list.items

        print("[\(name)] filling in `\(items.count)` items")

        await actorData.append(contentsOf: items)
        await actorData.endLoading()

        if items.count == 0 {
            endReached = true
        }

        _ = await copyFromActor()
    }

    func loadMoreContentIfNeeded(currentItem item: M?) async throws {
        guard let item = item else {
            try await loadNextPage()
            return
        }

        let thresholdIndex = data.count - 10
        let index = data.lastIndex(where: { $0.id == item.id }) ?? thresholdIndex
        let isLoading = await actorData.loading

        if index >= thresholdIndex, isLoading == false {
            try await loadNextPage()
        }
    }
}
