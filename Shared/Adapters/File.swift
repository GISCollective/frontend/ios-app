//
//  File.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 29.04.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

let iconData: [Icon] = load("iconData.json")
let featureData: [Feature] = load("featureData.json")
let campaignData: [Campaign] = load("campaignData.json")
let mapData: [Map] = load("mapData.json")

func load<T: Decodable>(_ filename: String) -> T {
    let data: Data

    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
    else {
        fatalError("Couldn't find \(filename) in main bundle.")
    }

    do {
        data = try Data(contentsOf: file)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }

    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}
