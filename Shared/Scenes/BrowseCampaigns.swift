//
//  BrowseCampaigns.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 05.05.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct BrowseCampaigns: View {
    @State var page: Page?

    init() {
        UITableView.appearance().separatorStyle = .none
    }

    var body: some View {
        VStack {
            if let page = page {
                PageView(page: page)
            }
        }
        .task {
            page = try? await Store.instance.pages.getById(id: "campaigns")
            try? await page?.loadLayout()
        }
    }
}

class CampaignListData: ListData<Campaign, CampaignList, CampaignItem> {
    private var loadTask: URLSessionTask?

    var latitude: String = ""
    var longitude: String = ""

    override func nextPageOptions() -> [String: String] {
        return ["lat": latitude, "lon": longitude]
    }

    init() {
        super.init(store: Store.instance.campaigns, name: "all campaigns")
        pageSize = 1000
    }
}

struct BrowseCampaigns_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            BrowseCampaigns()
        }
    }
}
