//
//  FeatureList.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 05.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import CoreLocation
import SwiftUI

struct WhatsNearbyScene: View {
    @ObservedObject var model: FeatureListData
    @ObservedObject var locationManager = LocationManager.instance!
    @State var mapCenter: CLLocationCoordinate2D? = nil
    var options: NSDictionary
    var settingsUrl: URL

    @Environment(\.openURL) var openURL

    init(featureList: FeatureListData) {
        model = featureList
        options = [:]
        settingsUrl = URL(string: UIApplication.openSettingsURLString)!
    }

    func refreshList() {
        locationManager.onPin = {
            self.model.latitude = locationManager.pinnedLatitude
            self.model.longitude = locationManager.pinnedLongitude

            Task {
                try await self.model.reload()
            }
        }
        locationManager.pinPosition()
    }

    var body: some View {
        if self.locationManager.isError {
            VStack {
                Label("", systemImage: "location.slash.fill")
                    .font(.title)
                    .foregroundColor(Color.red)
                    .padding(10)
                Text(self.locationManager.statusMessage)
                Button(action: { openURL(settingsUrl) }) {
                    Text("Change location settings")
                        .padding()
                }
            }
        }

        if !self.locationManager.isError {
            FeatureListView(mapCenter: $mapCenter, refreshList: refreshList, model: model)
                .ignoresSafeArea()
                .onDisappear {
                    self.model.stop()
                }
                .onAppear {
                    locationManager.pinPosition()
                }
                .onReceive(locationManager.$pinnedLocation) { value in
                    if let coordinate = value?.coordinate {
                        mapCenter = coordinate
                        self.model.latitude = "\(coordinate.latitude)"
                        self.model.longitude = "\(coordinate.longitude)"
                    }
                }
        }
    }
}

class FeatureListData: ListData<Feature, FeatureList, FeatureItem> {
    var latitude: String?
    var longitude: String?
    var map: Map? {
        didSet {
            name = "\(map?.id ?? "some-id") \(map?.name ?? "some-map")"
        }
    }

    override func nextPageOptions() -> [String: String] {
        var options: [String: String] = [:]

        if let lat = latitude {
            options["lat"] = lat
        }

        if let lon = longitude {
            options["lon"] = lon
        }

        if let map = map, map.id != "" {
            options["map"] = map.id
        } else {
            options["onMainMap"] = "true"
        }

        return options
    }

    init(name: String) {
        super.init(store: Store.instance.features, name: name)
        preferCache = false
    }
}
