//
//  LoginScene.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 10.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

enum LoginStates {
    case login,
         loading,
         success,
         error,
         disconnected
}

struct LoginScene: View {
    @State var email: String = ""
    @State var password: String = ""
    @State var onLoginStateChange: () -> Void
    @ObservedObject var userSession = UserSessionService.instance
    @State var state: LoginStates = .login
    @Environment(\.openURL) var openURL

    internal let inspection = Inspection<Self>()

    func handleSignIn() {
        Task {
            state = .loading

            do {
                _ = try await userSession.login(email: email, password: password)

                state = .success
                onLoginStateChange()
                await Persistence.instance?.clearCache()
                await Store.instance.clearCache()
            } catch RequestError.disconnected {
                state = .disconnected
            } catch {
                state = .error
            }
        }
    }

    var body: some View {
        VStack {
            if state == .loading {
                Spacer()
                Text("Please wait")
                ProgressView()
                Spacer()
            }

            if state != .loading && state != .success {
                VStack(alignment: .leading, spacing: 30) {
                    HStack {
                        Spacer()
                        Text("Login").font(.title)
                        Spacer()
                    }

                    if state == .error {
                        HStack {
                            Spacer()
                            Text("Invalid password or E-Mail")
                                .foregroundColor(Color.red)
                            Spacer()
                        }
                    }

                    if state == .disconnected {
                        HStack {
                            Spacer()
                            Text("Can't connect to the server")
                                .foregroundColor(Color.red)
                            Spacer()
                        }
                    }

                    EmailInput(value: $email)

                    HStack {
                        Image(systemName: "lock.fill")
                        SecureField("Password", text: $password)
                    }
                    .padding(15)
                    .background(Color(red: 0.5, green: 0.5, blue: 0.5, opacity: 0.2))
                    .cornerRadius(10)

                    PrimaryButton(action: handleSignIn, label: "Sign in")

                    HStack {
                        Spacer()
                        ForgotPasswordButton()
                        Spacer()
                    }

                    HStack {
                        Spacer()
                        RegisterAccountButton()
                        Spacer()
                    }
                }
                .padding()
            }

            if state == .success {
                Text("welcome")
            }
        }
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct LoginScene_Previews: PreviewProvider {
    static var previews: some View {
        LoginScene {}
    }
}
