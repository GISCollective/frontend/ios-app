//
//  BrowseMapsScene.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 16.11.21.
//

import SwiftUI

struct BrowseMapsScene: View {
    @ObservedObject var model: MapListData
    @ObservedObject var locationManager = LocationManager.instance!

    var columns: [GridItem] = Array(repeating: .init(.adaptive(minimum: 150), alignment: .top), count: 2)

    @Environment(\.openURL) var openURL

    var body: some View {
        VStack {
            if model.data.count > 0 {
                ScrollView {
                    Text("Maps")
                        .font(.largeTitle)
                        .bold()
                        .frame(maxWidth: /*@START_MENU_TOKEN@*/ .infinity/*@END_MENU_TOKEN@*/, alignment: .leading)
                        .padding(.horizontal, 20)
                        .padding(.top, 17)
                        .id("browse-maps-header")

                    LazyVGrid(columns: columns, alignment: .center) {
                        ForEach(self.model.data) { map in
                            MapRow(map: map)
                                .padding(.bottom, 20)
                                .task {
                                    try? await self.model.loadMoreContentIfNeeded(currentItem: map)
                                }
                        }
                    }
                    .padding(10)
                }
            } else if model.isLoading {
                Spacer()
                ProgressView()
                Spacer()
            } else {
                Spacer()
                Text("There are no public maps")
                Spacer()
            }
        }
        .onAppear {
            self.model.longitude = locationManager.pinnedLongitude
            self.model.latitude = locationManager.pinnedLatitude
        }
        .task {
            _ = try? await self.model.reload()
        }
        .onDisappear {
            self.model.stop()
        }
    }
}

class MapListData: ListData<Map, MapList, MapItem> {
    var latitude: String = ""
    var longitude: String = ""

    override func nextPageOptions() -> [String: String] {
        return ["lat": latitude, "lon": longitude]
    }

    init() {
        super.init(store: Store.instance.maps, name: "maps")
    }
}
