//
//  ProfileScene.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 12.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct ProfileScene: View {
    @State var onClose: () -> Void
    @State var profile: UserProfile?
    @State var isEditing: Bool = false
    @StateObject var userSession = UserSessionService.instance
    @State private var isShowingDialog = false

    func handleSignOut() async {
        UserSessionService.instance.clear()
        onClose()
        await Persistence.instance?.clearCache()
    }

    func updateImage(_ newProfile: UserProfile, _ image: UIImage?) async throws -> String? {
        if let image = image, let imageData = image.jpegData(compressionQuality: 0.9) {
            let picture = Picture(id: "")
            picture.name = "profile image for \(newProfile.fullName())"
            picture.picture = "data:image/jpeg;base64," + imageData.base64EncodedString()

            let createdPicture = try await Store.instance.pictures.create(item: picture)

            return createdPicture.id
        }

        return newProfile.pictureId
    }

    func updateProfile(_ newProfile: UserProfile, _ image: UIImage?) async {
        let pictureId = try? await updateImage(newProfile, image)

        await waitForPublish {
            newProfile.pictureId = pictureId
        }

        profile = try? await Store.instance.userProfiles.update(item: newProfile)
    }

    var body: some View {
        VStack {
            Spacer()

            if let profile = self.profile {
                ProfileView(profile: profile, onEdit: {
                    isEditing = true
                })
            } else {
                Text("Please wait")
                ProgressView()
            }
            Spacer()
            PrimaryButton(action: { isShowingDialog = true }, label: "Sign out")
                .padding()
                .confirmationDialog(
                    "Sign out",
                    isPresented: $isShowingDialog
                ) {
                    Button("Yes", role: .destructive) {
                        Task {
                            await handleSignOut()
                        }
                    }
                    Button("Cancel", role: .cancel) {
                        isShowingDialog = false
                    }
                } message: { Text("Are you sure you want to sign out?") }
        }
        .sheet(isPresented: $isEditing, content: {
            ProfileEditView(profile: profile!, onUpdate: { newProfile, image in
                self.profile = nil

                Task {
                    await self.updateProfile(newProfile, image)
                }

                self.isEditing = false
            })
        })
        .task {
            if profile != nil {
                return
            }

            do {
                profile = try await userSession.loadCurrentProfile()

                try await profile?.loadPicture()
            } catch {
                fatalError("Couldn't loadthe profile:\n\(error)")
            }
        }
    }
}

struct ProfileScene_Previews: PreviewProvider {
    static var previews: some View {
        ProfileScene(onClose: {})
    }
}
