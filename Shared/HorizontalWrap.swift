//
//  HorizontalWrap.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 11.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import CoreGraphics
import Foundation

func nextPosition(index: Int, size: Int, distance: Int, bucketRect: CGRect) -> CGPoint {
    let width = bucketRect.width
    var numberPerRow = Int(width / CGFloat(size + distance))
    var positionOnRow: Int

    if numberPerRow == 0 {
        numberPerRow = 1
    }

    let halfSize = CGFloat(size / 2)

    var xPosition = CGFloat(index * (size + distance))
    var yPosition = CGFloat(0)

    if index >= numberPerRow {
        let rowNumber = Int(index / numberPerRow)
        positionOnRow = Int(index % numberPerRow)

        xPosition = CGFloat(positionOnRow * size + distance * positionOnRow)
        yPosition = CGFloat(rowNumber * size + distance)
    }

    return CGPoint(x: xPosition + halfSize, y: yPosition + halfSize)
}

func getHeight(iconsCount: Int, size: Int, spacing: Int, bucketRect: CGRect) -> CGFloat {
    let lastPosition = nextPosition(index: iconsCount - 1, size: size, distance: spacing, bucketRect: bucketRect)

    return CGFloat(lastPosition.y) + CGFloat(size) / CGFloat(2)
}

func getIconsCountPerRow(size: Int, spacing: Int, bucketRect: CGRect) -> Int {
    let availableWidth = bucketRect.width
    return Int(availableWidth) / (size + spacing)
}
