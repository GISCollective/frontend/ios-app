//
//  AppTask.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 24.09.21.
//

import Foundation

enum AppTaskState {
    case empty, ready, running, canceled, success, error
}

class AppTask: ObservableObject {
    private var urlSessionTaskList: [URLSessionDataTask] = []
    private var tasks: [AppTask] = []
    private var completions: [(AppTaskState) -> Void] = []
    private var observations: [NSKeyValueObservation] = []
    @Published var progress: Double = 0

    var state: AppTaskState {
        if tasks.count == 0, urlSessionTaskList.count == 0 {
            return .empty
        }

        if urlSessionTaskList.first(where: { $0.state != .completed }) != nil {
            return .success
        }

        if tasks.first(where: { $0.state != .success }) != nil {
            return .running
        }

        return .success
    }

    init() {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)

        #if DEBUG
            print("[DEBUG] Device path:", paths[0])
        #endif
    }

    init(_ urlSessionTask: URLSessionDataTask) {
        add(urlSessionTask)
    }

    init(_: [AppTask?]) {
        tasks.forEach { self.add($0) }
    }

    private func updateProgress() {
        if urlSessionTaskList.count == 1 {
            progress = urlSessionTaskList[0].progress.fractionCompleted
        }

        if urlSessionTaskList.count > 1 {
            progress = urlSessionTaskList.map { $0.progress.fractionCompleted }.reduce(0) { $0 + $1 } / Double(urlSessionTaskList.count)
        }
    }

    func onComplete(_ completion: @escaping (AppTaskState) -> Void) {
        completions.append(completion)
        updateProgress()

        if state == .success {
            completion(state)
            return
        }
    }

    func cancel() {
        tasks.forEach { $0.cancel() }
        urlSessionTaskList.forEach { $0.cancel() }
    }

    func add(_ urlSessionTask: URLSessionDataTask) {
        urlSessionTaskList.append(urlSessionTask)

        let observation = urlSessionTask.progress.observe(\.fractionCompleted) { progress, _ in
            if progress.fractionCompleted == 1.0 {
                self.handleCompletion(.success)
            }

            self.updateProgress()
        }

        observations.append(observation)
    }

    func add(_ task: AppTask?) {
        if let task = task {
            tasks.append(task)
            task.onComplete(handleCompletion)
        }
    }

    private func handleCompletion(_ state: AppTaskState) {
        if state == .success {
            completions.forEach { completion in
                completion(state)
            }
        }
    }
}

func waitForPublish(publisherFunction: @Sendable @escaping (_: () -> Void) -> Void) async {
    let _: Bool = await withUnsafeContinuation { continuation in
        DispatchQueue.main.async {
            let done = {
                continuation.resume(returning: true)
            }
            publisherFunction(done)
        }
    }
}

func waitForPublish(publisherFunction: @Sendable @escaping () -> Void) async {
    let _: Bool = await withUnsafeContinuation { continuation in
        DispatchQueue.main.async {
            publisherFunction()
            continuation.resume(returning: true)
        }
    }
}
