//
//  AppDelegate.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 04.09.20.
//  Copyright © 2022 GISCollective. All rights reserved.
//

import CoreData
import SDWebImageSVGCoder
import Sentry
import SwiftUI
import UIKit

@main
struct GISCollectiveApp: App {
    init() {
        // FontAwesome.register()
        let SVGCoder = SDImageSVGCoder.shared
        SDImageCodersManager.shared.addCoder(SVGCoder)

        SentrySDK.start { options in
            options.dsn = "https://990bdf84a3b44e2d8d33c17ccce8b8dd@o356529.ingest.sentry.io/5996118"

            #if DEBUG
                options.debug = false
            #endif
        }

        RemoteConnection.instance = RemoteConnection(name: RemoteConnection.name, url: RemoteConnection.url)

        #if DEBUG
//            try? Persistence.instance?.clear()
//            try? Persistence.instance?.clearCache()
        #endif
    }

    var body: some Scene {
        WindowGroup {
            MainView()
                .onAppear {
                    // MatomoTracker.instance?.track(eventWithCategory: "ios", action: "open", name: "", value: 1, url: nil)
                }
        }
    }
}
