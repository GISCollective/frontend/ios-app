//
//  MainScene.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 04.05.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct MainView: View {
    @StateObject var featureList = FeatureListData(name: "What's nearby")
    @StateObject var mapList = MapListData()

    @State private var campaignAnswerStore = CampaignAnswerStore.instance
    @State private var tabSelection = 1
    @State private var tappedTwice: Bool = false
    @State var storedItemCount: Int = 0

    var onTabSelect: Binding<Int> { Binding(
        get: { self.tabSelection },
        set: {
            tappedTwice = $0 == self.tabSelection
            self.tabSelection = $0
        }
    ) }

    func loginStateChange() {
        featureList.clear()
        tabSelection = 1
    }

    var body: some View {
        ScrollViewReader { proxy in
            ZStack(alignment: .topTrailing) {
                TabView(selection: onTabSelect) {
                    WhatsNearbyScene(featureList: featureList)
                        .id("whats-nearby-scene")
                        .onChange(of: tappedTwice, perform: { tapped in
                            if !tapped || !tappedTwice || tabSelection != 1 {
                                return
                            }

                            withAnimation {
                                proxy.scrollTo("whats-nearby-header")
                            }
                            tappedTwice = false
                        })
                        .tabItem {
                            Label("What's nearby", systemImage: "house.fill")
                        }
                        .tag(1)

                    BrowseMapsScene(model: mapList)
                        .id("browse-maps-scene")
                        .onChange(of: tappedTwice, perform: { tapped in
                            if !tapped || !tappedTwice || tabSelection != 2 {
                                return
                            }

                            withAnimation {
                                proxy.scrollTo("browse-maps-header")
                            }
                            tappedTwice = false
                        })
                        .tabItem {
                            Label("Maps", systemImage: "map.fill")
                        }
                        .tag(2)

                    BrowseCampaigns()
                        .id("browse-campaigns-scene")
                        .onChange(of: tappedTwice, perform: { tapped in
                            if !tapped || !tappedTwice || tabSelection != 3 {
                                return
                            }

                            withAnimation {
                                proxy.scrollTo("campaigns-header")
                            }
                            tappedTwice = false
                        })
                        .tabItem {
                            Label("Campaigns", systemImage: "pencil.and.ellipsis.rectangle")
                        }
                        .tag(3)

                    if UserSessionService.instance.isAuthenticated {
                        ProfileScene(onClose: loginStateChange)
                            .id("profile-scene")
                            .tabItem {
                                Label("Profile", systemImage: "person.crop.circle")
                            }
                            .tag(4)
                    } else {
                        LoginScene(onLoginStateChange: loginStateChange)
                            .id("login-scene")
                            .tabItem {
                                Label("Login", systemImage: "person.crop.circle")
                            }
                            .tag(4)
                    }
                }

                if storedItemCount > 0 {
                    StoreStateView(campaignAnswerStore: campaignAnswerStore)
                        .padding(.top, 50)
                        .padding(.trailing, 30)
                }
            }
        }
        .ignoresSafeArea()
        .task {
            try? await campaignAnswerStore.updateStoredItemCount()
        }
        .onReceive(campaignAnswerStore.$storedItemCount) { value in
            self.storedItemCount = value
        }
    }
}
