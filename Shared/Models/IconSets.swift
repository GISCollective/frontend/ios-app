import Combine
import Foundation

class IconSets: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: IconSets, rhs: IconSets) -> Bool {
        if lhs.useCustomList != rhs.useCustomList { return false }
        if lhs.list != rhs.list { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(useCustomList)
        hasher.combine(list)
    }

    private var cancellables = Set<AnyCancellable>()

    @Published var useCustomList: Bool = false
    @Published var list: [IconSet] = []
    @Published var listId: [String] = [] {
        didSet {
            observableList = []
        }
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(Bool.self, forKey: .useCustomList) {
            useCustomList = value
        }

        if let value = try? values.decode([String].self, forKey: .listId) {
            listId = value
        }
    }

    enum CodingKeys: String, CodingKey {
        case useCustomList
        case listId = "list"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(useCustomList, forKey: .useCustomList)
        try? container.encode(listId, forKey: .listId)
    }

    private var observableList: [ObservableModel<IconSet>] = []
    @Published var firstList: IconSet?

    func loadList() async throws {
        if list.count > 0 {
            return
        }

        var result: [IconSet] = []
        for id in listId {
            let item = try await Store.instance.iconSets.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.list = []
                self.list.append(contentsOf: iresult)
                self.firstList = self.list.first

                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadList())
    }

    func copy(_ from: IconSets) {
        useCustomList = from.useCustomList
        listId = from.listId
    }
}
