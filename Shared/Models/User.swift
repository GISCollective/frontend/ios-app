import Combine
import Foundation

struct UserList: CrateModelList, @unchecked Sendable {
    static let name: String = "users"
    var users: [User]
    var items: [User] { return users }

    init(_ items: [User]) {
        users = items
    }
}

struct UserItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "users"
    var item: User { return user }
    var user: User

    init(_ item: User) {
        user = item
    }
}

class User: CrateModel, @unchecked Sendable {
    static let modelName: String = "users"

    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var salutation: String? = nil
    @Published var title: String? = nil
    @Published var firstName: String? = nil
    @Published var lastName: String? = nil
    @Published var username: String = ""
    @Published var email: String = ""
    @Published var isActive: Bool = false
    @Published var lastActivity: UInt? = nil
    @Published var createdAt: Date? = nil

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String?.self, forKey: .salutation) {
            salutation = value
        }

        if let value = try? values.decode(String?.self, forKey: .title) {
            title = value
        }

        if let value = try? values.decode(String?.self, forKey: .firstName) {
            firstName = value
        }

        if let value = try? values.decode(String?.self, forKey: .lastName) {
            lastName = value
        }

        if let value = try? values.decode(String.self, forKey: .username) {
            username = value
        }

        if let value = try? values.decode(String.self, forKey: .email) {
            email = value
        }

        if let value = try? values.decode(Bool.self, forKey: .isActive) {
            isActive = value
        }

        if let value = try? values.decode(UInt?.self, forKey: .lastActivity) {
            lastActivity = value
        }

        if let value = try? values.decode(Date?.self, forKey: .createdAt) {
            createdAt = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case salutation
        case title
        case firstName
        case lastName
        case username
        case email
        case isActive
        case lastActivity
        case createdAt
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(salutation, forKey: .salutation)
        try? container.encode(title, forKey: .title)
        try? container.encode(firstName, forKey: .firstName)
        try? container.encode(lastName, forKey: .lastName)
        try? container.encode(username, forKey: .username)
        try? container.encode(email, forKey: .email)
        try? container.encode(isActive, forKey: .isActive)
        try? container.encode(lastActivity, forKey: .lastActivity)
        try? container.encode(createdAt, forKey: .createdAt)
    }

    func copy(_ from: User) {
        salutation = from.salutation
        title = from.title
        firstName = from.firstName
        lastName = from.lastName
        username = from.username
        email = from.email
        isActive = from.isActive
        lastActivity = from.lastActivity
        createdAt = from.createdAt
    }
}
