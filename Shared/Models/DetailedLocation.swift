import Combine
import Foundation

class DetailedLocation: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: DetailedLocation, rhs: DetailedLocation) -> Bool {
        if lhs.country != rhs.country { return false }
        if lhs.province != rhs.province { return false }
        if lhs.city != rhs.city { return false }
        if lhs.postalCode != rhs.postalCode { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(country)
        hasher.combine(province)
        hasher.combine(city)
        hasher.combine(postalCode)
    }

    var country: String?
    var province: String?
    var city: String?
    var postalCode: String?

    func copy(_ from: DetailedLocation) {
        country = from.country
        province = from.province
        city = from.city
        postalCode = from.postalCode
    }
}
