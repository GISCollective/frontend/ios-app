import Combine
import Foundation

class LayoutRow: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: LayoutRow, rhs: LayoutRow) -> Bool {
        if lhs.options != rhs.options { return false }
        if lhs.cols != rhs.cols { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(options)
        hasher.combine(cols)
    }

    var options: [String]?
    var cols: [LayoutCol]?

    func copy(_ from: LayoutRow) {
        options = from.options
        cols = from.cols
    }
}
