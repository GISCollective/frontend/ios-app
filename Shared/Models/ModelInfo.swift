import Combine
import Foundation

class ModelInfo: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: ModelInfo, rhs: ModelInfo) -> Bool {
        if lhs.changeIndex != rhs.changeIndex { return false }
        if lhs.author != rhs.author { return false }
        if lhs.originalAuthor != rhs.originalAuthor { return false }
        if lhs.createdOn != rhs.createdOn { return false }
        if lhs.lastChangeOn != rhs.lastChangeOn { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(changeIndex)
        hasher.combine(author)
        hasher.combine(originalAuthor)
        hasher.combine(createdOn)
        hasher.combine(lastChangeOn)
    }

    var changeIndex: UInt = .init()
    var author: String = ""
    var originalAuthor: String?
    var createdOn: Date = .init()
    var lastChangeOn: Date = .init()

    func copy(_ from: ModelInfo) {
        changeIndex = from.changeIndex
        author = from.author
        originalAuthor = from.originalAuthor
        createdOn = from.createdOn
        lastChangeOn = from.lastChangeOn
    }
}
