import Combine
import Foundation

class MapFileOptions: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: MapFileOptions, rhs: MapFileOptions) -> Bool {
        if lhs.uuid != rhs.uuid { return false }
        if lhs.destinationMap != rhs.destinationMap { return false }
        if lhs.updateBy != rhs.updateBy { return false }
        if lhs.crs != rhs.crs { return false }
        if lhs.extraIcons != rhs.extraIcons { return false }
        if lhs.analyzedAt != rhs.analyzedAt { return false }
        if lhs.fields != rhs.fields { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(uuid)
        hasher.combine(destinationMap)
        hasher.combine(updateBy)
        hasher.combine(crs)
        hasher.combine(extraIcons)
        hasher.combine(analyzedAt)
        hasher.combine(fields)
    }

    var uuid: String?
    var destinationMap: String?
    var updateBy: String?
    var crs: String?
    var extraIcons: [String]?
    var analyzedAt: Date?
    var fields: [FieldMapping]?

    func copy(_ from: MapFileOptions) {
        uuid = from.uuid
        destinationMap = from.destinationMap
        updateBy = from.updateBy
        crs = from.crs
        extraIcons = from.extraIcons
        analyzedAt = from.analyzedAt
        fields = from.fields
    }
}
