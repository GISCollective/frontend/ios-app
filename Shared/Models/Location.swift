import Combine
import Foundation

class Location: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: Location, rhs: Location) -> Bool {
        if lhs.isDetailed != rhs.isDetailed { return false }
        if lhs.simple != rhs.simple { return false }
        if lhs.detailedLocation != rhs.detailedLocation { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(isDetailed)
        hasher.combine(simple)
        hasher.combine(detailedLocation)
    }

    var isDetailed: Bool = false
    var simple: String = ""
    var detailedLocation: DetailedLocation?

    func copy(_ from: Location) {
        isDetailed = from.isDetailed
        simple = from.simple
        detailedLocation = from.detailedLocation
    }
}
