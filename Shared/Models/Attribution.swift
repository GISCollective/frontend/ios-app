import Combine
import Foundation

class Attribution: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: Attribution, rhs: Attribution) -> Bool {
        if lhs.name != rhs.name { return false }
        if lhs.url != rhs.url { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(url)
    }

    var name: String?
    var url: String?

    func copy(_ from: Attribution) {
        name = from.name
        url = from.url
    }
}
