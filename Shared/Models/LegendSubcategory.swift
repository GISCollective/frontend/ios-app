import Combine
import Foundation

class LegendSubcategory: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: LegendSubcategory, rhs: LegendSubcategory) -> Bool {
        if lhs.name != rhs.name { return false }
        if lhs.uid != rhs.uid { return false }
        if lhs.count != rhs.count { return false }
        if lhs.icons != rhs.icons { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(uid)
        hasher.combine(count)
        hasher.combine(icons)
    }

    var name: String = ""
    var uid: String = ""
    var count: UInt = .init()
    var icons: [LegendIcon] = []

    func copy(_ from: LegendSubcategory) {
        name = from.name
        uid = from.uid
        count = from.count
        icons = from.icons
    }
}
