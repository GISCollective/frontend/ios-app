import Combine
import Foundation

class LayoutCol: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: LayoutCol, rhs: LayoutCol) -> Bool {
        if lhs.type != rhs.type { return false }
        if lhs.data != rhs.data { return false }
        if lhs.options != rhs.options { return false }
        if lhs.componentCount != rhs.componentCount { return false }
        if lhs.name != rhs.name { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(type)
        hasher.combine(data)
        hasher.combine(options)
        hasher.combine(componentCount)
        hasher.combine(name)
    }

    var type: String?
    var data: [String: String]?
    var options: [String]?
    var componentCount: Int32?
    var name: String?

    func copy(_ from: LayoutCol) {
        type = from.type
        data = from.data
        options = from.options
        componentCount = from.componentCount
        name = from.name
    }
}
