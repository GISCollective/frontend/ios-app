import Combine
import Foundation

struct CampaignList: CrateModelList, @unchecked Sendable {
    static let name: String = "campaigns"
    var campaigns: [Campaign]
    var items: [Campaign] { return campaigns }

    init(_ items: [Campaign]) {
        campaigns = items
    }
}

struct CampaignItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "campaigns"
    var item: Campaign { return campaign }
    var campaign: Campaign

    init(_ item: Campaign) {
        campaign = item
    }
}

class Campaign: CrateModel, @unchecked Sendable {
    static let modelName: String = "campaigns"

    static func == (lhs: Campaign, rhs: Campaign) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var article: ArticleBody = .init()
    @Published var visibility: Visibility = .init()
    @Published var info: ModelInfo = .init()
    @Published var map: OptionalMap? = nil
    @Published var startDate: Date? = nil
    @Published var endDate: Date? = nil
    @Published var options: CampaignOptions? = nil
    @Published var cover: Picture = .init()
    @Published var coverId: String = "" {
        didSet {}
    }

    @Published var icons: [Icon] = []
    @Published var iconsId: [String] = [] {
        didSet {
            observableIcons = []
        }
    }

    @Published var optionalIcons: [Icon]? = nil
    @Published var optionalIconsId: [String]? = nil {
        didSet {
            observableOptionalIcons = []
        }
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(ArticleBody.self, forKey: .article) {
            article = value
        }

        if let value = try? values.decode(Visibility.self, forKey: .visibility) {
            visibility = value
        }

        if let value = try? values.decode(ModelInfo.self, forKey: .info) {
            info = value
        }

        if let value = try? values.decode(OptionalMap?.self, forKey: .map) {
            map = value
        }

        if let value = try? values.decode(Date?.self, forKey: .startDate) {
            startDate = value
        }

        if let value = try? values.decode(Date?.self, forKey: .endDate) {
            endDate = value
        }

        if let value = try? values.decode(CampaignOptions?.self, forKey: .options) {
            options = value
        }

        if let value = try? values.decode(String.self, forKey: .coverId) {
            coverId = value
        }

        if let value = try? values.decode([String].self, forKey: .iconsId) {
            iconsId = value
        }

        if let value = try? values.decode([String]?.self, forKey: .optionalIconsId) {
            optionalIconsId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case article
        case visibility
        case info
        case map
        case startDate
        case endDate
        case options
        case coverId = "cover"
        case iconsId = "icons"
        case optionalIconsId = "optionalIcons"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(article, forKey: .article)
        try? container.encode(visibility, forKey: .visibility)
        try? container.encode(info, forKey: .info)
        try? container.encode(map, forKey: .map)
        try? container.encode(startDate, forKey: .startDate)
        try? container.encode(endDate, forKey: .endDate)
        try? container.encode(options, forKey: .options)
        try? container.encode(coverId, forKey: .coverId)
        try? container.encode(iconsId, forKey: .iconsId)
        try? container.encode(optionalIconsId, forKey: .optionalIconsId)
    }

    func loadCover() async throws {
        let result = try await Store.instance.pictures.getById(id: coverId)

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.cover = result
                continuation.resume(returning: true)
            }
        }
    }

    private var observableIcons: [ObservableModel<Icon>] = []
    @Published var firstIcons: Icon?

    func loadIcons() async throws {
        if icons.count > 0 {
            return
        }

        var result: [Icon] = []
        for id in iconsId {
            let item = try await Store.instance.icons.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.icons = []
                self.icons.append(contentsOf: iresult)
                self.firstIcons = self.icons.first

                continuation.resume(returning: true)
            }
        }
    }

    private var observableOptionalIcons: [ObservableModel<Icon>] = []
    @Published var firstOptionalIcons: Icon?

    func loadOptionalIcons() async throws {
        if optionalIcons?.count ?? 0 > 0 {
            return
        }

        var result: [Icon] = []
        for id in optionalIconsId ?? [] {
            let item = try await Store.instance.icons.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.optionalIcons = []
                self.optionalIcons?.append(contentsOf: iresult)
                self.firstOptionalIcons = self.optionalIcons?.first

                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadCover(), loadIcons(), loadOptionalIcons())
    }

    func copy(_ from: Campaign) {
        name = from.name
        article = from.article
        visibility = from.visibility
        info = from.info
        map = from.map
        startDate = from.startDate
        endDate = from.endDate
        options = from.options
        coverId = from.coverId
        iconsId = from.iconsId
        optionalIconsId = from.optionalIconsId
    }
}
