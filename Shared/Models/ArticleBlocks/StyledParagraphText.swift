//
//  StyledParagraphText.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 07.02.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

class StyledParagraphText: NSObject, XMLParserDelegate {
    var pieces: [StyledWord] = []

    private var isBold: Int = 0
    private var isItalic: Int = 0
    private var isLink: Int = 0
    private var href: String = ""
    let originalText: String

    private let entities: [String: String] = [
        "rsquo": "'",
        "mdash": "—",
        "nbsp": " ",
    ]

    init(text: String) {
        originalText = text

        super.init()

        if text != "" {
            parse(text: text)
        }
    }

    func toString() -> String {
        return pieces.map { piece in
            piece.text
        }.joined()
    }

    func parser(_ parser: XMLParser, resolveExternalEntityName name: String, systemID _: String?) -> Data? {
        self.parser(parser, foundCharacters: entities[name, default: ""])
        return entities[name, default: ""].data(using: .utf8)
    }

    func parser(_ parser: XMLParser, resolveInternalEntityName name: String, systemID _: String?) -> Data? {
        self.parser(parser, foundCharacters: entities[name, default: ""])
        return entities[name, default: ""].data(using: .utf8)
    }

    func parser(_: XMLParser, didStartElement elementName: String, namespaceURI _: String?, qualifiedName _: String?, attributes attributeDict: [String: String] = [:]) {
        if elementName == "b" {
            isBold += 1
        }
        if elementName == "i" {
            isItalic += 1
        }
        if elementName == "a", attributeDict["href"] != nil {
            isLink += 1
            href = attributeDict["href"]!
        }
    }

    func parser(_: XMLParser, foundCharacters string: String) {
        var currentElement = StyledWord()
        currentElement.text = string
        currentElement.href = href
        currentElement.isLink = isLink > 0
        currentElement.isBold = isBold > 0
        currentElement.isItalic = isItalic > 0

        pieces.append(currentElement)
    }

    func parser(_: XMLParser, didEndElement elementName: String, namespaceURI _: String?, qualifiedName _: String?) {
        if elementName == "b" {
            isBold -= 1
        }
        if elementName == "i" {
            isItalic -= 1
        }
        if elementName == "a" {
            isLink -= 1

            if isLink == 0 {
                href = ""
            }
        }
    }

    func parser(_: XMLParser, parseErrorOccurred _: Error) {
        // print("parse error: ", parseError.localizedDescription)
    }

    func parser(_: XMLParser, validationErrorOccurred validationError: Error) {
        print("validationError error: ", validationError.localizedDescription)
    }

    func parse(text: String) {
        let dtd = """
        <?xml version="1.0" encoding="utf-8"?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        """
        let parser = XMLParser(data: ("\(dtd)<_>\(text)<_>".data(using: .utf8) ?? "".data(using: .utf8))!)
        parser.delegate = self
        parser.shouldResolveExternalEntities = true
        parser.parse()
    }
}

struct StyledWord {
    var text: String = ""
    var isBold: Bool = false
    var isItalic = false
    var isLink = false
    var href: String = ""
}
