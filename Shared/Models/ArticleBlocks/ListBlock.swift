//
//  ListBlock.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 18.11.21.
//

import Foundation

struct ListBlock: Codable {
    var items: [String] = []
    var style: String = "unordered"

    var styledItems: [StyledParagraphText] {
        return items.map { StyledParagraphText(text: $0) }
    }

    init(items: [String]) {
        self.items = items
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        style = try container.decode(String.self, forKey: .style)
        items = try container.decode([String].self, forKey: .items)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(style, forKey: .style)
        try? container.encode(items, forKey: .items)
    }

    enum CodingKeys: String, CodingKey {
        case style
        case items
    }
}
