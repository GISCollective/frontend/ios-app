//
//  ArticleBody.swift
//  GISCollective
//
//  Created by Bogdan on 13.01.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

class ArticleBody: Hashable, Codable {
    var blockContainer = EditorJsBlockContainer()

    init(paragraph: String) {
        blockContainer.blocks.append(EditorJsBlock(paragraph: paragraph))
    }

    init() {
        blockContainer.blocks.append(EditorJsBlock(paragraph: ""))
    }

    required init(from decoder: Decoder) throws {
        if let strVal = try? decoder.singleValueContainer().decode(String.self) {
            blockContainer.blocks.append(EditorJsBlock(paragraph: strVal))
        }

        if let blockVal = try? decoder.singleValueContainer().decode(EditorJsBlockContainer.self) {
            blockContainer = blockVal
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(blockContainer.blocks, forKey: CodingKeys.blocks)
    }

    static func == (_: ArticleBody, _: ArticleBody) -> Bool {
        return false
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine("")
    }

    func setDefaultTitle(title: String) {
        if !blockContainer.blocks.contains(where: { $0.type == "header" }) {
            blockContainer.blocks.insert(EditorJsBlock(header: title), at: 0)
        }
    }

    enum CodingKeys: String, CodingKey {
        case blocks
    }
}

struct EditorJsBlockContainer: Hashable, Codable {
    var blocks: [EditorJsBlock] = []
}

class EditorJsBlock: Hashable, Codable {
    var type: String = ""

    private var header: HeaderBlock?
    private var paragraph: ParagraphBlock?
    private var list: ListBlock?

    init(paragraph: String) {
        type = "paragraph"
        self.paragraph = ParagraphBlock(fromText: paragraph)
    }

    init(header: String) {
        type = "header"
        self.header = HeaderBlock(fromText: header)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let type = try container.decode(String.self, forKey: .type)
        self.type = type

        if type == "header" {
            header = try? container.decode(HeaderBlock.self, forKey: .data)
        }

        if type == "paragraph" {
            paragraph = try? container.decode(ParagraphBlock.self, forKey: .data)
        }

        if type == "list" {
            list = try? container.decode(ListBlock.self, forKey: .data)
        }
    }

    static func == (_: EditorJsBlock, _: EditorJsBlock) -> Bool {
        return false
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(type, forKey: CodingKeys.type)

        if type == "header" {
            try container.encode(header, forKey: CodingKeys.data)
        }

        if type == "paragraph" {
            try container.encode(paragraph, forKey: CodingKeys.data)
        }
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(type)
    }

    func asHeader() -> HeaderBlock {
        if let header = header {
            return header
        }

        return HeaderBlock(fromText: "")
    }

    func asParagraph() -> ParagraphBlock {
        if let paragraph = paragraph {
            return paragraph
        }

        return ParagraphBlock(fromText: "")
    }

    func asList() -> ListBlock {
        if let list = list {
            return list
        }

        return ListBlock(items: [])
    }

    enum CodingKeys: String, CodingKey {
        case type, data
    }
}
