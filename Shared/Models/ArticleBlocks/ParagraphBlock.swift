//
//  ParagraphBlock.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 07.02.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

struct ParagraphBlock: Codable {
    var text = StyledParagraphText(text: "")

    init(fromText: String) {
        text = StyledParagraphText(text: fromText)
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let text = try container.decode(String.self, forKey: .text)
        self.text = StyledParagraphText(text: text)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(text.originalText, forKey: .text)
    }

    enum CodingKeys: String, CodingKey {
        case text
    }
}
