//
//  HeaderBlock.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 07.02.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

struct HeaderBlock: Codable {
    var level: Int
    var text: String

    init(fromText: String) {
        text = fromText
        level = 1
    }

    init(text: String, level: Int) {
        self.text = text
        self.level = level
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let text = try container.decode(String.self, forKey: .text)
        level = try container.decode(Int.self, forKey: .level)
        self.text = StyledParagraphText(text: text).toString()
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(text, forKey: .text)
        try? container.encode(level, forKey: .level)
    }

    enum CodingKeys: String, CodingKey {
        case text
        case level
    }
}
