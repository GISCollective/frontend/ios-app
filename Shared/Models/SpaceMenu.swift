import Combine
import Foundation

class SpaceMenu: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: SpaceMenu, rhs: SpaceMenu) -> Bool {
        if lhs.items != rhs.items { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(items)
    }

    var items: [SpaceMenuItem]?

    func copy(_ from: SpaceMenu) {
        items = from.items
    }
}
