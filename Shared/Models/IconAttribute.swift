import Combine
import Foundation

class IconAttribute: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: IconAttribute, rhs: IconAttribute) -> Bool {
        if lhs.name != rhs.name { return false }
        if lhs.type != rhs.type { return false }
        if lhs.options != rhs.options { return false }
        if lhs.isPrivate != rhs.isPrivate { return false }
        if lhs.isRequired != rhs.isRequired { return false }
        if lhs.displayName != rhs.displayName { return false }
        if lhs.help != rhs.help { return false }
        if lhs.isInherited != rhs.isInherited { return false }
        if lhs.otherNames != rhs.otherNames { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(type)
        hasher.combine(options)
        hasher.combine(isPrivate)
        hasher.combine(isRequired)
        hasher.combine(displayName)
        hasher.combine(help)
        hasher.combine(isInherited)
        hasher.combine(otherNames)
    }

    var name: String = ""
    var type: String = ""
    var options: String?
    var isPrivate: Bool?
    var isRequired: Bool?
    var displayName: String?
    var help: String?
    var isInherited: Bool?
    var otherNames: [String]?

    func copy(_ from: IconAttribute) {
        name = from.name
        type = from.type
        options = from.options
        isPrivate = from.isPrivate
        isRequired = from.isRequired
        displayName = from.displayName
        help = from.help
        isInherited = from.isInherited
        otherNames = from.otherNames
    }
}
