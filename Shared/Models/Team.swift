import Combine
import Foundation

struct TeamList: CrateModelList, @unchecked Sendable {
    static let name: String = "teams"
    var teams: [Team]
    var items: [Team] { return teams }

    init(_ items: [Team]) {
        teams = items
    }
}

struct TeamItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "teams"
    var item: Team { return team }
    var team: Team

    init(_ item: Team) {
        team = item
    }
}

class Team: CrateModel, @unchecked Sendable {
    static let modelName: String = "teams"

    static func == (lhs: Team, rhs: Team) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var about: ArticleBody = .init()
    @Published var owners: [String] = []
    @Published var leaders: [String] = []
    @Published var members: [String] = []
    @Published var guests: [String] = []
    @Published var isPublic: Bool = false
    @Published var isDefault: Bool? = nil
    @Published var isPublisher: Bool? = nil
    @Published var allowCustomDataBindings: Bool? = nil
    @Published var logo: Picture? = nil
    @Published var logoId: String? = nil {
        didSet {}
    }

    @Published var pictures: [Picture]? = nil
    @Published var picturesId: [String]? = nil {
        didSet {
            observablePictures = []
        }
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(ArticleBody.self, forKey: .about) {
            about = value
        }

        if let value = try? values.decode([String].self, forKey: .owners) {
            owners = value
        }

        if let value = try? values.decode([String].self, forKey: .leaders) {
            leaders = value
        }

        if let value = try? values.decode([String].self, forKey: .members) {
            members = value
        }

        if let value = try? values.decode([String].self, forKey: .guests) {
            guests = value
        }

        if let value = try? values.decode(Bool.self, forKey: .isPublic) {
            isPublic = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .isDefault) {
            isDefault = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .isPublisher) {
            isPublisher = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .allowCustomDataBindings) {
            allowCustomDataBindings = value
        }

        if let value = try? values.decode(String?.self, forKey: .logoId) {
            logoId = value
        }

        if let value = try? values.decode([String]?.self, forKey: .picturesId) {
            picturesId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case about
        case owners
        case leaders
        case members
        case guests
        case isPublic
        case isDefault
        case isPublisher
        case allowCustomDataBindings
        case logoId = "logo"
        case picturesId = "pictures"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(about, forKey: .about)
        try? container.encode(owners, forKey: .owners)
        try? container.encode(leaders, forKey: .leaders)
        try? container.encode(members, forKey: .members)
        try? container.encode(guests, forKey: .guests)
        try? container.encode(isPublic, forKey: .isPublic)
        try? container.encode(isDefault, forKey: .isDefault)
        try? container.encode(isPublisher, forKey: .isPublisher)
        try? container.encode(allowCustomDataBindings, forKey: .allowCustomDataBindings)
        try? container.encode(logoId, forKey: .logoId)
        try? container.encode(picturesId, forKey: .picturesId)
    }

    func loadLogo() async throws {
        if let logoId = logoId {
            let result = try await Store.instance.pictures.getById(id: logoId)

            let _: Bool = await withUnsafeContinuation { continuation in
                DispatchQueue.main.async {
                    self.logo = result
                    continuation.resume(returning: true)
                }
            }
        }
    }

    private var observablePictures: [ObservableModel<Picture>] = []
    @Published var firstPictures: Picture?

    func loadPictures() async throws {
        if pictures?.count ?? 0 > 0 {
            return
        }

        var result: [Picture] = []
        for id in picturesId ?? [] {
            let item = try await Store.instance.pictures.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.pictures = []
                self.pictures?.append(contentsOf: iresult)
                self.firstPictures = self.pictures?.first

                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadLogo(), loadPictures())
    }

    func copy(_ from: Team) {
        name = from.name
        about = from.about
        owners = from.owners
        leaders = from.leaders
        members = from.members
        guests = from.guests
        isPublic = from.isPublic
        isDefault = from.isDefault
        isPublisher = from.isPublisher
        allowCustomDataBindings = from.allowCustomDataBindings
        logoId = from.logoId
        picturesId = from.picturesId
    }
}
