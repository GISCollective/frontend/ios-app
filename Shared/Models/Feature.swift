import Combine
import Foundation
import GEOSwift

struct FeatureList: CrateModelList, @unchecked Sendable {
    static let name: String = "features"
    var features: [Feature]
    var items: [Feature] { return features }

    init(_ items: [Feature]) {
        features = items
    }
}

struct FeatureItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "features"
    var item: Feature { return feature }
    var feature: Feature

    init(_ item: Feature) {
        feature = item
    }
}

class Feature: CrateModel, @unchecked Sendable {
    static let modelName: String = "features"

    static func == (lhs: Feature, rhs: Feature) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var description: ArticleBody = .init()
    @Published var visibility: Int32? = nil
    @Published var contributors: [String]? = nil
    @Published var attributes: [String: AttributeGroup]? = nil
    @Published var position: Geometry = try! Geometry(wkt: "POINT (0 0)")
    @Published var info: ModelInfo? = nil
    @Published var decorators: FeatureDecorators? = nil
    @Published var source: Source? = nil
    @Published var isMasked: Bool? = nil
    @Published var maps: [Map] = []
    @Published var mapsId: [String] = [] {
        didSet {
            observableMaps = []
        }
    }

    @Published var pictures: [Picture]? = nil
    @Published var picturesId: [String]? = nil {
        didSet {
            observablePictures = []
        }
    }

    @Published var sounds: [Sound]? = nil
    @Published var soundsId: [String]? = nil {
        didSet {
            observableSounds = []
        }
    }

    @Published var icons: [Icon]? = nil
    @Published var iconsId: [String]? = nil {
        didSet {
            observableIcons = []
        }
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(Bool.self, forKey: .isMasked) {
            isMasked = value
        }

        if let value = try? values.decode(ArticleBody.self, forKey: .description) {
            description = value
        }

        if let value = try? values.decode(Int32?.self, forKey: .visibility) {
            visibility = value
        }

        if let value = try? values.decode([String]?.self, forKey: .contributors) {
            contributors = value
        }

        if let value = try? values.decode([String: AttributeGroup]?.self, forKey: .attributes) {
            attributes = value
        }

        if let value = try? values.decode(Geometry.self, forKey: .position) {
            position = value
        }

        if let value = try? values.decode(ModelInfo?.self, forKey: .info) {
            info = value
        }

        if let value = try? values.decode(FeatureDecorators?.self, forKey: .decorators) {
            decorators = value
        }

        if let value = try? values.decode(Source?.self, forKey: .source) {
            source = value
        }

        if let value = try? values.decode([String].self, forKey: .mapsId) {
            mapsId = value
        }

        if let value = try? values.decode([String]?.self, forKey: .picturesId) {
            picturesId = value
        }

        if let value = try? values.decode([String]?.self, forKey: .soundsId) {
            soundsId = value
        }

        if let value = try? values.decode([String]?.self, forKey: .iconsId) {
            iconsId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case description
        case visibility
        case contributors
        case attributes
        case position
        case info
        case decorators
        case source
        case mapsId = "maps"
        case picturesId = "pictures"
        case soundsId = "sounds"
        case iconsId = "icons"
        case isMasked
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(description, forKey: .description)
        try? container.encode(visibility, forKey: .visibility)
        try? container.encode(contributors, forKey: .contributors)
        try? container.encode(attributes, forKey: .attributes)
        try? container.encode(position, forKey: .position)
        try? container.encode(info, forKey: .info)
        try? container.encode(decorators, forKey: .decorators)
        try? container.encode(source, forKey: .source)
        try? container.encode(mapsId, forKey: .mapsId)
        try? container.encode(picturesId, forKey: .picturesId)
        try? container.encode(soundsId, forKey: .soundsId)
        try? container.encode(iconsId, forKey: .iconsId)
    }

    private var observableMaps: [ObservableModel<Map>] = []
    @Published var firstMaps: Map?

    func loadMaps() async throws {
        if maps.count > 0 {
            return
        }

        var result: [Map] = []
        for id in mapsId {
            let item = try await Store.instance.maps.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.maps = []
                self.maps.append(contentsOf: iresult)
                self.firstMaps = self.maps.first

                continuation.resume(returning: true)
            }
        }
    }

    private var observablePictures: [ObservableModel<Picture>] = []
    @Published var firstPictures: Picture?

    func loadPictures() async throws {
        if pictures?.count ?? 0 > 0 {
            return
        }

        var result: [Picture] = []
        for id in picturesId ?? [] {
            let item = try await Store.instance.pictures.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.pictures = []
                self.pictures?.append(contentsOf: iresult)
                self.firstPictures = self.pictures?.first

                continuation.resume(returning: true)
            }
        }
    }

    private var observableSounds: [ObservableModel<Sound>] = []
    @Published var firstSounds: Sound?

    func loadSounds() async throws {
        if sounds?.count ?? 0 > 0 {
            return
        }

        var result: [Sound] = []
        for id in soundsId ?? [] {
            let item = try await Store.instance.sounds.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.sounds = []
                self.sounds?.append(contentsOf: iresult)
                self.firstSounds = self.sounds?.first

                continuation.resume(returning: true)
            }
        }
    }

    private var observableIcons: [ObservableModel<Icon>] = []
    @Published var firstIcons: Icon?

    func loadIcons() async throws {
        if icons?.count ?? 0 > 0 {
            return
        }

        var result: [Icon] = []
        for id in iconsId ?? [] {
            let item = try await Store.instance.icons.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.icons = []
                self.icons?.append(contentsOf: iresult)
                self.firstIcons = self.icons?.first

                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadMaps(), loadPictures(), loadSounds(), loadIcons())
    }

    func copy(_ from: Feature) {
        name = from.name
        description = from.description
        visibility = from.visibility
        contributors = from.contributors
        attributes = from.attributes
        position = from.position
        info = from.info
        decorators = from.decorators
        source = from.source
        mapsId = from.mapsId
        picturesId = from.picturesId
        soundsId = from.soundsId
        iconsId = from.iconsId
    }
}
