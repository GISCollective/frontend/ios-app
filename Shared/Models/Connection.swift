import Combine
import Foundation

class Connection: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: Connection, rhs: Connection) -> Bool {
        if lhs.type != rhs.type { return false }
        if lhs.config != rhs.config { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(type)
        hasher.combine(config)
    }

    var type: String = ""
    var config: Json = .init()

    func copy(_ from: Connection) {
        type = from.type
        config = from.config
    }
}
