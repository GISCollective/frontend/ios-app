import Combine
import Foundation

class InheritedStyle: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: InheritedStyle, rhs: InheritedStyle) -> Bool {
        if lhs.hasCustomStyle != rhs.hasCustomStyle { return false }
        if lhs.types != rhs.types { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(hasCustomStyle)
        hasher.combine(types)
    }

    var hasCustomStyle: Bool = false
    var types: IconStyle?

    func copy(_ from: InheritedStyle) {
        hasCustomStyle = from.hasCustomStyle
        types = from.types
    }
}
