import Combine
import Foundation

struct BaseMapList: CrateModelList, @unchecked Sendable {
    static let name: String = "basemaps"
    var baseMaps: [BaseMap]
    var items: [BaseMap] { return baseMaps }

    init(_ items: [BaseMap]) {
        baseMaps = items
    }
}

struct BaseMapItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "basemaps"
    var item: BaseMap { return baseMap }
    var baseMap: BaseMap

    init(_ item: BaseMap) {
        baseMap = item
    }
}

class BaseMap: CrateModel, @unchecked Sendable {
    static let modelName: String = "basemaps"

    static func == (lhs: BaseMap, rhs: BaseMap) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var icon: String = ""
    @Published var defaultOrder: Int32? = nil
    @Published var layers: [MapLayer] = []
    @Published var visibility: Visibility = .init()
    @Published var attributions: [Attribution]? = nil
    @Published var cover: Picture? = nil
    @Published var coverId: String? = nil {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(String.self, forKey: .icon) {
            icon = value
        }

        if let value = try? values.decode(Int32?.self, forKey: .defaultOrder) {
            defaultOrder = value
        }

        if let value = try? values.decode([MapLayer].self, forKey: .layers) {
            layers = value
        }

        if let value = try? values.decode(Visibility.self, forKey: .visibility) {
            visibility = value
        }

        if let value = try? values.decode([Attribution]?.self, forKey: .attributions) {
            attributions = value
        }

        if let value = try? values.decode(String?.self, forKey: .coverId) {
            coverId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case icon
        case defaultOrder
        case layers
        case visibility
        case attributions
        case coverId = "cover"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(icon, forKey: .icon)
        try? container.encode(defaultOrder, forKey: .defaultOrder)
        try? container.encode(layers, forKey: .layers)
        try? container.encode(visibility, forKey: .visibility)
        try? container.encode(attributions, forKey: .attributions)
        try? container.encode(coverId, forKey: .coverId)
    }

    func loadCover() async throws {
        if let coverId = coverId {
            let result = try await Store.instance.pictures.getById(id: coverId)

            let _: Bool = await withUnsafeContinuation { continuation in
                DispatchQueue.main.async {
                    self.cover = result
                    continuation.resume(returning: true)
                }
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadCover())
    }

    func copy(_ from: BaseMap) {
        name = from.name
        icon = from.icon
        defaultOrder = from.defaultOrder
        layers = from.layers
        visibility = from.visibility
        attributions = from.attributions
        coverId = from.coverId
    }
}
