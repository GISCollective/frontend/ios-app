import Combine
import Foundation

class OptionalMap: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: OptionalMap, rhs: OptionalMap) -> Bool {
        if lhs.isEnabled != rhs.isEnabled { return false }
        if lhs.map != rhs.map { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(isEnabled)
        hasher.combine(map)
    }

    var isEnabled: Bool = false
    var map: String = ""

    func copy(_ from: OptionalMap) {
        isEnabled = from.isEnabled
        map = from.map
    }
}
