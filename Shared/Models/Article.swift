import Combine
import Foundation

struct ArticleList: CrateModelList, @unchecked Sendable {
    static let name: String = "articles"
    var articles: [Article]
    var items: [Article] { return articles }

    init(_ items: [Article]) {
        articles = items
    }
}

struct ArticleItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "articles"
    var item: Article { return article }
    var article: Article

    init(_ item: Article) {
        article = item
    }
}

class Article: CrateModel, @unchecked Sendable {
    static let modelName: String = "articles"

    static func == (lhs: Article, rhs: Article) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var slug: String = ""
    @Published var title: String = ""
    @Published var content: ArticleBody = .init()
    @Published var info: ModelInfo = .init()
    @Published var visibility: Visibility = .init()

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .slug) {
            slug = value
        }

        if let value = try? values.decode(String.self, forKey: .title) {
            title = value
        }

        if let value = try? values.decode(ArticleBody.self, forKey: .content) {
            content = value
        }

        if let value = try? values.decode(ModelInfo.self, forKey: .info) {
            info = value
        }

        if let value = try? values.decode(Visibility.self, forKey: .visibility) {
            visibility = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case slug
        case title
        case content
        case info
        case visibility
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(slug, forKey: .slug)
        try? container.encode(title, forKey: .title)
        try? container.encode(content, forKey: .content)
        try? container.encode(info, forKey: .info)
        try? container.encode(visibility, forKey: .visibility)
    }

    func copy(_ from: Article) {
        slug = from.slug
        title = from.title
        content = from.content
        info = from.info
        visibility = from.visibility
    }
}
