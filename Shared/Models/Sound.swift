import Combine
import Foundation

struct SoundList: CrateModelList, @unchecked Sendable {
    static let name: String = "sounds"
    var sounds: [Sound]
    var items: [Sound] { return sounds }

    init(_ items: [Sound]) {
        sounds = items
    }
}

struct SoundItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "sounds"
    var item: Sound { return sound }
    var sound: Sound

    init(_ item: Sound) {
        sound = item
    }
}

class Sound: CrateModel, @unchecked Sendable {
    static let modelName: String = "sounds"

    static func == (lhs: Sound, rhs: Sound) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var feature: String? = nil
    @Published var sourceUrl: String? = nil
    @Published var attributions: String? = nil
    @Published var visibility: Visibility = .init()
    @Published var info: ModelInfo = .init()
    @Published var sound: String = ""

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(String?.self, forKey: .feature) {
            feature = value
        }

        if let value = try? values.decode(String?.self, forKey: .sourceUrl) {
            sourceUrl = value
        }

        if let value = try? values.decode(String?.self, forKey: .attributions) {
            attributions = value
        }

        if let value = try? values.decode(Visibility.self, forKey: .visibility) {
            visibility = value
        }

        if let value = try? values.decode(ModelInfo.self, forKey: .info) {
            info = value
        }

        if let value = try? values.decode(String.self, forKey: .sound) {
            sound = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case feature
        case sourceUrl
        case attributions
        case visibility
        case info
        case sound
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(feature, forKey: .feature)
        try? container.encode(sourceUrl, forKey: .sourceUrl)
        try? container.encode(attributions, forKey: .attributions)
        try? container.encode(visibility, forKey: .visibility)
        try? container.encode(info, forKey: .info)
        try? container.encode(sound, forKey: .sound)
    }

    func copy(_ from: Sound) {
        name = from.name
        feature = from.feature
        sourceUrl = from.sourceUrl
        attributions = from.attributions
        visibility = from.visibility
        info = from.info
        sound = from.sound
    }
}
