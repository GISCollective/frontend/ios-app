import Combine
import Foundation

class PageContainer: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: PageContainer, rhs: PageContainer) -> Bool {
        if lhs.backgroundColor != rhs.backgroundColor { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(backgroundColor)
    }

    var backgroundColor: String = ""

    func copy(_ from: PageContainer) {
        backgroundColor = from.backgroundColor
    }
}
