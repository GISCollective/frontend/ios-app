import Combine
import Foundation

class PointStyleProperties: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: PointStyleProperties, rhs: PointStyleProperties) -> Bool {
        if lhs.isVisible != rhs.isVisible { return false }
        if lhs.shape != rhs.shape { return false }
        if lhs.borderColor != rhs.borderColor { return false }
        if lhs.backgroundColor != rhs.backgroundColor { return false }
        if lhs.borderWidth != rhs.borderWidth { return false }
        if lhs.size != rhs.size { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(isVisible)
        hasher.combine(shape)
        hasher.combine(borderColor)
        hasher.combine(backgroundColor)
        hasher.combine(borderWidth)
        hasher.combine(size)
    }

    var isVisible: Bool?
    var shape: String?
    var borderColor: String?
    var backgroundColor: String?
    var borderWidth: UInt32?
    var size: UInt32?

    func copy(_ from: PointStyleProperties) {
        isVisible = from.isVisible
        shape = from.shape
        borderColor = from.borderColor
        backgroundColor = from.backgroundColor
        borderWidth = from.borderWidth
        size = from.size
    }
}
