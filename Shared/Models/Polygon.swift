import Combine
import Foundation

class Polygon: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: Polygon, rhs: Polygon) -> Bool {
        if lhs.coordinates != rhs.coordinates { return false }
        if lhs.type != rhs.type { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(coordinates)
        hasher.combine(type)
    }

    var coordinates: [[[Double]]] = [[[]]]
    var type: String = ""

    func copy(_ from: Polygon) {
        coordinates = from.coordinates
        type = from.type
    }
}
