import Combine
import Foundation

class License: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: License, rhs: License) -> Bool {
        if lhs.name != rhs.name { return false }
        if lhs.url != rhs.url { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(url)
    }

    var name: String?
    var url: String?

    func copy(_ from: License) {
        name = from.name
        url = from.url
    }
}
