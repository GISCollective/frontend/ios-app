import Combine
import Foundation

struct TranslationList: CrateModelList, @unchecked Sendable {
    static let name: String = "translations"
    var translations: [Translation]
    var items: [Translation] { return translations }

    init(_ items: [Translation]) {
        translations = items
    }
}

struct TranslationItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "translations"
    var item: Translation { return translation }
    var translation: Translation

    init(_ item: Translation) {
        translation = item
    }
}

class Translation: CrateModel, @unchecked Sendable {
    static let modelName: String = "translations"

    static func == (lhs: Translation, rhs: Translation) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var locale: String = ""
    @Published var customTranslations: [String: String]? = nil
    @Published var file: String? = nil

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(String.self, forKey: .locale) {
            locale = value
        }

        if let value = try? values.decode([String: String]?.self, forKey: .customTranslations) {
            customTranslations = value
        }

        if let value = try? values.decode(String?.self, forKey: .file) {
            file = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case locale
        case customTranslations
        case file
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(locale, forKey: .locale)
        try? container.encode(customTranslations, forKey: .customTranslations)
        try? container.encode(file, forKey: .file)
    }

    func copy(_ from: Translation) {
        name = from.name
        locale = from.locale
        customTranslations = from.customTranslations
        file = from.file
    }
}
