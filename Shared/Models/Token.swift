import Combine
import Foundation

class Token: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: Token, rhs: Token) -> Bool {
        if lhs.name != rhs.name { return false }
        if lhs.scopes != rhs.scopes { return false }
        if lhs.type != rhs.type { return false }
        if lhs.meta != rhs.meta { return false }
        if lhs.expire != rhs.expire { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(scopes)
        hasher.combine(type)
        hasher.combine(meta)
        hasher.combine(expire)
    }

    var name: String = ""
    var scopes: [String] = []
    var type: String = ""
    var meta: [String: String]?
    var expire: Date = .init()

    func copy(_ from: Token) {
        name = from.name
        scopes = from.scopes
        type = from.type
        meta = from.meta
        expire = from.expire
    }
}
