import Combine
import Foundation

class LegendIcon: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: LegendIcon, rhs: LegendIcon) -> Bool {
        if lhs.uid != rhs.uid { return false }
        if lhs.count != rhs.count { return false }
        if lhs.icon != rhs.icon { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(uid)
        hasher.combine(count)
        hasher.combine(icon)
    }

    private var cancellables = Set<AnyCancellable>()

    @Published var uid: String = ""
    @Published var count: UInt = .init()
    @Published var icon: Icon = .init()
    @Published var iconId: String = "" {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .uid) {
            uid = value
        }

        if let value = try? values.decode(UInt.self, forKey: .count) {
            count = value
        }

        if let value = try? values.decode(String.self, forKey: .iconId) {
            iconId = value
        }
    }

    enum CodingKeys: String, CodingKey {
        case uid
        case count
        case iconId = "icon"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(uid, forKey: .uid)
        try? container.encode(count, forKey: .count)
        try? container.encode(iconId, forKey: .iconId)
    }

    func loadIcon() async throws {
        let result = try await Store.instance.icons.getById(id: iconId)

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.icon = result
                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadIcon())
    }

    func copy(_ from: LegendIcon) {
        uid = from.uid
        count = from.count
        iconId = from.iconId
    }
}
