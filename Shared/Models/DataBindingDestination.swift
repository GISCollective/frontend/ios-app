import Combine
import Foundation

class DataBindingDestination: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: DataBindingDestination, rhs: DataBindingDestination) -> Bool {
        if lhs.type != rhs.type { return false }
        if lhs.modelId != rhs.modelId { return false }
        if lhs.deleteNonSyncedRecords != rhs.deleteNonSyncedRecords { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(type)
        hasher.combine(modelId)
        hasher.combine(deleteNonSyncedRecords)
    }

    var type: String = ""
    var modelId: String = ""
    var deleteNonSyncedRecords: Bool = false

    func copy(_ from: DataBindingDestination) {
        type = from.type
        modelId = from.modelId
        deleteNonSyncedRecords = from.deleteNonSyncedRecords
    }
}
