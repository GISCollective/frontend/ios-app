import Combine
import Foundation

struct StatList: CrateModelList, @unchecked Sendable {
    static let name: String = "stats"
    var stats: [Stat]
    var items: [Stat] { return stats }

    init(_ items: [Stat]) {
        stats = items
    }
}

struct StatItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "stats"
    var item: Stat { return stat }
    var stat: Stat

    init(_ item: Stat) {
        stat = item
    }
}

class Stat: CrateModel, @unchecked Sendable {
    static let modelName: String = "stats"

    static func == (lhs: Stat, rhs: Stat) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var value: Double = .init()
    @Published var lastUpdate: Date? = nil

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(Double.self, forKey: .value) {
            self.value = value
        }

        if let value = try? values.decode(Date?.self, forKey: .lastUpdate) {
            lastUpdate = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case value
        case lastUpdate
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(value, forKey: .value)
        try? container.encode(lastUpdate, forKey: .lastUpdate)
    }

    func copy(_ from: Stat) {
        name = from.name
        value = from.value
        lastUpdate = from.lastUpdate
    }
}
