import Combine
import Foundation
import GEOSwift

struct MapList: CrateModelList, @unchecked Sendable {
    static let name: String = "maps"
    var maps: [Map]
    var items: [Map] { return maps }

    init(_ items: [Map]) {
        maps = items
    }
}

struct MapItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "maps"
    var item: Map { return map }
    var map: Map

    init(_ item: Map) {
        map = item
    }
}

class Map: CrateModel, @unchecked Sendable {
    static let modelName: String = "maps"

    static func == (lhs: Map, rhs: Map) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var description: ArticleBody = .init()
    @Published var tagLine: String? = nil
    @Published var isIndex: Bool? = nil
    @Published var hideOnMainMap: Bool? = nil
    @Published var showPublicDownloadLinks: Bool? = nil
    @Published var addFeaturesAsPending: Bool? = nil
    @Published var visibility: Visibility = .init()
    @Published var area: Geometry? = nil
    @Published var iconSets: IconSets = .init()
    @Published var baseMaps: BaseMaps? = nil
    @Published var startDate: Date? = nil
    @Published var endDate: Date? = nil
    @Published var info: ModelInfo? = nil
    @Published var mask: Mask? = nil
    @Published var license: License? = nil
    @Published var cluster: Cluster? = nil
    @Published var cover: Picture = .init()
    @Published var coverId: String = "" {
        didSet {}
    }

    @Published var squareCover: Picture? = nil
    @Published var squareCoverId: String? = nil {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(ArticleBody.self, forKey: .description) {
            description = value
        }

        if let value = try? values.decode(String?.self, forKey: .tagLine) {
            tagLine = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .isIndex) {
            isIndex = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .hideOnMainMap) {
            hideOnMainMap = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .showPublicDownloadLinks) {
            showPublicDownloadLinks = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .addFeaturesAsPending) {
            addFeaturesAsPending = value
        }

        if let value = try? values.decode(Visibility.self, forKey: .visibility) {
            visibility = value
        }

        if let value = try? values.decode(Geometry?.self, forKey: .area) {
            area = value
        }

        if let value = try? values.decode(IconSets.self, forKey: .iconSets) {
            iconSets = value
        }

        if let value = try? values.decode(BaseMaps?.self, forKey: .baseMaps) {
            baseMaps = value
        }

        if let value = try? values.decode(Date?.self, forKey: .startDate) {
            startDate = value
        }

        if let value = try? values.decode(Date?.self, forKey: .endDate) {
            endDate = value
        }

        if let value = try? values.decode(ModelInfo?.self, forKey: .info) {
            info = value
        }

        if let value = try? values.decode(Mask?.self, forKey: .mask) {
            mask = value
        }

        if let value = try? values.decode(License?.self, forKey: .license) {
            license = value
        }

        if let value = try? values.decode(Cluster?.self, forKey: .cluster) {
            cluster = value
        }

        if let value = try? values.decode(String.self, forKey: .coverId) {
            coverId = value
        }

        if let value = try? values.decode(String?.self, forKey: .squareCoverId) {
            squareCoverId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case description
        case tagLine
        case isIndex
        case hideOnMainMap
        case showPublicDownloadLinks
        case addFeaturesAsPending
        case visibility
        case area
        case iconSets
        case baseMaps
        case startDate
        case endDate
        case info
        case mask
        case license
        case cluster
        case coverId = "cover"
        case squareCoverId = "squareCover"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(description, forKey: .description)
        try? container.encode(tagLine, forKey: .tagLine)
        try? container.encode(isIndex, forKey: .isIndex)
        try? container.encode(hideOnMainMap, forKey: .hideOnMainMap)
        try? container.encode(showPublicDownloadLinks, forKey: .showPublicDownloadLinks)
        try? container.encode(addFeaturesAsPending, forKey: .addFeaturesAsPending)
        try? container.encode(visibility, forKey: .visibility)
        try? container.encode(area, forKey: .area)
        try? container.encode(iconSets, forKey: .iconSets)
        try? container.encode(baseMaps, forKey: .baseMaps)
        try? container.encode(startDate, forKey: .startDate)
        try? container.encode(endDate, forKey: .endDate)
        try? container.encode(info, forKey: .info)
        try? container.encode(mask, forKey: .mask)
        try? container.encode(license, forKey: .license)
        try? container.encode(cluster, forKey: .cluster)
        try? container.encode(coverId, forKey: .coverId)
        try? container.encode(squareCoverId, forKey: .squareCoverId)
    }

    func loadCover() async throws {
        let result = try await Store.instance.pictures.getById(id: coverId)

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.cover = result
                continuation.resume(returning: true)
            }
        }
    }

    func loadSquareCover() async throws {
        if let squareCoverId = squareCoverId {
            let result = try await Store.instance.pictures.getById(id: squareCoverId)

            let _: Bool = await withUnsafeContinuation { continuation in
                DispatchQueue.main.async {
                    self.squareCover = result
                    continuation.resume(returning: true)
                }
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadCover(), loadSquareCover())
    }

    func copy(_ from: Map) {
        name = from.name
        description = from.description
        tagLine = from.tagLine
        isIndex = from.isIndex
        hideOnMainMap = from.hideOnMainMap
        showPublicDownloadLinks = from.showPublicDownloadLinks
        addFeaturesAsPending = from.addFeaturesAsPending
        visibility = from.visibility
        area = from.area
        iconSets = from.iconSets
        baseMaps = from.baseMaps
        startDate = from.startDate
        endDate = from.endDate
        info = from.info
        mask = from.mask
        license = from.license
        cluster = from.cluster
        coverId = from.coverId
        squareCoverId = from.squareCoverId
    }
}
