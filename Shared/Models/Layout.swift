import Combine
import Foundation

struct LayoutList: CrateModelList, @unchecked Sendable {
    static let name: String = "layouts"
    var layouts: [Layout]
    var items: [Layout] { return layouts }

    init(_ items: [Layout]) {
        layouts = items
    }
}

struct LayoutItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "layouts"
    var item: Layout { return layout }
    var layout: Layout

    init(_ item: Layout) {
        layout = item
    }
}

class Layout: CrateModel, @unchecked Sendable {
    static let modelName: String = "layouts"

    static func == (lhs: Layout, rhs: Layout) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var info: ModelInfo = .init()
    @Published var rows: [LayoutRow]? = nil
    @Published var containers: [LayoutContainer]? = nil

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(ModelInfo.self, forKey: .info) {
            info = value
        }

        if let value = try? values.decode([LayoutRow]?.self, forKey: .rows) {
            rows = value
        }

        if let value = try? values.decode([LayoutContainer]?.self, forKey: .containers) {
            containers = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case info
        case rows
        case containers
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(info, forKey: .info)
        try? container.encode(rows, forKey: .rows)
        try? container.encode(containers, forKey: .containers)
    }

    func copy(_ from: Layout) {
        name = from.name
        info = from.info
        rows = from.rows
        containers = from.containers
    }
}
