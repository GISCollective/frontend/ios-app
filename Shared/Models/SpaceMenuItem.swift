import Combine
import Foundation

class SpaceMenuItem: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: SpaceMenuItem, rhs: SpaceMenuItem) -> Bool {
        if lhs.name != rhs.name { return false }
        if lhs.link != rhs.link { return false }
        if lhs.dropDown != rhs.dropDown { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(link)
        hasher.combine(dropDown)
    }

    var name: String?
    var link: Link?
    var dropDown: [SpaceMenuChildItem]?

    func copy(_ from: SpaceMenuItem) {
        name = from.name
        link = from.link
        dropDown = from.dropDown
    }
}
