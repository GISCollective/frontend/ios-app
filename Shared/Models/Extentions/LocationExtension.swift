//
//  LocationExtension.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 11.10.21.
//

import Foundation

extension Location {
    static func withData(detailedLocation: DetailedLocation? = nil, isDetailed: Bool = false, simple: String = "") -> Location {
        let instance = Location()

        instance.detailedLocation = detailedLocation
        instance.isDetailed = isDetailed
        instance.simple = simple

        return instance
    }

    func pretty() -> String {
        if !isDetailed, simple.trim() != "" {
            return simple
        }

        if let detailedLocation = detailedLocation, isDetailed {
            return detailedLocation.pretty()
        }

        return "---"
    }
}
