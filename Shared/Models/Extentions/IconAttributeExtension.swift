//
//  IconAttributeExtension.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 12.10.21.
//

import Foundation

extension IconAttribute {
    static func withData(name: String = "", type: String? = nil, options: String? = nil, isPrivate: Bool = false, isRequired: Bool = false, displayName: String? = nil, help: String? = nil, isInherited: Bool = false) -> IconAttribute {
        let instance = IconAttribute()

        instance.name = name
        instance.type = type ?? ""
        instance.options = options
        instance.isPrivate = isPrivate
        instance.isRequired = isRequired
        instance.displayName = displayName
        instance.help = help
        instance.isInherited = isInherited

        return instance
    }
}
