//
//  OptionalIconImageExtension.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 12.10.21.
//

import Foundation

extension OptionalIconImage {
    static func withData(useParent: Bool, value: String) -> OptionalIconImage {
        let instance = OptionalIconImage()

        instance.useParent = useParent
        instance.value = value

        return instance
    }
}
