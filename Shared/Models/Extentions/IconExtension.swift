//
//  IconExtension.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 10.10.21.
//

import Foundation

extension Icon {
    enum Holder {
        static var images: [String: ObservableUIImage] = [:]
    }

    var processedDescription: ArticleBody? {
        description?.setDefaultTitle(title: name)
        return description
    }

    func uiImage() -> ObservableUIImage {
        if let image = image.value {
            return uiImage(image)
        }

        return ObservableUIImage.defaultValue
    }

    func uiImage(_ image: String) -> ObservableUIImage {
        if Holder.images[id] == nil {
            Holder.images[id] = ObservableUIImage(baseUrl: image, size: "png")
            Holder.images[id]?.load()
        }

        return Holder.images[id] ?? ObservableUIImage.defaultValue
    }

    func url() -> String {
        if let picture = image.value {
            return "\(picture)/png"
        }

        return ""
    }
}
