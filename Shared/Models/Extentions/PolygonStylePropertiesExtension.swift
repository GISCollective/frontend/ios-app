//
//  PolygonStylePropertiesExtension.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 12.10.21.
//

import Foundation

extension PolygonStyleProperties {
    static func withData(borderColor: String, backgroundColor: String, borderWidth: UInt32, lineDash: [UInt32], hideBackgroundOnZoom: Bool) -> PolygonStyleProperties {
        let instance = PolygonStyleProperties()

        instance.borderColor = borderColor
        instance.backgroundColor = backgroundColor
        instance.borderWidth = borderWidth
        instance.lineDash = lineDash
        instance.hideBackgroundOnZoom = hideBackgroundOnZoom

        return instance
    }
}
