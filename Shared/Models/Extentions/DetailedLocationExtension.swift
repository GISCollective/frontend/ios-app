//
//  DetailedLocationExtension.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 11.10.21.
//

import Foundation

extension DetailedLocation {
    static func withData(city: String, country: String, postalCode: String, province: String) -> DetailedLocation {
        let instance = DetailedLocation()

        instance.city = city
        instance.country = country
        instance.postalCode = postalCode
        instance.province = province

        return instance
    }

    func pretty() -> String {
        var result: [String] = []

        if let postalCode = postalCode, postalCode.trim() != "" {
            result.append(postalCode)
        }

        if let city = city, city.trim() != "" {
            result.append(city)
        }

        if let province = province, province.trim() != "" {
            result.append(province)
        }

        if let country = country, country.trim() != "" {
            result.append(country)
        }

        if result.count == 0 {
            return "---"
        }

        return result.joined(separator: ", ")
    }
}
