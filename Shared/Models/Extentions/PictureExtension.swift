//
//  Picture.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 10.10.21.
//

import Foundation

import Combine
import Foundation
import SwiftUI

extension Picture {
    enum Holder {
        static var images: [String: ObservableUIImage] = [:]
    }

    func uiImage() -> ObservableUIImage {
        return fetchImage(size: "md", baseUrl: picture)
    }

    func uiImage(_ baseUrl: String) -> ObservableUIImage {
        return fetchImage(size: "md", baseUrl: baseUrl)
    }

    func uiImageSm() -> ObservableUIImage {
        return fetchImage(size: "sm", baseUrl: picture)
    }

    func uiImageSm(_: String) -> ObservableUIImage {
        return fetchImage(size: "sm", baseUrl: picture)
    }

    func fetchImage(size: String, baseUrl: String) -> ObservableUIImage {
        if baseUrl == "" {
            return ObservableUIImage.defaultValue
        }

        let key = "\(id)\(size)"

        if Holder.images[key] == nil {
            Holder.images[key] = ObservableUIImage(baseUrl: baseUrl, size: size)
            Holder.images[key]?.load()
        }

        return Holder.images[key] ?? ObservableUIImage.defaultValue
    }

    static var defaultObservable: ObservableModel<Picture> = {
        var picture = Picture(id: "")
        var observable = ObservableModel<Picture>(value: picture)
        return observable
    }()
}

protocol ImageCache {
    subscript(_: URL) -> UIImage? { get set }
}

class TemporaryImageCache: ImageCache {
    static var instance = TemporaryImageCache()

    private let cache = NSCache<NSURL, UIImage>()

    subscript(_ key: URL) -> UIImage? {
        get { cache.object(forKey: key as NSURL) }
        set { newValue == nil ? cache.removeObject(forKey: key as NSURL) : cache.setObject(newValue!, forKey: key as NSURL) }
    }
}
