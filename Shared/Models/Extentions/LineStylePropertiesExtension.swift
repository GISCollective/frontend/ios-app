//
//  File.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 12.10.21.
//

import Foundation

extension LineStyleProperties {
    static func withData(borderColor: String = "", backgroundColor: String = "", borderWidth: UInt32, lineDash: [UInt32]) -> LineStyleProperties {
        let instance = LineStyleProperties()

        instance.borderColor = borderColor
        instance.backgroundColor = backgroundColor
        instance.borderWidth = borderWidth
        instance.lineDash = lineDash

        return instance
    }
}
