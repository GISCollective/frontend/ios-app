//
//  Attributes.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 14.07.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

enum AttributeGroup: Codable {
    case list([[String: Attribute]])
    case group([String: Attribute])
    case undefined

    init(from decoder: Decoder) throws {
        if var container = try? decoder.unkeyedContainer() {
            var elements: [[String: Attribute]] = []

            if let count = container.count {
                elements.reserveCapacity(count)

                while !container.isAtEnd {
                    if let element = try? container.decode([String: Attribute].self) {
                        elements.append(element)
                    }
                }
            }

            self = .list(elements)
            return
        }

        if let container = try? decoder.singleValueContainer().decode([String: Attribute].self) {
            self = .group(container)
            return
        }

        self = .undefined
    }

    func encode(to encoder: Encoder) throws {
        if case let .group(dictionary) = self {
            var container = encoder.container(keyedBy: DynamicKey.self)

            for (key, value) in dictionary {
                let dynamicKey = DynamicKey(stringValue: key)!
                try container.encode(value, forKey: dynamicKey)
            }
        }

        if case let .list(list) = self {
            var container = encoder.unkeyedContainer()

            for value in list {
                try container.encode(value)
            }
        }
    }

    var count: Int? {
        if case let .list(value) = self {
            return value.count
        }

        return nil
    }

    func asList() -> [[String: Attribute]]? {
        if case let .list(value) = self {
            return value
        }

        return nil
    }

    func asGroup() -> [String: Attribute]? {
        if case let .group(value) = self {
            return value
        }

        return nil
    }

    subscript(index: Int) -> [String: Attribute]? {
        get {
            if case let .list(value) = self {
                if index >= value.count {
                    return nil
                }

                return value[index]
            }

            return nil
        }

        set(newElm) {
            if case var .list(value) = self {
                while value.count <= index {
                    value.append([:])
                }

                value[index] = newElm ?? [:]
                self = .list(value)
            }
        }
    }

    subscript(key: String) -> Attribute? {
        get {
            if case let .group(value) = self {
                return value[key]
            }

            return nil
        }

        set(newValue) {
            if case var .group(value) = self {
                value[key] = newValue
                self = .group(value)
            }
        }
    }
}

enum Attribute: Codable, Equatable {
    case string(String)
    case number(Int)
    case double(Double)
    case bool(Bool)
    case undefined

    init(from decoder: Decoder) throws {
        if let value = try? decoder.singleValueContainer().decode(String.self) {
            self = .string(value)
            return
        }

        if let value = try? decoder.singleValueContainer().decode(Int.self) {
            self = .number(value)
            return
        }

        if let value = try? decoder.singleValueContainer().decode(Double.self) {
            self = .double(value)
            return
        }

        if let value = try? decoder.singleValueContainer().decode(Bool.self) {
            self = .bool(value)
            return
        }

        self = .undefined
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        if case let .string(value) = self {
            try container.encode(value)
        }

        if case let .number(value) = self {
            try container.encode(value)
        }

        if case let .double(value) = self {
            try container.encode(value)
        }

        if case let .bool(value) = self {
            try container.encode(value)
        }

        if case .undefined = self {
            try container.encodeNil()
        }
    }
}

struct DynamicKey: CodingKey {
    var stringValue: String

    init?(stringValue: String) {
        self.stringValue = stringValue
    }

    var intValue: Int? { return nil }

    init?(intValue _: Int) { return nil }
}
