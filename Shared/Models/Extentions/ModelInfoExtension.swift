//
//  ModelInfo.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 12.10.21.
//

import Foundation

extension ModelInfo {
    static func withData(changeIndex: UInt, createdOn: String, lastChangeOn: String, originalAuthor: String, author: String) -> ModelInfo {
        let instance = ModelInfo()

        instance.changeIndex = changeIndex
        instance.createdOn = Date.fromISOString(createdOn) ?? Date()
        instance.lastChangeOn = Date.fromISOString(lastChangeOn) ?? Date()
        instance.originalAuthor = originalAuthor
        instance.author = author

        return instance
    }
}
