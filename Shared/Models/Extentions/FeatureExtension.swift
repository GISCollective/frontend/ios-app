//
//  FeatureExtension.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 14.10.21.
//

import Foundation
import MapKit

extension Feature {
    var processedDescription: ArticleBody {
        description.setDefaultTitle(title: name)
        return description
    }

    var coordinate: CLLocationCoordinate2D {
        if let centroid = try? position.centroid() {
            return centroid.toCLLocationCoordinate2D()
        }

        return CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }
}
