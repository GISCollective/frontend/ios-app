//
//  MapExtension.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 17.11.21.
//

import CoreLocation
import Foundation
import GEOSwift

extension Map {
    var center: CLLocation {
        if let area = area, let centroid = try? area.centroid().toCLLocationCoordinate2D() {
            return CLLocation(latitude: centroid.latitude, longitude: centroid.longitude)
        }

        return CLLocation(latitude: 10, longitude: 20)
    }

    var processedDescription: ArticleBody {
        description.setDefaultTitle(title: name)
        return description
    }
}
