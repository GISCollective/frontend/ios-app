//
//  UserProfileExtension.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 10.10.21.
//

import Combine
import Foundation

extension UserProfile {
    func fullName() -> String {
        var result: [String] = []

        if firstName.trim() != "" {
            result.append(firstName.trim())
        }

        if lastName.trim() != "" {
            result.append(lastName.trim())
        }

        if result.count == 0 {
            return "---"
        }

        if title?.trim() != "" {
            result.insert(title?.trim() ?? "", at: 0)
        }

        return result.joined(separator: " ")
    }

    func hasFullName() -> Bool {
        return fullName() != "---"
    }

    func prettyJoinedTime() -> String {
        if let value = joinedTime {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_us")
            formatter.setLocalizedDateFormatFromTemplate("dd MMMM YYYY")
            return formatter.string(from: value)
        }

        return "---"
    }

    func prettyLocation() -> String {
        return location.pretty()
    }

    func prettyJob() -> String {
        var values: [String] = []

        if jobTitle.trim() != "" {
            values.append(jobTitle)
        }

        if organization.trim() != "" {
            values.append(organization)
        }

        if values.count == 0 {
            return "---"
        }

        return values.joined(separator: " at ")
    }

    func hasLinks() -> Bool {
        return twitter.trim() != "" && skype.trim() != "" && linkedin.trim() != ""
    }
}
