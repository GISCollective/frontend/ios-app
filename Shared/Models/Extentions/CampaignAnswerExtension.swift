//
//  CampaignExtension.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 10.10.21.
//

import Foundation
import GEOSwift

extension CampaignAnswer {
    func updateValue(groupName: String, index: Int, key: String, value: Attribute) {
        let allowMany = icons?.first(where: { groupName == $0.name })?.allowMany ?? false

        if allowMany {
            updateListValue(groupName: groupName, index: index, key: key, value: value)
        } else {
            updateGroupValue(groupName: groupName, index: index, key: key, value: value)
        }
    }

    private func updateListValue(groupName: String, index: Int, key: String, value: Attribute) {
        if attributes[groupName] == nil {
            attributes[groupName] = AttributeGroup.list([])
        }

        attributes[groupName]![index] = [key: value]
    }

    private func updateGroupValue(groupName: String, index _: Int, key: String, value: Attribute) {
        if attributes[groupName] == nil {
            attributes[groupName] = AttributeGroup.group([:])
        }

        if case .group = attributes[groupName] {
            self.attributes[groupName]?[key] = value
        }
    }
}
