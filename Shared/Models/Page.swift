import Combine
import Foundation

struct PageList: CrateModelList, @unchecked Sendable {
    static let name: String = "pages"
    var pages: [Page]
    var items: [Page] { return pages }

    init(_ items: [Page]) {
        pages = items
    }
}

struct PageItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "pages"
    var item: Page { return page }
    var page: Page

    init(_ item: Page) {
        page = item
    }
}

class Page: CrateModel, @unchecked Sendable {
    static let modelName: String = "pages"

    static func == (lhs: Page, rhs: Page) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var slug: String = ""
    @Published var info: ModelInfo = .init()
    @Published var visibility: Visibility = .init()
    @Published var cols: [PageCol] = []
    @Published var containers: [PageContainer]? = nil
    @Published var layoutContainers: [LayoutContainer]? = nil
    @Published var layout: Layout? = nil
    @Published var layoutId: String? = nil {
        didSet {}
    }

    @Published var space: Space? = nil
    @Published var spaceId: String? = nil {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(String.self, forKey: .slug) {
            slug = value
        }

        if let value = try? values.decode(ModelInfo.self, forKey: .info) {
            info = value
        }

        if let value = try? values.decode(Visibility.self, forKey: .visibility) {
            visibility = value
        }

        if let value = try? values.decode([PageCol].self, forKey: .cols) {
            cols = value
        }

        if let value = try? values.decode([PageContainer]?.self, forKey: .containers) {
            containers = value
        }

        if let value = try? values.decode([LayoutContainer]?.self, forKey: .layoutContainers) {
            layoutContainers = value
        }

        if let value = try? values.decode(String?.self, forKey: .layoutId) {
            layoutId = value
        }

        if let value = try? values.decode(String?.self, forKey: .spaceId) {
            spaceId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case slug
        case info
        case visibility
        case cols
        case containers
        case layoutContainers
        case layoutId = "layout"
        case spaceId = "space"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(slug, forKey: .slug)
        try? container.encode(info, forKey: .info)
        try? container.encode(visibility, forKey: .visibility)
        try? container.encode(cols, forKey: .cols)
        try? container.encode(containers, forKey: .containers)
        try? container.encode(layoutContainers, forKey: .layoutContainers)
        try? container.encode(layoutId, forKey: .layoutId)
        try? container.encode(spaceId, forKey: .spaceId)
    }

    func loadLayout() async throws {
        if let layoutId = layoutId {
            let result = try await Store.instance.layouts.getById(id: layoutId)

            let _: Bool = await withUnsafeContinuation { continuation in
                DispatchQueue.main.async {
                    self.layout = result
                    continuation.resume(returning: true)
                }
            }
        }
    }

    func loadSpace() async throws {
        if let spaceId = spaceId {
            let result = try await Store.instance.spaces.getById(id: spaceId)

            let _: Bool = await withUnsafeContinuation { continuation in
                DispatchQueue.main.async {
                    self.space = result
                    continuation.resume(returning: true)
                }
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadLayout(), loadSpace())
    }

    func copy(_ from: Page) {
        name = from.name
        slug = from.slug
        info = from.info
        visibility = from.visibility
        cols = from.cols
        containers = from.containers
        layoutContainers = from.layoutContainers
        layoutId = from.layoutId
        spaceId = from.spaceId
    }
}
