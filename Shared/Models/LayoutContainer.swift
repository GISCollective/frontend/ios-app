import Combine
import Foundation

class LayoutContainer: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: LayoutContainer, rhs: LayoutContainer) -> Bool {
        if lhs.options != rhs.options { return false }
        if lhs.rows != rhs.rows { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(options)
        hasher.combine(rows)
    }

    var options: [String]?
    var rows: [LayoutRow]?

    func copy(_ from: LayoutContainer) {
        options = from.options
        rows = from.rows
    }
}
