import Combine
import Foundation

struct IconList: CrateModelList, @unchecked Sendable {
    static let name: String = "icons"
    var icons: [Icon]
    var items: [Icon] { return icons }

    init(_ items: [Icon]) {
        icons = items
    }
}

struct IconItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "icons"
    var item: Icon { return icon }
    var icon: Icon

    init(_ item: Icon) {
        icon = item
    }
}

class Icon: CrateModel, @unchecked Sendable {
    static let modelName: String = "icons"

    static func == (lhs: Icon, rhs: Icon) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var category: String? = nil
    @Published var subcategory: String? = nil
    @Published var name: String = ""
    @Published var description: ArticleBody? = nil
    @Published var verticalIndex: Int32? = nil
    @Published var minZoom: UInt32? = nil
    @Published var maxZoom: UInt32? = nil
    @Published var parent: String? = nil
    @Published var allowMany: Bool? = nil
    @Published var keepWhenSmall: Bool? = nil
    @Published var otherNames: [String]? = nil
    @Published var localName: String? = nil
    @Published var enforceSvg: Bool? = nil
    @Published var image: OptionalIconImage = .init()
    @Published var attributes: [IconAttribute] = []
    @Published var styles: InheritedStyle? = nil
    @Published var measurements: IconMeasurements? = nil
    @Published var iconSet: IconSet = .init()
    @Published var iconSetId: String = "" {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String?.self, forKey: .category) {
            category = value
        }

        if let value = try? values.decode(String?.self, forKey: .subcategory) {
            subcategory = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(ArticleBody?.self, forKey: .description) {
            description = value
        }

        if let value = try? values.decode(Int32?.self, forKey: .verticalIndex) {
            verticalIndex = value
        }

        if let value = try? values.decode(UInt32?.self, forKey: .minZoom) {
            minZoom = value
        }

        if let value = try? values.decode(UInt32?.self, forKey: .maxZoom) {
            maxZoom = value
        }

        if let value = try? values.decode(String?.self, forKey: .parent) {
            parent = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .allowMany) {
            allowMany = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .keepWhenSmall) {
            keepWhenSmall = value
        }

        if let value = try? values.decode([String]?.self, forKey: .otherNames) {
            otherNames = value
        }

        if let value = try? values.decode(String?.self, forKey: .localName) {
            localName = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .enforceSvg) {
            enforceSvg = value
        }

        if let value = try? values.decode(OptionalIconImage.self, forKey: .image) {
            image = value
        }

        if let value = try? values.decode([IconAttribute].self, forKey: .attributes) {
            attributes = value
        }

        if let value = try? values.decode(InheritedStyle?.self, forKey: .styles) {
            styles = value
        }

        if let value = try? values.decode(IconMeasurements?.self, forKey: .measurements) {
            measurements = value
        }

        if let value = try? values.decode(String.self, forKey: .iconSetId) {
            iconSetId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case category
        case subcategory
        case name
        case description
        case verticalIndex
        case minZoom
        case maxZoom
        case parent
        case allowMany
        case keepWhenSmall
        case otherNames
        case localName
        case enforceSvg
        case image
        case attributes
        case styles
        case measurements
        case iconSetId = "iconSet"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(category, forKey: .category)
        try? container.encode(subcategory, forKey: .subcategory)
        try? container.encode(name, forKey: .name)
        try? container.encode(description, forKey: .description)
        try? container.encode(verticalIndex, forKey: .verticalIndex)
        try? container.encode(minZoom, forKey: .minZoom)
        try? container.encode(maxZoom, forKey: .maxZoom)
        try? container.encode(parent, forKey: .parent)
        try? container.encode(allowMany, forKey: .allowMany)
        try? container.encode(keepWhenSmall, forKey: .keepWhenSmall)
        try? container.encode(otherNames, forKey: .otherNames)
        try? container.encode(localName, forKey: .localName)
        try? container.encode(enforceSvg, forKey: .enforceSvg)
        try? container.encode(image, forKey: .image)
        try? container.encode(attributes, forKey: .attributes)
        try? container.encode(styles, forKey: .styles)
        try? container.encode(measurements, forKey: .measurements)
        try? container.encode(iconSetId, forKey: .iconSetId)
    }

    func loadIconSet() async throws {
        let result = try await Store.instance.iconSets.getById(id: iconSetId)

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.iconSet = result
                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadIconSet())
    }

    func copy(_ from: Icon) {
        category = from.category
        subcategory = from.subcategory
        name = from.name
        description = from.description
        verticalIndex = from.verticalIndex
        minZoom = from.minZoom
        maxZoom = from.maxZoom
        parent = from.parent
        allowMany = from.allowMany
        keepWhenSmall = from.keepWhenSmall
        otherNames = from.otherNames
        localName = from.localName
        enforceSvg = from.enforceSvg
        image = from.image
        attributes = from.attributes
        styles = from.styles
        measurements = from.measurements
        iconSetId = from.iconSetId
    }
}
