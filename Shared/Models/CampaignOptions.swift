import Combine
import Foundation

class CampaignOptions: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: CampaignOptions, rhs: CampaignOptions) -> Bool {
        if lhs.featureNamePrefix != rhs.featureNamePrefix { return false }
        if lhs.showNameQuestion != rhs.showNameQuestion { return false }
        if lhs.nameLabel != rhs.nameLabel { return false }
        if lhs.showDescriptionQuestion != rhs.showDescriptionQuestion { return false }
        if lhs.descriptionLabel != rhs.descriptionLabel { return false }
        if lhs.iconsLabel != rhs.iconsLabel { return false }
        if lhs.registrationMandatory != rhs.registrationMandatory { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(featureNamePrefix)
        hasher.combine(showNameQuestion)
        hasher.combine(nameLabel)
        hasher.combine(showDescriptionQuestion)
        hasher.combine(descriptionLabel)
        hasher.combine(iconsLabel)
        hasher.combine(registrationMandatory)
    }

    var featureNamePrefix: String = ""
    var showNameQuestion: Bool = false
    var nameLabel: String = ""
    var showDescriptionQuestion: Bool = false
    var descriptionLabel: String = ""
    var iconsLabel: String = ""
    var registrationMandatory: Bool = false

    func copy(_ from: CampaignOptions) {
        featureNamePrefix = from.featureNamePrefix
        showNameQuestion = from.showNameQuestion
        nameLabel = from.nameLabel
        showDescriptionQuestion = from.showDescriptionQuestion
        descriptionLabel = from.descriptionLabel
        iconsLabel = from.iconsLabel
        registrationMandatory = from.registrationMandatory
    }
}
