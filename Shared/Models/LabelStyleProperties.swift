import Combine
import Foundation

class LabelStyleProperties: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: LabelStyleProperties, rhs: LabelStyleProperties) -> Bool {
        if lhs.isVisible != rhs.isVisible { return false }
        if lhs.text != rhs.text { return false }
        if lhs.align != rhs.align { return false }
        if lhs.baseline != rhs.baseline { return false }
        if lhs.weight != rhs.weight { return false }
        if lhs.color != rhs.color { return false }
        if lhs.borderColor != rhs.borderColor { return false }
        if lhs.borderWidth != rhs.borderWidth { return false }
        if lhs.size != rhs.size { return false }
        if lhs.lineHeight != rhs.lineHeight { return false }
        if lhs.offsetX != rhs.offsetX { return false }
        if lhs.offsetY != rhs.offsetY { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(isVisible)
        hasher.combine(text)
        hasher.combine(align)
        hasher.combine(baseline)
        hasher.combine(weight)
        hasher.combine(color)
        hasher.combine(borderColor)
        hasher.combine(borderWidth)
        hasher.combine(size)
        hasher.combine(lineHeight)
        hasher.combine(offsetX)
        hasher.combine(offsetY)
    }

    var isVisible: Bool?
    var text: String?
    var align: String?
    var baseline: String?
    var weight: String?
    var color: String?
    var borderColor: String?
    var borderWidth: Int32?
    var size: Int32?
    var lineHeight: Int32?
    var offsetX: Int32?
    var offsetY: Int32?

    func copy(_ from: LabelStyleProperties) {
        isVisible = from.isVisible
        text = from.text
        align = from.align
        baseline = from.baseline
        weight = from.weight
        color = from.color
        borderColor = from.borderColor
        borderWidth = from.borderWidth
        size = from.size
        lineHeight = from.lineHeight
        offsetX = from.offsetX
        offsetY = from.offsetY
    }
}
