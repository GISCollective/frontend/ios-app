import Combine
import Foundation

class Mask: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: Mask, rhs: Mask) -> Bool {
        if lhs.isEnabled != rhs.isEnabled { return false }
        if lhs.map != rhs.map { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(isEnabled)
        hasher.combine(map)
    }

    var isEnabled: Bool = false
    var map: String = ""

    func copy(_ from: Mask) {
        isEnabled = from.isEnabled
        map = from.map
    }
}
