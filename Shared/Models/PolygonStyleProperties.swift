import Combine
import Foundation

class PolygonStyleProperties: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: PolygonStyleProperties, rhs: PolygonStyleProperties) -> Bool {
        if lhs.hideBackgroundOnZoom != rhs.hideBackgroundOnZoom { return false }
        if lhs.showAsLineAfterZoom != rhs.showAsLineAfterZoom { return false }
        if lhs.borderColor != rhs.borderColor { return false }
        if lhs.backgroundColor != rhs.backgroundColor { return false }
        if lhs.borderWidth != rhs.borderWidth { return false }
        if lhs.lineDash != rhs.lineDash { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(hideBackgroundOnZoom)
        hasher.combine(showAsLineAfterZoom)
        hasher.combine(borderColor)
        hasher.combine(backgroundColor)
        hasher.combine(borderWidth)
        hasher.combine(lineDash)
    }

    var hideBackgroundOnZoom: Bool?
    var showAsLineAfterZoom: UInt32?
    var borderColor: String?
    var backgroundColor: String?
    var borderWidth: UInt32?
    var lineDash: [UInt32]?

    func copy(_ from: PolygonStyleProperties) {
        hideBackgroundOnZoom = from.hideBackgroundOnZoom
        showAsLineAfterZoom = from.showAsLineAfterZoom
        borderColor = from.borderColor
        backgroundColor = from.backgroundColor
        borderWidth = from.borderWidth
        lineDash = from.lineDash
    }
}
