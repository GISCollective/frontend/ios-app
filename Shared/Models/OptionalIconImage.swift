import Combine
import Foundation

class OptionalIconImage: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: OptionalIconImage, rhs: OptionalIconImage) -> Bool {
        if lhs.useParent != rhs.useParent { return false }
        if lhs.value != rhs.value { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(useParent)
        hasher.combine(value)
    }

    var useParent: Bool?
    var value: String?

    func copy(_ from: OptionalIconImage) {
        useParent = from.useParent
        value = from.value
    }
}
