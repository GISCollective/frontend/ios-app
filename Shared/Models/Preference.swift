import Combine
import Foundation

struct PreferenceList: CrateModelList, @unchecked Sendable {
    static let name: String = "preferences"
    var preferences: [Preference]
    var items: [Preference] { return preferences }

    init(_ items: [Preference]) {
        preferences = items
    }
}

struct PreferenceItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "preferences"
    var item: Preference { return preference }
    var preference: Preference

    init(_ item: Preference) {
        preference = item
    }
}

class Preference: CrateModel, @unchecked Sendable {
    static let modelName: String = "preferences"

    static func == (lhs: Preference, rhs: Preference) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var value: String = ""
    @Published var isSecret: Bool? = nil

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(String.self, forKey: .value) {
            self.value = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .isSecret) {
            isSecret = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case value
        case isSecret
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(value, forKey: .value)
        try? container.encode(isSecret, forKey: .isSecret)
    }

    func copy(_ from: Preference) {
        name = from.name
        value = from.value
        isSecret = from.isSecret
    }
}
