import Combine
import Foundation

class Cluster: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: Cluster, rhs: Cluster) -> Bool {
        if lhs.mode != rhs.mode { return false }
        if lhs.map != rhs.map { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(mode)
        hasher.combine(map)
    }

    var mode: Int32 = .init()
    var map: String?

    func copy(_ from: Cluster) {
        mode = from.mode
        map = from.map
    }
}
