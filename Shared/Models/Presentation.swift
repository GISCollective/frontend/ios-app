import Combine
import Foundation

struct PresentationList: CrateModelList, @unchecked Sendable {
    static let name: String = "presentations"
    var presentations: [Presentation]
    var items: [Presentation] { return presentations }

    init(_ items: [Presentation]) {
        presentations = items
    }
}

struct PresentationItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "presentations"
    var item: Presentation { return presentation }
    var presentation: Presentation

    init(_ item: Presentation) {
        presentation = item
    }
}

class Presentation: CrateModel, @unchecked Sendable {
    static let modelName: String = "presentations"

    static func == (lhs: Presentation, rhs: Presentation) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var slug: String = ""
    @Published var hasIntro: Bool? = nil
    @Published var hasEnd: Bool? = nil
    @Published var introTimeout: Int32? = nil
    @Published var endSlideOptions: Json? = nil
    @Published var info: ModelInfo = .init()
    @Published var visibility: Visibility = .init()
    @Published var cols: [PageCol] = []
    @Published var layout: Layout = .init()
    @Published var layoutId: String = "" {
        didSet {}
    }

    @Published var cover: Picture? = nil
    @Published var coverId: String? = nil {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(String.self, forKey: .slug) {
            slug = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .hasIntro) {
            hasIntro = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .hasEnd) {
            hasEnd = value
        }

        if let value = try? values.decode(Int32?.self, forKey: .introTimeout) {
            introTimeout = value
        }

        if let value = try? values.decode(Json?.self, forKey: .endSlideOptions) {
            endSlideOptions = value
        }

        if let value = try? values.decode(ModelInfo.self, forKey: .info) {
            info = value
        }

        if let value = try? values.decode(Visibility.self, forKey: .visibility) {
            visibility = value
        }

        if let value = try? values.decode([PageCol].self, forKey: .cols) {
            cols = value
        }

        if let value = try? values.decode(String.self, forKey: .layoutId) {
            layoutId = value
        }

        if let value = try? values.decode(String?.self, forKey: .coverId) {
            coverId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case slug
        case hasIntro
        case hasEnd
        case introTimeout
        case endSlideOptions
        case info
        case visibility
        case cols
        case layoutId = "layout"
        case coverId = "cover"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(slug, forKey: .slug)
        try? container.encode(hasIntro, forKey: .hasIntro)
        try? container.encode(hasEnd, forKey: .hasEnd)
        try? container.encode(introTimeout, forKey: .introTimeout)
        try? container.encode(endSlideOptions, forKey: .endSlideOptions)
        try? container.encode(info, forKey: .info)
        try? container.encode(visibility, forKey: .visibility)
        try? container.encode(cols, forKey: .cols)
        try? container.encode(layoutId, forKey: .layoutId)
        try? container.encode(coverId, forKey: .coverId)
    }

    func loadLayout() async throws {
        let result = try await Store.instance.layouts.getById(id: layoutId)

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.layout = result
                continuation.resume(returning: true)
            }
        }
    }

    func loadCover() async throws {
        if let coverId = coverId {
            let result = try await Store.instance.pictures.getById(id: coverId)

            let _: Bool = await withUnsafeContinuation { continuation in
                DispatchQueue.main.async {
                    self.cover = result
                    continuation.resume(returning: true)
                }
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadLayout(), loadCover())
    }

    func copy(_ from: Presentation) {
        name = from.name
        slug = from.slug
        hasIntro = from.hasIntro
        hasEnd = from.hasEnd
        introTimeout = from.introTimeout
        endSlideOptions = from.endSlideOptions
        info = from.info
        visibility = from.visibility
        cols = from.cols
        layoutId = from.layoutId
        coverId = from.coverId
    }
}
