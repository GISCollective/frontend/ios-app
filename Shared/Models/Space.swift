import Combine
import Foundation

struct SpaceList: CrateModelList, @unchecked Sendable {
    static let name: String = "spaces"
    var spaces: [Space]
    var items: [Space] { return spaces }

    init(_ items: [Space]) {
        spaces = items
    }
}

struct SpaceItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "spaces"
    var item: Space { return space }
    var space: Space

    init(_ item: Space) {
        space = item
    }
}

class Space: CrateModel, @unchecked Sendable {
    static let modelName: String = "spaces"

    static func == (lhs: Space, rhs: Space) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var landingPageId: String? = nil
    @Published var domain: String? = nil
    @Published var domainValidationKey: String? = nil
    @Published var allowDomainChange: Bool? = nil
    @Published var hasDomainValidated: Bool? = nil
    @Published var matomoUrl: String? = nil
    @Published var matomoSiteId: String? = nil
    @Published var menu: SpaceMenu? = nil
    @Published var info: ModelInfo = .init()
    @Published var visibility: Visibility = .init()
    @Published var attributions: [Attribution]? = nil
    @Published var cols: [String: PageCol]? = nil
    @Published var logo: Picture? = nil
    @Published var logoId: String? = nil {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(String?.self, forKey: .landingPageId) {
            landingPageId = value
        }

        if let value = try? values.decode(String?.self, forKey: .domain) {
            domain = value
        }

        if let value = try? values.decode(String?.self, forKey: .domainValidationKey) {
            domainValidationKey = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .allowDomainChange) {
            allowDomainChange = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .hasDomainValidated) {
            hasDomainValidated = value
        }

        if let value = try? values.decode(String?.self, forKey: .matomoUrl) {
            matomoUrl = value
        }

        if let value = try? values.decode(String?.self, forKey: .matomoSiteId) {
            matomoSiteId = value
        }

        if let value = try? values.decode(SpaceMenu?.self, forKey: .menu) {
            menu = value
        }

        if let value = try? values.decode(ModelInfo.self, forKey: .info) {
            info = value
        }

        if let value = try? values.decode(Visibility.self, forKey: .visibility) {
            visibility = value
        }

        if let value = try? values.decode([Attribution]?.self, forKey: .attributions) {
            attributions = value
        }

        if let value = try? values.decode([String: PageCol]?.self, forKey: .cols) {
            cols = value
        }

        if let value = try? values.decode(String?.self, forKey: .logoId) {
            logoId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case landingPageId
        case domain
        case domainValidationKey
        case allowDomainChange
        case hasDomainValidated
        case matomoUrl
        case matomoSiteId
        case menu
        case info
        case visibility
        case attributions
        case cols
        case logoId = "logo"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(landingPageId, forKey: .landingPageId)
        try? container.encode(domain, forKey: .domain)
        try? container.encode(domainValidationKey, forKey: .domainValidationKey)
        try? container.encode(allowDomainChange, forKey: .allowDomainChange)
        try? container.encode(hasDomainValidated, forKey: .hasDomainValidated)
        try? container.encode(matomoUrl, forKey: .matomoUrl)
        try? container.encode(matomoSiteId, forKey: .matomoSiteId)
        try? container.encode(menu, forKey: .menu)
        try? container.encode(info, forKey: .info)
        try? container.encode(visibility, forKey: .visibility)
        try? container.encode(attributions, forKey: .attributions)
        try? container.encode(cols, forKey: .cols)
        try? container.encode(logoId, forKey: .logoId)
    }

    func loadLogo() async throws {
        if let logoId = logoId {
            let result = try await Store.instance.pictures.getById(id: logoId)

            let _: Bool = await withUnsafeContinuation { continuation in
                DispatchQueue.main.async {
                    self.logo = result
                    continuation.resume(returning: true)
                }
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadLogo())
    }

    func copy(_ from: Space) {
        name = from.name
        landingPageId = from.landingPageId
        domain = from.domain
        domainValidationKey = from.domainValidationKey
        allowDomainChange = from.allowDomainChange
        hasDomainValidated = from.hasDomainValidated
        matomoUrl = from.matomoUrl
        matomoSiteId = from.matomoSiteId
        menu = from.menu
        info = from.info
        visibility = from.visibility
        attributions = from.attributions
        cols = from.cols
        logoId = from.logoId
    }
}
