import Combine
import Foundation

struct MapFileList: CrateModelList, @unchecked Sendable {
    static let name: String = "mapfiles"
    var mapFiles: [MapFile]
    var items: [MapFile] { return mapFiles }

    init(_ items: [MapFile]) {
        mapFiles = items
    }
}

struct MapFileItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "mapfiles"
    var item: MapFile { return mapFile }
    var mapFile: MapFile

    init(_ item: MapFile) {
        mapFile = item
    }
}

class MapFile: CrateModel, @unchecked Sendable {
    static let modelName: String = "mapfiles"

    static func == (lhs: MapFile, rhs: MapFile) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var file: String = ""
    @Published var options: MapFileOptions? = nil
    @Published var map: Map = .init()
    @Published var mapId: String = "" {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .file) {
            file = value
        }

        if let value = try? values.decode(MapFileOptions?.self, forKey: .options) {
            options = value
        }

        if let value = try? values.decode(String.self, forKey: .mapId) {
            mapId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case file
        case options
        case mapId = "map"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(file, forKey: .file)
        try? container.encode(options, forKey: .options)
        try? container.encode(mapId, forKey: .mapId)
    }

    func loadMap() async throws {
        let result = try await Store.instance.maps.getById(id: mapId)

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.map = result
                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadMap())
    }

    func copy(_ from: MapFile) {
        file = from.file
        options = from.options
        mapId = from.mapId
    }
}
