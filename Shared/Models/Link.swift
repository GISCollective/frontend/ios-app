import Combine
import Foundation

class Link: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: Link, rhs: Link) -> Bool {
        if lhs.pageId != rhs.pageId { return false }
        if lhs.url != rhs.url { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(pageId)
        hasher.combine(url)
    }

    var pageId: String?
    var url: String?

    func copy(_ from: Link) {
        pageId = from.pageId
        url = from.url
    }
}
