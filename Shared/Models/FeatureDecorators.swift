import Combine
import Foundation

class FeatureDecorators: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: FeatureDecorators, rhs: FeatureDecorators) -> Bool {
        if lhs.useDefault != rhs.useDefault { return false }
        if lhs.center != rhs.center { return false }
        if lhs.showAsLineAfterZoom != rhs.showAsLineAfterZoom { return false }
        if lhs.showLineMarkers != rhs.showLineMarkers { return false }
        if lhs.keepWhenSmall != rhs.keepWhenSmall { return false }
        if lhs.changeIndex != rhs.changeIndex { return false }
        if lhs.minZoom != rhs.minZoom { return false }
        if lhs.maxZoom != rhs.maxZoom { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(useDefault)
        hasher.combine(center)
        hasher.combine(showAsLineAfterZoom)
        hasher.combine(showLineMarkers)
        hasher.combine(keepWhenSmall)
        hasher.combine(changeIndex)
        hasher.combine(minZoom)
        hasher.combine(maxZoom)
    }

    var useDefault: Bool?
    var center: [Double]?
    var showAsLineAfterZoom: Int32?
    var showLineMarkers: Bool?
    var keepWhenSmall: Bool?
    var changeIndex: UInt?
    var minZoom: UInt32?
    var maxZoom: UInt32?

    func copy(_ from: FeatureDecorators) {
        useDefault = from.useDefault
        center = from.center
        showAsLineAfterZoom = from.showAsLineAfterZoom
        showLineMarkers = from.showLineMarkers
        keepWhenSmall = from.keepWhenSmall
        changeIndex = from.changeIndex
        minZoom = from.minZoom
        maxZoom = from.maxZoom
    }
}
