import Combine
import Foundation

class Visibility: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: Visibility, rhs: Visibility) -> Bool {
        if lhs.isPublic != rhs.isPublic { return false }
        if lhs.isDefault != rhs.isDefault { return false }
        if lhs.team != rhs.team { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(isPublic)
        hasher.combine(isDefault)
        hasher.combine(team)
    }

    private var cancellables = Set<AnyCancellable>()

    @Published var isPublic: Bool = false
    @Published var isDefault: Bool? = nil
    @Published var team: Team = .init()
    @Published var teamId: String = "" {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(Bool.self, forKey: .isPublic) {
            isPublic = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .isDefault) {
            isDefault = value
        }

        if let value = try? values.decode(String.self, forKey: .teamId) {
            teamId = value
        }
    }

    enum CodingKeys: String, CodingKey {
        case isPublic
        case isDefault
        case teamId = "team"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(isPublic, forKey: .isPublic)
        try? container.encode(isDefault, forKey: .isDefault)
        try? container.encode(teamId, forKey: .teamId)
    }

    func loadTeam() async throws {
        let result = try await Store.instance.teams.getById(id: teamId)

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.team = result
                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadTeam())
    }

    func copy(_ from: Visibility) {
        isPublic = from.isPublic
        isDefault = from.isDefault
        teamId = from.teamId
    }
}
