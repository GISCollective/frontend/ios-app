import Combine
import Foundation

class PictureMeta: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: PictureMeta, rhs: PictureMeta) -> Bool {
        if lhs.attributions != rhs.attributions { return false }
        if lhs.renderMode != rhs.renderMode { return false }
        if lhs.link != rhs.link { return false }
        if lhs.data != rhs.data { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(attributions)
        hasher.combine(renderMode)
        hasher.combine(link)
        hasher.combine(data)
    }

    var attributions: String?
    var renderMode: String?
    var link: Json?
    var data: Json?

    func copy(_ from: PictureMeta) {
        attributions = from.attributions
        renderMode = from.renderMode
        link = from.link
        data = from.data
    }
}
