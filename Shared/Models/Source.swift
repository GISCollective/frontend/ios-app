import Combine
import Foundation

class Source: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: Source, rhs: Source) -> Bool {
        if lhs.type != rhs.type { return false }
        if lhs.modelId != rhs.modelId { return false }
        if lhs.remoteId != rhs.remoteId { return false }
        if lhs.syncAt != rhs.syncAt { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(type)
        hasher.combine(modelId)
        hasher.combine(remoteId)
        hasher.combine(syncAt)
    }

    var type: String?
    var modelId: String?
    var remoteId: String?
    var syncAt: Date?

    func copy(_ from: Source) {
        type = from.type
        modelId = from.modelId
        remoteId = from.remoteId
        syncAt = from.syncAt
    }
}
