import Combine
import Foundation

struct PictureList: CrateModelList, @unchecked Sendable {
    static let name: String = "pictures"
    var pictures: [Picture]
    var items: [Picture] { return pictures }

    init(_ items: [Picture]) {
        pictures = items
    }
}

struct PictureItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "pictures"
    var item: Picture { return picture }
    var picture: Picture

    init(_ item: Picture) {
        picture = item
    }
}

class Picture: CrateModel, @unchecked Sendable {
    static let modelName: String = "pictures"

    static func == (lhs: Picture, rhs: Picture) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var owner: String? = nil
    @Published var hash: String? = nil
    @Published var sourceUrl: String? = nil
    @Published var picture: String = ""
    @Published var meta: PictureMeta? = nil

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(String?.self, forKey: .owner) {
            owner = value
        }

        if let value = try? values.decode(String?.self, forKey: .hash) {
            hash = value
        }

        if let value = try? values.decode(String?.self, forKey: .sourceUrl) {
            sourceUrl = value
        }

        if let value = try? values.decode(String.self, forKey: .picture) {
            picture = value
        }

        if let value = try? values.decode(PictureMeta?.self, forKey: .meta) {
            meta = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case owner
        case hash
        case sourceUrl
        case picture
        case meta
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(owner, forKey: .owner)
        try? container.encode(hash, forKey: .hash)
        try? container.encode(sourceUrl, forKey: .sourceUrl)
        try? container.encode(picture, forKey: .picture)
        try? container.encode(meta, forKey: .meta)
    }

    func copy(_ from: Picture) {
        name = from.name
        owner = from.owner
        hash = from.hash
        sourceUrl = from.sourceUrl
        picture = from.picture
        meta = from.meta
    }
}
