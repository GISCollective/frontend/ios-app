import Combine
import Foundation

struct ModelList: CrateModelList, @unchecked Sendable {
    static let name: String = "models"
    var models: [Model]
    var items: [Model] { return models }

    init(_ items: [Model]) {
        models = items
    }
}

struct ModelItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "models"
    var item: Model { return model }
    var model: Model

    init(_ item: Model) {
        model = item
    }
}

class Model: CrateModel, @unchecked Sendable {
    static let modelName: String = "models"

    static func == (lhs: Model, rhs: Model) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var itemCount: UInt = .init()

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(UInt.self, forKey: .itemCount) {
            itemCount = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case itemCount
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(itemCount, forKey: .itemCount)
    }

    func copy(_ from: Model) {
        name = from.name
        itemCount = from.itemCount
    }
}
