import Combine
import Foundation

class GeoJsonGeometry: Hashable, Codable, @unchecked Sendable {
    static func == (_: GeoJsonGeometry, _: GeoJsonGeometry) -> Bool {
        return true
    }

    func hash(into _: inout Hasher) {}

    func copy(_: GeoJsonGeometry) {}
}
