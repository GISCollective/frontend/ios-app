import Combine
import Foundation

class SpaceMenuChildItem: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: SpaceMenuChildItem, rhs: SpaceMenuChildItem) -> Bool {
        if lhs.name != rhs.name { return false }
        if lhs.link != rhs.link { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(link)
    }

    var name: String?
    var link: Link?

    func copy(_ from: SpaceMenuChildItem) {
        name = from.name
        link = from.link
    }
}
