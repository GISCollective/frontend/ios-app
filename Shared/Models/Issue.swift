import Combine
import Foundation

struct IssueList: CrateModelList, @unchecked Sendable {
    static let name: String = "issues"
    var issues: [Issue]
    var items: [Issue] { return issues }

    init(_ items: [Issue]) {
        issues = items
    }
}

struct IssueItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "issues"
    var item: Issue { return issue }
    var issue: Issue

    init(_ item: Issue) {
        issue = item
    }
}

class Issue: CrateModel, @unchecked Sendable {
    static let modelName: String = "issues"

    static func == (lhs: Issue, rhs: Issue) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var feature: String = ""
    @Published var author: String = ""
    @Published var title: String = ""
    @Published var description: String? = nil
    @Published var attributions: String? = nil
    @Published var other: String? = nil
    @Published var assignee: String? = nil
    @Published var processed: Bool? = nil
    @Published var status: String? = nil
    @Published var type: String? = nil
    @Published var creationDate: Date = .init()
    @Published var file: String? = nil
    @Published var resolveDate: Date? = nil

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .feature) {
            feature = value
        }

        if let value = try? values.decode(String.self, forKey: .author) {
            author = value
        }

        if let value = try? values.decode(String.self, forKey: .title) {
            title = value
        }

        if let value = try? values.decode(String?.self, forKey: .description) {
            description = value
        }

        if let value = try? values.decode(String?.self, forKey: .attributions) {
            attributions = value
        }

        if let value = try? values.decode(String?.self, forKey: .other) {
            other = value
        }

        if let value = try? values.decode(String?.self, forKey: .assignee) {
            assignee = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .processed) {
            processed = value
        }

        if let value = try? values.decode(String?.self, forKey: .status) {
            status = value
        }

        if let value = try? values.decode(String?.self, forKey: .type) {
            type = value
        }

        if let value = try? values.decode(Date.self, forKey: .creationDate) {
            creationDate = value
        }

        if let value = try? values.decode(String?.self, forKey: .file) {
            file = value
        }

        if let value = try? values.decode(Date?.self, forKey: .resolveDate) {
            resolveDate = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case feature
        case author
        case title
        case description
        case attributions
        case other
        case assignee
        case processed
        case status
        case type
        case creationDate
        case file
        case resolveDate
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(feature, forKey: .feature)
        try? container.encode(author, forKey: .author)
        try? container.encode(title, forKey: .title)
        try? container.encode(description, forKey: .description)
        try? container.encode(attributions, forKey: .attributions)
        try? container.encode(other, forKey: .other)
        try? container.encode(assignee, forKey: .assignee)
        try? container.encode(processed, forKey: .processed)
        try? container.encode(status, forKey: .status)
        try? container.encode(type, forKey: .type)
        try? container.encode(creationDate, forKey: .creationDate)
        try? container.encode(file, forKey: .file)
        try? container.encode(resolveDate, forKey: .resolveDate)
    }

    func copy(_ from: Issue) {
        feature = from.feature
        author = from.author
        title = from.title
        description = from.description
        attributions = from.attributions
        other = from.other
        assignee = from.assignee
        processed = from.processed
        status = from.status
        type = from.type
        creationDate = from.creationDate
        file = from.file
        resolveDate = from.resolveDate
    }
}
