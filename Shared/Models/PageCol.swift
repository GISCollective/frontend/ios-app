import Combine
import Foundation

class PageCol: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: PageCol, rhs: PageCol) -> Bool {
        if lhs.container != rhs.container { return false }
        if lhs.col != rhs.col { return false }
        if lhs.row != rhs.row { return false }
        if lhs.data != rhs.data { return false }
        if lhs.type != rhs.type { return false }
        if lhs.gid != rhs.gid { return false }
        if lhs.name != rhs.name { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(container)
        hasher.combine(col)
        hasher.combine(row)
        hasher.combine(data)
        hasher.combine(type)
        hasher.combine(gid)
        hasher.combine(name)
    }

    var container: Int32?
    var col: Int32?
    var row: Int32?
    var data: [String: Json]?
    var type: String?
    var gid: String?
    var name: String?

    func copy(_ from: PageCol) {
        container = from.container
        col = from.col
        row = from.row
        data = from.data
        type = from.type
        gid = from.gid
        name = from.name
    }
}
