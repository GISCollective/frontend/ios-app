import Combine
import Foundation

struct IconSetList: CrateModelList, @unchecked Sendable {
    static let name: String = "iconsets"
    var iconSets: [IconSet]
    var items: [IconSet] { return iconSets }

    init(_ items: [IconSet]) {
        iconSets = items
    }
}

struct IconSetItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "iconsets"
    var item: IconSet { return iconSet }
    var iconSet: IconSet

    init(_ item: IconSet) {
        iconSet = item
    }
}

class IconSet: CrateModel, @unchecked Sendable {
    static let modelName: String = "iconsets"

    static func == (lhs: IconSet, rhs: IconSet) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var description: ArticleBody? = nil
    @Published var ver: UInt? = nil
    @Published var visibility: Visibility = .init()
    @Published var styles: IconStyle? = nil
    @Published var source: Source? = nil
    @Published var cover: Picture? = nil
    @Published var coverId: String? = nil {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(ArticleBody?.self, forKey: .description) {
            description = value
        }

        if let value = try? values.decode(UInt?.self, forKey: .ver) {
            ver = value
        }

        if let value = try? values.decode(Visibility.self, forKey: .visibility) {
            visibility = value
        }

        if let value = try? values.decode(IconStyle?.self, forKey: .styles) {
            styles = value
        }

        if let value = try? values.decode(Source?.self, forKey: .source) {
            source = value
        }

        if let value = try? values.decode(String?.self, forKey: .coverId) {
            coverId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case description
        case ver
        case visibility
        case styles
        case source
        case coverId = "cover"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(description, forKey: .description)
        try? container.encode(ver, forKey: .ver)
        try? container.encode(visibility, forKey: .visibility)
        try? container.encode(styles, forKey: .styles)
        try? container.encode(source, forKey: .source)
        try? container.encode(coverId, forKey: .coverId)
    }

    func loadCover() async throws {
        if let coverId = coverId {
            let result = try await Store.instance.pictures.getById(id: coverId)

            let _: Bool = await withUnsafeContinuation { continuation in
                DispatchQueue.main.async {
                    self.cover = result
                    continuation.resume(returning: true)
                }
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadCover())
    }

    func copy(_ from: IconSet) {
        name = from.name
        description = from.description
        ver = from.ver
        visibility = from.visibility
        styles = from.styles
        source = from.source
        coverId = from.coverId
    }
}
