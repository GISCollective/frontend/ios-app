import Combine
import Foundation

class MapLayer: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: MapLayer, rhs: MapLayer) -> Bool {
        if lhs.type != rhs.type { return false }
        if lhs.options != rhs.options { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(type)
        hasher.combine(options)
    }

    var type: String = ""
    var options: [String: String]?

    func copy(_ from: MapLayer) {
        type = from.type
        options = from.options
    }
}
