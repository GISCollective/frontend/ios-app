import Combine
import Foundation

class IconStyle: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: IconStyle, rhs: IconStyle) -> Bool {
        if lhs.site != rhs.site { return false }
        if lhs.siteLabel != rhs.siteLabel { return false }
        if lhs.line != rhs.line { return false }
        if lhs.lineMarker != rhs.lineMarker { return false }
        if lhs.lineLabel != rhs.lineLabel { return false }
        if lhs.polygon != rhs.polygon { return false }
        if lhs.polygonMarker != rhs.polygonMarker { return false }
        if lhs.polygonLabel != rhs.polygonLabel { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(site)
        hasher.combine(siteLabel)
        hasher.combine(line)
        hasher.combine(lineMarker)
        hasher.combine(lineLabel)
        hasher.combine(polygon)
        hasher.combine(polygonMarker)
        hasher.combine(polygonLabel)
    }

    var site: PointStyleProperties?
    var siteLabel: LabelStyleProperties?
    var line: LineStyleProperties?
    var lineMarker: PointStyleProperties?
    var lineLabel: LabelStyleProperties?
    var polygon: PolygonStyleProperties?
    var polygonMarker: PointStyleProperties?
    var polygonLabel: LabelStyleProperties?

    func copy(_ from: IconStyle) {
        site = from.site
        siteLabel = from.siteLabel
        line = from.line
        lineMarker = from.lineMarker
        lineLabel = from.lineLabel
        polygon = from.polygon
        polygonMarker = from.polygonMarker
        polygonLabel = from.polygonLabel
    }
}
