import Combine
import Foundation

struct LegendList: CrateModelList, @unchecked Sendable {
    static let name: String = "legends"
    var legends: [Legend]
    var items: [Legend] { return legends }

    init(_ items: [Legend]) {
        legends = items
    }
}

struct LegendItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "legends"
    var item: Legend { return legend }
    var legend: Legend

    init(_ item: Legend) {
        legend = item
    }
}

class Legend: CrateModel, @unchecked Sendable {
    static let modelName: String = "legends"

    static func == (lhs: Legend, rhs: Legend) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: UInt = .init()
    @Published var name: String = ""
    @Published var count: UInt = .init()
    @Published var subcategories: [LegendSubcategory] = []

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(UInt.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(UInt.self, forKey: .count) {
            count = value
        }

        if let value = try? values.decode([LegendSubcategory].self, forKey: .subcategories) {
            subcategories = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case count
        case subcategories
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(count, forKey: .count)
        try? container.encode(subcategories, forKey: .subcategories)
    }

    func copy(_ from: Legend) {
        name = from.name
        count = from.count
        subcategories = from.subcategories
    }
}
