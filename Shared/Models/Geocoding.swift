import Combine
import Foundation
import GEOSwift

struct GeocodingList: CrateModelList, @unchecked Sendable {
    static let name: String = "geocodings"
    var geocodings: [Geocoding]
    var items: [Geocoding] { return geocodings }

    init(_ items: [Geocoding]) {
        geocodings = items
    }
}

struct GeocodingItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "geocodings"
    var item: Geocoding { return geocoding }
    var geocoding: Geocoding

    init(_ item: Geocoding) {
        geocoding = item
    }
}

class Geocoding: CrateModel, @unchecked Sendable {
    static let modelName: String = "geocodings"

    static func == (lhs: Geocoding, rhs: Geocoding) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var icons: [String] = []
    @Published var score: Double = .init()
    @Published var geometry: Geometry = try! Geometry(wkt: "POINT (0 0)")
    @Published var license: License = .init()

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode([String].self, forKey: .icons) {
            icons = value
        }

        if let value = try? values.decode(Double.self, forKey: .score) {
            score = value
        }

        if let value = try? values.decode(Geometry.self, forKey: .geometry) {
            geometry = value
        }

        if let value = try? values.decode(License.self, forKey: .license) {
            license = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case icons
        case score
        case geometry
        case license
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(icons, forKey: .icons)
        try? container.encode(score, forKey: .score)
        try? container.encode(geometry, forKey: .geometry)
        try? container.encode(license, forKey: .license)
    }

    func copy(_ from: Geocoding) {
        name = from.name
        icons = from.icons
        score = from.score
        geometry = from.geometry
        license = from.license
    }
}
