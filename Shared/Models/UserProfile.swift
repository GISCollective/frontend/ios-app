import Combine
import Foundation

struct UserProfileList: CrateModelList, @unchecked Sendable {
    static let name: String = "userprofiles"
    var userProfiles: [UserProfile]
    var items: [UserProfile] { return userProfiles }

    init(_ items: [UserProfile]) {
        userProfiles = items
    }
}

struct UserProfileItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "userprofiles"
    var item: UserProfile { return userProfile }
    var userProfile: UserProfile

    init(_ item: UserProfile) {
        userProfile = item
    }
}

class UserProfile: CrateModel, @unchecked Sendable {
    static let modelName: String = "userprofiles"

    static func == (lhs: UserProfile, rhs: UserProfile) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var statusEmoji: String = ""
    @Published var statusMessage: String = ""
    @Published var salutation: String? = nil
    @Published var title: String? = nil
    @Published var firstName: String = ""
    @Published var lastName: String = ""
    @Published var skype: String = ""
    @Published var linkedin: String = ""
    @Published var twitter: String = ""
    @Published var website: String = ""
    @Published var jobTitle: String = ""
    @Published var organization: String = ""
    @Published var bio: String = ""
    @Published var canEdit: Bool = false
    @Published var showPrivateContributions: Bool = false
    @Published var showCalendarContributions: Bool? = nil
    @Published var showWelcomePresentation: Bool? = nil
    @Published var location: Location = .init()
    @Published var joinedTime: Date? = nil
    @Published var picture: Picture? = nil
    @Published var pictureId: String? = nil {
        didSet {}
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .statusEmoji) {
            statusEmoji = value
        }

        if let value = try? values.decode(String.self, forKey: .statusMessage) {
            statusMessage = value
        }

        if let value = try? values.decode(String?.self, forKey: .salutation) {
            salutation = value
        }

        if let value = try? values.decode(String?.self, forKey: .title) {
            title = value
        }

        if let value = try? values.decode(String.self, forKey: .firstName) {
            firstName = value
        }

        if let value = try? values.decode(String.self, forKey: .lastName) {
            lastName = value
        }

        if let value = try? values.decode(String.self, forKey: .skype) {
            skype = value
        }

        if let value = try? values.decode(String.self, forKey: .linkedin) {
            linkedin = value
        }

        if let value = try? values.decode(String.self, forKey: .twitter) {
            twitter = value
        }

        if let value = try? values.decode(String.self, forKey: .website) {
            website = value
        }

        if let value = try? values.decode(String.self, forKey: .jobTitle) {
            jobTitle = value
        }

        if let value = try? values.decode(Bool.self, forKey: .canEdit) {
            canEdit = value
        }

        if let value = try? values.decode(String.self, forKey: .organization) {
            organization = value
        }

        if let value = try? values.decode(String.self, forKey: .bio) {
            bio = value
        }

        if let value = try? values.decode(Bool.self, forKey: .showPrivateContributions) {
            showPrivateContributions = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .showCalendarContributions) {
            showCalendarContributions = value
        }

        if let value = try? values.decode(Bool?.self, forKey: .showWelcomePresentation) {
            showWelcomePresentation = value
        }

        if let value = try? values.decode(Location.self, forKey: .location) {
            location = value
        }

        if let value = try? values.decode(Date?.self, forKey: .joinedTime) {
            joinedTime = value
        }

        if let value = try? values.decode(String?.self, forKey: .pictureId) {
            pictureId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case statusEmoji
        case statusMessage
        case salutation
        case title
        case firstName
        case lastName
        case skype
        case linkedin
        case twitter
        case website
        case jobTitle
        case organization
        case bio
        case showPrivateContributions
        case showCalendarContributions
        case showWelcomePresentation
        case location
        case joinedTime
        case pictureId = "picture"
        case canEdit
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(statusEmoji, forKey: .statusEmoji)
        try? container.encode(statusMessage, forKey: .statusMessage)
        try? container.encode(salutation, forKey: .salutation)
        try? container.encode(title, forKey: .title)
        try? container.encode(firstName, forKey: .firstName)
        try? container.encode(lastName, forKey: .lastName)
        try? container.encode(skype, forKey: .skype)
        try? container.encode(linkedin, forKey: .linkedin)
        try? container.encode(twitter, forKey: .twitter)
        try? container.encode(website, forKey: .website)
        try? container.encode(jobTitle, forKey: .jobTitle)
        try? container.encode(organization, forKey: .organization)
        try? container.encode(bio, forKey: .bio)
        try? container.encode(showPrivateContributions, forKey: .showPrivateContributions)
        try? container.encode(showCalendarContributions, forKey: .showCalendarContributions)
        try? container.encode(showWelcomePresentation, forKey: .showWelcomePresentation)
        try? container.encode(location, forKey: .location)
        try? container.encode(joinedTime, forKey: .joinedTime)
        try? container.encode(pictureId, forKey: .pictureId)
    }

    func loadPicture() async throws {
        if let pictureId = pictureId {
            let result = try await Store.instance.pictures.getById(id: pictureId)

            let _: Bool = await withUnsafeContinuation { continuation in
                DispatchQueue.main.async {
                    self.picture = result
                    continuation.resume(returning: true)
                }
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadPicture())
    }

    func copy(_ from: UserProfile) {
        statusEmoji = from.statusEmoji
        statusMessage = from.statusMessage
        salutation = from.salutation
        title = from.title
        firstName = from.firstName
        lastName = from.lastName
        skype = from.skype
        linkedin = from.linkedin
        twitter = from.twitter
        website = from.website
        jobTitle = from.jobTitle
        organization = from.organization
        bio = from.bio
        showPrivateContributions = from.showPrivateContributions
        showCalendarContributions = from.showCalendarContributions
        showWelcomePresentation = from.showWelcomePresentation
        location = from.location
        joinedTime = from.joinedTime
        pictureId = from.pictureId
    }
}
