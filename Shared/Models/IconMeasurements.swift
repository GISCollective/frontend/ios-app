import Combine
import Foundation

class IconMeasurements: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: IconMeasurements, rhs: IconMeasurements) -> Bool {
        if lhs.width != rhs.width { return false }
        if lhs.height != rhs.height { return false }
        if lhs.content != rhs.content { return false }
        if lhs.stretchX != rhs.stretchX { return false }
        if lhs.stretchY != rhs.stretchY { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(width)
        hasher.combine(height)
        hasher.combine(content)
        hasher.combine(stretchX)
        hasher.combine(stretchY)
    }

    var width: UInt32?
    var height: UInt32?
    var content: [Int32]?
    var stretchX: [[Int32]]?
    var stretchY: [[Int32]]?

    func copy(_ from: IconMeasurements) {
        width = from.width
        height = from.height
        content = from.content
        stretchX = from.stretchX
        stretchY = from.stretchY
    }
}
