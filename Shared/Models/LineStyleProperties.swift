import Combine
import Foundation

class LineStyleProperties: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: LineStyleProperties, rhs: LineStyleProperties) -> Bool {
        if lhs.borderColor != rhs.borderColor { return false }
        if lhs.backgroundColor != rhs.backgroundColor { return false }
        if lhs.borderWidth != rhs.borderWidth { return false }
        if lhs.lineDash != rhs.lineDash { return false }
        if lhs.markerInterval != rhs.markerInterval { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(borderColor)
        hasher.combine(backgroundColor)
        hasher.combine(borderWidth)
        hasher.combine(lineDash)
        hasher.combine(markerInterval)
    }

    var borderColor: String?
    var backgroundColor: String?
    var borderWidth: UInt32?
    var lineDash: [UInt32]?
    var markerInterval: UInt32?

    func copy(_ from: LineStyleProperties) {
        borderColor = from.borderColor
        backgroundColor = from.backgroundColor
        borderWidth = from.borderWidth
        lineDash = from.lineDash
        markerInterval = from.markerInterval
    }
}
