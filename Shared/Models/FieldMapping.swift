import Combine
import Foundation

class FieldMapping: Hashable, Codable, @unchecked Sendable {
    static func == (lhs: FieldMapping, rhs: FieldMapping) -> Bool {
        if lhs.key != rhs.key { return false }
        if lhs.destination != rhs.destination { return false }
        if lhs.preview != rhs.preview { return false }

        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(key)
        hasher.combine(destination)
        hasher.combine(preview)
    }

    var key: String?
    var destination: String?
    var preview: [String]?

    func copy(_ from: FieldMapping) {
        key = from.key
        destination = from.destination
        preview = from.preview
    }
}
