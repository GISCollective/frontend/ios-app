import Combine
import Foundation

struct DataBindingList: CrateModelList, @unchecked Sendable {
    static let name: String = "databindings"
    var dataBindings: [DataBinding]
    var items: [DataBinding] { return dataBindings }

    init(_ items: [DataBinding]) {
        dataBindings = items
    }
}

struct DataBindingItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "databindings"
    var item: DataBinding { return dataBinding }
    var dataBinding: DataBinding

    init(_ item: DataBinding) {
        dataBinding = item
    }
}

class DataBinding: CrateModel, @unchecked Sendable {
    static let modelName: String = "databindings"

    static func == (lhs: DataBinding, rhs: DataBinding) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var descriptionTemplate: Json = .init()
    @Published var extraIcons: [String] = []
    @Published var updateBy: String = ""
    @Published var crs: String = ""
    @Published var destination: DataBindingDestination = .init()
    @Published var fields: [FieldMapping] = []
    @Published var analyzedAt: Date? = nil
    @Published var connection: Connection = .init()
    @Published var visibility: Visibility = .init()
    @Published var info: ModelInfo = .init()

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(Json.self, forKey: .descriptionTemplate) {
            descriptionTemplate = value
        }

        if let value = try? values.decode([String].self, forKey: .extraIcons) {
            extraIcons = value
        }

        if let value = try? values.decode(String.self, forKey: .updateBy) {
            updateBy = value
        }

        if let value = try? values.decode(String.self, forKey: .crs) {
            crs = value
        }

        if let value = try? values.decode(DataBindingDestination.self, forKey: .destination) {
            destination = value
        }

        if let value = try? values.decode([FieldMapping].self, forKey: .fields) {
            fields = value
        }

        if let value = try? values.decode(Date?.self, forKey: .analyzedAt) {
            analyzedAt = value
        }

        if let value = try? values.decode(Connection.self, forKey: .connection) {
            connection = value
        }

        if let value = try? values.decode(Visibility.self, forKey: .visibility) {
            visibility = value
        }

        if let value = try? values.decode(ModelInfo.self, forKey: .info) {
            info = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case descriptionTemplate
        case extraIcons
        case updateBy
        case crs
        case destination
        case fields
        case analyzedAt
        case connection
        case visibility
        case info
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(descriptionTemplate, forKey: .descriptionTemplate)
        try? container.encode(extraIcons, forKey: .extraIcons)
        try? container.encode(updateBy, forKey: .updateBy)
        try? container.encode(crs, forKey: .crs)
        try? container.encode(destination, forKey: .destination)
        try? container.encode(fields, forKey: .fields)
        try? container.encode(analyzedAt, forKey: .analyzedAt)
        try? container.encode(connection, forKey: .connection)
        try? container.encode(visibility, forKey: .visibility)
        try? container.encode(info, forKey: .info)
    }

    func copy(_ from: DataBinding) {
        name = from.name
        descriptionTemplate = from.descriptionTemplate
        extraIcons = from.extraIcons
        updateBy = from.updateBy
        crs = from.crs
        destination = from.destination
        fields = from.fields
        analyzedAt = from.analyzedAt
        connection = from.connection
        visibility = from.visibility
        info = from.info
    }
}
