import Combine
import Foundation
import GEOSwift

struct CampaignAnswerList: CrateModelList, @unchecked Sendable {
    static let name: String = "campaignanswers"
    var campaignAnswers: [CampaignAnswer]
    var items: [CampaignAnswer] { return campaignAnswers }

    init(_ items: [CampaignAnswer]) {
        campaignAnswers = items
    }
}

struct CampaignAnswerItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "campaignanswers"
    var item: CampaignAnswer { return campaignAnswer }
    var campaignAnswer: CampaignAnswer

    init(_ item: CampaignAnswer) {
        campaignAnswer = item
    }
}

class CampaignAnswer: CrateModel, @unchecked Sendable {
    static let modelName: String = "campaignanswers"

    static func == (lhs: CampaignAnswer, rhs: CampaignAnswer) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var attributes: [String: AttributeGroup] = [:]
    @Published var status: Int32? = nil
    @Published var featureId: String? = nil
    @Published var info: ModelInfo = .init()
    @Published var position: Geometry? = nil
    @Published var campaign: Campaign = .init()
    @Published var campaignId: String = "" {
        didSet {}
    }

    @Published var icons: [Icon]? = nil
    @Published var iconsId: [String]? = nil {
        didSet {
            observableIcons = []
        }
    }

    @Published var pictures: [Picture]? = nil
    @Published var picturesId: [String]? = nil {
        didSet {
            observablePictures = []
        }
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode([String: AttributeGroup].self, forKey: .attributes) {
            attributes = value
        }

        if let value = try? values.decode(Int32?.self, forKey: .status) {
            status = value
        }

        if let value = try? values.decode(String?.self, forKey: .featureId) {
            featureId = value
        }

        if let value = try? values.decode(ModelInfo.self, forKey: .info) {
            info = value
        }

        if let value = try? values.decode(Geometry?.self, forKey: .position) {
            position = value
        }

        if let value = try? values.decode(String.self, forKey: .campaignId) {
            campaignId = value
        }

        if let value = try? values.decode([String]?.self, forKey: .iconsId) {
            iconsId = value
        }

        if let value = try? values.decode([String]?.self, forKey: .picturesId) {
            picturesId = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case attributes
        case status
        case featureId
        case info
        case position
        case campaignId = "campaign"
        case iconsId = "icons"
        case picturesId = "pictures"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(attributes, forKey: .attributes)
        try? container.encode(status, forKey: .status)
        try? container.encode(featureId, forKey: .featureId)
        try? container.encode(info, forKey: .info)
        try? container.encode(position, forKey: .position)
        try? container.encode(campaignId, forKey: .campaignId)
        try? container.encode(iconsId, forKey: .iconsId)
        try? container.encode(picturesId, forKey: .picturesId)
    }

    func loadCampaign() async throws {
        let result = try await Store.instance.campaigns.getById(id: campaignId)

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.campaign = result
                continuation.resume(returning: true)
            }
        }
    }

    private var observableIcons: [ObservableModel<Icon>] = []
    @Published var firstIcons: Icon?

    func loadIcons() async throws {
        if icons?.count ?? 0 > 0 {
            return
        }

        var result: [Icon] = []
        for id in iconsId ?? [] {
            let item = try await Store.instance.icons.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.icons = []
                self.icons?.append(contentsOf: iresult)
                self.firstIcons = self.icons?.first

                continuation.resume(returning: true)
            }
        }
    }

    private var observablePictures: [ObservableModel<Picture>] = []
    @Published var firstPictures: Picture?

    func loadPictures() async throws {
        if pictures?.count ?? 0 > 0 {
            return
        }

        var result: [Picture] = []
        for id in picturesId ?? [] {
            let item = try await Store.instance.pictures.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.pictures = []
                self.pictures?.append(contentsOf: iresult)
                self.firstPictures = self.pictures?.first

                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await(loadCampaign(), loadIcons(), loadPictures())
    }

    func copy(_ from: CampaignAnswer) {
        attributes = from.attributes
        status = from.status
        featureId = from.featureId
        info = from.info
        position = from.position
        campaignId = from.campaignId
        iconsId = from.iconsId
        picturesId = from.picturesId
    }
}
