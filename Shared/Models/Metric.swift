import Combine
import Foundation

struct MetricList: CrateModelList, @unchecked Sendable {
    static let name: String = "metrics"
    var metrics: [Metric]
    var items: [Metric] { return metrics }

    init(_ items: [Metric]) {
        metrics = items
    }
}

struct MetricItem: CrateModelItem, @unchecked Sendable {
    static let name: String = "metrics"
    var item: Metric { return metric }
    var metric: Metric

    init(_ item: Metric) {
        metric = item
    }
}

class Metric: CrateModel, @unchecked Sendable {
    static let modelName: String = "metrics"

    static func == (lhs: Metric, rhs: Metric) -> Bool {
        return lhs.id == rhs.id
    }

    @Published var id: String = ""
    @Published var name: String = ""
    @Published var type: String = ""
    @Published var reporter: String = ""
    @Published var value: Double = .init()
    @Published var labels: [String: String]? = nil
    @Published var time: Date? = nil

    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            id = value
        }

        if let value = try? values.decode(String.self, forKey: .name) {
            name = value
        }

        if let value = try? values.decode(String.self, forKey: .type) {
            type = value
        }

        if let value = try? values.decode(String.self, forKey: .reporter) {
            reporter = value
        }

        if let value = try? values.decode(Double.self, forKey: .value) {
            self.value = value
        }

        if let value = try? values.decode([String: String]?.self, forKey: .labels) {
            labels = value
        }

        if let value = try? values.decode(Date?.self, forKey: .time) {
            time = value
        }
    }

    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case type
        case reporter
        case value
        case labels
        case time
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(name, forKey: .name)
        try? container.encode(type, forKey: .type)
        try? container.encode(reporter, forKey: .reporter)
        try? container.encode(value, forKey: .value)
        try? container.encode(labels, forKey: .labels)
        try? container.encode(time, forKey: .time)
    }

    func copy(_ from: Metric) {
        name = from.name
        type = from.type
        reporter = from.reporter
        value = from.value
        labels = from.labels
        time = from.time
    }
}
