//
//  FeatureDetail.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 05.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import Combine
import GEOSwift
import MapKit
import SwiftUI

struct FeatureDetail: View {
    @ObservedObject var feature: Feature
    @State var onClose: () -> Void
    @State var name: String = ""
    @State var position = Geometry.point(Point(x: 0, y: 0))
    @State var scrollPadding: CGFloat = 0
    @State var pictureOnTop: Bool = true
    @State var scrollPosition: CGFloat = 0
    @State var shaddowRadius: CGFloat = 0
    @State var originalAuthor: UserProfile?
    @State var icons: [Icon] = []
    @State var attributes: [String: AttributeGroup] = [:]

    @State private var cancellables = Set<AnyCancellable>()

    var pictures: [Picture] {
        return feature.pictures ?? []
    }

    var body: some View {
        ZStack(alignment: .top) {
            if self.pictures.count > 0 {
                TabView {
                    ForEach(pictures) { picture in
                        AsyncPicture(picture: picture)
                    }
                }
                .tabViewStyle(PageTabViewStyle())
                .zIndex(pictureOnTop ? 10 : 0)
                .frame(height: scrollPadding + scrollPosition, alignment: .center)
            }

            GeometryReader { geometry in
                CustomScrollView(offsetChanged: { point in
                    if point.y >= 0 {
                        scrollPosition = point.y
                        pictureOnTop = true
                        shaddowRadius = 0
                    } else {
                        scrollPosition = 0
                        pictureOnTop = false
                        shaddowRadius = max(min(10, 10 - (30 / point.y) * -10), 0)
                    }
                }) {
                    ZStack {
                        VStack(spacing: 0) {
                            VStack(alignment: .leading, spacing: 0) {
                                IconListDetail(icons: $icons, size: 40, spacing: 15)
                                    .padding(.top, 10)

                                EditorJsBlocksView(blockContainer: feature.processedDescription.blockContainer)
                            }
                            .padding(.horizontal)

                            if attributes.count > 0 {
                                FeatureAttributes(attributes: $attributes, icons: $icons)
                                    .padding()
                            }

                            if let originalAuthor = originalAuthor, originalAuthor.firstName.trim() != "", originalAuthor.lastName.trim() != "" {
                                HStack {
                                    UserProfileCard(profile: originalAuthor)
                                    Spacer()
                                }
                                .padding()
                            }

                            Spacer()

                            if feature.isMasked == true {
                                FeatureMaskedGeometryMessage()
                                    .padding()
                            } else {
                                GetDirectionsButtonView(name: $name, position: $position)
                                    .fixedSize(horizontal: false, vertical: true)
                                    .padding()
                            }
                        }
                        .background(Color(UIColor.systemBackground))
                        .padding(.top, scrollPadding)
                    }
                    .frame(minHeight: geometry.size.height)
                }

                .shadow(radius: 0)
            }

            HStack {
                Spacer()
                ButtonClose(action: onClose)
            }
            .padding()
            .zIndex(pictureOnTop ? 10 : 0)
        }
        .edgesIgnoringSafeArea(.bottom)
        .onReceive(feature.$attributes) { value in
            if let value = value {
                attributes = value
            }
        }
        .onReceive(feature.$icons) { value in
            if let value = value {
                icons = value
            }
        }
        .onReceive(feature.$info) { info in
            if let id = info?.originalAuthor, id != "" {
                Task {
                    self.originalAuthor = try? await Store.instance.userProfiles.getById(id: id)
                }
            }
        }
        .task {
            try? await self.feature.loadAll()
            self.name = feature.name
            self.position = feature.position

            if self.feature.pictures?.count ?? 0 > 0 {
                self.scrollPadding = 300
            }
        }
    }
}

struct FeatureDetail_Previews: PreviewProvider {
    static var previews: some View {
        FeatureDetail(feature: featureData[1], onClose: {})
            .environment(\.colorScheme, .dark)
        FeatureDetail(feature: featureData[2], onClose: {})
            .environment(\.colorScheme, .light)
        FeatureDetail(feature: featureData[3], onClose: {})
            .environment(\.colorScheme, .light)
    }
}
