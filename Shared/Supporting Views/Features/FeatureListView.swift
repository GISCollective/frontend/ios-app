//
//  FeatureListView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 06.10.21.
//

import CoreLocation
import GEOSwift
import SwiftUI

struct FeatureListView: View {
    @ObservedObject var locationManager = LocationManager.instance!
    @Binding var mapCenter: CLLocationCoordinate2D?
    @State var refreshList: (() -> Void)?
    @ObservedObject var model: FeatureListData
    @State var content: EditorJsBlockContainer?
    @State var scrollPosition: CGFloat = 0
    @State var mapOnTop: Bool = true
    @State var shaddowRadius: CGFloat = 0
    @State var selectedFeature: Feature?
    @State var hasSelectedFeature: Bool = false

    @State private var map: Map?

    var onDrag: ((_: CLLocationCoordinate2D) -> Void)? = nil

    @Environment(\.verticalSizeClass) var verticalSizeClass: UserInterfaceSizeClass?
    @Environment(\.horizontalSizeClass) var horizontalSizeClass: UserInterfaceSizeClass?

    var columns: [GridItem] {
        if horizontalSizeClass == .compact, verticalSizeClass == .regular {
            return Array(repeating: .init(alignment: .top), count: 1)
        }

        return Array(repeating: .init(.adaptive(minimum: 200), alignment: .top), count: 3)
    }

    func onClose() {
        selectedFeature = nil
        hasSelectedFeature = false
    }

    var body: some View {
        ZStack(alignment: .top) {
            if let refreshList = refreshList {
                LocationRefreshButtonView(refreshList: refreshList, locationManager: locationManager)
                    .zIndex(20)
            }

            FeatureListMapView(map: $map, center: $mapCenter, selectedFeature: $selectedFeature)
                .frame(height: 600)
                .offset(y: -100 + scrollPosition)
                .zIndex(mapOnTop ? 10 : 0)
                .id("FeatureListMap")
                .onAppear {
                    map = model.map
                }

            CustomScrollView(offsetChanged: { point in
                if point.y >= 0 {
                    scrollPosition = point.y
                    mapOnTop = true
                    shaddowRadius = 0
                } else {
                    scrollPosition = 0
                    mapOnTop = false
                    shaddowRadius = max(min(10, 10 - (30 / point.y) * -10), 0)
                }
            }) {
                VStack {
                    if model.data.count > 0 {
                        VStack {
                            if let content = content {
                                EditorJsBlocksView(blockContainer: content)
                                    .padding(.horizontal, 20)
                                    .padding(.top, 30)
                                    .padding(.bottom, 10)
                            } else {
                                Text(model.name)
                                    .font(.largeTitle)
                                    .bold()
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.horizontal, 20)
                                    .padding(.top, 30)
                                    .padding(.bottom, 10)
                                    .id("whats-nearby-header")
                            }

                            LazyVGrid(columns: columns, alignment: .center) {
                                ForEach(self.model.data) { feature in
                                    FeatureRow(feature: feature)
                                        .task {
                                            try? await feature.loadAll()
                                        }
                                        .task {
                                            _ = try? await self.model.loadMoreContentIfNeeded(currentItem: feature)
                                        }
                                        .padding(.bottom, 20)
                                }
                            }
                            .padding(10)
                        }
                        .background(Color(UIColor.systemBackground))
                        .shadow(radius: 0)
                    } else if model.isLoading {
                        Spacer()
                        ProgressView()
                        Spacer()
                    } else {
                        Spacer()
                        Text("There is nothing around")
                        Spacer()
                    }
                }
                .shadow(radius: shaddowRadius)
                .padding(.top, 500)
            }
            .zIndex(1)
        }
        .onChange(of: selectedFeature) { value in
            if value == nil {
                hasSelectedFeature = false
            }

            if value != nil {
                hasSelectedFeature = true
            }
        }
        .onChange(of: hasSelectedFeature) { value in
            if !value {
                selectedFeature = nil
            }
        }
        .task {
            _ = try? await model.reload()
        }
        .sheet(isPresented: $hasSelectedFeature, content: {
            if let feature = self.selectedFeature {
                FeatureDetail(feature: feature, onClose: onClose)
            }
        })
    }
}

//
// struct FeatureListView_Previews: PreviewProvider {
//    static var previews: some View {
//        FeatureListView()
//    }
// }
