//
//  FeatureRow.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 05.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import SwiftUI

struct FeatureRow: View {
    @ObservedObject var feature: Feature
    @State private var showingSheet = false
    @State private var icons: [Icon] = []
    @State private var picture = Picture(id: "")

    var body: some View {
        Button(action: { self.showingSheet = true }) {
            VStack(alignment: .leading) {
                AsyncPicture(picture: picture)
                    .aspectRatio(1.0, contentMode: .fit)
                    .fixedSize(horizontal: false, vertical: true)
                    .cornerRadius(10)
                Text(self.feature.name)
                    .font(.headline)
                    .fontWeight(.regular)
                    .multilineTextAlignment(.leading)
                    .lineLimit(3)
                    .padding(.horizontal, 5.0)
                    .padding(0.0)
                IconListView(icons: $icons, size: 30, spacing: 15)
                    .padding(.horizontal, 0.0)
            }
        }
        .task {
            try? await feature.loadIcons()
            try? await feature.loadPictures()
        }
        .onReceive(feature.$icons) { value in
            if let value = value {
                icons = value
            }
        }
        .onReceive(feature.$firstPictures) { value in
            if let value = value {
                picture = value
            }
        }
        .sheet(isPresented: $showingSheet, content: {
            FeatureDetail(feature: feature, onClose: { showingSheet = false })
        })
    }
}

struct FeatureRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            VStack {
                FeatureRow(feature: featureData[0])
                    .environment(\.colorScheme, .dark)
                FeatureRow(feature: featureData[1])
            }
            .environment(\.sizeCategory, .large)
        }
        .previewLayout(.sizeThatFits)
    }
}
