//
//  AttributeGroupListView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 20.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct AttributeGroupListView: View {
    @Binding var attributes: [String: AttributeGroup]
    @Binding var icon: Icon
    @State var groups: [GroupWithValues] = []

    internal let inspection = Inspection<Self>()

    func createGroups() -> [GroupWithValues] {
        var result: [GroupWithValues] = []
        let groupName = attributes.keys.filter { icon.name == $0 || icon.otherNames?.contains($0) ?? false }.first

        if let groupName = groupName, case let .list(values) = attributes[groupName] {
            let key = icon.name

            for (index, value) in values.enumerated() {
                result.append(GroupWithValues(index: index, attributes: [key: .group(value)]))
            }
        }

        return result
    }

    var body: some View {
        VStack {
            ForEach(groups, id: \.self) { item in
                AttributeGroupView(attributes: Binding(get: {
                    item.attributes
                }, set: { _ in }), icon: $icon, index: item.index)
            }
        }
        .onAppear(perform: {
            groups = self.createGroups()
        })
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct GroupWithValues: Hashable {
    static func == (lhs: GroupWithValues, rhs: GroupWithValues) -> Bool {
        return lhs.index == rhs.index
    }

    var index: Int = 0
    var attributes: [String: AttributeGroup] = [:]

    func hash(into hasher: inout Hasher) {
        hasher.combine(index)
    }
}

struct AttributeGroupListView_Previews: PreviewProvider {
    static var previews: some View {
        AttributeGroupListView(attributes: Binding<[String: AttributeGroup]>(get: {
            [:]
        }, set: { _ in }), icon: Binding<Icon>(get: {
            Icon(id: "")
        }, set: { _ in }))
    }
}
