//
//  AttributeGroupView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 19.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct AttributeGroupView: View {
    @Binding var attributes: [String: AttributeGroup]
    @Binding var icon: Icon
    @State var fields: [FieldWithValue] = []
    @State var index: Int?

    internal let inspection = Inspection<Self>()

    var title: String {
        let iconName = icon.localName ?? icon.name

        if let index = index {
            return "\(index + 1). \(iconName)"
        }

        return iconName
    }

    var body: some View {
        VStack(alignment: .leading) {
            if fields.count > 0 {
                HStack {
                    if let url = URL(string: icon.image.value ?? "") {
                        AsyncImage(url: url)
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 40, height: 40)
                    }

                    CampaignGroupLabel(title: title)
                }
                Divider()

                ForEach(fields, id: \.name) { field in
                    VStack(alignment: .leading) {
                        FieldLabel(label: field.name, isRequired: false)
                        FieldValue(value: field.value)
                    }
                    .padding(.bottom, 5)
                }
            }
        }
        .padding(.bottom, 5)
        .onAppear(perform: {
            let groupName = attributes.keys.filter { icon.name == $0 || icon.otherNames?.contains($0) ?? false }.first

            if let groupName = groupName, case let .group(values) = attributes[groupName] {
                for attribute in icon.attributes {
                    if values.keys.contains(attribute.name) {
                        let name = attribute.displayName ?? attribute.name
                        let key = attribute.name
                        fields.append(FieldWithValue(name: name, value: values[key] ?? .undefined))
                    }
                }
            }
        })
        .onReceive(inspection.notice) {
            self.inspection.visit(self, $0)
        }
    }
}

struct FieldWithValue {
    var name: String
    var value: Attribute
}

struct AttributeGroupView_Previews: PreviewProvider {
    static var previews: some View {
        AttributeGroupView(attributes: Binding<[String: AttributeGroup]>(get: {
            [:]
        }, set: { _ in }), icon: Binding<Icon>(get: {
            Icon(id: "")
        }, set: { _ in }))
    }
}
