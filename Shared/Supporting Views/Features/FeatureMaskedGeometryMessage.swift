//
//  FeatureMaskedGeometryMessage.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 19.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct FeatureMaskedGeometryMessage: View {
    var body: some View {
        Text("Location masking is applied, so the location displayed publicly will be imprecise.")
            .fixedSize(horizontal: false, vertical: true)
            .foregroundColor(.purple)
    }
}

struct FeatureMaskedGeometryMessage_Previews: PreviewProvider {
    static var previews: some View {
        FeatureMaskedGeometryMessage()
    }
}
