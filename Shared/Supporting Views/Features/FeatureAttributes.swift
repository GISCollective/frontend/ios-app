//
//  FeatureAttributes.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 19.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct FeatureAttributes: View {
    @Binding var attributes: [String: AttributeGroup]
    @Binding var icons: [Icon]

    internal let inspection = Inspection<Self>()

    var body: some View {
        VStack(alignment: .leading) {
            ForEach(icons) { icon in
                if icon.allowMany == true {
                    AttributeGroupListView(attributes: $attributes, icon: Binding(get: { icon }, set: { _ in }))
                } else {
                    AttributeGroupView(attributes: $attributes, icon: Binding(get: { icon }, set: { _ in }))
                }
            }
        }
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct FeatureAttributes_Previews: PreviewProvider {
    static var previews: some View {
        FeatureAttributes(attributes: Binding<[String: AttributeGroup]>(get: {
            [:]
        }, set: { _ in }), icons: Binding<[Icon]>(get: { [] }, set: { _ in }))
    }
}
