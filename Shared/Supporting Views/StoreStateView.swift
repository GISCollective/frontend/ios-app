//
//  StoreStateView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 20.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct StoreStateView: View {
    @ObservedObject var campaignAnswerStore: CampaignAnswerStore
    @State var storedItemCount: Int = 0
    @State private var showingAlert = false
    @State private var errorMessage = ""
    @State private var errorTitle = ""

    func sendAll() {
        Task {
            do {
                try await campaignAnswerStore.sendAll()
            } catch let StoreError.runtimeError(message) {
                DispatchQueue.main.async {
                    self.errorTitle = "Runtime Error"
                    self.errorMessage = message
                    self.showingAlert = true
                }
            } catch let StoreError.NotImplemented(message) {
                DispatchQueue.main.async {
                    self.errorTitle = "Sorry!"
                    self.errorMessage = message
                    self.showingAlert = true
                }
            } catch let StoreError.DecoderError(message) {
                DispatchQueue.main.async {
                    self.errorTitle = "Can't parse server response"
                    self.errorMessage = message
                    self.showingAlert = true
                }
            } catch {
                DispatchQueue.main.async {
                    self.errorTitle = "Error"
                    self.errorMessage = error.localizedDescription
                    self.showingAlert = true
                }
            }
        }
    }

    func selectOption(option _: String) {
        sendAll()
    }

    var body: some View {
        if campaignAnswerStore.isSending {
            StoreBusyView(campaignAnswerStore: campaignAnswerStore)
        } else {
            StoreReadyView(campaignAnswerStore: campaignAnswerStore, onSelect: self.selectOption)
                .alert(isPresented: $showingAlert) {
                    Alert(title: Text(errorTitle), message: Text(errorMessage), dismissButton: .default(Text("Got it!")))
                }
        }
    }
}

struct StoreBusyView: View {
    @ObservedObject var campaignAnswerStore: CampaignAnswerStore
    @State var storedItemCount: Int = 0

    var body: some View {
        ZStack(alignment: .center) {
            VStack(alignment: .center) {}
                .frame(width: 45, height: 45)
                .background(Color.gray)
                .clipShape(Circle())

            VStack(alignment: .center) {}
                .frame(width: 39, height: 39)
                .background(Color.white)
                .clipShape(Circle())
                .padding(2.5)

            Circle()
                .trim(from: 0.0, to: CGFloat(min(self.campaignAnswerStore.progress, 1.0)))
                .stroke(style: StrokeStyle(lineWidth: 3.0, lineCap: .round, lineJoin: .round))
                .frame(width: 42, height: 42)
                .foregroundColor(Color.green)
                .rotationEffect(Angle(degrees: 270.0))
                .animation(.linear)

            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: Color.green))
        }
        .onAppear {
            storedItemCount = campaignAnswerStore.storedItemCount
        }
        .onReceive(campaignAnswerStore.$storedItemCount) { value in
            self.storedItemCount = value
        }
    }
}

struct StoreReadyView: View {
    @ObservedObject var campaignAnswerStore: CampaignAnswerStore
    @State var storedItemCount: Int = 0
    @State var onSelect: (_: String) -> Void

    var color: Color {
        return Color.primary
    }

    var body: some View {
        Menu {
            Text("There are \(storedItemCount) answer(s)")

            TaskButton(action: {
                onSelect("send-all")
            }) {
                Text("Send all answers")
            }
        }
            label: {
            ZStack(alignment: .center) {
                VStack(alignment: .center) {}
                    .frame(width: 45, height: 45)
                    .background(Color.gray)
                    .clipShape(Circle())

                VStack(alignment: .center) {
                    Text(String(storedItemCount))
                        .font(.callout)
                        .foregroundColor(.gray)
                }
                .frame(width: 39, height: 39)
                .background(Color.white)
                .clipShape(Circle())
                .padding(2.5)
            }
        }
        .task {
            try? await campaignAnswerStore.updateStoredItemCount()
        }
        .onReceive(campaignAnswerStore.$storedItemCount) { value in
            self.storedItemCount = value
        }
    }
}

struct StoreStateView_Previews: PreviewProvider {
    static var previews: some View {
        HStack {
            StoreStateView(campaignAnswerStore: CampaignAnswerStore.instance)
            StoreBusyView(campaignAnswerStore: CampaignAnswerStore.instance)
        }
    }
}
