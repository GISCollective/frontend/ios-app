import SDWebImageSwiftUI
import SwiftUI

struct IconListView: View {
    @Binding var icons: [Icon]

    var size: Int
    var spacing: Int

    var body: some View {
        LazyVGrid(columns: [
            GridItem(.adaptive(minimum: CGFloat(size))),
        ]) {
            if let icons = icons {
                ForEach(icons.indices, id: \.self) { idx in
                    AsyncIcon(icon: $icons[idx])
                        .scaledToFit()
                        .frame(width: CGFloat(self.size), height: CGFloat(self.size))
                }
            }
        }
    }
}

// struct IconList_Previews: PreviewProvider {
//    static var previews: some View {
//        IconListView(icons: featureData[1].iconUrls, size: 40, spacing: 15)
//            .frame(width: 360, height: 300)
//    }
// }
