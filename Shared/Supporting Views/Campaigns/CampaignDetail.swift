//
//  CampaignDetail.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 31.05.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct CampaignDetail: View {
    private let groupPadding: CGFloat = 40

    @State var campaign: Campaign
    @State var remoteData: CampaignFormData
    @State var onClose: () -> Void

    @StateObject var campaignAnswer = CampaignAnswer(id: "")
    @ObservedObject var locationManager = LocationManager.instance!

    @State var scrollPadding: CGFloat = 500
    @State var pictureOnTop: Bool = true
    @State var scrollPosition: CGFloat = 0
    @State var shaddowRadius: CGFloat = 0
    @State var isValid: Bool = true
    @State var pictures: [PickedMediaItem] = []
    @State private var picture = Picture(id: "")

    private var canShowForm: Bool {
        if campaign.options?.registrationMandatory == false || campaign.options?.registrationMandatory == nil {
            return true
        }

        return UserSessionService.instance.isAuthenticated
    }

    func handleSubmit() async throws {
        remoteData.answer.campaignId = campaign.id
        remoteData.answer.iconsId = campaign.iconsId

        onClose()

        try await CampaignAnswerStore.instance.save(item: AnswerWithPictures(answer: remoteData.answer, pictures: pictures.map { $0.item.value ?? ObservableUIImage.defaultValue.value! }))

        try await CampaignAnswerStore.instance.sendAll()
    }

    var body: some View {
        ZStack(alignment: .top) {
            AsyncPicture(picture: picture)
                .zIndex(pictureOnTop ? 10 : 0)
                .frame(height: scrollPadding + scrollPosition, alignment: .center)

            CustomScrollView(offsetChanged: { point in
                if point.y >= 0 {
                    scrollPosition = point.y
                    pictureOnTop = true
                    shaddowRadius = 0
                } else {
                    scrollPosition = 0
                    pictureOnTop = false
                    shaddowRadius = max(min(10, 10 - (30 / point.y) * -10), 0)
                }
            }) {
                VStack(alignment: .leading) {
                    VStack(alignment: .leading) {
                        EditorJsBlocksView(blockContainer: campaign.article.blockContainer)
                            .fixedSize(horizontal: false, vertical: true)

                        if canShowForm {
                            CampaignForm(remoteData: remoteData, isValid: $isValid, pictures: $pictures)

                            PrimaryButton(action: handleSubmit, label: "Submit")
                                .padding(.top, groupPadding)
                                .opacity(isValid ? 1 : 0.5)
                                .disabled(!isValid)
                        } else {
                            CampaignRegistrationMessage()
                        }

                        Spacer()
                            .padding(.bottom, groupPadding)
                    }
                    .padding()
                }
                .background(Color(UIColor.systemBackground))
                .padding(.top, scrollPadding)
            }

            HStack {
                Spacer()
                ButtonClose(action: onClose)
            }
            .padding()
            .zIndex(pictureOnTop ? 10 : 0)
        }
        .edgesIgnoringSafeArea(.bottom)
        .task {
            try? await campaign.loadCover()
        }
        .onReceive(campaign.$cover) { value in
            self.picture = value
        }
    }
}

struct CampaignDetail_Previews: PreviewProvider {
    static var previews: some View {
        CampaignDetail(campaign: campaignData[0], remoteData: CampaignFormData(campaignData[0])) {}
            .environment(\.colorScheme, .dark)
        CampaignDetail(campaign: campaignData[1], remoteData: CampaignFormData(campaignData[1])) {}
            .environment(\.colorScheme, .light)
    }
}
