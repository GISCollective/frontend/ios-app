//
//  CampaignForm.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 16.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import GEOSwift
import SwiftUI

struct CampaignForm: View {
    private let groupPadding: CGFloat = 40

    @StateObject var remoteData: CampaignFormData
    @State var primaryIcon: Icon?

    @State var selectedIcons = Set<Icon>()
    @ObservedObject var locationManager = LocationManager.instance!
    @Binding var isValid: Bool
    @Binding var pictures: [PickedMediaItem]
    @State var optionalIcons: [Icon] = []

    var hasOptionalIcons: Bool {
        return remoteData.campaign.optionalIcons?.count ?? 0 > 0
    }

    var hasInfoGroup: Bool {
        remoteData.campaign.options?.showNameQuestion ?? false || remoteData.campaign.options?.showDescriptionQuestion ?? false
    }

    var iconsLabel: String {
        let value = remoteData.campaign.options?.iconsLabel ?? ""

        if value.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Choose one or more icons"
        }

        return value
    }

    func handleChange(_ groupName: String, _ index: Int, _ key: String, _ value: Attribute?) {
        remoteData.groupList.updateValue(groupName: groupName, index: index, key: key, value: value ?? .undefined)
        remoteData.answer.attributes = remoteData.groupList.attributes
        isValid = remoteData.groupList.isValid
    }

    func handleDelete(_ icon: Icon, _ index: Int) {
        remoteData.removeGroup(icon: icon, index: index)
        isValid = remoteData.groupList.isValid
    }

    func handlePositionChange(_ position: Geometry) {
        remoteData.answer.position = position
        isValid = remoteData.groupList.isValid
    }

    var body: some View {
        VStack {
            CampaignLocationGroup(answer: $remoteData.answer, onChange: handleChange, onPositionChange: handlePositionChange)
                .padding(.bottom, groupPadding)

            if self.hasInfoGroup {
                CampaignAboutGroup(options: remoteData.campaign.options ?? CampaignOptions(), onChange: handleChange)
                    .padding(.bottom, groupPadding)
            }

            if self.hasOptionalIcons {
                CampaignOptionalIcons(title: self.iconsLabel, viewContent: {
                    VerticalIconList(icons: $selectedIcons)
                }, editContent: {
                    SelectableIconsView(icons: $optionalIcons, multiSelection: $selectedIcons, title: self.iconsLabel) {
                        remoteData.updateSelectedIcons(selectedIcons: Array(selectedIcons))
                    }
                })
                .padding(.bottom, groupPadding)
            }

            CampaignPictures(pictures: $pictures)
                .padding(.bottom, groupPadding)

            ForEach(remoteData.iconGroupList, id: \.id) { group in
                if group.isAddButton {
                    CampaignGroupAdd(action: {
                        remoteData.addIcon(group.icon)
                    }, icon: group.icon)
                        .padding(.bottom, groupPadding)
                } else {
                    CampaignGroupIcon(group: group, onChange: handleChange, onDelete: handleDelete, allowDelete: remoteData.allowDelete(group.icon))
                        .padding(.bottom, groupPadding)
                        .onAppear {
                            isValid = remoteData.groupList.isValid
                        }
                }
            }
        }
        .onReceive(remoteData.campaign.$optionalIcons) { value in
            self.optionalIcons = value ?? []
        }
        .onTapGesture {
            UIApplication.shared.hideKeyboard()
        }
    }
}

struct CampaignGroupAdd: View {
    var action: () -> Void
    @State var icon: Icon

    var body: some View {
        Button(action: action) {
            HStack {
                Image(systemName: "plus")
                Text(icon.localName ?? "")
            }
            .padding()
            .foregroundColor(.gray)
        }
        .border(Color.gray, width: 1)
    }
}

class CampaignFormData: ObservableObject {
    private var cancellableSet = Set<AnyCancellable>()

    @Published var campaign: Campaign
    @Published var answer: CampaignAnswer
    @Published var mandatoryIcons: [Icon] = []
    @Published var selectedIcons: [Icon] = []
    @Published var iconGroupList: [IconGroup] = []

    var groupList = IconGroupList()

    init(_ campaign: Campaign) {
        self.campaign = campaign
        answer = CampaignAnswer(id: "")
        answer.position = LocationManager.instance?.userLocation
        answer.updateValue(groupName: "position details", index: 0, key: "type", value: .string("gps"))
        mandatoryIcons = campaign.icons

        groupList.sync(requiredIcons: mandatoryIcons, optionalIcons: [])
        answer.icons = mandatoryIcons

        mandatoryIcons.filter { $0.allowMany == true }.forEach { icon in
            self.groupList.add(icon: icon, isRequired: true)
        }

        iconGroupList = groupList.list
    }

    func updateSelectedIcons(selectedIcons: [Icon]) {
        self.selectedIcons = selectedIcons

        groupList.sync(requiredIcons: mandatoryIcons, optionalIcons: selectedIcons)
        answer.icons = groupList.icons
        iconGroupList = groupList.list
    }

    func addIcon(_ icon: Icon) {
        let isRequired = mandatoryIcons.first { $0.id == icon.id } != nil

        groupList.add(icon: icon, isRequired: isRequired)
        iconGroupList = groupList.list
    }

    func removeGroup(icon: Icon, index: Int) {
        let name: String = icon.name

        if var list = answer.attributes[name]?.asList(), list.count > index {
            list.remove(at: index)
            answer.attributes[name] = .list(list)
        }

        if answer.attributes[name]?.asGroup() != nil {
            answer.attributes.removeValue(forKey: name)
        }

        groupList.removeGroup(icon: icon, index: index)
        iconGroupList = groupList.list
    }

    func allowDelete(_ icon: Icon) -> Bool {
        return groupList.canDelete(icon: icon)
    }
}

struct CampaignForm_Previews: PreviewProvider {
    static var previews: some View {
        CampaignForm(remoteData: CampaignFormData(campaignData[0]), isValid: Binding<Bool>(get: { false }, set: { _ in }), pictures: Binding(get: { [] }, set: { _ in }))
            .environment(\.colorScheme, .dark)
        CampaignForm(remoteData: CampaignFormData(campaignData[1]), isValid: Binding<Bool>(get: { false }, set: { _ in }), pictures: Binding(get: { [] }, set: { _ in }))
            .environment(\.colorScheme, .light)
    }
}
