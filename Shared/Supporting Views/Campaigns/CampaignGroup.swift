//
//  CampaignSectionView.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 08.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct CampaignGroup<ViewContent: View>: View {
    @State var title: String
    @Binding var allowDelete: Bool
    @State var onDelete: () -> Void = {}

    let viewContent: () -> ViewContent
    var editAction: (() -> Void)? = nil

    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .center) {
                CampaignGroupLabel(title: title)

                if allowDelete {
                    Spacer()
                    Button(action: onDelete) {
                        Image(systemName: "trash.circle")
                            .imageScale(.large)
                    }
                    .accessibilityLabel("Delete group")
                }

                if let editAction = self.editAction {
                    Spacer()
                    Button(action: editAction) {
                        Image(systemName: "pencil.circle")
                            .imageScale(.large)
                    }
                }
            }
            .padding(.horizontal, 5)
            Divider()
            viewContent()
                .padding(.all, 5)
        }
    }
}

struct CampaignGroupLabel: View {
    @State var title: String

    var body: some View {
        Text(title)
            .font(.headline)
            .fixedSize(horizontal: false, vertical: true)
    }
}

struct CampaignSection_Previews: PreviewProvider {
    static var previews: some View {
        CampaignGroup(title: "some title", allowDelete: Binding(get: {
            true
        }, set: { _ in })) {
            Text("View mode")
        }
    }
}
