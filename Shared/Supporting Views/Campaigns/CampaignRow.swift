//
//  CampaignRow.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 28.05.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct CampaignRow: View {
    @ObservedObject var campaign: Campaign
    @State private var showingSheet = false
    @State private var picture = Picture(id: "")

    var body: some View {
        Button(action: { self.showingSheet = true }) {
            VStack(alignment: .leading) {
                AsyncPicture(picture: picture)
                    .aspectRatio(0.7, contentMode: .fit)
                    .cornerRadius(10)
                Text(self.campaign.name)
                    .font(.headline)
                    .fontWeight(.regular)
                    .foregroundColor(.primary)
                    .multilineTextAlignment(.leading)
                    .lineLimit(4)
                    .padding(0.0)
            }
        }
        .task {
            try? await campaign.loadAll()
        }
        .onReceive(campaign.$cover) { value in
            self.picture = value
        }
        .sheet(isPresented: $showingSheet, content: {
            CampaignDetail(campaign: campaign, remoteData: CampaignFormData(campaign), onClose: {
                showingSheet = false
            })
        })
    }
}

struct CampaignRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            VStack {
                CampaignRow(campaign: campaignData[0])
                    .environment(\.colorScheme, .dark)
                CampaignRow(campaign: campaignData[1])
            }
            .environment(\.sizeCategory, .large)
        }
        .previewLayout(.sizeThatFits)
    }
}
