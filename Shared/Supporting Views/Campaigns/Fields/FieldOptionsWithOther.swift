//
//  FieldOptionsWithOther.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 17.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct FieldOptionsWithOther: View {
    @Binding var value: Attribute
    @State var options: String
    @State var onChange: (_ value: Attribute) -> Void

    internal let inspection = Inspection<Self>()

    private var optionsWithOther: String {
        return "\(options),other"
    }

    private var isOther: Bool {
        return checkOther(value: value)
    }

    private var selectorValue: Binding<Attribute> {
        Binding<Attribute>(
            get: {
                if self.isOther {
                    return .string("other")
                }

                return self.value
            },
            set: {
                self.value = $0
            }
        )
    }

    private var otherValue: Binding<Attribute> {
        Binding<Attribute>(
            get: {
                if case let .string(value) = value {
                    if value == "other" {
                        return .string("")
                    }
                }

                return self.value
            },
            set: {
                self.value = $0
            }
        )
    }

    func checkOther(value: Attribute) -> Bool {
        if case let .string(value) = value {
            if value == "other" || !optionsWithOther.split(whereSeparator: { $0 == "," }).map(String.init).contains(value) {
                return true
            }
        }

        return false
    }

    var body: some View {
        VStack(alignment: .leading) {
            FieldOptions(value: selectorValue, options: optionsWithOther) { newValue in
                if !self.checkOther(value: newValue) {
                    self.onChange(newValue)
                }
            }

            if isOther {
                Text("Please specify if you selected “Other”.")
                    .fixedSize(horizontal: false, vertical: true)
                    .foregroundColor(Color.green)
                    .font(.body.bold())
                    .padding(.bottom, 10)

                FieldText(value: otherValue, onChange: self.onChange)
            }
        }
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct FieldOptionsWithOther_Previews: PreviewProvider {
    static var previews: some View {
        FieldOptionsWithOther(value: Binding(get: { .string("a") }, set: { _ in }), options: "a,b,c") { _ in }
    }
}
