//
//  FieldView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 17.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct FieldBoolean: View {
    @Binding var value: Attribute
    @State var onChange: (_ value: Attribute) -> Void

    var isTrue: Bool {
        if case let .bool(value) = value {
            return value == true
        }

        return false
    }

    var isFalse: Bool {
        if case let .bool(value) = value {
            return value == false
        }

        return false
    }

    var isUnknown: Bool {
        if case .undefined = value {
            return true
        }

        return false
    }

    func setValue(_ newValue: Attribute) {
        value = newValue
        onChange(newValue)
    }

    var body: some View {
        HStack(spacing: 0) {
            Button("Yes", action: { setValue(.bool(true)) })
                .padding(5)
                .fixedSize(horizontal: false, vertical: true)
                .frame(maxWidth: .infinity)
                .accentColor(isTrue ? .white : .accentColor)
                .background(isTrue ? .accentColor : Color(red: 0, green: 0, blue: 0, opacity: 0))

            Button("No", action: { setValue(.bool(false)) })
                .padding(5)
                .fixedSize(horizontal: false, vertical: true)
                .frame(maxWidth: .infinity)
                .accentColor(isFalse ? .white : .accentColor)
                .background(isFalse ? .accentColor : Color(red: 0, green: 0, blue: 0, opacity: 0))

            Button("Unknown", action: { setValue(.undefined) })
                .padding(5)
                .fixedSize(horizontal: false, vertical: true)
                .frame(maxWidth: .infinity)
                .accentColor(isUnknown ? .white : .accentColor)
                .background(isUnknown ? .accentColor : Color(red: 0, green: 0, blue: 0, opacity: 0))
        }
    }
}

struct FieldBoolean_Previews: PreviewProvider {
    static var previews: some View {
        FieldBoolean(value: Binding(get: {
            .bool(true)
        }, set: { _ in
        })) { _ in }
    }
}
