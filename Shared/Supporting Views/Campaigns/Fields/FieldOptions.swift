//
//  FieldOptions.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 16.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct FieldOptionsItem: Identifiable {
    let id: String
    let value: String

    init(value: String) {
        id = value
        self.value = value
    }
}

struct FieldOptions: View {
    @Binding var value: Attribute
    @State var options: String
    @State var isOpen: Bool = false
    @State var onChange: (_ value: Attribute) -> Void

    internal let inspection = Inspection<Self>()

    private var optionList: [FieldOptionsItem] {
        options.split { $0 == "," }.map(String.init).map { FieldOptionsItem(value: $0) }
    }

    private var text: String {
        if case let .string(value) = value {
            return value.trimmingCharacters(in: .whitespacesAndNewlines)
        }

        return ""
    }

    private var hasValue: Bool {
        if case .string = value {
            return true
        }

        return false
    }

    func select(_ value: String) {
        isOpen = false

        let newValue: Attribute = .string(value)
        self.value = newValue
        onChange(newValue)
    }

    var body: some View {
        VStack(alignment: .leading) {
            if hasValue {
                HStack(alignment: .top) {
                    Text(text)
                        .fixedSize(horizontal: false, vertical: true)
                    Spacer()
                    Button("change", action: {
                        self.isOpen.toggle()
                    })
                }
            } else {
                Button("Select a value from the list", action: {
                    self.isOpen.toggle()
                })
            }

            if isOpen {
                ForEach(optionList) { option in
                    Divider()
                    Button(action: {
                        self.select(option.value)
                    }) {
                        Text(option.value)
                            .multilineTextAlignment(.center)
                            .fixedSize(horizontal: false, vertical: true)
                            .frame(maxWidth: .infinity)
                    }
                    .fixedSize(horizontal: false, vertical: true)
                }
            }
        }
        .padding(.vertical, 10)
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct FieldOptions_Previews: PreviewProvider {
    static var previews: some View {
        FieldOptions(value: Binding(get: { .string("a") }, set: { _ in }), options: "a,b,c") { _ in }
    }
}
