//
//  CampaignField.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 16.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct CampaignField: View {
    @State var name: String
    @State var index: Int
    @State var attributeWithValue: AttributeWithValue
    @State var value: Attribute = .undefined
    @State var onChange: (_ name: String, _ index: Int, _ key: String, _ value: Attribute?) -> Void

    internal let inspection = Inspection<Self>()

    var displayName: String {
        if attributeWithValue.attribute.displayName?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || attributeWithValue.attribute.displayName == nil {
            return attributeWithValue.attribute.name
        }

        return attributeWithValue.attribute.displayName ?? ""
    }

    var help: String? {
        if attributeWithValue.attribute.help?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || attributeWithValue.attribute.help == nil {
            return nil
        }

        return attributeWithValue.attribute.help ?? ""
    }

    var key: String {
        return attributeWithValue.attribute.name
    }

    func handleChange(_ value: Attribute) {
        onChange(name, index, key, value)
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            VStack(alignment: .leading, spacing: 0) {
                FieldLabel(label: self.displayName, isRequired: attributeWithValue.attribute.isRequired ?? false)

                if let help = self.help {
                    Text(help)
                        .fixedSize(horizontal: false, vertical: true)
                        .foregroundColor(Color.gray)
                        .font(.body)
                }
            }
            .padding(.top, 5)
            .padding(.bottom, 5)
            .onChange(of: value, perform: { value in
                self.onChange(self.name, self.index, self.key, value)
            })

            if attributeWithValue.attribute.type == "options" {
                FieldOptions(value: $value, options: attributeWithValue.attribute.options ?? "", onChange: handleChange)
            } else if attributeWithValue.attribute.type == "options with other" {
                FieldOptionsWithOther(value: $value, options: attributeWithValue.attribute.options ?? "", onChange: handleChange)
            } else if attributeWithValue.attribute.type == "integer" {
                FieldInteger(value: $value, onChange: handleChange)
            } else if attributeWithValue.attribute.type == "boolean" {
                FieldBoolean(value: $value, onChange: handleChange)
            } else if attributeWithValue.attribute.type == "decimal" {
                FieldDecimal(value: $value, onChange: handleChange)
            } else if attributeWithValue.attribute.type == "long text" {
                FieldLongText(value: $value, onChange: handleChange)
            } else {
                FieldText(value: $value, onChange: handleChange)
            }
        }
        .onAppear {
            self.value = attributeWithValue.value
        }
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct CampaignField_Previews: PreviewProvider {
    static var previews: some View {
        VStack(alignment: .leading, spacing: 0) {
            Divider()
//            CampaignField(name: "_", index: 0, attributeWithValue: AttributeWithValue(attribute: IconAttribute(name: "test", type: "text", options: "", isPrivate: false, isRequired: false, displayName: "test", help: "help", isInherited: false)), onChange: { _, _, _, _ in })
            Divider()
        }
    }
}
