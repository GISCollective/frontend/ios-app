//
//  FieldDecimal.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 17.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import SwiftUI

struct FieldDecimal: View {
    @Binding var value: Attribute
    @State var onChange: (_ value: Attribute) -> Void
    @State private var text: String = ""

    var body: some View {
        TextField("", text: $text, onCommit: {
            DispatchQueue.main.async {
                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
            }
        })
        .keyboardType(.decimalPad)
        .onChange(of: text, perform: { value in
            let filtered = value.replacingOccurrences(of: ",", with: ".").filter { "0123456789.".contains($0) }
            if filtered != value {
                self.text = value
            }

            let attribute: Attribute = .double(Double(filtered) ?? 0)
            self.value = attribute
            self.onChange(attribute)
        })
        .onAppear {
            if case let .number(value) = value {
                text = "\(value)"
            }

            if case let .double(value) = value {
                text = "\(value)"
            }
        }
    }
}

struct FieldDecimal_Previews: PreviewProvider {
    static var previews: some View {
        FieldDecimal(value: Binding(get: {
            .string("2")
        }, set: { _ in
        })) { _ in }
            .background(Color.red)
    }
}
