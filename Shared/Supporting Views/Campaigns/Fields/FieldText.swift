//
//  FieldText.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 16.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct FieldText: View {
    @Binding var value: Attribute
    @State var onChange: (_ value: Attribute) -> Void
    @State private var text: String = ""

    internal let inspection = Inspection<Self>()

    var body: some View {
        TextField("", text: $text, onCommit: {
            DispatchQueue.main.async {
                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
            }
        })
        .onChange(of: text, perform: { value in
            self.value = .string(value)
            self.onChange(.string(value))
        })
        .onAppear {
            if case let .string(value) = value {
                text = "\(value)"
            }
        }
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct FieldText_Previews: PreviewProvider {
    static var previews: some View {
        FieldText(value: Binding(get: {
            .undefined
        }, set: { _ in
        }), onChange: { _ in })
    }
}
