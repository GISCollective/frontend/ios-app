//
//  FieldLongText.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 17.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct FieldLongText: View {
    @Binding var value: Attribute
    @State var onChange: (_ value: Attribute) -> Void
    @State private var text: String = ""

    var body: some View {
        ZStack(alignment: .topLeading) {
            TextEditor(text: $text)
                .frame(minHeight: 30, alignment: .leading)
                .onChange(of: text, perform: { value in
                    self.value = .string(value)
                    self.onChange(.string(value))
                })

            Text(text)
                .fixedSize(horizontal: false, vertical: true)
                .padding(.all, 8)
                .opacity(0)
        }
        .onAppear {
            if case let .string(value) = value {
                text = "\(value)"
            }
        }
    }
}

struct FieldLongText_Previews: PreviewProvider {
    static var previews: some View {
        FieldLongText(value: Binding(get: {
            .undefined
        }, set: { _ in
        }), onChange: { _ in })
    }
}
