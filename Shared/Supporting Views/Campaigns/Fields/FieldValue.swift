//
//  FieldValue.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 19.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct FieldValue: View {
    @State var value: Attribute

    var strValue: String {
        if case let .string(value) = value {
            return value
        }

        if case let .bool(value) = value {
            return value ? "Yes" : "No"
        }

        if case let .double(value) = value {
            return String(value)
        }

        if case let .number(value) = value {
            return String(value)
        }

        return "unknown"
    }

    var body: some View {
        Text(strValue)
    }
}

struct FieldValue_Previews: PreviewProvider {
    static var previews: some View {
        FieldValue(value: .string("value"))
    }
}
