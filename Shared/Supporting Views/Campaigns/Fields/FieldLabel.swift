//
//  FieldLabel.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 20.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct FieldLabel: View {
    @State var label: String
    @State var isRequired: Bool

    var body: some View {
        HStack(alignment: .firstTextBaseline) {
            Text(label)
                .fixedSize(horizontal: false, vertical: true)
                .foregroundColor(Color.green)
                .font(.body.bold())

            if isRequired {
                Text("*")
                    .foregroundColor(Color.red)
                    .font(.body.bold())
            }
        }
    }
}

struct FieldLabel_Previews: PreviewProvider {
    static var previews: some View {
        FieldLabel(label: "label", isRequired: false)
        FieldLabel(label: "requiredlabel", isRequired: true)
    }
}
