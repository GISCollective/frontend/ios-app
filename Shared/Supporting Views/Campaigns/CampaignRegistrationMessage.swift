//
//  CampaignRegistrationMessage.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 15.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct CampaignRegistrationMessage: View {
    var body: some View {
        VStack {
            VStack(alignment: .leading) {
                Text("Authentication required")
                    .font(.title)
                Text("In order to contribute to this campaign you need to sign in to your account.")
                    .fixedSize(horizontal: false, vertical: true)
            }
            .padding()
        }
        .background(Color.yellow.opacity(0.4))
        .cornerRadius(10)
    }
}

struct CampaignRegistrationMessage_Previews: PreviewProvider {
    static var previews: some View {
        CampaignRegistrationMessage()
    }
}
