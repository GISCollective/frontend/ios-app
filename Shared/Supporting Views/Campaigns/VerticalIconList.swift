//
//  SelectableIconList.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 06.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct VerticalIconList: View {
    @Binding var icons: Set<Icon>

    var body: some View {
        if icons.count > 0 {
            VStack(alignment: .leading) {
                ForEach(Array(icons)) { icon in
                    IconRow(icon: icon)
                }
            }
        }
    }
}

struct IconRow: View {
    @State var icon: Icon

    var body: some View {
        HStack {
            if let url = URL(string: icon.image.value ?? "") {
                AsyncImage(url: url)
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 40, height: 40)
            }
            Text(icon.name)
        }
    }
}
