//
//  SelectableIconList.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 06.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct SelectableIconsView: View {
    @Binding var icons: [Icon]
    @Binding var multiSelection: Set<Icon>
    @State var title: String
    @State var onDone: () -> Void

    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                HStack(alignment: .center) {
                    Text(title)
                        .font(.headline)
                        .padding()
                    Spacer()
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                        self.onDone()
                    }) {
                        Text("Done")
                            .padding()
                    }
                }
                ForEach(icons) { icon in
                    Divider()
                    SelectableIconRow(icon: icon, multiSelection: $multiSelection)
                }
            }
        }
    }
}

struct SelectableIconRow: View {
    @ObservedObject var icon: Icon
    @Binding var multiSelection: Set<Icon>
    @State private var isSelected: Bool = false

    private func setInitialSelection() {
        if multiSelection.contains(icon) {
            isSelected = true
        }
    }

    private func updateSelection() {
        isSelected.toggle()
        if isSelected {
            multiSelection.insert(icon)
        } else {
            multiSelection.remove(icon)
        }
    }

    var checkboxImageName: String {
        if isSelected {
            return "checkmark.circle.fill"
        }
        return "circle"
    }

    var checkboxImageColor: Color {
        if isSelected {
            return Color(.systemBlue)
        }
        return Color(.systemGray)
    }

    var body: some View {
        HStack {
            Image(systemName: checkboxImageName)
                .imageScale(.large)
                .padding(.horizontal, 10)
                .foregroundColor(checkboxImageColor)
            if let url = URL(string: icon.image.value ?? "") {
                AsyncImage(url: url)
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 40, height: 40)
            }
            Text(icon.localName ?? "")
            Spacer()
        }.onAppear {
            setInitialSelection()
        }
        .onTapGesture {
            updateSelection()
        }
    }
}

struct SelectableIconsView_Previews: PreviewProvider {
    @State static var multiSelection = Set<Icon>()

    static var previews: some View {
        SelectableIconsView(icons: Binding(get: { [] }, set: { _ in }), multiSelection: $multiSelection, title: "Select some icons") {}
    }
}
