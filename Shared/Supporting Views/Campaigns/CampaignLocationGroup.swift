//
//  CampaignLocationGroup.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 06.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import GEOSwift
import MapKit
import SwiftUI

struct CampaignLocationGroup: View {
    private static var defaultPosition: Geometry = try! Geometry(wkt: "POINT (0 0)")

    @ObservedObject var locationManager = LocationManager.instance!

    @Binding var answer: CampaignAnswer

    @State var onChange: (_ group: String, _ index: Int, _ key: String, _ value: Attribute?) -> Void
    @State var onPositionChange: (_ position: Geometry) -> Void

    @State internal var showingSheet = false
    @State private var position: Geometry = CampaignLocationGroup.defaultPosition

    @State private var currentLocation = CampaignLocationMarker(coordinate: CampaignLocationGroup.defaultPosition.centroidCoordinate())
    @State private var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 0, longitude: 0),
        latitudinalMeters: 750,
        longitudinalMeters: 750
    )
    @State private var allowDelete = false

    internal let inspection = Inspection<Self>()

    func updateRegion(_ newValue: Geometry) {
        position = newValue
        currentLocation = CampaignLocationMarker(coordinate: newValue.centroidCoordinate())
        region = MKCoordinateRegion(
            center: newValue.centroidCoordinate(),
            latitudinalMeters: 750,
            longitudinalMeters: 750
        )
    }

    var usingGps: Bool {
        if case let .string(value) = answer.attributes["position details"]?["type"], value == "gps" {
            return true
        }

        return false
    }

    var editor: CampaignLocationEditor {
        CampaignLocationEditor(showingSheet: $showingSheet, geometry: position, usingGps: usingGps) { key, value in
            self.onChange("position details", 0, key, value)
        } onPositionChange: { value in
            self.updateRegion(value)
            self.onPositionChange(value)
        } onUseCurrentLocation: {
            self.updateRegion(locationManager.userLocation ?? CampaignLocationGroup.defaultPosition)
            self.onChange("position details", 0, "type", .string("gps"))
        }
    }

    var body: some View {
        CampaignGroup(title: "Location", allowDelete: $allowDelete, viewContent: {
            MapKit.Map(coordinateRegion: $region, showsUserLocation: true, annotationItems: [currentLocation]) { marker in
                MapMarker(coordinate: marker.coordinate)
            }
            .frame(height: 200)
        }, editAction: {
            self.showingSheet = true
        })
        .onAppear {
            self.updateRegion(answer.position ?? CampaignLocationGroup.defaultPosition)
        }
        .sheet(isPresented: $showingSheet, content: { self.editor })
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct CampaignLocationEditor: View {
    @Binding var showingSheet: Bool
    @State var geometry: Geometry
    @State var usingGps: Bool
    @State var onChange: (_ key: String, _ value: Attribute?) -> Void
    @State var onPositionChange: (_ value: Geometry) -> Void
    @State var onUseCurrentLocation: () -> Void

    internal let inspection = Inspection<Self>()

    @State private var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 0, longitude: 0),
        latitudinalMeters: 750,
        longitudinalMeters: 750
    )

    var body: some View {
        VStack(alignment: .center) {
            Text("Location")
                .font(.headline)
                .padding()

            ZStack(alignment: .bottom) {
                ZStack(alignment: .center) {
                    GeometryMapView(geometry: self.geometry, icon: nil)
                    MapKit.Map(coordinateRegion: $region, showsUserLocation: true)

                    Image(systemName: "circle")
                        .font(.system(size: 50.0, weight: .thin))
                        .foregroundColor(.black)

                    Image(systemName: "cross.fill")
                        .font(.system(size: 10, weight: .thin))
                        .foregroundColor(.black)
                }

                if !usingGps {
                    VStack {
                        Button("Use current location", action: {
                            self.showingSheet = false
                            self.onUseCurrentLocation()
                        })
                        .foregroundColor(.white)
                        .padding(10)
                        .background(Color.accentColor)
                        .cornerRadius(8)
                    }.padding()
                }
            }

            HStack {
                Button("Select point", action: {
                    self.showingSheet = false
                    self.onChange("type", .string("manual"))
                    self.onChange("accuracy", .undefined)
                    self.onChange("altitudeAccuracy", .undefined)
                    self.onChange("altitude", .undefined)

                    let center = region.center
                    let point = Point(x: center.longitude, y: center.latitude).geometry
                    self.onPositionChange(point)
                })
                .padding()
                .padding(.bottom, 0)

                Spacer()

                Button("Cancel", action: {
                    self.showingSheet = false
                })
                .padding()
                .padding(.bottom, 0)
            }
        }
        .onAppear {
            region.center = geometry.centroidCoordinate()
        }
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct CampaignLocationMarker: Identifiable {
    let id = UUID()
    var coordinate: CLLocationCoordinate2D
}

struct CampaignLocationGroup_Previews: PreviewProvider {
    static var previews: some View {
        CampaignLocationGroup(answer: Binding<CampaignAnswer>(get: {
            CampaignAnswer(id: "")
        }, set: { _ in }), onChange: { _, _, _, _ in }, onPositionChange: { _ in })
    }
}
