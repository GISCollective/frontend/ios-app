//
//  CampaignSectionView.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 08.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct CampaignOptionalIcons<ViewContent: View, EditContent: View>: View {
    @State var title: String
    let viewContent: () -> ViewContent
    let editContent: () -> EditContent
    @State private var showingSheet = false
    @State var allowDelete = false

    var body: some View {
        VStack(alignment: .leading) {
            CampaignGroupLabel(title: "Icons")

            Divider()

            Text(title)
                .fixedSize(horizontal: false, vertical: true)
                .foregroundColor(Color.green)
                .font(.body.bold())
                .padding(.top, 5)

            viewContent()
                .padding(.bottom, 5)

            Button(action: {
                showingSheet.toggle()
            }) {
                HStack {
                    Image(systemName: "pencil")
                    Text("Select icons")
                }
                .frame(maxWidth: .infinity)
                .padding()
                .foregroundColor(.gray)
            }
            .border(Color.gray, width: 1)
            .sheet(isPresented: $showingSheet, content: editContent)
            Divider()
        }
    }
}

struct CampaignOptionalIconsView_Previews: PreviewProvider {
    static var previews: some View {
        CampaignOptionalIcons(title: "some title", viewContent: {
            Text("View mode")
        }, editContent: {
            Text("Edit mode")
        })
    }
}
