//
//  CampaignInfoGroup.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 20.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct CampaignAboutGroup: View {
    @State var options: CampaignOptions
    @State var nameAttribute: Attribute = .undefined
    @State var descriptionAttribute: Attribute = .undefined
    @State var onChange: (_ group: String, _ index: Int, _ key: String, _ value: Attribute?) -> Void
    @State var allowDelete = false

    var nameLabel: String {
        let value = options.nameLabel

        if value.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "What's the name of this place?"
        }

        return value
    }

    var descriptionLabel: String {
        let value = options.descriptionLabel

        if value.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "What do you like about this place?"
        }

        return value
    }

    var body: some View {
        CampaignGroup(title: "About", allowDelete: $allowDelete, viewContent: {
            VStack(alignment: .leading) {
                if options.showNameQuestion == true {
                    FieldLabel(label: self.nameLabel, isRequired: false)
                    FieldText(value: $nameAttribute, onChange: { value in self.onChange("about", 0, "name", value) })
                    Divider()
                }

                if options.showDescriptionQuestion == true {
                    FieldLabel(label: self.descriptionLabel, isRequired: false)
                    FieldLongText(value: $descriptionAttribute, onChange: { value in self.onChange("about", 0, "description", value) })
                    Divider()
                }
            }
        })
    }
}

struct CampaignAboutGroup_Previews: PreviewProvider {
    static var previews: some View {
        CampaignAboutGroup(options: CampaignOptions()) { _, _, _, _ in }
    }
}
