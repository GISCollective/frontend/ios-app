//
//  CampaignGroupIcon.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 16.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct CampaignGroupIcon: View {
    @ObservedObject var group: IconGroup
    @State var onChange: (_ group: String, _ index: Int, _ key: String, _ value: Attribute?) -> Void
    @State var onDelete: (_ icon: Icon, _ index: Int) -> Void
    @State var allowDelete: Bool

    internal let inspection = Inspection<Self>()

    var title: String {
        let name = group.icon.localName ?? ""

        if !group.hasMany {
            return name
        }

        return "\(group.index + 1).\(name)"
    }

    func handleDelete() {
        onDelete(group.icon, group.index)
    }

    var body: some View {
        CampaignGroup(title: self.title, allowDelete: $allowDelete, onDelete: handleDelete) {
            VStack(alignment: .leading, spacing: 0) {
                ForEach(group.attributesWithValues, id: \.self) { pair in
                    CampaignField(name: group.icon.name, index: group.index, attributeWithValue: pair, onChange: onChange)
                        .padding(.top, 5)
                    Divider()
                }
            }
        }
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct CampaignGroupIcon_Previews: PreviewProvider {
    static var previews: some View {
        CampaignGroupIcon(group: IconGroup(icon: iconData[0], index: 0, isAddButton: false), onChange: { _, _, _, _ in
        }, onDelete: { _, _ in }, allowDelete: true)
    }
}
