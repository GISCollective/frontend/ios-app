//
//  CampaignPictures.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 27.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct CampaignPictures: View {
    enum SheetType: String, Identifiable {
        case imagePicker, camera
        var id: String {
            return rawValue
        }
    }

    let pictureSize: CGFloat = 100

    @State private var image = UIImage()
    @State private var sheetType: SheetType?
    @Binding var pictures: [PickedMediaItem]

    var body: some View {
        VStack(alignment: .leading) {
            CampaignGroupLabel(title: "Pictures")

            if pictures.count > 0 {
                Divider()
                LazyVGrid(columns: [
                    GridItem(.adaptive(minimum: CGFloat(pictureSize))),
                ]) {
                    ForEach(pictures, id: \.id) { picture in
                        Menu {
                            Button(action: {
                                pictures = pictures.filter { $0.id != picture.id }
                            }, label: {
                                Label("Delete", systemImage: "trash.fill")
                            })
                        }
                            label: {
                            Label {} icon: {
                                AsyncImageBis(image: picture.item)
                                    .frame(width: pictureSize, height: pictureSize)
                                    .aspectRatio(1.0, contentMode: .fill)
                                    .fixedSize(horizontal: true, vertical: true)
                            }
                        }
                    }
                }
            }

            HStack(alignment: .center, spacing: 0) {
                Menu {
                    Button("Photo Library", action: {
                        self.sheetType = .imagePicker
                    })
                    Button("Take Photo", action: {
                        self.sheetType = .camera
                    })
                }
                    label: {
                    Label {}
                            icon: {
                            HStack {
                                Image(systemName: "plus")
                                Text(pictures.count > 0 ? "Add more" : "Add pictures")
                            }
                            .frame(maxWidth: .infinity)
                            .padding()
                            .foregroundColor(.gray)
                        }
                }
                .sheet(item: $sheetType, content: { type in
                    switch type {
                    case .imagePicker:
                        PhotoLibraryView(pictures: $pictures) { _ in
                            sheetType = nil
                        }
                    case .camera:
                        CameraView(pictures: $pictures)
                            .ignoresSafeArea()
                    }
                })
            }
            .frame(maxWidth: .infinity)
            .border(Color.gray, width: 1)
            .padding(.horizontal, 5)

            Divider()
        }
    }
}
