//
//  RefreshableList.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 28.11.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import SwiftUI

@available(iOS 13.0, macOS 10.15, *)
public class RefreshData: ObservableObject {
    var spinnerSize: CGFloat = 40

    @Published var isLoading: Bool
    @Published var opacity: Double
    @Published var showText: String
    @Published var showRefreshView: Bool
    @Published var percentage: CGFloat
    @Published var showDone: Bool
    var action: ((_: @escaping () -> Void) -> Void)?

    func startPull() {
        if !isLoading {
            showDone = false
            showText = "Pull to refresh"
        }
    }

    func startAction() {
        if action == nil || isLoading {
            return
        }

        isLoading = true
        showText = "Loading"

        action!({
            self.endAction()
        })
    }

    func endAction() {
        isLoading = false
        showText = "Refresh done"
        showDone = true
    }

    init() {
        showText = ""
        showRefreshView = false
        percentage = 0
        opacity = 0
        showDone = false
        isLoading = false
    }
}

@available(iOS 13.0, macOS 10.15, *)
public struct RefreshableNavigationView<Content: View>: View {
    let content: () -> Content
    private var title: String

    @ObservedObject var data: RefreshData

    public init(title: String, data: RefreshData, @ViewBuilder content: @escaping () -> Content) {
        self.title = title
        self.content = content
        self.data = data
    }

    public var body: some View {
        NavigationView {
            RefreshableList(data: data) {
                self.content()
            }.navigationBarTitle(title)
                .edgesIgnoringSafeArea(.bottom)
        }
    }
}

@available(iOS 13.0, macOS 10.15, *)
public struct RefreshableList<Content: View>: View {
    @ObservedObject var data: RefreshData

    let RefreshStartOffset: CGFloat = 185

    let content: () -> Content

    init(data: RefreshData, @ViewBuilder content: @escaping () -> Content) {
        self.data = data
        self.content = content
        UITableViewHeaderFooterView.appearance().tintColor = UIColor.clear
    }

    public var body: some View {
        List {
            Section(header: HStack { PullToRefreshView(data: self.data) }) {
                content()
            }
        }
        .listStyle(GroupedListStyle())
        .offset(y: self.data.spinnerSize * -1)
        .onPreferenceChange(RefreshableKeyTypes.PrefKey.self) { values in
            values.forEach { value in
                self.refresh(offset: value.bounds.origin.y)
            }
        }
    }

    func refresh(offset: CGFloat) {
        data.opacity = min(1, Double((offset - 106) / 80))
        data.percentage = CGFloat((offset - 106) / 80)

        if offset <= RefreshStartOffset {
            data.startPull()
            return
        }

        if data.isLoading {
            return
        }

        data.startAction()
    }
}

@available(iOS 13.0, macOS 10.15, *)
struct PullToRefreshView: View {
    @ObservedObject var data: RefreshData

    var body: some View {
        GeometryReader { geometry in
            RefreshView(data: self.data)
                .opacity(data.opacity)
                .preference(key: RefreshableKeyTypes.PrefKey.self, value: [RefreshableKeyTypes.PrefData(bounds: geometry.frame(in: CoordinateSpace.global))])
                .offset(y: self.data.spinnerSize * -2)
        }
        .offset(y: self.data.spinnerSize * -1)
    }
}

@available(iOS 13.0, macOS 10.15, *)
struct RefreshView: View {
    @ObservedObject var data: RefreshData

    var body: some View {
        HStack {
            VStack(alignment: .center) {
                if self.data.showDone {
                    HStack {
                        Spacer()
                        Image(systemName: "checkmark.circle")
                            .frame(width: self.data.spinnerSize, height: self.data.spinnerSize)
                            .foregroundColor(Color.green)
                            .imageScale(.large)
                        Spacer()
                    }
                } else if !data.isLoading {
                    HStack {
                        Spacer()
                        Spinner(percentage: self.$data.percentage, size: self.$data.spinnerSize)
                        Spacer()
                    }
                } else {
                    HStack {
                        Spacer()
                        ActivityIndicator(isAnimating: .constant(true), style: .large)
                            .frame(width: self.data.spinnerSize, height: self.data.spinnerSize)
                        Spacer()
                    }
                }
                Text(self.data.showText).font(.caption)
            }
        }
    }
}

@available(iOS 13.0, macOS 10.15, *)
struct Spinner: View {
    @Binding var percentage: CGFloat
    @Binding var size: CGFloat

    var body: some View {
        GeometryReader { _ in
            ForEach(1 ... 10, id: \.self) { i in
                Rectangle()
                    .fill(Color.gray)
                    .cornerRadius(1)
                    .frame(width: 2.5, height: 8)
                    .opacity(self.percentage * 10 >= CGFloat(i) ? Double(i) / 10.0 : 0)
                    .offset(x: 0, y: -8)
                    .rotationEffect(.degrees(Double(36 * i)), anchor: .bottom)
            }.offset(x: 20, y: 12)
        }.frame(width: self.size, height: self.size)
    }
}

@available(iOS 13.0, macOS 10.15, *)
struct ActivityIndicator: UIViewRepresentable {
    @Binding var isAnimating: Bool
    let style: UIActivityIndicatorView.Style

    func makeUIView(context _: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }

    func updateUIView(_ uiView: UIActivityIndicatorView, context _: UIViewRepresentableContext<ActivityIndicator>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}

@available(iOS 13.0, macOS 10.15, *)
enum RefreshableKeyTypes {
    struct PrefData: Equatable {
        let bounds: CGRect
    }

    struct PrefKey: PreferenceKey {
        static var defaultValue: [PrefData] = []

        static func reduce(value: inout [PrefData], nextValue: () -> [PrefData]) {
            value.append(contentsOf: nextValue())
        }

        typealias Value = [PrefData]
    }
}

@available(iOS 13.0, macOS 10.15, *)
struct RefreshableNavigationView_Previews: PreviewProvider {
    static var data = RefreshData()

    static var previews: some View {
        RefreshableNavigationView(title: "What's nearby", data: self.data) { ZStack {
            FeatureRow(feature: featureData[0])
            NavigationLink(destination: FeatureDetail(feature: featureData[0], onClose: {})) {
                EmptyView()
            }
            .buttonStyle(PlainButtonStyle())
        }}
    }
}
