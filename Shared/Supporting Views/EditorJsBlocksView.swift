//
//  Article.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 07.02.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct EditorJsBlocksView: View {
    var blockContainer: EditorJsBlockContainer

    var body: some View {
        VStack(alignment: .leading) {
            ForEach(self.blockContainer.blocks.indices, id: \.self) { idx in
                if self.blockContainer.blocks[idx].type == "header" {
                    ArticleHeader(header: self.blockContainer.blocks[idx].asHeader())
                        .padding(.bottom, idx == self.blockContainer.blocks.count ? 0 : 10)
                }

                if self.blockContainer.blocks[idx].type == "paragraph" {
                    ArticleParagraph(paragraph: self.blockContainer.blocks[idx].asParagraph())
                }

                if self.blockContainer.blocks[idx].type == "list" {
                    ArticleListView(list: self.blockContainer.blocks[idx].asList())
                }
            }
        }
    }
}

struct EditorJsBlocksView_Previews: PreviewProvider {
    static var previews: some View {
        EditorJsBlocksView(blockContainer: featureData[1].description.blockContainer)
            .environment(\.colorScheme, .dark)
        EditorJsBlocksView(blockContainer: featureData[2].description.blockContainer)
            .environment(\.colorScheme, .light)
        EditorJsBlocksView(blockContainer: featureData[3].description.blockContainer)
            .environment(\.colorScheme, .light)
    }
}
