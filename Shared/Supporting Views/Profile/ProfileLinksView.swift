//
//  ProfileLinksView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 30.09.21.
//

import SwiftUI

struct ProfileLinksView: View {
    @Environment(\.openURL) var openURL

    @ObservedObject var profile: UserProfile

    var body: some View {
        HStack {
            Spacer()

            if let skype = profile.skype, let link = URL(string: "skype:\(skype)") {
                Button(action: { openURL(link) }) {
//                    Text(AwesomeIcon.skype.rawValue)
//                        .font(.awesome(style: .brand, size: 20))
//                        .padding()
                }
            }

            if let linkedin = profile.linkedin, let link = URL(string: "https://www.linkedin.com/in/\(linkedin)/") {
                Button(action: { openURL(link) }) {
//                    Text(AwesomeIcon.linkedin.rawValue)
//                        .font(.awesome(style: .brand, size: 20))
//                        .padding()
                }
            }

            if let twitter = profile.twitter, let link = URL(string: "https://twitter.com/\(twitter)") {
                Button(action: { openURL(link) }) {
//                    Text(AwesomeIcon.twitter.rawValue)
//                        .font(.awesome(style: .brand, size: 20))
//                        .padding()
                }
            }

            Spacer()
        }
    }
}

struct ProfileLinksView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileLinksView(profile: UserProfile(id: ""))
    }
}
