//
//  ProfileView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 13.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
    @ObservedObject var profile: UserProfile
    @Environment(\.openURL) var openURL
    @State var onEdit: (() -> Void)?

    var body: some View {
        VStack {
            Spacer()

            ProfilePictureView(profile: profile)

            if let joinedTime = profile.prettyJoinedTime(), joinedTime != "---" {
                Spacer()
                Text("Member since \(joinedTime)")
            }

            if let prettyLocation = profile.prettyLocation(), prettyLocation != "---" {
                Spacer()
                HStack {
                    Spacer()
                    Image(systemName: "mappin")
                    Text(prettyLocation)
                    Spacer()
                }
            }

            if let prettyJob = profile.prettyJob(), prettyJob != "---" {
                Spacer()
                HStack {
                    Spacer()
                    Image(systemName: "briefcase")
                    Text(prettyJob)
                    Spacer()
                }
            }

            if profile.hasLinks() == true {
                Spacer()
                ProfileLinksView(profile: profile)
            }

            if let website = profile.website, let link = URL(string: website), website.trim() != "" {
                Spacer()
                Button(profile.website) {
                    openURL(link)
                }
            }

            if let bio = profile.bio, bio.trim() != "" {
                Spacer()
                Text(bio)
            }

            if profile.canEdit == true {
                Spacer()
                Button("Edit", action: {
                    onEdit?()
                })
            }

            Spacer()
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(profile: UserProfile(id: ""))
    }
}
