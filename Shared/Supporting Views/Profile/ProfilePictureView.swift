//
//  ProfilePictureView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 30.09.21.
//

import SwiftUI

struct ProfilePictureView: View {
    @ObservedObject var profile: UserProfile
    @State var picture: Picture?

    var body: some View {
        VStack {
            if let picture = picture {
                AsyncPicture(picture: picture)
                    .aspectRatio(1.0, contentMode: .fit)
                    .fixedSize(horizontal: false, vertical: true)
                    .frame(minWidth: 200, idealWidth: 200, maxWidth: 200, minHeight: 200, idealHeight: 200, maxHeight: 200, alignment: .center)
                    .clipShape(Circle())
                    .shadow(radius: 10)
                    .overlay(Circle().stroke(Color.primary, lineWidth: 5))
            }

            Text(profile.fullName())
                .font(.headline)
                .padding(5)
        }
        .onReceive(profile.$picture) { value in
            if let value = value {
                self.picture = value
            }
        }
        .task {
            try? await self.profile.loadAll()
        }
    }
}

struct ProfilePictureView_Previews: PreviewProvider {
    static var previews: some View {
        ProfilePictureView(profile: UserProfile(id: ""))
    }
}
