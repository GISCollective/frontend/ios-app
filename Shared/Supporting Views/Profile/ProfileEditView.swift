//
//  ProfileEditView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 30.09.21.
//

import SwiftUI

struct ProfileEditView: View {
    enum SheetType: String, Identifiable {
        case imagePicker, camera
        var id: String {
            return rawValue
        }
    }

    @State var profile: UserProfile
    @State var salutationOptions = ["Mr.", "Ms.", "Mrs."]

    @State var textFields: [EditFieldProps] = []
    @State var onUpdate: (_ profile: UserProfile, _ image: UIImage?) -> Void

    @State private var salutation: Binding<String> = Binding(get: { "" }, set: { _ in })
    @State private var sheetType: SheetType?
    @State private var selectedPictures: [PickedMediaItem] = []
    @State private var picture = Picture(id: "")
    @State private var pictureImage = ObservableUIImage.createDefault()

    internal let inspection = Inspection<Self>()

    var selectedPicture: ObservableUIImage? {
        if let image = selectedPictures.last?.item.value {
            return ObservableUIImage(resolved: image)
        }

        return nil
    }

    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                VStack {
                    AsyncImageBis(image: selectedPicture ?? pictureImage)
                        .aspectRatio(1.0, contentMode: .fit)
                        .fixedSize(horizontal: false, vertical: true)
                        .frame(minWidth: 200, idealWidth: 200, maxWidth: 200, minHeight: 200, idealHeight: 200, maxHeight: 200, alignment: .center)
                        .clipShape(Circle())
                        .shadow(radius: 10)
                        .overlay(Circle().stroke(Color.primary, lineWidth: 5))
                        .onReceive(profile.$picture) { value in
                            if let value = value {
                                self.picture = value
                            }
                        }
                        .onReceive(picture.$picture) { value in
                            self.pictureImage = picture.uiImage(value)
                        }

                    Menu {
                        Button("Photo Library", action: {
                            self.sheetType = .imagePicker
                        })
                        Button("Take Photo", action: {
                            self.sheetType = .camera
                        })
                    }
                        label: {
                        Label {}
                                icon: {
                                HStack {
                                    Text("Change picture")
                                }
                                .frame(maxWidth: .infinity)
                                .padding()
                                .foregroundColor(.gray)
                            }
                    }
                    .sheet(item: $sheetType, content: { type in
                        switch type {
                        case .imagePicker:
                            PhotoLibraryView(pictures: $selectedPictures, selectionLimit: 1) { _ in
                                sheetType = nil
                            }
                        case .camera:
                            CameraView(pictures: $selectedPictures)
                                .ignoresSafeArea()
                        }
                    })
                }

                VStack(alignment: .leading, spacing: 0) {
                    Text("Salutation:")
                        .font(.subheadline)
                        .padding(.bottom, 5)
                        .foregroundColor(.accentColor)
                    Picker(selection: salutation, label: Text("Salutation")) {
                        Text("None").tag("")

                        ForEach(salutationOptions, id: \.self) {
                            Text($0).tag($0)
                        }
                    }
                    .pickerStyle(.segmented)
                    .padding(.top, 10)
                }

                ForEach(textFields, id: \.label) { field in
                    TextInputWithProps(props: field)
                        .padding(.top, 10)
                }

                PrimaryButton(action: {
                    onUpdate(profile, selectedPictures.first?.item.value)
                }, label: "Save")
                    .padding(.top, 10)
            }
            .padding()
        }
        .onAppear {
            self.salutation = Binding(get: {
                if salutationOptions.contains(profile.salutation ?? "") {
                    return profile.salutation ?? ""
                }

                return ""
            }, set: {
                profile.salutation = $0
            })

            textFields = [
                EditFieldProps(label: "Title:", bindedValue: $profile.title),
                EditFieldProps(label: "First name:", bindedValue: $profile.firstName),
                EditFieldProps(label: "Last name:", bindedValue: $profile.lastName),
            ]

            if profile.location.isDetailed == true {
                textFields.append(EditFieldProps(label: "City:", bindedValue: Binding(get: { profile.location.detailedLocation?.city ?? "" }, set: { profile.location.detailedLocation?.city = $0 })))
                textFields.append(EditFieldProps(label: "Province:", bindedValue: Binding(get: { profile.location.detailedLocation?.province ?? "" }, set: { profile.location.detailedLocation?.province = $0 })))
                textFields.append(EditFieldProps(label: "Country:", bindedValue: Binding(get: { profile.location.detailedLocation?.country ?? "" }, set: { profile.location.detailedLocation?.country = $0 })))
                textFields.append(EditFieldProps(label: "Postal code:", bindedValue: Binding(get: { profile.location.detailedLocation?.postalCode ?? "" }, set: { profile.location.detailedLocation?.postalCode = $0 })))

            } else {
                textFields.append(EditFieldProps(label: "Location:", bindedValue: $profile.location.simple))
            }

            textFields.append(contentsOf: [
                EditFieldProps(label: "Skype:", bindedValue: $profile.skype),
                EditFieldProps(label: "LinkedIn:", bindedValue: $profile.linkedin),
                EditFieldProps(label: "Twitter:", bindedValue: $profile.twitter),
                EditFieldProps(label: "Website URL:", bindedValue: $profile.website),
                EditFieldProps(label: "Job title:", bindedValue: $profile.jobTitle),
                EditFieldProps(label: "Organization:", bindedValue: $profile.organization),
                EditFieldProps(label: "Bio:", bindedValue: $profile.bio),
            ])
        }
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct ProfileEditView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileEditView(profile: UserProfile(id: ""), onUpdate: { _, _ in })
    }
}
