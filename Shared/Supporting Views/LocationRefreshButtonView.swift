//
//  LocationRefreshButtonView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 07.07.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct LocationRefreshButtonView: View {
    static let visibileTop: CGFloat = 100
    static let hiddenTop: CGFloat = -50

    @State var refreshList: () -> Void

    @State var positionStatusTop: CGFloat = hiddenTop
    @ObservedObject var locationManager = LocationManager.instance!

    var body: some View {
        GeometryReader { geometry in
            VStack(alignment: .leading) {
                HStack(alignment: /*@START_MENU_TOKEN@*/ .center/*@END_MENU_TOKEN@*/) {
                    Spacer()
                    Button {
                        refreshList()
                    } label: {
                        Text("Refresh list")
                    }
                    .padding(.vertical, 10)
                    .padding(.horizontal, 30)
                    .background(Color(red: 1, green: 1, blue: 1))
                    .foregroundColor(.accentColor)
                    .clipShape(Capsule())
                    .shadow(radius: 10)
                    Spacer()
                }
                .frame(width: geometry.size.width, alignment: .top)
                .padding(.top, positionStatusTop)
                Spacer()
            }
            .frame(width: geometry.size.width, height: geometry.size.height, alignment: .top)
            .onReceive(locationManager.$canBeUpdated, perform: { canBeUpdated in
                withAnimation(.easeInOut(duration: 1)) {
                    if canBeUpdated {
                        positionStatusTop = LocationRefreshButtonView.visibileTop
                    } else {
                        positionStatusTop = LocationRefreshButtonView.hiddenTop
                    }
                }
            })
        }
    }
}

struct LocationRefreshButtonView_Previews: PreviewProvider {
    static var previews: some View {
        LocationRefreshButtonView(refreshList: {}, locationManager: LocationManager())
    }
}
