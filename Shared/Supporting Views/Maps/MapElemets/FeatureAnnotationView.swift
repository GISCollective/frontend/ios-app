//
//  FeatureAnnotationView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 30.01.22.
//

import Foundation
import GEOSwift
import GEOSwiftMapKit
import MapKit
import SDWebImage
import SwiftUI

struct FeatureAnnotationView: View {
    @State var feature: Feature
    @State var firstIcon: Icon = .init()

    #if TESTING
        internal let inspection = Inspection<Self>()
    #endif

    var imageSize: CGFloat {
        CGFloat(firstIcon.styles?.types?.site?.size ?? 18) * 1.5
    }

    var body: some View {
        ZStack {
            if let site = firstIcon.styles?.types?.site {
                CircleAnnotationView(size: CGFloat(site.size ?? 18), width: CGFloat(site.borderWidth ?? 1), fill: Color.parseColor(value: site.backgroundColor ?? "white"), stroke: Color.parseColor(value: site.borderColor ?? "gray"))

                AsyncIcon(icon: self.$firstIcon)
                    .scaledToFit()
                    .frame(width: imageSize, height: imageSize)
            }
        }
        .onReceive(feature.$firstIcons) { value in
            if let value = value {
                self.firstIcon = value
            }
        }
        .task {
            try? await feature.loadIcons()
        }
        #if TESTING
            .id("map-feature-\(feature.id)")
            .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
        #endif
    }
}

struct CircleAnnotationView: View {
    @State var size: CGFloat
    @State var width: CGFloat
    @State var fill: Color
    @State var stroke: Color

    var shapeSize: CGFloat {
        return size * 2
    }

    var body: some View {
        ZStack {
            Circle()
                .fill(fill)
                .frame(width: shapeSize, height: shapeSize)

            Circle()
                .strokeBorder(stroke, lineWidth: width)
                .frame(width: shapeSize, height: shapeSize)
        }
    }
}
