//
//  MapRow.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 16.11.21.
//

import CoreLocation
import GEOSwift
import SwiftUI

struct MapRow: View {
    @ObservedObject var map: Map
    @State private var showingSheet = false
    @State private var picture = Picture(id: "")

    @State var extent: Geometry?
    @State var center: CLLocationCoordinate2D?
    @StateObject var model = FeatureListData(name: "unknown map")

    func onMapClose() {
        showingSheet = false
    }

    func onMapOpen() {
        extent = map.area
        showingSheet = true
    }

    func onDrag(point: CLLocationCoordinate2D) {
        if model.endReached == true {
            return
        }

        if model.data.count > 0, model.data.count < model.pageSize {
            return
        }

        model.stop()
        model.pageSize = 100
        model.latitude = String(point.latitude)
        model.longitude = String(point.longitude)

        Task {
            _ = try await model.reload()
        }
    }

    var body: some View {
        Button(action: onMapOpen) {
            VStack(alignment: .leading) {
                AsyncPicture(picture: picture)
                    .aspectRatio(1.0, contentMode: .fit)
                    .fixedSize(horizontal: false, vertical: true)
                    .cornerRadius(10)
                Text(map.name)
                    .font(.headline)
                    .fontWeight(.regular)
                    .multilineTextAlignment(.leading)
                    .lineLimit(3)
                    .padding(.horizontal, 5.0)
                    .padding(0.0)
            }
        }
        .sheet(isPresented: $showingSheet) {
            ZStack(alignment: .topLeading) {
                FeatureListView(mapCenter: $center, model: model, content: map.processedDescription.blockContainer, onDrag: onDrag)
                    .ignoresSafeArea()
                    .onDisappear {
                        self.model.stop()
                    }

                HStack {
                    Spacer()
                    ButtonClose(action: onMapClose)
                }
                .padding()
                .zIndex(10)
            }
        }
        .task {
            try? await map.loadAll()
        }
        .onAppear {
            self.model.map = map
        }
        .onReceive(map.$cover) { value in
            if picture.id == "" {
                picture = value
            }
        }
        .onReceive(map.$squareCover) { value in
            if let value = value {
                picture = value
            }
        }
    }
}

struct MapRow_Previews: PreviewProvider {
    static var previews: some View {
        MapRow(map: mapData[0])
    }
}
