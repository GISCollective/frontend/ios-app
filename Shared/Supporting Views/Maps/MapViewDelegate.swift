//
//  MapViewDelegate.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 30.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import Foundation
import MapKit

class MapViewDelegate: NSObject, MKMapViewDelegate {
    var features: [Feature] = [] {
        willSet {
            if hasFeatureUpdates {
                return
            }

            if features.count != newValue.count {
                hasFeatureUpdates = true
                return
            }

            let currentIds = features.map { "\($0.id)-\($0.firstIcons?.id ?? "")" }.joined(separator: ".")
            let newIds = newValue.map { "\($0.id)-\($0.firstIcons?.id ?? "")" }.joined(separator: ".")

            hasFeatureUpdates = currentIds != newIds
        }
    }

    private var hasFeatureUpdates: Bool = false
    private var annotations: [PointAnnotation] = []
    private var lineOverlays: [LineAnnotation] = []
    private var polygonOverlays: [PolygonAnnotation] = []
    private var cancellables = Set<AnyCancellable>()

    var locationManager = LocationManager.instance
    var onDrag: ((_: CLLocationCoordinate2D) -> Void)?

    func updateFeatures(uiView: MKMapView) {
        if !hasFeatureUpdates {
            return
        }
        hasFeatureUpdates = false

        uiView.removeAnnotations(annotations)
        uiView.removeOverlays(lineOverlays)
        uiView.removeOverlays(polygonOverlays)

        annotations.removeAll()

        annotations = uiView.annotations
            .map { $0 as? PointAnnotation }
            .filter { $0 != nil }
            .map { $0! }

        features.forEach { feature in
            if let annotation = feature.toMKAnnotation() {
                self.annotations.append(annotation)
            }

            self.lineOverlays.append(contentsOf: feature.toLineOverlay())
            self.polygonOverlays.append(contentsOf: feature.toPolygonOverlay())
        }

        uiView.addAnnotations(annotations)
        uiView.addOverlays(lineOverlays)
        uiView.addOverlays(polygonOverlays)
    }

    func contains(uiView _: MKMapView, annotation: MKAnnotation) -> Bool {
        if let pointAnnotation = annotation as? PointAnnotation {
            let exists = annotations.contains(where: { $0.feature.id == pointAnnotation.feature.id })

            return exists
        }

        return false
    }

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated _: Bool) {
        onDrag?(mapView.centerCoordinate)
    }

    func mapView(_: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let polygonOverlay = overlay as? PolygonAnnotation {
            let renderer = MKPolygonRenderer(overlay: overlay)

            polygonOverlay.feature?.$firstIcons.sink { icon in
                icon?.setStyle(renderer: renderer)
            }.store(in: &cancellables)

            return renderer
        }

        if let lineOverlay = overlay as? LineAnnotation {
            let renderer = MKPolylineRenderer(overlay: overlay)
            lineOverlay.feature?.$firstIcons.sink { icon in
                icon?.setStyle(renderer: renderer)
            }.store(in: &cancellables)

            return renderer
        }

        return MKOverlayRenderer(overlay: overlay)
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let point = annotation as? PointAnnotation {
            let Identifier = "MapMarker"
            let annotationView: MapIconMarker = mapView.dequeueReusableAnnotationView(withIdentifier: Identifier) as? MapIconMarker ?? MapIconMarker(annotation: annotation, reuseIdentifier: Identifier)

            annotationView.feature = point.feature

            return annotationView
        }

        return nil
    }
}
