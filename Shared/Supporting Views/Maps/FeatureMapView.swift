//
//  MapView.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 06.10.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import GEOSwift
import GEOSwiftMapKit
import Mapbox
import MapKit
import SDWebImage
import SwiftUI

struct FeatureMapView: UIViewRepresentable {
    @State var feature: Feature

    func makeUIView(context: Context) -> MGLMapView {
        // Build the style url
        let styleURL = RemoteConnection.instance.getMapStyleUrl(id: nil)

        // create the mapview
        let mapView = MGLMapView(frame: .zero, styleURL: styleURL)

        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.logoView.isHidden = true
        mapView.showsUserLocation = true
        mapView.isPitchEnabled = true
        mapView.delegate = context.coordinator

        context.coordinator.mapView = mapView

        return mapView
    }

    func updateUIView(_: MGLMapView, context _: Context) {}

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    final class Coordinator: NSObject, MGLMapViewDelegate {
        var control: FeatureMapView
        var mapView: MGLMapView?

        init(_ control: FeatureMapView) {
            self.control = control
        }

        func checkMapCenter() {
            if let mapView = mapView {
                mapView.setCenter(control.feature.coordinate, zoomLevel: 15, animated: false)
            }
        }

        func mapViewDidFinishLoadingMap(_: MGLMapView) {
            checkMapCenter()
        }

        func mapView(_: MGLMapView, didFinishLoading _: MGLStyle) {
            checkMapCenter()
        }
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        let view = FeatureMapView(feature: featureData[0])
        return view
    }
}

extension Feature {
    func toMKAnnotation() -> PointAnnotation? {
        if firstIcons == nil {
            return nil
        }

        if case .point = position {
            return PointAnnotation(feature: self)
        }

        if case .lineString = position {
            let firstPoint = self.position.firstPoint()
            return PointAnnotation(feature: self, center: firstPoint)
        }

        if case .multiLineString = position {
            let firstPoint = self.position.firstPoint()
            return PointAnnotation(feature: self, center: firstPoint)
        }

        if case let .polygon(polygon) = position, let center = try? polygon.centroid().toCLLocationCoordinate2D() {
            return PointAnnotation(feature: self, center: center)
        }

        if case let .multiPolygon(polygon) = position, let center = try? polygon.centroid().toCLLocationCoordinate2D() {
            return PointAnnotation(feature: self, center: center)
        }

        return nil
    }

    func toLineOverlay() -> [LineAnnotation] {
        var overlays: [LineAnnotation] = []

        if firstIcons == nil {
            return overlays
        }

        if case let .lineString(line) = position {
            let coordinates = line.points.map { CLLocationCoordinate2DMake($0.y, $0.x) }
            let annotation = LineAnnotation(coordinates: coordinates, count: coordinates.count)
            annotation.feature = self
            annotation.title = self.name

            overlays.append(annotation)
        }

        if case let .multiLineString(multiLine) = position {
            multiLine.lineStrings.forEach { line in
                let coordinates = line.points.map { CLLocationCoordinate2DMake($0.y, $0.x) }
                let annotation = LineAnnotation(coordinates: coordinates, count: coordinates.count)
                annotation.feature = self
                annotation.title = self.name

                overlays.append(annotation)
            }
        }

        return overlays
    }

    func toPolygonOverlay() -> [PolygonAnnotation] {
        var overlays: [PolygonAnnotation] = []

        if firstIcons == nil {
            return overlays
        }

        if case let .polygon(polygon) = position {
            let coordinates = polygon.exterior.points.map { CLLocationCoordinate2DMake($0.y, $0.x) }
            let annotation = PolygonAnnotation(coordinates: coordinates, count: coordinates.count)
            annotation.feature = self
            annotation.title = self.name

            overlays.append(annotation)
        }

        return overlays
    }
}

extension Icon {
    func setStyle(renderer: MKPolygonRenderer) {
        renderer.lineWidth = CGFloat(styles?.types?.polygon?.borderWidth ?? 2)

        if let borderColor = styles?.types?.polygon?.borderColor {
            renderer.strokeColor = UIColor.parseColor(value: borderColor)
        } else {
            renderer.strokeColor = UIColor.parseColor(value: "#0033cc")
        }

        if let backgroundColor = styles?.types?.polygon?.backgroundColor {
            renderer.fillColor = UIColor.parseColor(value: backgroundColor)
        }
    }

    func setStyle(renderer: MKPolylineRenderer) {
        renderer.lineWidth = CGFloat(styles?.types?.polygon?.borderWidth ?? 2)
        renderer.strokeColor = UIColor.parseColor(value: styles?.types?.polygon?.borderColor)
    }
}

class PointAnnotation: MKPointAnnotation {
    var feature: Feature

    init(feature: Feature) {
        self.feature = feature

        super.init()
        title = self.feature.name

        if case let .point(pt) = feature.position {
            self.coordinate = CLLocationCoordinate2D(latitude: pt.y, longitude: pt.x)
        }
    }

    init(feature: Feature, center: CLLocationCoordinate2D) {
        self.feature = feature

        super.init()
        title = self.feature.name
        coordinate = center
    }
}

class LineAnnotation: MKPolyline {
    var feature: Feature?
}

class PolygonAnnotation: MKPolygon {
    var feature: Feature?
}
