//
//  GeometryMapView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 04.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

import GEOSwift
import MapKit
import SDWebImage
import SwiftUI

struct GeometryMapView: UIViewRepresentable {
    @State var geometry: Geometry?
    @State var icon: Icon?

    func makeUIView(context: Context) -> MKMapView {
        let view = MKMapView(frame: .zero)
        view.delegate = context.coordinator

        return view
    }

    func updateUIView(_ uiView: MKMapView, context: Context) {
        uiView.showsUserLocation = true

        if let geometry = geometry {
            let feature = Feature(id: "")
            feature.position = geometry
            context.coordinator.features = [feature]
            context.coordinator.updateFeatures(uiView: uiView)
        }

        if let centroid = try? geometry?.centroid() {
            let region = MKCoordinateRegion(center: centroid.toCLLocationCoordinate2D(), latitudinalMeters: CLLocationDistance(exactly: 500)!, longitudinalMeters: CLLocationDistance(exactly: 500)!)
            uiView.setRegion(uiView.regionThatFits(region), animated: true)
        }
    }

    func makeCoordinator() -> Coordinator {
        let delegate = Coordinator()
        return delegate
    }

    class Coordinator: MapViewDelegate {}
}

struct GeometryMapView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryMapView(geometry: Geometry.point(Point(x: 13, y: 24)))
    }
}
