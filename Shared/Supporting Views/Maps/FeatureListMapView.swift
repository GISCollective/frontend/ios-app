//
//  FeatureListMapView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 17.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import GEOSwift
import GEOSwiftMapKit
import Mapbox
import MapKit
import SDWebImage
import SwiftUI

typealias MapUi = MapKit.Map

struct FeatureListMapViewUI: View {
    @Binding var features: [Feature]
    @Binding var center: CLLocation?
    @Binding var extent: Geometry?
    @Binding var selectedFeature: Feature?

    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: 13.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))

    var onDrag: ((_: CLLocationCoordinate2D) -> Void)? = nil

    func update(extent: Geometry?) {
        if let extent = extent, let coordinateRegion = try? MKCoordinateRegion(containing: extent) {
            region = coordinateRegion
        }
    }

    func update(center: CLLocation?) {
        if let center = center {
            region = MKCoordinateRegion(center: center.coordinate, latitudinalMeters: CLLocationDistance(exactly: 5000)!, longitudinalMeters: CLLocationDistance(exactly: 5000)!)
        }
    }

    var body: some View {
        ZStack {
            MapUi(coordinateRegion: $region,
                  showsUserLocation: true, annotationItems: features, annotationContent: { feature in
                      MapAnnotation(coordinate: feature.coordinate) {
                          FeatureAnnotationView(feature: feature)
                      }
                  })
        }
        .onAppear {
            update(center: self.center)
            update(extent: self.extent)
        }
        .onChange(of: extent, perform: { extent in
            update(extent: extent)
        })
        .onChange(of: center, perform: { center in
            update(center: center)
        })
    }
}

struct FeatureListMapView: UIViewRepresentable {
    @Binding var map: Map?
    @Binding var center: CLLocationCoordinate2D?
    @Binding var selectedFeature: Feature?

    var selectedFeatureId: String? {
        get {
            return selectedFeature?.id
        }

        set {
            if let id = newValue {
                let publisher = $selectedFeature
                Task {
                    let feature = try? await Store.instance.features.getById(id: id)
                    publisher.wrappedValue = feature
                }
            } else {
                selectedFeature = nil
            }
        }
    }

    func makeUIView(context: Context) -> MGLMapView {
        // Build the style url
        let styleURL = RemoteConnection.instance.getMapStyleUrl(id: map?.id ?? "_")

        // create the mapview
        let mapView = MGLMapView(frame: .zero, styleURL: styleURL)

        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.logoView.isHidden = true
        mapView.showsUserLocation = true
        mapView.isPitchEnabled = true
        mapView.delegate = context.coordinator

        context.coordinator.map = map
        context.coordinator.mapView = mapView

        let singleTap = UITapGestureRecognizer(target: context.coordinator, action: #selector(Coordinator.triggerTouchAction(sender:)))
        for recognizer in mapView.gestureRecognizers! where recognizer is UITapGestureRecognizer {
            singleTap.require(toFail: recognizer)
        }
        mapView.addGestureRecognizer(singleTap)

        return mapView
    }

    func updateUIView(_ mapView: MGLMapView, context: Context) {
        let styleURL = RemoteConnection.instance.getMapStyleUrl(id: map?.id ?? "_")

        if center != nil {
            context.coordinator.checkMapCenter(mapView)
        }

        if mapView.styleURL != styleURL {
            mapView.styleURL = styleURL
        }

        context.coordinator.map = map
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    final class Coordinator: NSObject, MGLMapViewDelegate {
        var control: FeatureListMapView
        var mapView: MGLMapView?
        var watchedLayers = Set<String>()
        var map: Map?

        var mapId: String {
            return map?.id ?? "_"
        }

        init(_ control: FeatureListMapView) {
            self.control = control
        }

        func checkMapCenter(_ mapView: MGLMapView) {
            if let extent = map?.area, let extent = try? extent.envelope() {
                let bounds = MGLCoordinateBounds(sw: CLLocationCoordinate2D(latitude: extent.maxY, longitude: extent.maxX), ne: CLLocationCoordinate2D(latitude: extent.minY, longitude: extent.minX))
                mapView.setVisibleCoordinateBounds(bounds, animated: false)
            }

            if let center = control.center {
                mapView.setCenter(center, zoomLevel: 15, animated: false)
            }
        }

        func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
            checkMapCenter(mapView)
        }

        func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
            let mapLayerPrefix = "\(mapId)_"

            for layer in style.layers {
                if layer.identifier.starts(with: mapLayerPrefix) {
                    watchedLayers.insert(layer.identifier)
                }
            }

            checkMapCenter(mapView)
        }

        @objc func triggerTouchAction(sender: UITapGestureRecognizer) {
            let spot = sender.location(in: mapView)

            let rect = CGRect(x: spot.x - 20, y: spot.y - 20, width: 40, height: 40)

            if let features = mapView?.visibleFeatures(in: rect, styleLayerIdentifiers: watchedLayers), features.count > 0 {
                if let id = features[0].attributes["_id"] as? String {
                    control.selectedFeatureId = id
                }
            }
        }
    }
}
