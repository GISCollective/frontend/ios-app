//
//  Picture.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 24.11.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import SwiftUI

enum UrlAsyncImageType {
    case defaultImage, hashImage, actualImage
}

struct UrlAsyncImage: View {
    @Binding var url: String
    @Binding var blurHash: String?
    @State var size: String = "md"

    @State private var defaultImage: UIImage = ObservableUIImage.defaultValue.value!
    @State private var blurHashImage: UIImage = ObservableUIImage.defaultValue.value!
    @State private var image: UIImage = ObservableUIImage.defaultValue.value!
    @State private var blurHashRendered: Bool = false
    @State private var isLoading: Bool = false
    @State private var imageType: UrlAsyncImageType = .defaultImage

    @State private var blurHashOpacity: Double = 0
    @State private var imageOpacity: Double = 0
    @State private var timeStamp: Double = 0

    func generateBlurHashImage() {
        if blurHash == "" {
            return
        }

        if let image = UIImage(blurHash: blurHash ?? "", size: CGSize(width: 40, height: 40)) {
            DispatchQueue.main.async {
                blurHashImage = image

                withAnimation(.easeIn(duration: 0.1)) {
                    blurHashOpacity = 1.0
                }

                timeStamp = CACurrentMediaTime()
            }
        }
    }

    func loadRemotePicture() async {
        if let image = try? await PictureImageCache.instance.get(baseUrl: url, size: size) {
            DispatchQueue.main.async {
                self.image = image

                if timeStamp == 0 {
                    timeStamp = CACurrentMediaTime()
                }

                let diff = CACurrentMediaTime() - timeStamp
                let duration = diff < 0.3 ? 0 : 0.5

                imageType = .actualImage
                withAnimation(.easeIn(duration: duration)) {
                    imageOpacity = 1.0
                }
            }
        }
    }

    var body: some View {
        ZStack(alignment: .center) {
            Image(uiImage: defaultImage)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(
                    minWidth: 0,
                    maxWidth: .infinity,
                    minHeight: 0,
                    maxHeight: .infinity,
                    alignment: .center
                )
                .clipped()

            if blurHash != nil {
                Image(uiImage: blurHashImage)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .center
                    )
                    .clipped()
                    //                .opacity(self.blurHashOpacity)
                    .task {
                        self.generateBlurHashImage()
                    }
            }

            if url != "" {
                Image(uiImage: image)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .center
                    )
                    .clipped()
                    .opacity(self.imageOpacity)
                    .task {
                        await self.loadRemotePicture()
                    }
            }
        }
    }
}

struct AsyncImageBis: View {
    @ObservedObject var image: ObservableUIImage
    @State var renderedImage: UIImage = ObservableUIImage.defaultValue.value!
    @State var hideDefault = false

    var blurHash: String?
    @State private var opacity: Double = 0
    @State private var blurHashRendered: Bool = false
    @State private var isLoading: Bool = false

    func updateOpacity(isDefault: Bool) {
        if isDefault == true, hideDefault {
            opacity = 0
            return
        }

        opacity = 1
    }

    var body: some View {
        content
    }

    private var content: some View {
        ZStack(alignment: .center) {
            Image(uiImage: renderedImage)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(
                    minWidth: 0,
                    maxWidth: .infinity,
                    minHeight: 0,
                    maxHeight: .infinity,
                    alignment: .center
                )
                .clipped()
                .opacity(opacity)
                .onAppear {
                    updateOpacity(isDefault: image.isDefault)

                    if blurHash == nil || blurHashRendered || !image.isLoading {
                        return
                    }

                    DispatchQueue.global(qos: .userInitiated).async {
                        if let blurHash = blurHash, let blurHashImage = UIImage(blurHash: blurHash, size: CGSize(width: 40, height: 40)) {
                            renderedImage = blurHashImage
                            blurHashRendered = true
                        }
                    }
                }

            if isLoading {
                HStack {
                    Spacer()
                    ProgressView()
                    Spacer()
                }
            }
        }
        .onReceive(image.$isDefault) { value in
            updateOpacity(isDefault: value)
        }
        .onReceive(image.$isLoading) { value in
            self.isLoading = value
            updateOpacity(isDefault: image.isDefault)
        }
        .onReceive(image.$value) { value in
            if let value = value {
                renderedImage = value
            }
        }
    }
}

struct AsyncIcon: View {
    @Binding var icon: Icon
    @State var image = ObservableUIImage.createDefault()

    var body: some View {
        AsyncImageBis(image: image, hideDefault: true)
            .onAppear {
                image = icon.uiImage()
            }
            .onReceive(icon.$image) { iconImage in
                if let value = iconImage.value {
                    image = icon.uiImage(value)
                }
            }
    }
}

struct AsyncPicture_Previews: PreviewProvider {
    static var previews: some View {
        AsyncImageBis(image: ObservableUIImage.defaultValue, blurHash: "")
        //        AsyncPicture(url: URL(string:  "https://new.opengreenmap.org/api-v1/pictures/5e4f6343fb54b301005d420e/picture/lg")!)
    }
}
