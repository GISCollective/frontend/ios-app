//
//  Picture.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 24.11.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import SwiftUI

struct BlurPicture: View {
    var seed: String?

    init(id: String) {
        seed = id
    }

    func defaultImage() -> UIImage {
        return UIImage(blurHash: "UKO2?V%2Tw=w]~RBVZRi};RPxuwHtLOtxZ%g", size: CGSize(width: 40, height: 40))!
    }

    var body: some View {
        content
    }

    private var content: some View {
        Image(uiImage: defaultImage())
            .resizable()
            .aspectRatio(1.0, contentMode: .fit)
            .clipped()
    }
}
