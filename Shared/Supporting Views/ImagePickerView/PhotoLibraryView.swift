//
//  PhotoLibraryView.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 27.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import PhotosUI
import SwiftUI

struct PhotoLibraryView: UIViewControllerRepresentable {
    typealias UIViewControllerType = PHPickerViewController
    @Binding var pictures: [PickedMediaItem]
    @State var selectionLimit = 0

    var didFinishPicking: (_ didSelectItems: Bool) -> Void

    func makeUIViewController(context: Context) -> PHPickerViewController {
        var config = PHPickerConfiguration()
        config.filter = .any(of: [.images, .livePhotos])
        config.selectionLimit = selectionLimit
        config.preferredAssetRepresentationMode = .current

        let controller = PHPickerViewController(configuration: config)

        controller.delegate = context.coordinator

        return controller
    }

    func updateUIViewController(_: PHPickerViewController, context _: Context) {}

    func makeCoordinator() -> Coordinator {
        Coordinator(with: self)
    }

    class Coordinator: PHPickerViewControllerDelegate {
        var photoLibrary: PhotoLibraryView
        init(with photoLibrary: PhotoLibraryView) {
            self.photoLibrary = photoLibrary
        }

        func picker(_: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            photoLibrary.didFinishPicking(!results.isEmpty)
            guard !results.isEmpty else {
                return
            }
            results.forEach { result in
                let itemProvider = result.itemProvider
                self.getPhoto(from: itemProvider)
            }
        }

        private func getPhoto(from itemProvider: NSItemProvider) {
            if itemProvider.canLoadObject(ofClass: UIImage.self) {
                itemProvider.loadObject(ofClass: UIImage.self) { object, error in
                    if let error = error {
                        print(error.localizedDescription)
                    }

                    if let image = object as? UIImage {
                        DispatchQueue.main.async {
                            self.photoLibrary.pictures.append(PickedMediaItem(with: image))
                        }
                    }
                }
            }
        }
    }
}

struct PickedMediaItem: Identifiable {
    var item: ObservableUIImage
    var id: String

    init(with item: UIImage) {
        id = UUID().uuidString
        self.item = ObservableUIImage(resolved: item)
    }
}
