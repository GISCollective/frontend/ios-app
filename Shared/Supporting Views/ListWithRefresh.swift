//
//  ListWithRefresh.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 23.04.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

struct ListWithRefresh<Content: View, M, ML: CrateModelList, MI: CrateModelItem>: UIViewRepresentable where ML.M == M, MI.M == M {
    private var title: String
    private var factory: (_: M) -> Content
    @ObservedObject private var list: ListData<M, ML, MI>

    init(title: String, list: ListData<M, ML, MI>, factory: @escaping (_: M) -> Content) {
        self.title = title
        self.list = list
        self.factory = factory
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self, factory: factory)
    }

    func makeUIView(context: Context) -> UITableView {
        return context.coordinator.tableView!
    }

    func updateUIView(_ uiView: UITableView, context _: Context) {
        uiView.reloadData()
    }

    class Coordinator: UITableViewController {
        var control: ListWithRefresh
        var factory: (_: M) -> Content

        lazy var _refreshControl: UIRefreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(self.handleRefresh(_:)),
                for: UIControl.Event.valueChanged)

            return refreshControl
        }()

        @available(*, unavailable)
        required init?(coder _: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        init(_ control: ListWithRefresh, factory: @escaping (_: M) -> Content) {
            self.control = control
            self.factory = factory
            super.init(style: .plain)

            tableView.register(HostingCell<Content>.self, forCellReuseIdentifier: "HostingCell")
        }

        override func viewDidLoad() {
            tableView.addSubview(_refreshControl)
        }

        @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
            Task {
                _ = try await control.list.reload()
                self.tableView.reloadData()
                refreshControl.endRefreshing()
            }
        }

        override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
            return control.list.data.count
        }

        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HostingCell", for: indexPath)

            if let hostingCell = cell as? HostingCell<Content> {
                let model = control.list.data[indexPath.item]
                hostingCell.set(rootView: factory(model), parentController: self)
            }

            return cell
        }

        override func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if control.list.isLoading == true {
                return
            }

            let offsetY = scrollView.contentOffset.y
            let contentHeight = scrollView.contentSize.height

            if offsetY > contentHeight - scrollView.frame.height * 3 {
                // control.list.loadNextPage()
            }
        }
    }
}

final class HostingCell<Content: View>: UITableViewCell {
    private let hostingController = UIHostingController<Content?>(rootView: nil)

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        hostingController.view.backgroundColor = .clear
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func set(rootView: Content, parentController: UIViewController) {
        hostingController.rootView = rootView
        hostingController.view.invalidateIntrinsicContentSize()

        let requiresControllerMove = hostingController.parent != parentController
        if requiresControllerMove {
            parentController.addChild(hostingController)
        }

        if !contentView.subviews.contains(hostingController.view) {
            contentView.addSubview(hostingController.view)
            hostingController.view.translatesAutoresizingMaskIntoConstraints = false
            hostingController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
            hostingController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
            hostingController.view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            hostingController.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        }

        if requiresControllerMove {
            hostingController.didMove(toParent: parentController)
        }
    }
}
