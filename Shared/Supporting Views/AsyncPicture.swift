//
//  AsyncPicture.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 11.05.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct AsyncPicture: View {
    @ObservedObject var picture: Picture
    @State var size: String?

    var body: some View {
        UrlAsyncImage(url: $picture.picture, blurHash: $picture.hash, size: size ?? "md")
    }
}

//
// struct AsyncPicture_Previews: PreviewProvider {
//    static var previews: some View {
//        AsyncPicture()
//    }
// }
