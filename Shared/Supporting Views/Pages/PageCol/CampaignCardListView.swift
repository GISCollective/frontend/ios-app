//
//  CampaignCartListView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 04.03.22.
//

import SwiftUI

struct CampaignCardListView: View {
    @State var data: [String: Json] = [:]
    @StateObject var model = CampaignListData()

    @ObservedObject var locationManager = LocationManager.instance!
    var columns: [GridItem] = Array(repeating: .init(.adaptive(minimum: 150), alignment: .top), count: 2)

    var body: some View {
        VStack {
            if model.data.count > 0 {
                LazyVGrid(columns: columns, alignment: .center) {
                    ForEach(self.model.data) { model in
                        CampaignRow(campaign: model)
                    }
                }
                .refreshable {}
            } else if model.isLoading {
                Spacer()
                ProgressView()
                Spacer()
            } else {
                Spacer()
                Text("There are no public campaigns")
                Spacer()
            }
        }
        .onAppear {
            self.model.longitude = locationManager.pinnedLongitude
            self.model.latitude = locationManager.pinnedLatitude
        }
        .task {
            _ = try? await self.model.reload()
        }
    }
}
