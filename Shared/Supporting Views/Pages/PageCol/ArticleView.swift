//
//  ArticleView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 04.03.22.
//

import SwiftUI

struct ArticleView: View {
    @State var data: [String: Json] = [:]
    @State var blocks: ArticleBody?

    @ObservedObject var locationManager = LocationManager.instance!

    var body: some View {
        VStack {
            if let blockContainer = blocks?.blockContainer {
                EditorJsBlocksView(blockContainer: blockContainer)
            }
        }
        .task {
            if case let .String(id) = data["id"] {
                let article = try? await Store.instance.articles.getById(id: id)

                blocks = article?.content
            }
        }
    }
}

// struct ArticleView_Previews: PreviewProvider {
//    static var previews: some View {
//        ArticleView()
//    }
// }
