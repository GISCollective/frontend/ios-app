//
//  Page.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 25.02.22.
//

import SwiftUI

struct PageView: View {
    @StateObject var page: Page
    @State private var cols: [PageCol] = []
    @State private var layoutContainers: [LayoutContainer] = []

    var body: some View {
        ScrollView {
            LayoutView(layoutContainers: $layoutContainers) { name in
                HStack {
                    PageColView(name: name, cols: $cols)
                }
            }
        }
        .onReceive(page.$cols) { value in
            cols = value
        }
        .onReceive(page.$layoutContainers) { value in
            layoutContainers = value ?? []
        }
    }
}

//
// struct Page_Previews: PreviewProvider {
//    static var previews: some View {
//        Page(page: Page(id: ""))
//    }
// }
