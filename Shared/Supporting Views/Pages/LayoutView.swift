//
//  LayoutView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 03.03.22.
//

import Foundation
import SwiftUI

struct LayoutView<Content: View>: View {
    @Binding var layoutContainers: [LayoutContainer]
    let content: (_ name: String) -> Content

    var body: some View {
        VStack {
            ForEach(layoutContainers.indices, id: \.self) { containerIndex in
                VStack {
                    if let rows = layoutContainers[containerIndex].rows {
                        ForEach(rows.indices) { rowIndex in
                            VStack {
                                if let cols = rows[rowIndex].cols {
                                    ForEach(cols.indices) { colIndex in
                                        if let name = cols[colIndex].name {
                                            self.content(name)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                .padding(.leading, 10)
                .padding(.trailing, 10)
            }
        }
    }
}
