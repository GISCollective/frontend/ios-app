//
//  PageColView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 04.03.22.
//

import SwiftUI

struct PageColView: View {
    @State var name: String
    @Binding var cols: [PageCol]

    @State private var selectedCol: PageCol = .init()

    var body: some View {
        VStack {
            if selectedCol.type == "campaign-card-list" {
                CampaignCardListView(data: selectedCol.data ?? [:])
            }

            if selectedCol.type == "article" {
                ArticleView(data: selectedCol.data ?? [:])
            }
        }
        .onChange(of: cols) { value in
            selectedCol = value.first(where: { $0.name ?? "" == name }) ?? PageCol()
        }
        .onAppear {
            selectedCol = cols.first(where: { $0.name ?? "" == name }) ?? PageCol()
        }
    }
}
