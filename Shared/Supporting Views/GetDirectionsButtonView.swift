//
//  GetDirectionsButtonView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 18.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import GEOSwift
import SwiftUI

struct GetDirectionsButtonView: View {
    @Binding var name: String
    @Binding var position: Geometry

    var body: some View {
        PrimaryButton(action: { getDirections(name: name, position: position) }, label: "Get directions")
    }
}

// struct GetDirectionsButtonView_Previews: PreviewProvider {
//    static var previews: some View {
//        GetDirectionsButtonView(name: "name", position: Geometry.point(Point(x: 1, y: 1)))
//    }
// }
