//
//  TextInput.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 30.09.21.
//

import Combine
import SwiftUI

struct TextInput: View {
    @State var label: String
    @State var placeholder: String = ""
    @Binding var value: String

    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            Text(label)
                .font(.subheadline)
                .padding(.bottom, 5)
                .foregroundColor(.accentColor)
            TextField(placeholder, text: $value)
                .padding(15)
                .background(Color(red: 0.5, green: 0.5, blue: 0.5, opacity: 0.2))
                .cornerRadius(10)
        }
    }
}

struct TextInputWithProps: View {
    @ObservedObject var props: EditFieldProps
    @State var placeholder: String = ""

    var body: some View {
        TextInput(label: props.label, placeholder: placeholder, value: $props.value)
    }
}

struct TextInput_Previews: PreviewProvider {
    static var previews: some View {
        TextInput(label: "Label", value: Binding(get: { "value" }, set: { _ in }))
    }
}

class EditFieldProps: ObservableObject, Hashable {
    @Published var label: String
    @Published var value: String
    var cancelable: Cancellable?

    init(label: String, value: String?) {
        self.label = label
        self.value = value ?? ""
    }

    init(label: String, bindedValue: Binding<String?>) {
        self.label = label
        value = bindedValue.wrappedValue ?? ""

        cancelable = $value
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { value in
                bindedValue.wrappedValue = value
            })
    }

    init(label: String, bindedValue: Binding<String>) {
        self.label = label
        value = bindedValue.wrappedValue

        cancelable = $value
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { value in
                bindedValue.wrappedValue = value
            })
    }

    static func == (lhs: EditFieldProps, rhs: EditFieldProps) -> Bool {
        return lhs.label == rhs.label
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(label)
    }
}
