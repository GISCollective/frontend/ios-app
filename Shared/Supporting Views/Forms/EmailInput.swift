//
//  EmailInput.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 24.10.21.
//

import SwiftUI

struct EmailInput: View {
    @Binding var value: String

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Image(systemName: "envelope.fill")
                TextField("E-Mail", text: $value)
                    .textContentType(.emailAddress)
                    .keyboardType(.emailAddress)
            }
            .padding(15)
            .background(Color(red: 0.5, green: 0.5, blue: 0.5, opacity: 0.2))
            .cornerRadius(10)
        }
    }
}

struct EmailInput_Previews: PreviewProvider {
    static var previews: some View {
        EmailInput(value: Binding<String>(get: {
            "asd@asd.asd"
        }, set: { _ in }))
    }
}
