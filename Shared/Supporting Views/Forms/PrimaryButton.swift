//
//  PrimaryButton.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 10.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct PrimaryButton: View {
    @State var action: () async throws -> Void
    @State var label: String
    @State private var isDisabled = false

    init(action: @escaping () -> Void, label: String) {
        self.action = action
        self.label = label
    }

    init(action: @escaping () async -> Void, label: String) {
        self.action = action
        self.label = label
    }

    init(action: @escaping () async throws -> Void, label: String) {
        self.action = action
        self.label = label
    }

    func handleAction() async throws {
        isDisabled = true

        try await action()

        DispatchQueue.main.async {
            isDisabled = false
        }
    }

    var body: some View {
        TaskButton(action: handleAction) {
            HStack {
                Spacer()

                Text(label)
                    .fontWeight(.semibold)
                    .font(.headline)
                    .padding()

                Spacer()
            }
            .foregroundColor(.white)
            .background(Color.blue)
            .cornerRadius(15)
            .disabled(isDisabled)
        }
    }
}

struct PrimaryButton_Previews: PreviewProvider {
    static var previews: some View {
        PrimaryButton(action: {}, label: "Submit")
    }
}
