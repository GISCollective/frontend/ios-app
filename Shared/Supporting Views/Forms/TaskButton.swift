//
//  TaskButton.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 17.06.22.
//

import SwiftUI

struct TaskButton<Content: View>: View {
    let content: () -> Content

    @State var action: () async throws -> Void
    @State private var isDisabled = false
    @State private var showingAlert = false
    @State private var errorMessage = ""

    init(action: @escaping () -> Void, @ViewBuilder content: @escaping () -> Content) {
        self.action = action
        self.content = content
    }

    init(action: @escaping () async -> Void, @ViewBuilder content: @escaping () -> Content) {
        self.action = action
        self.content = content
    }

    init(action: @escaping () async throws -> Void, @ViewBuilder content: @escaping () -> Content) {
        self.action = action
        self.content = content
    }

    func handleAction() {
        isDisabled = true

        Task {
            do {
                try await self.action()
            } catch {
                DispatchQueue.main.async {
                    self.errorMessage = error.localizedDescription
                    self.showingAlert = true
                }
            }

            DispatchQueue.main.async {
                isDisabled = false
            }
        }
    }

    var body: some View {
        Button(action: handleAction) {
            self.content()
        }
        .alert(isPresented: $showingAlert) {
            Alert(title: Text("Error"), message: Text(errorMessage), dismissButton: .default(Text("Got it!")))
        }
    }
}

struct TaskButton_Previews: PreviewProvider {
    static var previews: some View {
        TaskButton(action: {}) {
            Text("hello")
        }
    }
}
