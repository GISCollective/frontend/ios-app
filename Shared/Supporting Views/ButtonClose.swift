//
//  ButtonClose.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 16.11.21.
//

import SwiftUI

struct ButtonClose: View {
    var action: () -> Void

    var body: some View {
        Button(action: action) {
            Image(systemName: "xmark")
        }
        .foregroundColor(Color(light: .white, dark: .black))
        .padding(.horizontal, 12)
        .padding(.vertical, 12)
        .background(
            RoundedRectangle(
                cornerRadius: 25,
                style: .continuous
            )
        )
    }
}

struct ButtonCloseStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label.padding().modifier(MakeSquareBounds()).background(Circle().fill(Color.blue))
    }
}

struct MakeSquareBounds: ViewModifier {
    @State var size: CGFloat = 1000
    func body(content: Content) -> some View {
        let c = ZStack {
            content.alignmentGuide(HorizontalAlignment.center) { vd -> CGFloat in
                DispatchQueue.main.async {
                    self.size = max(vd.height, vd.width)
                }
                return vd[HorizontalAlignment.center]
            }
        }
        return c.frame(width: size, height: size)
    }
}

struct ButtonClose_Previews: PreviewProvider {
    static var previews: some View {
        ButtonClose(action: {})
    }
}
