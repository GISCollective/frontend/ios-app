//
//  MapMarker.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 24.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import Foundation
import MapKit
import UIKit

class MapIconMarker: MKMarkerAnnotationView {
    private var cancellables = Set<AnyCancellable>()
    var feature: Feature? {
        willSet {
            cancellables.forEach { $0.cancel() }
        }

        didSet {
            feature?.$firstIcons.sink { icon in
                self.icon = icon
            }.store(in: &cancellables)
        }
    }

    var observableImage: ObservableUIImage? {
        didSet {
            observableImage?.$value.sink { image in
                let size = CGFloat(self.icon?.styles?.types?.site?.size ?? 25)
                self.glyphImage = image?.resized(to: CGSize(width: size, height: size))
                self.setupUi()
            }.store(in: &cancellables)
        }
    }

    var icon: Icon? {
        didSet {
            if let icon = icon {
                icon.$image.sink { image in
                    if let value = image.value {
                        self.observableImage = icon.uiImage(value)
                    }
                }
                .store(in: &cancellables)
            }

            setupUi()
        }
    }

    private func setupUi() {
        let color = UIColor.parseColor(value: icon?.styles?.types?.site?.backgroundColor ?? "")

        markerTintColor = color.withAlphaComponent(1)
    }
}
