//
//  SFSafariViewWrapper.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 24.10.21.
//

import SwiftUI

import SafariServices
struct SFSafariViewWrapper: UIViewControllerRepresentable {
    let url: URL

    func makeUIViewController(context _: UIViewControllerRepresentableContext<Self>) -> SFSafariViewController {
        return SFSafariViewController(url: url)
    }

    func updateUIViewController(_: SFSafariViewController, context _: UIViewControllerRepresentableContext<SFSafariViewWrapper>) {}
}
