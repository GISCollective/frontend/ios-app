//
//  HorisontalHandlerIcon.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 16.11.21.
//

import SwiftUI

struct HorisontalHandlerIcon: View {
    var body: some View {
        HStack {
            Spacer()
            Rectangle()
                .fill(Color.red)
                .frame(width: 50, height: 10)
            Spacer()
        }
    }
}

struct HorisontalHandlerIcon_Previews: PreviewProvider {
    static var previews: some View {
        HorisontalHandlerIcon()
    }
}
