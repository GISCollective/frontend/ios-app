import SwiftUI

struct PageSwiperView<Page: View>: View {
    var pages: [Page]
    @State private var currentPage = 0

    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            if pages.count == 1 {
                pages[0]
            }
            if pages.count > 1 {
                PageViewController(pages: pages, currentPage: $currentPage)
                PageControl(numberOfPages: pages.count, currentPage: $currentPage)
                    .frame(width: CGFloat(pages.count * 18))
            }
        }
    }
}

// struct PageView_Previews: PreviewProvider {
//    static var previews: some View {
//        Group {
//            PageView(pages: featureData[0].pictures?.filter { $0 != nil } .map {
//                AsyncImage(image: $0.uiImage(),
//                           blurHash: $0.hash ?? "")
//
//            })
//            PageView(pages: featureData[1].pictures?.filter { $0 != nil } .map {
//                AsyncImage(image: $0.uiImage(),
//                           blurHash: $0.hash ?? "")
//
//            })
//        }
//    }
// }
