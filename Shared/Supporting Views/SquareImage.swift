//
//  SquareImage.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 05.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import SwiftUI

struct SquareImage: View {
    var image: Image

    var body: some View {
        image
            .clipShape(RoundedRectangle(cornerRadius: 15, style: .continuous))
    }
}

struct SquareImage_Previews: PreviewProvider {
    static var previews: some View {
        SquareImage(image: Image("turtlerock"))
    }
}
