//
//  ArticleHeader.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 07.02.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI
import WebKit

struct ArticleParagraph: View {
    @State var paragraph: ParagraphBlock

    var body: some View {
        ArticleText(text: paragraph.text)
            .padding(.bottom, 20)
    }
}
