//
//  ArticleHeader.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 07.02.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct ArticleHeader: View {
    var header: HeaderBlock

    var body: some View {
        if header.level == 1 {
            Text(header.text)
                .font(.title)
        }

        if header.level == 2 {
            Text(header.text)
                .font(.system(size: 20, weight: .bold, design: .default))
        }

        if header.level == 3 {
            Text(header.text)
                .font(.system(size: 20, weight: .thin, design: .default))
        }

        if header.level == 4 {
            Text(header.text)
                .font(.system(size: 16, weight: .bold, design: .default))
        }

        if header.level == 5 {
            Text(header.text)
                .font(.system(size: 16, weight: .thin, design: .default))
        }

        if header.level == 6 {
            Text(header.text)
                .font(.system(size: 14, weight: .semibold, design: .default))
        }
    }
}

struct ArticleHeader_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            ArticleHeader(header: HeaderBlock(text: "Level 1", level: 1))
            ArticleHeader(header: HeaderBlock(text: "Level 2", level: 2))
            ArticleHeader(header: HeaderBlock(text: "Level 3", level: 3))
            ArticleHeader(header: HeaderBlock(text: "Level 4", level: 4))
            ArticleHeader(header: HeaderBlock(text: "Level 5", level: 5))
            ArticleHeader(header: HeaderBlock(text: "Level 6", level: 6))
        }
    }
}
