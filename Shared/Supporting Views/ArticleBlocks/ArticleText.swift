//
//  ArticleHeader.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 07.02.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI
import WebKit

struct ArticleText: View {
    @State var text: StyledParagraphText

    func concatenatedTexts() -> Text {
        var result = Text("")

        text.pieces.forEach { styledText in
            var text = Text(styledText.text)

            if styledText.isBold {
                text = text.bold()
            }

            if styledText.isItalic {
                text = text.italic()
            }

            result = result + text
        }

        return result
    }

    var body: some View {
        self.concatenatedTexts()
    }
}
