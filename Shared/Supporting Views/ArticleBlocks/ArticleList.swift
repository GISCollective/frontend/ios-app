//
//  ArticleList.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 18.11.21.
//

import SwiftUI

struct ArticleListView: View {
    @State var list: ListBlock

    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            ForEach(list.styledItems, id: \.self) {
                ArticleListViewItem(text: $0)
            }
        }
        .padding(.bottom, 20)
    }
}

struct ArticleListViewItem: View {
    @State var text: StyledParagraphText

    var body: some View {
        HStack {
            Text("- ")
            ArticleText(text: text)
        }
    }
}

struct ArticleListView_Previews: PreviewProvider {
    static var previews: some View {
        ArticleListView(list: ListBlock(items: ["item1", "item2", "item3"]))
    }
}
