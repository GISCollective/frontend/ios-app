//
//  PositionStatus.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 11.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct PositionStatusView: View {
    @ObservedObject var locationManager = LocationManager.instance!

    static let connectingGradientStart = Color(red: 148 / 255, green: 180 / 255, blue: 148 / 255)
    static let connectingGradientEnd = Color(red: 148 / 255, green: 148 / 255, blue: 148 / 255)

    static let connectedGradientStart = Color(red: 25 / 255, green: 115 / 255, blue: 182 / 255)
    static let connectedGradientEnd = Color(red: 25 / 255, green: 154 / 255, blue: 182 / 255)

    static let errorGradientStart = Color(red: 115 / 255, green: 25 / 255, blue: 25 / 255)
    static let errorGradientEnd = Color(red: 154 / 255, green: 25 / 255, blue: 25 / 255)

    @State var size: CGFloat = 20
    @State var extraSize: CGFloat = 20
    @State var gradientStart = connectingGradientStart
    @State var gradientEnd = connectingGradientEnd

    @State var opacity: Double = 0
    @State var timer = Timer.publish(every: 2, on: .main, in: .common).autoconnect()

    var body: some View {
        HStack(alignment: .center) {
            ZStack(alignment: .center) {
                Circle()
                    .fill(LinearGradient(
                        gradient: Gradient(colors: [gradientStart, gradientEnd]),
                        startPoint: UnitPoint(x: 0.5, y: 0),
                        endPoint: UnitPoint(x: 0.5, y: 0.6)
                    ))
                    .shadow(color: .white, radius: 5)
                    .frame(width: size, height: size)

                Circle()
                    .fill(LinearGradient(
                        gradient: Gradient(colors: [gradientStart, gradientEnd]),
                        startPoint: UnitPoint(x: 0.5, y: 0),
                        endPoint: UnitPoint(x: 0.5, y: 0.6)
                    ))
                    .opacity(opacity)
                    .frame(width: size + extraSize, height: size + extraSize)
                    .onReceive(timer) { _ in
                        extraSize = 0
                        opacity = 0.8

                        withAnimation(.easeIn(duration: 2)) {
                            extraSize = 20
                            opacity = 0
                        }
                    }
                    .onReceive(locationManager.$isPinned) { _ in
                        if locationManager.isPinned {
                            gradientStart = PositionStatusView.connectedGradientStart
                            gradientEnd = PositionStatusView.connectedGradientEnd
                        }

                        if !locationManager.isPinned {
                            gradientStart = PositionStatusView.connectingGradientStart
                            gradientEnd = PositionStatusView.connectingGradientEnd
                        }

                        if locationManager.isError {
                            gradientStart = PositionStatusView.errorGradientStart
                            gradientEnd = PositionStatusView.errorGradientEnd
                        }
                    }
            }
            .frame(width: size, height: size)

            Text(locationManager.statusMessage)
                .shadow(color: .white, radius: 3)
                .font(.footnote)
        }
    }
}

struct PositionStatus_Previews: PreviewProvider {
    static var previews: some View {
        PositionStatusView(locationManager: LocationManager())
    }
}
