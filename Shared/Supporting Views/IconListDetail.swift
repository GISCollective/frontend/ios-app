//
//  IconList.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 05.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import SDWebImageSwiftUI
import SwiftUI

struct IconListDetail: View {
    @Binding var icons: [Icon]
    var size: Int
    var spacing: Int

    @State var frameHeight = CGFloat(50)

    private func saveHeight(rectangle: CGRect) {
        let height = getHeight(iconsCount: icons.count, size: size, spacing: spacing, bucketRect: rectangle)
        frameHeight = height
    }

    var body: some View {
        GeometryReader { geometry in
            IconListView(icons: $icons, size: self.size, spacing: self.spacing)
                .onAppear {
                    self.saveHeight(rectangle: CGRect(x: CGFloat(0),
                                                      y: CGFloat(0),
                                                      width: geometry.size.width,
                                                      height: geometry.size.height))
                }
        }
        .frame(height: self.frameHeight)
    }
}

// struct IconListDetail_Previews: PreviewProvider {
//    static var previews: some View {
//        IconListView(icons: featureData[1].icons, size: 40, spacing: 15)
//            .frame(width: 360, height: 300)
//    }
// }
