//
//  ForgotPasswordForm.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 24.10.21.
//

import SwiftUI

enum ResetPasswordStates {
    case ready,
         loading,
         done,
         disconnected
}

struct ForgotPasswordForm: View {
    @State var email: String = ""
    @Binding var isOpen: Bool
    @State var state: ResetPasswordStates = .ready
    @ObservedObject var userSession = UserSessionService.instance

    internal let inspection = Inspection<Self>()

    func handleResetPassword() {
        Task {
            state = .loading
            _ = try await userSession.resetPassword(email: email)

//        { _, _, status in
//            if status == .disconnected {
//                state = .disconnected
//            } else {
//                state = .done
//            }
//        }

            state = .done
        }
    }

    var body: some View {
        VStack {
            Spacer()
            if state == .loading {
                Text("Please wait")
                ProgressView()
            } else {
                Text("Forgot your password?").font(.title)

                Spacer()

                if state == .done {
                    Text("If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.")
                }

                if state == .disconnected {
                    HStack {
                        Spacer()
                        Text("Please connect to the internet and try again.")
                            .foregroundColor(Color.red)
                        Spacer()
                    }
                }

                if state == .ready || state == .disconnected {
                    EmailInput(value: $email)
                    PrimaryButton(action: handleResetPassword, label: "Reset password")
                }

                Spacer()

                Button("Back to login", action: {
                    isOpen = false
                })
            }
            Spacer()
        }
        .padding()
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct ForgotPasswordForm_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordForm(isOpen: Binding(get: { true }, set: { _ in }))
    }
}
