//
//  RegisterAccountButton.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 24.10.21.
//

import SwiftUI

struct RegisterAccountButton: View {
    @State var isVisible: Bool = false

    var body: some View {
        VStack(alignment: .center) {
            Text("You don't have an account?")
            Button("Register here.", action: {
                isVisible = true
            })
        }
        .sheet(isPresented: $isVisible) {
            RegisterAccountForm(isVisible: $isVisible)
        }
    }
}

struct RegisterAccountButton_Previews: PreviewProvider {
    static var previews: some View {
        RegisterAccountButton()
    }
}
