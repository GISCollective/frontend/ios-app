//
//  ForgotPasswordButton.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 24.10.21.
//

import SwiftUI

struct ForgotPasswordButton: View {
    @State var isVisible: Bool = false

    internal let inspection = Inspection<Self>()

    var body: some View {
        Button("Forgot your password?", action: {
            isVisible = true
        })
        .sheet(isPresented: $isVisible) {
            ForgotPasswordForm(isOpen: $isVisible)
        }
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
}

struct ForgotPasswordButton_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordButton()
    }
}
