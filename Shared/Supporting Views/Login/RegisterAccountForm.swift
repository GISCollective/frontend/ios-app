//
//  RegisterAccountForm.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 24.10.21.
//

import SwiftUI

struct RegisterAccountForm: View {
    @Binding var isVisible: Bool

    var body: some View {
        VStack {
            SFSafariViewWrapper(url: RemoteConnection.instance.registrationUrl)
        }
    }
}

//
// struct RegisterAccountForm_Previews: PreviewProvider {
//    static var previews: some View {
//        //RegisterAccountForm()
//    }
// }
