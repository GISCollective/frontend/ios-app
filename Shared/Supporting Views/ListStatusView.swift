//
//  ListStatusView.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 15.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import SwiftUI

struct ListStatusView: View {
    static let visibileTop: CGFloat = 80
    static let hiddenTop: CGFloat = -80

    @State var positionStatusTop: CGFloat = visibileTop
    @ObservedObject var locationManager = LocationManager.instance!

    var body: some View {
        GeometryReader { geometry in
            HStack(alignment: /*@START_MENU_TOKEN@*/ .center/*@END_MENU_TOKEN@*/) {
                Spacer()
                PositionStatusView()
                Spacer()
            }
            .frame(width: geometry.size.width, height: geometry.size.height, alignment: .top)
            .padding(.top, positionStatusTop)
            .onReceive(locationManager.$isPinned, perform: { isPinned in
                withAnimation(.easeInOut(duration: 1)) {
                    if isPinned {
                        positionStatusTop = ListStatusView.hiddenTop
                    } else {
                        positionStatusTop = ListStatusView.visibileTop
                    }
                }
            })
        }
    }
}

struct ListStatusView_Previews: PreviewProvider {
    static var previews: some View {
        ListStatusView(locationManager: LocationManager())
    }
}
