//
//  UserProfileCard.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 27.09.21.
//

import SwiftUI

struct UserProfileCard: View {
    @ObservedObject var profile: UserProfile
    @State var showingSheet: Bool = false

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Image(systemName: "minus")
                    .foregroundColor(Color.gray)
                Text(profile.fullName())
                    .lineLimit(1)
                    .font(.callout)
                    .foregroundColor(Color.primary)
                Spacer()
            }

            if let bio = profile.bio, bio.trim() != "" {
                Text(bio)
                    .lineLimit(2)
                    .font(.caption)
                    .foregroundColor(Color.secondary)
                    .padding(.leading, 25)
            }
        }
        .onTapGesture {
            showingSheet = true
        }
        .sheet(isPresented: $showingSheet, content: {
            ProfileView(profile: profile)
        })
    }
}

struct UserProfileCard_Previews: PreviewProvider {
    static var profile = UserProfile(id: "")

    static var previews: some View {
        UserProfileCard(profile: profile)
            .onAppear {
                profile.firstName = "Bogdan"
                profile.lastName = "Szabo"
                profile.salutation = "Mr."
                profile.title = "Dr."
                profile.picture = Picture(id: "")
                profile.bio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi"
                profile.picture?.picture = "https://new.opengreenmap.org/api-v1/pictures/6054983e09891701003ac476/picture"
            }
    }
}
