//
//  GetDirections.swift
//  GISCollective
//
//  Created by Alexandra Casapu on 21.01.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import GEOSwift
import MapKit
import UIKit

func getDirections(name: String, position: Geometry) {
    if let coordinate = try? position.centroid().toCLLocationCoordinate2D() {
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
        mapItem.name = name
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDefault])
    }
}
