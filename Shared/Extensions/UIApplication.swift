//
//  UIApplication.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 21.09.21.
//

import Foundation
import SwiftUI

extension UIApplication {
    func hideKeyboard() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
