//
//  GEOSwiftGeometry.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 30.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import GEOSwift
import MapKit

extension GEOSwift.Geometry {
    func firstPoint() -> CLLocationCoordinate2D {
        if case let .lineString(line) = self {
            return CLLocationCoordinate2D(latitude: line.firstPoint.y, longitude: line.firstPoint.x)
        }

        if case let .multiLineString(line) = self {
            let validLines = line.lineStrings.filter { $0.points.count > 0 }

            if validLines.count > 0 {
                return CLLocationCoordinate2D(latitude: validLines[0].firstPoint.y, longitude: validLines[0].firstPoint.x)
            }
        }

        return CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }

    func centroidCoordinate() -> CLLocationCoordinate2D {
        let center = try? centroid()

        return CLLocationCoordinate2D(latitude: center?.y ?? 0, longitude: center?.x ?? 0)
    }
}
