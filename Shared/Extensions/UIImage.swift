//
//  UIImage.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 24.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func resized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}
