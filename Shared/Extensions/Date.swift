//
//  Date.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 12.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

extension Date {
    func toISOString() -> String {
        let isoDateFormatter = ISO8601DateFormatter()
        let timestamp = isoDateFormatter.string(from: self)
        return timestamp
    }

    static func fromISOString(_ string: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        let result = dateFormatter.date(from: string)

        if result != nil {
            return result
        }

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

        return dateFormatter.date(from: string)
    }
}
