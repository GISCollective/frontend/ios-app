//
//  UIColor.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 28.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

import Foundation
import UIKit

extension UIColor {
    static func parseColor(value: String?) -> UIColor {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 1

        if let value = value {
            if value == "transparent" {
                red = 1
                green = 1
                blue = 1
                alpha = 0
            }

            if value.hasPrefix("#") {
                let start = value.index(value.startIndex, offsetBy: 1)
                let hexColor = String(value[start...])

                if hexColor.count == 6 {
                    let scanner = Scanner(string: hexColor)
                    var hexNumber: UInt64 = 0

                    if scanner.scanHexInt64(&hexNumber) {
                        red = CGFloat((hexNumber & 0x00FF_0000) >> 16) / 255
                        green = CGFloat((hexNumber & 0x0000_FF00) >> 8) / 255
                        blue = CGFloat(hexNumber & 0x0000_00FF) / 255
                    }
                }

                if hexColor.count == 8 {
                    let scanner = Scanner(string: hexColor)
                    var hexNumber: UInt64 = 0

                    if scanner.scanHexInt64(&hexNumber) {
                        red = CGFloat((hexNumber & 0xFF00_0000) >> 24) / 255
                        green = CGFloat((hexNumber & 0x00FF_0000) >> 16) / 255
                        blue = CGFloat((hexNumber & 0x0000_FF00) >> 8) / 255
                        alpha = CGFloat(hexNumber & 0x0000_00FF) / 255
                    }
                }
            }

            if value.hasPrefix("rgb") {
                let strColors = value
                    .replacingOccurrences(of: "rgba", with: "")
                    .replacingOccurrences(of: "rgb", with: "")
                    .replacingOccurrences(of: "(", with: "")
                    .replacingOccurrences(of: ")", with: "")
                    .split(separator: ",")
                    .map { $0.trimmingCharacters(in: .whitespacesAndNewlines) }

                let colors = strColors
                    .map { Double($0) ?? 0 }

                red = CGFloat(colors[0]) / 255
                green = CGFloat(colors[1]) / 255
                blue = CGFloat(colors[2]) / 255

                if colors.count > 3 {
                    alpha = CGFloat(colors[3])
                }
            }
        }

        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }

    convenience init(light: UIColor, dark: UIColor) {
        self.init { traitCollection in
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                return light
            case .dark:
                return dark
            @unknown default:
                return light
            }
        }
    }
}
