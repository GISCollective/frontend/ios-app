//
//  String.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 14.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

extension String {
    func trim() -> String {
        return trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
