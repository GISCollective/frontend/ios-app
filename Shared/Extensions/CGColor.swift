//
//  CGColor.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 24.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import UIKit

extension CGColor {
    static func parseColor(value: String) -> CGColor {
        let color = UIColor.parseColor(value: value)

        return color.cgColor
    }
}
