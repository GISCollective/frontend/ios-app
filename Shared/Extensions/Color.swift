//
//  Color.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 07.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import SwiftUI

extension Color {
    static func parseColor(value: String) -> Color {
        let color = UIColor.parseColor(value: value)

        return Color(color)
    }

    init(light: Color, dark: Color) {
        self.init(UIColor(light: UIColor(light), dark: UIColor(dark)))
    }
}
