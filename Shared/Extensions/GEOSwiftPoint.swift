//
//  GEOSwiftPoint.swift
//  GISCollective
//
//  Created by Bogdan Szabo on 30.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import GEOSwift
import MapKit

extension GEOSwift.Point {
    func toCLLocationCoordinate2D() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: y, longitude: x)
    }
}
