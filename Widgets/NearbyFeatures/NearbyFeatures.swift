//
//  NearbyFeatures.swift
//  NearbyFeatures
//
//  Created by Bogdan Szabo on 29.09.21.
//

import Combine
import GEOSwift
import Intents
import SwiftUI
import WidgetKit

class WidgetLocationManager: NSObject, CLLocationManagerDelegate {
    var locationManager: CLLocationManager?
    private var handler: ((CLLocation) -> Void)?

    override init() {
        super.init()

        print("setup \(RemoteConnection.instance.name) at \(RemoteConnection.instance.url)")

        DispatchQueue.main.async {
            self.locationManager = CLLocationManager()
            self.locationManager!.delegate = self
            if self.locationManager!.authorizationStatus == .notDetermined {
                self.locationManager!.requestWhenInUseAuthorization()
            }
        }
    }

    func fetchLocation(handler: @escaping (CLLocation) -> Void) {
        self.handler = handler
        locationManager!.requestLocation()
    }

    func fetchLocation() async -> CLLocation {
        return await withUnsafeContinuation { continuation in
            self.handler = { result in
                continuation.resume(returning: result)
            }

            locationManager!.requestLocation()
        }
    }

    func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let handler = handler, let last = locations.last {
            handler(last)
            self.handler = nil
        }
    }

    func locationManager(_: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

struct Provider: IntentTimelineProvider {
    var widgetLocationManager = WidgetLocationManager()
    var features = Store.instance.features
    var pictures = Store.instance.pictures
    var picturesCache = PictureImageCache.instance

    func placeholder(in _: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), configuration: ConfigurationIntent())
    }

    func getSnapshot(for configuration: ConfigurationIntent, in _: Context, completion: @escaping (SimpleEntry) -> Void) {
        let entry = SimpleEntry(date: Date(), configuration: configuration)
        completion(entry)
    }

    func handleList(items: [Feature], location: CLLocation, completion: @escaping (_ feature: Feature?, _ image: UIImage?, _ distance: String) -> Void) async throws {
        var strDistance = ""

        if items.count == 0 {
            completion(nil, nil, strDistance)
            return
        }

        let feature = items[0]

        let userLocation = Geometry.point(Point(x: location.coordinate.longitude, y: location.coordinate.latitude))

        if let distance = try? feature.position.distance(to: userLocation) {
            if distance < 20 {
                strDistance = "20m"
            } else if distance < 100 {
                strDistance = "100m"
            } else if distance < 500 {
                strDistance = "500m"
            } else if distance < 1000 {
                strDistance = "1km"
            } else {
                strDistance = "1km+"
            }
        }

        if feature.picturesId?.count == 0 {
            completion(feature, nil, strDistance)
            return
        }

        let pictureId = feature.picturesId![0]

        let image = try await picturesCache.get(id: pictureId)

        let strDistanceConst = strDistance
        DispatchQueue.main.async {
            completion(feature, image, strDistanceConst)
        }
    }

    func getDistance(_ feature: Feature, _ location: CLLocation) -> String {
        let userLocation = Geometry.point(Point(x: location.coordinate.longitude, y: location.coordinate.latitude))

        guard let distance = try? feature.position.distance(to: userLocation) else {
            return ""
        }

        if distance < 20 {
            return "20m"
        }

        if distance < 100 {
            return "100m"
        }

        if distance < 500 {
            return "500m"
        }

        if distance < 1000 {
            return "1km"
        }

        return "1km+"
    }

    func getImage(_ feature: Feature) async throws -> UIImage? {
        if let pictures = feature.pictures, pictures.count == 0 {
            return nil
        }

        if let baseUrl = feature.firstPictures?.picture {
            return try await PictureImageCache.instance.get(baseUrl: baseUrl, size: "md")
        }

        return nil
    }

    func getTimeline(for configuration: ConfigurationIntent, in _: Context, completion: @escaping (Timeline<Entry>) -> Void) {
        Task {
            let location = await widgetLocationManager.fetchLocation()
            let params = [
                "lon": String(location.coordinate.longitude),
                "lat": String(location.coordinate.latitude),
                "limit": "1",
            ]

            let features = try await features.query(params: params, collection: "nearby-features")

            if features.items.count == 0 {
                return
            }

            var entries: [SimpleEntry] = []
            let currentDate = Date()
            var entry = SimpleEntry(date: currentDate, configuration: configuration)

            entry.feature = features.items[0]
            entry.distance = self.getDistance(features.items[0], location)

            try? await features.items[0].loadPictures()
            entry.image = try? await self.getImage(features.items[0])

            entries.append(entry)
            let timeline = Timeline(entries: entries, policy: .atEnd)

            DispatchQueue.main.async {
                completion(timeline)
            }
        }
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
    let configuration: ConfigurationIntent
    var feature: Feature?
    var image: UIImage?
    var distance: String = "Far away"
}

struct TranslucentContainerView<Content: View>: View {
    @ViewBuilder var content: Content

    var body: some View {
        if #available(iOSApplicationExtension 15.0, *) {
            HStack {
                content
            }
            .background(.thinMaterial)
        } else {
            HStack {
                content
            }
            .background(Color(UIColor.systemBackground))
        }
    }
}

struct NearbyFeaturesEntryView: View {
    @Environment(\.widgetFamily) var size

    var entry: Provider.Entry

    var textAlign: TextAlignment {
        if size == .systemSmall {
            return .center
        }

        return .leading
    }

    var textFont: Font {
        if size == .systemSmall {
            return .caption
        }

        return .subheadline
    }

    var body: some View {
        ZStack {
            GeometryReader { geometry in
                if let image = entry.image {
                    Image(uiImage: image)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                }

                VStack(alignment: .center, spacing: 0) {
                    Spacer()
                    Divider()

                    TranslucentContainerView {
                        if size == .systemSmall {
                            Spacer()
                        }

                        Text(entry.feature?.name ?? "---")
                            .multilineTextAlignment(textAlign)
                            .lineLimit(3)
                            .font(textFont)
                            .padding(.vertical, 7)
                            .padding(.horizontal, 15)
                        Spacer()

                        if size != .systemSmall {
                            Text(entry.distance)
                                .lineLimit(3)
                                .font(textFont)
                                .padding(.vertical, 7)
                                .padding(.horizontal, 15)
                                .foregroundColor(Color(UIColor.systemRed))
                        }
                    }
                }
            }
        }
    }
}

@main
struct NearbyFeatures: Widget {
    let kind: String = "NearbyFeatures"

    var body: some WidgetConfiguration {
        IntentConfiguration(kind: kind, intent: ConfigurationIntent.self, provider: Provider()) { entry in
            NearbyFeaturesEntryView(entry: entry)
        }
        .configurationDisplayName("What's nearby")
        .description("See what's closest to you.")
    }
}

struct NearbyFeatures_Previews: PreviewProvider {
    static var previews: some View {
        NearbyFeaturesEntryView(entry: SimpleEntry(date: Date(), configuration: ConfigurationIntent()))
            .previewContext(WidgetPreviewContext(family: .systemSmall))

        NearbyFeaturesEntryView(entry: SimpleEntry(date: Date(), configuration: ConfigurationIntent(), feature: featureData[1], image: ObservableUIImage.createDefault().value))
            .previewContext(WidgetPreviewContext(family: .systemSmall))

        NearbyFeaturesEntryView(entry: SimpleEntry(date: Date(), configuration: ConfigurationIntent(), feature: featureData[1], image: ObservableUIImage.createDefault().value))
            .previewContext(WidgetPreviewContext(family: .systemMedium))

        NearbyFeaturesEntryView(entry: SimpleEntry(date: Date(), configuration: ConfigurationIntent(), feature: featureData[1], image: ObservableUIImage.createDefault().value))
            .previewContext(WidgetPreviewContext(family: .systemLarge))

        if #available(iOSApplicationExtension 15.0, *) {
            NearbyFeaturesEntryView(entry: SimpleEntry(date: Date(), configuration: ConfigurationIntent(), feature: featureData[1], image: ObservableUIImage.createDefault().value))
                .previewContext(WidgetPreviewContext(family: .systemExtraLarge))
        } else {
            // Fallback on earlier versions
        }
    }
}
