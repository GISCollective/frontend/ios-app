//
//  File.swift
//  GISCollectiveTests
//
//  Created by Alexandra Casapu on 26.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective

class TestApi: ApiMethods {
    private let serializer = JSONEncoder()

    var response: [Data] = []
    var created: [String] = []
    var createdIndex: Int = 0
    var connectivity: ConnectionStatus = .highSpeed

    func refreshToken(token _: String, completion _: @escaping ApiResponseHandler) -> AppTask? {
        nil
    }

    func create<MI: CrateModelItem>(modelName _: String, record: MI) async throws -> Data {
        createdIndex += 1
        record.item.id = "\(createdIndex)"

        let serialized = try? JSONEncoder().encode(record)

        created.append(String(data: serialized ?? Data(), encoding: .utf8) ?? "")

        print("Record with id \(createdIndex) created.")
        return serialized ?? Data()
    }

    func update<MI: CrateModelItem>(modelName _: String, record _: MI) async throws -> Data {
        return Data()
    }

    func authToken(email _: String, password _: String) async throws -> Data {
        return Data()
    }

    func refreshToken(token _: String) async throws -> Data {
        return Data()
    }

    var baseUrl: String = ""
    var data: Data?
    var lastModelName: String?
    var lastParams: [String: String]?

    func setData(strData: String) {
        data = strData.data(using: .utf8)
    }

    func getAll(modelName: String) async throws -> Data {
        lastModelName = modelName

        return data ?? Data()
    }

    func getById(modelName: String, id _: String) async throws -> Data {
        lastModelName = modelName

        return data ?? Data()
    }

    func query(modelName: String, params: [String: String]) async throws -> Data {
        lastParams = params
        lastModelName = modelName

        return data ?? Data()
    }
}
