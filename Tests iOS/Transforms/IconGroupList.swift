//
//  IconGroups.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 02.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

@testable import GISCollective
import Nimble
import Quick

class IconGroupsTests: QuickSpec {
    override func spec() {
        describe("when it has no icons") {
            it("should have an empty group list") {
                let groups = IconGroupList()
                expect(groups.list.count).to(equal(0))
            }

            it("should have a group after an icon is added") {
                let groups = IconGroupList()
                let icon = Icon(id: "1")
                icon.attributes.append(IconAttribute())
                icon.name = "some icon"

                groups.add(icon: icon, isRequired: true)

                expect(groups.list.count).to(equal(1))
                expect(groups.list[0].icon.id).to(equal("1"))
            }

            it("should update the attributes when an icon is added") {
                let groups = IconGroupList()
                let attribute = IconAttribute.withData(name: "key", type: "text")
                let icon = Icon(id: "1")
                icon.attributes.append(attribute)
                icon.name = "some icon"

                groups.add(icon: icon, isRequired: true)

                expect(groups.list[0].attributesWithValues[0].attribute).to(equal(attribute))
                expect(groups.list[0].attributesWithValues[0].value).to(equal(.undefined))
            }

            it("should be able to be converted to attributes") {
                let groups = IconGroupList()
                let attribute = IconAttribute.withData(name: "key", type: "text")
                let icon = Icon(id: "1")
                icon.attributes.append(attribute)
                icon.name = "some icon"

                groups.add(icon: icon, isRequired: true)

                expect(groups.attributes["some icon"]?.asGroup()).to(equal(["key": .undefined]))
            }

            it("should have no groups when an icon without attributes is added") {
                let groups = IconGroupList()
                let icon = Icon(id: "1")

                groups.add(icon: icon, isRequired: true)

                expect(groups.list.count).to(equal(0))
            }

            it("should have a group when an icon is added twice") {
                let groups = IconGroupList()
                let icon = Icon(id: "1")
                icon.attributes.append(IconAttribute())
                icon.name = "some icon"

                groups.add(icon: icon, isRequired: true)
                groups.add(icon: icon, isRequired: true)

                expect(groups.list.count).to(equal(1))
                expect(groups.list[0].icon.id).to(equal("1"))
                expect(groups.list[0].isAddButton).to(equal(false))
            }

            it("should have an empty group list when the icon is removed") {
                let groups = IconGroupList()
                let icon = Icon(id: "1")
                icon.attributes.append(IconAttribute())
                icon.name = "some icon"

                groups.remove(icon: icon)

                expect(groups.list.count).to(equal(0))
            }

            it("should add an icon with allowMany=true as an add button") {
                let groups = IconGroupList()
                let icon = Icon(id: "1")
                icon.attributes.append(IconAttribute())
                icon.name = "some icon"
                icon.allowMany = true

                groups.add(icon: icon, isRequired: true)

                expect(groups.list.count).to(equal(1))
                expect(groups.list[0].isAddButton).to(equal(true))
            }

            it("adds all icons from the sync list once") {
                let groups = IconGroupList()
                let icon = Icon(id: "1")
                icon.attributes.append(IconAttribute())
                icon.name = "some icon"
                icon.allowMany = true

                groups.sync(requiredIcons: [icon], optionalIcons: [])
                groups.sync(requiredIcons: [], optionalIcons: [icon])

                expect(groups.list.count).to(equal(1))
                expect(groups.list[0].isAddButton).to(equal(true))
            }

            it("adds values to otherGroups") {
                let groups = IconGroupList()
                groups.other = ["about"]

                groups.updateValue(groupName: "about", index: 0, key: "key", value: .string("value"))

                let attributes = groups.attributes["about"]?.asGroup()

                expect(attributes).to(equal(["key": .string("value")]))
            }
        }

        describe("when it has two icons") {
            var groups: IconGroupList!
            var firstIcon: Icon!
            var secondIcon: Icon!

            beforeEach {
                groups = IconGroupList()
                firstIcon = Icon(id: "1")
                firstIcon!.attributes.append(IconAttribute())
                firstIcon!.name = "some icon"

                secondIcon = Icon(id: "2")
                secondIcon!.attributes.append(IconAttribute())
                secondIcon!.name = "other icon"

                groups!.add(icon: firstIcon!, isRequired: true)
                groups!.add(icon: secondIcon!, isRequired: true)
            }

            it("should remove an icon and its attributes") {
                groups!.remove(icon: firstIcon!)
                expect(groups!.list.count).to(equal(1))
                expect(groups!.list[0].icon.id).to(equal("2"))
            }

            it("keeps only the new icon on sync") {
                let icon = Icon(id: "3")
                icon.name = "some icon"
                icon.attributes.append(IconAttribute())
                icon.allowMany = true

                groups.sync(requiredIcons: [icon], optionalIcons: [])

                expect(groups!.list.count).to(equal(1))
                expect(groups!.list[0].icon.id).to(equal("3"))
            }
        }

        describe("when it has an allowMany=true icon") {
            var groups: IconGroupList!
            var firstIcon: Icon!

            beforeEach {
                groups = IconGroupList()
                firstIcon = Icon(id: "1")
                firstIcon!.attributes.append(IconAttribute.withData(name: "key"))
                firstIcon!.name = "some icon"
                firstIcon!.allowMany = true

                groups!.add(icon: firstIcon, isRequired: true)
            }

            it("should be able to add a group") {
                groups!.add(icon: firstIcon, isRequired: true)

                expect(groups!.list[0].id).to(equal("1-0"))
                expect(groups!.list[0].isAddButton).to(equal(false))

                expect(groups!.list[1].id).to(equal("1--1"))
                expect(groups!.list[1].isAddButton).to(equal(true))

                expect(groups!.list.count).to(equal(2))
            }

            it("should be able to add two groups") {
                groups!.add(icon: firstIcon, isRequired: true)
                groups!.add(icon: firstIcon, isRequired: true)

                expect(groups!.list[0].id).to(equal("1-0-1"))
                expect(groups!.list[0].isAddButton).to(equal(false))

                expect(groups!.list[1].id).to(equal("1-1"))
                expect(groups!.list[1].isAddButton).to(equal(false))

                expect(groups!.list[2].id).to(equal("1--1"))
                expect(groups!.list[2].isAddButton).to(equal(true))

                expect(groups!.list.count).to(equal(3))
            }

            it("should be able to get the attributes for two groups") {
                groups.add(icon: firstIcon, isRequired: true)
                groups.add(icon: firstIcon, isRequired: true)

                let attributes = groups.attributes

                expect(attributes["some icon"]?.asList()).notTo(beNil())
            }

            it("should remove a single group of attributes from an icon") {
                groups.add(icon: firstIcon, isRequired: true)
                groups.removeGroup(icon: firstIcon!, index: 0)

                expect(groups.list.count).to(equal(1))
                expect(groups.list[0].id).to(equal("1--1"))
                expect(groups.list[0].isAddButton).to(equal(true))
            }
        }

        describe("when it has two allowMany=true icons") {
            var groups: IconGroupList?
            var firstIcon: Icon?
            var secondIcon: Icon?

            beforeEach {
                groups = IconGroupList()
                firstIcon = Icon(id: "1")
                firstIcon!.attributes.append(IconAttribute())
                firstIcon!.name = "some icon"
                firstIcon!.allowMany = true

                secondIcon = Icon(id: "2")
                secondIcon!.attributes.append(IconAttribute())
                secondIcon!.name = "other icon"
                secondIcon!.allowMany = true

                groups!.add(icon: firstIcon!, isRequired: true)
                groups!.add(icon: secondIcon!, isRequired: true)
            }

            it("should add an attributes instance to the first icon after the first instance") {
                groups!.add(icon: firstIcon!, isRequired: true)
                expect(groups!.list.count).to(equal(3))
                expect(groups!.list[0].icon.id).to(equal("1"))
                expect(groups!.list[1].icon.id).to(equal("1"))
                expect(groups!.list[2].icon.id).to(equal("2"))
            }
        }

        describe("is required") {
            it("is valid when it has no groups") {
                let groups = IconGroupList()
                expect(groups.isValid).to(equal(true))
            }

            it("is invalid when it has a group with an undefined required value") {
                let groups = IconGroupList()
                let icon = Icon(id: "1")
                icon.attributes.append(IconAttribute.withData(name: "key", type: nil, options: nil, isRequired: true))
                icon.name = "some icon"
                icon.allowMany = false

                groups.add(icon: icon, isRequired: true)

                expect(groups.isValid).to(equal(false))
            }

            it("is valid when it has a group with a defined required value") {
                let groups = IconGroupList()
                let icon = Icon(id: "1")
                icon.attributes.append(IconAttribute.withData(name: "key", type: nil, options: nil, isRequired: true))
                icon.name = "icon"
                icon.allowMany = false

                groups.add(icon: icon, isRequired: true)
                groups.updateValue(groupName: "icon", index: 0, key: "key", value: .string("value"))

                expect(groups.isValid).to(equal(false))
            }
        }
    }
}
