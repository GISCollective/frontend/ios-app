//
//  Campaign.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 01.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import SwiftUI

extension Campaign {
    static func getWithNoIcons() -> Campaign {
        let campaign = Campaign(id: "1")
        campaign.name = ""
        campaign.options = CampaignOptions()
        campaign.options?.showNameQuestion = true
        campaign.options?.showDescriptionQuestion = true

        return campaign
    }

    static func getWithIconTextGroup() -> Campaign {
        let campaign = Campaign.getWithNoIcons()
        campaign.iconsId = ["1"]
        campaign.icons = [testIcons[1]!]

        return campaign
    }

    static func getWithIconIntegerGroup() -> Campaign {
        let campaign = Campaign.getWithNoIcons()
        campaign.iconsId = ["3"]
        campaign.icons = [testIcons[3]!]

        return campaign
    }

    static func getWithIconDecimalGroup() -> Campaign {
        let campaign = Campaign.getWithNoIcons()
        campaign.iconsId = ["4"]
        campaign.icons = [testIcons[4]!]

        return campaign
    }

    static func getWithIconBooleanGroup() -> Campaign {
        let campaign = Campaign.getWithNoIcons()
        campaign.iconsId = ["5"]
        campaign.icons = [testIcons[5]!]

        return campaign
    }

    static func getWithIconLongTextGroup() -> Campaign {
        let campaign = Campaign.getWithNoIcons()
        campaign.iconsId = ["6"]
        campaign.icons = [testIcons[6]!]

        return campaign
    }

    static func getWithIconOptionsGroup() -> Campaign {
        let campaign = Campaign.getWithNoIcons()
        campaign.iconsId = ["7"]
        campaign.icons = [testIcons[7]!]

        return campaign
    }

    static func getWithIconOptionsWithOtherGroup() -> Campaign {
        let campaign = Campaign.getWithNoIcons()
        campaign.iconsId = ["8"]
        campaign.icons = [testIcons[8]!]

        return campaign
    }

    static func getWithIconListGroup() -> Campaign {
        let campaign = Campaign.getWithNoIcons()
        campaign.iconsId = ["2"]
        campaign.icons = [testIcons[2]!]

        return campaign
    }
}
