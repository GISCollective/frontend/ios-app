//
//  setup.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 01.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective

var testIcons: [Int: Icon] = [:]

func setupTestData() async throws {
    let encoder = JSONEncoder()
    encoder.dataEncodingStrategy = .base64
    encoder.dateEncodingStrategy = .formatted(DateFormatter.fullISO8601)

    let persistence = Persistence.instance ?? Persistence(path: ":memory:")
    Persistence.instance = persistence

    await persistence.clearCache()

    let iconStore = await persistence.getModelStorage(name: Icon.modelName)

    let icon1 = Icon(id: "1")
    icon1.name = "Some icon group"
    icon1.localName = "Some icon group"
    icon1.attributes = [IconAttribute.withData(name: "phone", type: "text", options: nil, isPrivate: false, isRequired: false, displayName: "Phone", help: "", isInherited: false)]

    testIcons[1] = icon1
    try await iconStore.save(item: StoredPushQueueElement(id: 0, slug: "", remoteId: "1", parentId: "", data: try! encoder.encode(icon1), order: 0))

    let icon2 = Icon(id: "2")
    icon2.name = "Some icon list group"
    icon2.localName = "Some icon list group"
    icon2.allowMany = true
    icon2.attributes = [IconAttribute.withData(name: "phone", type: "text", options: nil, isPrivate: false, isRequired: false, displayName: "Phone", help: "", isInherited: false)]

    testIcons[2] = icon2
    try await iconStore.save(item: StoredPushQueueElement(id: 0, slug: "", remoteId: "2", parentId: "", data: try! encoder.encode(icon2), order: 0))

    let icon3 = Icon(id: "3")
    icon3.name = "Some icon group"
    icon3.localName = "Some icon group"
    icon3.attributes = [IconAttribute.withData(name: "phone", type: "integer", options: nil, isPrivate: false, isRequired: false, displayName: "Phone", help: "", isInherited: false)]

    testIcons[3] = icon3
    try await iconStore.save(item: StoredPushQueueElement(id: 0, slug: "", remoteId: "3", parentId: "", data: try! encoder.encode(icon3), order: 0))

    let icon4 = Icon(id: "4")
    icon4.name = "Some icon group"
    icon4.localName = "Some icon group"
    icon4.attributes = [IconAttribute.withData(name: "phone", type: "decimal", options: nil, isPrivate: false, isRequired: false, displayName: "Phone", help: "", isInherited: false)]

    testIcons[4] = icon4
    try await iconStore.save(item: StoredPushQueueElement(id: 0, slug: "", remoteId: "4", parentId: "", data: try! encoder.encode(icon4), order: 0))

    let icon5 = Icon(id: "5")
    icon5.name = "Some icon group"
    icon5.localName = "Some icon group"
    icon5.attributes = [IconAttribute.withData(name: "phone", type: "boolean", options: nil, isPrivate: false, isRequired: false, displayName: "Phone", help: "", isInherited: false)]

    testIcons[5] = icon5
    try await iconStore.save(item: StoredPushQueueElement(id: 0, slug: "", remoteId: "5", parentId: "", data: try! encoder.encode(icon5), order: 0))

    let icon6 = Icon(id: "6")
    icon6.name = "Some icon group"
    icon6.localName = "Some icon group"
    icon6.attributes = [IconAttribute.withData(name: "phone", type: "long text", options: nil, isPrivate: false, isRequired: false, displayName: "Phone", help: "", isInherited: false)]

    testIcons[6] = icon6
    try await iconStore.save(item: StoredPushQueueElement(id: 0, slug: "", remoteId: "6", parentId: "", data: try! encoder.encode(icon6), order: 0))

    let icon7 = Icon(id: "7")
    icon7.name = "Some icon group"
    icon7.localName = "Some icon group"
    icon7.attributes = [IconAttribute.withData(name: "phone", type: "options", options: "a,b,c", isPrivate: false, isRequired: false, displayName: "Phone", help: "", isInherited: false)]

    testIcons[7] = icon7
    try await iconStore.save(item: StoredPushQueueElement(id: 0, slug: "", remoteId: "7", parentId: "", data: try! encoder.encode(icon7), order: 0))

    let icon8 = Icon(id: "8")
    icon8.name = "Some icon group"
    icon8.localName = "Some icon group"
    icon8.attributes = [IconAttribute.withData(name: "phone", type: "options with other", options: "a,b,c", isPrivate: false, isRequired: false, displayName: "Phone", help: "", isInherited: false)]

    testIcons[8] = icon8
    try await iconStore.save(item: StoredPushQueueElement(id: 0, slug: "", remoteId: "8", parentId: "", data: try! encoder.encode(icon8), order: 0))
}
