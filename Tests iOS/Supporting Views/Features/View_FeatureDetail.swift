//
//  View_FeatureDetail.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 19.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_FeatureDetail: QuickSpec {
    override func spec() {
        var subject: FeatureDetail!

        describe("FeatureDetail") {
            var feature: Feature!

            describe("for a masked feature") {
                beforeEach {
                    feature = Feature(id: "")
                    feature.isMasked = true

                    subject = FeatureDetail(feature: feature, onClose: {})
                }

                it("does not show the get directions button") {
                    let directionsButton = try? subject.inspect().find(GetDirectionsButtonView.self)
                    expect(directionsButton).to(beNil())
                }

                it("shows a masked feature message") {
                    let message = try? subject.inspect().find(FeatureMaskedGeometryMessage.self)
                    expect(message).notTo(beNil())
                }
            }
        }
    }
}
