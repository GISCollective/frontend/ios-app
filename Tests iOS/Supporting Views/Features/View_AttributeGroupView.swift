//
//  View_AttributeGroupView.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 19.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_AttributeGroupView: QuickSpec {
    var view: InspectableView<ViewType.View<AttributeGroupView>>!
    override func spec() {
        var subject: AttributeGroupView!

        describe("AttributeGroupView") {
            var attributes: [String: AttributeGroup]!
            var icon: Icon!

            describe("when there is an icon group with attributes") {
                beforeEach {
                    icon = Icon(id: "")
                    icon.name = "name"
                    icon.localName = "local name"
                    icon.attributes = [IconAttribute.withData(name: "key1", displayName: "KEY1"), IconAttribute.withData(name: "key2")]

                    attributes = ["name": .group(["key1": .string("value1"), "key2": .string("value2"), "key3": .string("value3")])]

                    subject = AttributeGroupView(attributes: Binding(wrappedValue: attributes), icon: Binding(wrappedValue: icon))

                    let exp = subject.inspection.inspect(after: 0.1) { v in
                        self.view = v
                    }

                    ViewHosting.host(view: subject)
                    self.wait(for: [exp], timeout: 0.5)
                }

                it("the icon local name") {
                    let title = try? self.view.find(ViewType.Text.self).string()
                    expect(title).to(equal("local name"))
                }

                it("renders the defined attributes names") {
                    let labels = self.view.findAll(FieldLabel.self)

                    let strings = labels.map { try? $0.find(ViewType.Text.self).string() }
                    expect(strings).to(equal(["KEY1", "key2"]))
                }

                it("renders the defined attributes values") {
                    let values = self.view.findAll(FieldValue.self)

                    let strings = values.map { try? $0.find(ViewType.Text.self).string() }
                    expect(strings).to(equal(["value1", "value2"]))
                }
            }

            describe("when there is an icon group with attributes and index") {
                beforeEach {
                    icon = Icon(id: "")
                    icon.name = "name"
                    icon.localName = "local name"
                    icon.attributes = [IconAttribute.withData(name: "key1", displayName: "KEY1"), IconAttribute.withData(name: "key2")]

                    attributes = ["name": .group(["key1": .string("value1"), "key2": .string("value2"), "key3": .string("value3")])]

                    subject = AttributeGroupView(attributes: Binding(wrappedValue: attributes), icon: Binding(wrappedValue: icon), index: 0)

                    let exp = subject.inspection.inspect(after: 0.1) { v in
                        self.view = v
                    }

                    ViewHosting.host(view: subject)
                    self.wait(for: [exp], timeout: 0.5)
                }

                it("renders the icon local name with an index") {
                    let title = try? self.view.find(CampaignGroupLabel.self).find(ViewType.Text.self).string()
                    expect(title).to(equal("1. local name"))
                }
            }

            describe("when there is an icon group with no attributes") {
                beforeEach {
                    icon = Icon(id: "")
                    icon.name = "name"
                    icon.localName = "local name"
                    icon.attributes = [IconAttribute.withData(name: "key1", displayName: "KEY1"), IconAttribute.withData(name: "key2")]

                    attributes = ["name": .group(["key1": .string("value1"), "key2": .string("value2"), "key3": .string("value3")])]

                    let exp = subject.inspection.inspect(after: 0.1) { v in
                        self.view = v
                    }

                    ViewHosting.host(view: subject)
                    self.wait(for: [exp], timeout: 0.5)
                }

                it("does not render the title") {
                    let title = try? subject.inspect().find(CampaignGroupLabel.self).find(ViewType.Text.self)
                    expect(title).to(beNil())
                }
            }
        }
    }
}
