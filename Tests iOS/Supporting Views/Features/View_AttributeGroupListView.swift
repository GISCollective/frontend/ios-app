//
//  View_AttributeGroupListView.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 20.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_AttributeGroupListView: QuickSpec {
    override func spec() {
        var subject: AttributeGroupListView!

        describe("AttributeGroupListView") {
            var attributes: [String: AttributeGroup]!
            var icon: Icon!

            describe("when there is an icon group list with one item with attributes") {
                beforeEach {
                    icon = Icon(id: "")
                    icon.name = "name"
                    icon.localName = "local name"
                    icon.attributes = [IconAttribute.withData(name: "key1", displayName: "KEY1"), IconAttribute.withData(name: "key2")]

                    attributes = ["name": .list([["key1": .string("value1"), "key2": .string("value2"), "key3": .string("value3")]])]

                    subject = AttributeGroupListView(attributes: Binding(wrappedValue: attributes), icon: Binding(wrappedValue: icon))
                }

                it("should have one group") {
                    let groups = subject.createGroups()
                    expect(groups.count).to(equal(1))
                    expect(groups[0].index).to(equal(0))

                    let attributes = groups[0].attributes["name"]?.asGroup()
                    expect(attributes).to(equal(["key2": GISCollective.Attribute.string("value2"), "key1": GISCollective.Attribute.string("value1"), "key3": GISCollective.Attribute.string("value3")]))
                }
            }

            describe("when there is an icon group list with two items with attributes") {
                beforeEach {
                    icon = Icon(id: "")
                    icon.name = "name"
                    icon.localName = "local name"
                    icon.attributes = [IconAttribute.withData(name: "key1", displayName: "KEY1"), IconAttribute.withData(name: "key2")]

                    attributes = ["name": .list([["key1": .string("value1"), "key2": .string("value2"), "key3": .string("value3")], [:]])]

                    subject = AttributeGroupListView(attributes: Binding(wrappedValue: attributes), icon: Binding(wrappedValue: icon))
                }

                it("should have one group") {
                    let groups = subject.createGroups()
                    expect(groups.count).to(equal(2))
                    expect(groups[0].index).to(equal(0))
                    expect(groups[1].index).to(equal(1))

                    var attributes = groups[0].attributes["name"]?.asGroup()
                    expect(attributes).to(equal(["key2": GISCollective.Attribute.string("value2"), "key1": GISCollective.Attribute.string("value1"), "key3": GISCollective.Attribute.string("value3")]))

                    attributes = groups[1].attributes["name"]?.asGroup()
                    expect(attributes).to(equal([:]))
                }
            }
        }
    }
}
