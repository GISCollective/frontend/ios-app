//
//  View_FeatureAttribute.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 19.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_FeatureAttributes: QuickSpec {
    override func spec() {
        var subject: FeatureAttributes!

        describe("FeatureAttributes") {
            var attributes: [String: AttributeGroup]!
            var icons: [Icon]!

            describe("when there is an icon group with attributes") {
                beforeEach {
                    let icon = Icon(id: "")
                    icon.name = "name"
                    icon.localName = "local name"
                    icon.attributes = [IconAttribute.withData(name: "key1", displayName: "KEY1"), IconAttribute.withData(name: "key2")]

                    attributes = ["name": .group(["key1": .string("value1"), "key2": .string("value2"), "key3": .string("value3")])]
                    icons = [icon]
                    subject = FeatureAttributes(attributes: Binding(wrappedValue: attributes), icons: Binding(wrappedValue: icons))

                    let exp = waitFor(validation: {
                        let fieldLabels = try subject.inspect().findAll(AttributeGroupView.self)
                        return fieldLabels.count >= 1
                    })

                    ViewHosting.host(view: subject)
                    self.wait(for: [exp], timeout: 0.5)
                }

                it("renders one group") {
                    let groups = try? subject.inspect().findAll(AttributeGroupView.self)
                    expect(groups?.count).to(equal(1))
                }
            }

            describe("when there is an icon group list with attributes") {
                var icon: Icon!

                beforeEach {
                    icon = Icon(id: "")
                    icon.allowMany = true
                    icon.name = "name"
                    icon.localName = "local name"
                    icon.attributes = [IconAttribute.withData(name: "key1", displayName: "KEY1"), IconAttribute.withData(name: "key2")]

                    attributes = ["name": .list([["key1": .string("value1"), "key2": .string("value2"), "key3": .string("value3")], [:]])]

                    subject = FeatureAttributes(attributes: Binding(wrappedValue: attributes), icons: Binding(wrappedValue: [icon]))
                }

                it("renders a AttributeGroupListView") {
                    let groupList = try? subject.inspect().find(AttributeGroupListView.self).actualView()

                    expect(groupList?.icon).to(equal(icon))

                    let attributes: [String: AttributeGroup] = groupList?.attributes ?? [:]
                    let keys: [String] = attributes.keys.map { $0 }

                    expect(keys).to(equal(["name"]))

                    let list = attributes["name"]?.asList()
                    expect(list?.count).to(equal(2))
                }
            }
        }
    }
}
