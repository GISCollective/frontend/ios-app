//
//  View_FieldOption.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 18.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import Foundation
import GEOSwift
@testable import GISCollective
import MapKit
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_CampaignLocationEditorTests: QuickSpec {
    override func spec() {
        var showingSheet: Binding<Bool>!
        var subject: CampaignLocationEditor!
        var changes: [[String: Attribute?]] = []
        var position: Geometry!
        var onUseCurrentLocationCalled = false

        describe("CampaignLocationEditor view with gps position") {
            beforeEach {
                onUseCurrentLocationCalled = false
                position = try! Geometry(wkt: "POINT (1 1)")
                showingSheet = Binding<Bool>(wrappedValue: true)

                subject = CampaignLocationEditor(showingSheet: showingSheet, geometry: position, usingGps: true, onChange: { key, value in
                    changes.append([key: value])
                }, onPositionChange: { value in
                    position = value
                }, onUseCurrentLocation: {
                    onUseCurrentLocationCalled = true
                })

                changes.removeAll()
                ViewHosting.host(view: subject)
            }

            it("does not show the Use current location button") {
                let button = try? subject.inspect().find(button: "Use current location")

                expect(button).to(beNil())
            }
        }

        describe("CampaignLocationEditor view with manual position") {
            beforeEach {
                onUseCurrentLocationCalled = false
                position = try! Geometry(wkt: "POINT (1 1)")
                showingSheet = Binding<Bool>(wrappedValue: true)

                subject = CampaignLocationEditor(showingSheet: showingSheet, geometry: position, usingGps: false, onChange: { key, value in
                    changes.append([key: value])
                }, onPositionChange: { value in
                    position = value
                }, onUseCurrentLocation: {
                    onUseCurrentLocationCalled = true
                })

                changes.removeAll()
                ViewHosting.host(view: subject)
            }

            it("sets the showingSheet to false when the cancel button is pressed") {
                try subject.inspect().find(button: "Cancel").tap()

                expect(showingSheet.wrappedValue).to(equal(false))
            }

            it("sets the showingSheet to false when the Select point button is pressed") {
                try subject.inspect().find(button: "Select point").tap()

                expect(showingSheet.wrappedValue).to(equal(false))
            }

            it("sets the showingSheet to false when the use current location button is pressed") {
                try subject.inspect().find(button: "Use current location").tap()

                expect(showingSheet.wrappedValue).to(equal(false))
            }

            it("triggers onChange when the position is selected") {
                try subject.inspect().find(button: "Select point").tap()

                expect(changes.count).to(equal(4))
                expect(changes[0]).to(equal(["type": .string("manual")]))
                expect(changes[1]).to(equal(["accuracy": .undefined]))
                expect(changes[2]).to(equal(["altitudeAccuracy": .undefined]))
                expect(changes[3]).to(equal(["altitude": .undefined]))
            }

            it("triggers onUseCurrentLocation when the use current location button is pressed") {
                try subject.inspect().find(button: "Use current location").tap()

                expect(onUseCurrentLocationCalled).to(equal(true))
            }

            it("triggers onPositionChange when the position is selected") {
                let exp1 = subject.inspection.inspect { view in
                    let map = try view.find(ViewType.Map.self)
                    try map.setCoordinateRegion(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 20, longitude: 30), latitudinalMeters: 100, longitudinalMeters: 100))
                }

                let exp2 = subject.inspection.inspect { _ in
                    try subject.inspect().find(button: "Select point").tap()
                }

                let exp3 = waitFor(validation: {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(position)
                    let geoJson = String(data: data, encoding: .utf8) ?? ""

                    if geoJson.contains("1,1") {
                        return false
                    }

                    expect(geoJson).to(equal("{\"type\":\"Point\",\"coordinates\":[0,0]}"))

                    return true
                })

                self.wait(for: [exp1, exp2, exp3], timeout: 5)
            }
        }
    }
}
