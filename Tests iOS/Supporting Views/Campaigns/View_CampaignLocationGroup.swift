//
//  View_CampaignLocationGroup.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 07.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import Foundation
import GEOSwift
@testable import GISCollective
import MapKit
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_CampaignLocationGroup: QuickSpec {
    var answer: CampaignAnswer!
    var position: Geometry!
    var _group: String?
    var _index: Int?
    var _key: String?
    var _value: Attribute?

    override func spec() {
        var subject: CampaignLocationGroup!

        describe("CampaignLocationEditor view") {
            beforeEach {
                self.answer = CampaignAnswer(id: "")

                subject = CampaignLocationGroup(answer: Binding<CampaignAnswer>(wrappedValue: self.answer), onChange: { group, index, key, value in
                    self._group = group
                    self._index = index
                    self._key = key
                    self._value = value
                }, onPositionChange: { newGeometry in
                    self.position = newGeometry
                })
            }

            it("triggers onChange when an attribute is set") {
                let editor = subject.editor

                editor.onChange("key", .string("value"))

                expect(self._group).to(equal("position details"))
                expect(self._index).to(equal(0))
                expect(self._key).to(equal("key"))
                expect(self._value).to(equal(.string("value")))
            }

            it("triggers onPositionChange when the position is selected") {
                let editor = subject.editor

                editor.onPositionChange(Point(x: 12, y: 13).geometry)

                let encoder = JSONEncoder()
                let data = try encoder.encode(self.position)
                let geoJson = String(data: data, encoding: .utf8) ?? ""

                expect(geoJson).to(equal("{\"type\":\"Point\",\"coordinates\":[12,13]}"))
            }
        }
    }
}
