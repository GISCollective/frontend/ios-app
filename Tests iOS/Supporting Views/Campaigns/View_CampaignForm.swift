//
//  View_FieldOption.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 18.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector
import XCTest

class View_CampaignForm_when_there_is_a_campaign_with_no_icons_Test: XCTestCase {
    var subject: CampaignForm!

    override func setUp() {
        let campaign = Campaign.getWithNoIcons()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        subject = CampaignForm(remoteData: CampaignFormData(campaign), isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

        ViewHosting.host(view: subject)
    }

    override func tearDownWithError() throws {}

    func test_delete_the_first_group_from_the_list() async throws {
        let fieldLabels = try subject.inspect().findAll(CampaignGroupLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }
        expect(text).to(equal(["Location", "Pictures"]))
    }
}

class View_CampaignForm_the_Location_section_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() {
        campaign = Campaign.getWithNoIcons()

        data = CampaignFormData(campaign)
        let s = self

        // swift-format-ignore
        subject = CampaignForm(remoteData: s.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))
    }

    override func tearDownWithError() throws {}

    func test_updates_the_PositionDetails_attributes_when_they_are_set() async throws {
        let locationEditor = try subject?.inspect()
            .find(CampaignLocationGroup.self)
            .actualView()

        await locationEditor?.onChange("position details", 0, "key", .string("value"))

        let exp1 = waitFor(timeout: 5, validation: {
            if self.data?.answer.attributes["position details"] == nil {
                return false
            }

            if self.data?.answer.attributes["position details"]!["key"] == nil {
                return false
            }

            let value = self.data?.answer.attributes["position details"]!["key"]

            expect(value).to(equal(.string("value")))
            return true
        })

        wait(for: [exp1], timeout: 5)
    }

    func test_updates_the_position_when_is_set() async throws {
        let locationEditor = try subject?.inspect()
            .find(CampaignLocationGroup.self)
            .actualView()

        await locationEditor?.onPositionChange(Point(x: 10, y: 20).geometry)

        let encoder = JSONEncoder()
        let encodedData = try encoder.encode(data?.answer.position)
        let geoJson = String(data: encodedData, encoding: .utf8) ?? ""

        expect(geoJson).to(equal("{\"type\":\"Point\",\"coordinates\":[10,20]}"))
    }
}

class View_CampaignForm_the_About_and_Icons_sections_when_there_is_a_campaign_with_an_optional_icon_and_info_section_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() {
        let icon = testIcons[1]
        campaign = Campaign.getWithNoIcons()
        campaign.optionalIconsId = ["1"]
        campaign.optionalIcons = [icon!]
        campaign.options!.showNameQuestion = true
        campaign.options!.showDescriptionQuestion = true

        data = CampaignFormData(campaign)

        let s = self
        subject = CampaignForm(remoteData: s.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

        let exp = waitFor(validation: {
            let fieldLabels = try s.subject.inspect().findAll(CampaignGroupLabel.self)
            return fieldLabels.count == 4
        })

        ViewHosting.host(view: subject)
        wait(for: [exp], timeout: 2)
    }

    func test_renders_the_Location_About_Icons_and_Pictures_sections() async throws {
        let fieldLabels = try subject.inspect().findAll(CampaignGroupLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }
        expect(text).to(equal(["Location", "About", "Icons", "Pictures"]))
    }

    func test_renders_the_name_and_description_questions_in_the_info_section() async throws {
        let fieldLabels = try subject.inspect().find(CampaignAboutGroup.self).findAll(FieldLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }
        expect(text).to(equal(["What's the name of this place?", "What do you like about this place?"]))
    }

    func test_updates_the_about_group_fields_when_they_are_set() async throws {
        let nameField = try subject?.inspect()
            .find(FieldText.self)
            .textField()

        let descriptionField = try subject?.inspect()
            .find(FieldLongText.self)
            .zStack()
            .textEditor(0)

        try nameField?.callOnChange(newValue: "name")
        try descriptionField?.callOnChange(newValue: "description")

        let exp1 = waitFor(timeout: 5, validation: {
            if self.data.answer.attributes["about"]?["name"] == nil {
                return false
            }

            if self.data.answer.attributes["about"]?["description"] == nil {
                return false
            }

            let name = self.data.answer.attributes["about"]!["name"]
            let description = self.data.answer.attributes["about"]!["description"]

            expect(name).to(equal(.string("name")))
            expect(description).to(equal(.string("description")))

            return true
        })

        wait(for: [exp1], timeout: 5)
    }
}

class View_CampaignForm_the_About_and_Icons_sections_when_there_is_a_campaign_with_empty_custom_labels_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() {
        campaign = Campaign.getWithNoIcons()
        campaign.optionalIconsId = ["1"]
        campaign.optionalIcons = [testIcons[1]!]
        campaign.options!.showNameQuestion = true
        campaign.options!.showDescriptionQuestion = true
        campaign.options!.descriptionLabel = ""
        campaign.options!.nameLabel = ""
        campaign.options!.iconsLabel = ""

        data = CampaignFormData(campaign)
        let s = self

        subject = CampaignForm(remoteData: s.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

        let exp = waitFor(validation: {
            let fieldLabels = try self.subject.inspect().findAll(CampaignGroupLabel.self)
            return fieldLabels.count == 4
        })

        ViewHosting.host(view: subject)
        wait(for: [exp], timeout: 2)
    }

    func test_renders_the_Location_About_Icons_and_Pictures_sections() async throws {
        let fieldLabels = try subject.inspect().findAll(CampaignGroupLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }
        expect(text).to(equal(["Location", "About", "Icons", "Pictures"]))
    }

    func test_renders_the_name_and_description_questions_in_the_info_section() async throws {
        let fieldLabels = try subject.inspect().find(CampaignAboutGroup.self).findAll(FieldLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }
        expect(text).to(equal(["What's the name of this place?", "What do you like about this place?"]))
    }
}

class View_CampaignForm_the_About_and_Icons_sections_when_there_is_a_campaign_with_custom_labels_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() {
        campaign = Campaign.getWithNoIcons()
        campaign.optionalIconsId = ["1"]
        campaign.optionalIcons = [testIcons[1]!]
        campaign.options!.showNameQuestion = true
        campaign.options!.showDescriptionQuestion = true
        campaign.options!.descriptionLabel = "custom description label"
        campaign.options!.nameLabel = "custom name label"
        campaign.options!.iconsLabel = "custom icons label"

        data = CampaignFormData(campaign)
        let s = self

        subject = CampaignForm(remoteData: s.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

        let exp = waitFor(validation: {
            let fieldLabels = try s.subject.inspect().findAll(CampaignGroupLabel.self)
            return fieldLabels.count == 4
        })

        ViewHosting.host(view: subject)
        wait(for: [exp], timeout: 2)
    }

    func test_renders_the_Location_About_Icons_and_Pictures_sections() async throws {
        let fieldLabels = try subject.inspect().findAll(CampaignGroupLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }
        expect(text).to(equal(["Location", "About", "Icons", "Pictures"]))
    }

    func test_renders_the_name_and_description_questions_in_the_info_section() async throws {
        let fieldLabels = try subject.inspect().find(CampaignAboutGroup.self).findAll(FieldLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }
        expect(text).to(equal(["custom name label", "custom description label"]))
    }
}

class View_CampaignForm_the_About_and_Icons_sections_when_there_is_a_campaign_requesting_only_the_name_field_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() {
        campaign = Campaign.getWithNoIcons()
        campaign.options!.showNameQuestion = true
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign)
        let s = self

        subject = CampaignForm(remoteData: s.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

        let exp = waitFor(validation: {
            let fieldLabels = try s.subject.inspect().findAll(CampaignGroupLabel.self)
            return fieldLabels.count == 3
        })

        ViewHosting.host(view: subject)
        wait(for: [exp], timeout: 2)
    }

    func test_renders_the_name_questions_in_the_info_section() async throws {
        let fieldLabels = try subject.inspect().find(CampaignAboutGroup.self).findAll(FieldLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }
        expect(text).to(equal(["What's the name of this place?"]))
    }
}

class View_CampaignForm_the_About_and_Icons_sections_when_there_is_a_campaign_requesting_only_the_description_field_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() {
        campaign = Campaign.getWithNoIcons()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = true

        data = CampaignFormData(campaign)
        let s = self

        subject = CampaignForm(remoteData: s.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

        let exp = waitFor(validation: {
            let fieldLabels = try s.subject.inspect().findAll(CampaignGroupLabel.self)
            return fieldLabels.count == 3
        })

        ViewHosting.host(view: subject)
        wait(for: [exp], timeout: 2)
    }

    func test_renders_the_description_questions_in_the_info_section() async throws {
        let fieldLabels = try subject.inspect().find(CampaignAboutGroup.self).findAll(FieldLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }
        expect(text).to(equal(["What do you like about this place?"]))
    }
}

class View_CampaignForm_when_there_is_a_campaign_with_an_icon_with_allowMany_false_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconTextGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign!)
        let s = self

        DispatchQueue.main.sync {
            subject = CampaignForm(remoteData: s.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

            let exp = waitFor(timeout: 5, validation: {
                let fieldLabels = try self.subject.inspect().findAll(CampaignGroupLabel.self)
                return fieldLabels.count == 3
            })

            ViewHosting.host(view: subject)
            wait(for: [exp], timeout: 5)
        }
    }

    func test_renders_the_icon_section() async throws {
        let fieldLabels = try subject.inspect().findAll(CampaignGroupLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }

        expect(text).to(equal(["Location", "Pictures", "Some icon group"]))
    }

    func test_does_not_render_the_add_section_button() async throws {
        let found = subject!.canFindView(CampaignGroupAdd.self)
        expect(found).to(beFalse())
    }

    func test_updates_the_text_answer_attributes_when_the_values_are_changed() async throws {
        let field = try subject?.inspect()
            .find(FieldText.self)
            .textField()

        try field?.callOnChange(newValue: "text")

        let exp1 = waitFor(timeout: 5, validation: {
            if self.data.answer.attributes["Some icon group"] == nil {
                return false
            }

            if self.data.answer.attributes["Some icon group"]!["phone"] == nil {
                return false
            }

            let value = self.data.answer.attributes["Some icon group"]!["phone"]

            expect(value).to(equal(.string("text")))
            return true
        })

        wait(for: [exp1], timeout: 5)
    }
}

class View_CampaignForm_when_there_is_a_campaign_with_an_icon_with_allowMany_true_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconListGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        DispatchQueue.main.sync {
            data = CampaignFormData(campaign)
            subject = CampaignForm(remoteData: self.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

            let exp = waitFor(validation: {
                let fieldLabels = try self.subject.inspect().findAll(CampaignGroupLabel.self)
                return fieldLabels.count == 3
            })

            ViewHosting.host(view: subject)
            self.wait(for: [exp], timeout: 2)
        }
    }

    func test_renders_the_icon_section() async throws {
        let fieldLabels = try subject.inspect().findAll(CampaignGroupLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }

        expect(text).to(equal(["Location", "Pictures", "1.Some icon list group"]))
    }

    func test_renders_the_add_section_button() async throws {
        let found = subject.canFindView(CampaignGroupAdd.self)
        expect(found).to(beTrue())
    }

    func test_adds_another_section_when_the_add_button_is_pressed() async throws {
        try subject.inspect().find(CampaignGroupAdd.self).button().tap()

        let fieldLabels = try subject.inspect().findAll(CampaignGroupLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }

        expect(text).to(equal(["Location", "Pictures", "1.Some icon list group", "2.Some icon list group"]))
    }

    func test_updates_the_answer_attributes_when_the_values_are_changed() async throws {
        let field = try subject.inspect()
            .find(FieldText.self)
            .textField()

        try field.callOnChange(newValue: "text")

        let exp1 = waitFor(timeout: 5, validation: {
            if self.data.answer.attributes["Some icon list group"]?[0]?["phone"] == nil {
                return false
            }

            let value = self.data.answer.attributes["Some icon list group"]![0]!["phone"]

            expect(value).to(equal(.string("text")))
            return true
        })

        wait(for: [exp1], timeout: 5)
    }

    func test_updates_the_answer_attributes_from_the_second_group_when_the_values_are_changed() async throws {
        try subject.inspect().find(CampaignGroupAdd.self).button().tap()

        let field = try subject.inspect()
            .findAll(FieldText.self)[1]
            .textField()

        try field.callOnChange(newValue: "text")

        let value: [[String: Attribute]]? = data.answer.attributes["Some icon list group"]?.asList()

        expect(value).to(equal([["phone": .undefined], ["phone": .string("text")]]))
    }
}

class View_CampaignForm_when_there_is_a_campaign_with_an_icon_with_a_list_of_two_groups_with_values_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconListGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign)

        let exp1 = waitFor(timeout: 5, validation: {
            if self.data.mandatoryIcons.count == 0 {
                return false
            }

            DispatchQueue.main.sync {
                self.data.addIcon(self.data.mandatoryIcons[0])
                self.data.answer.updateValue(groupName: self.data.mandatoryIcons[0].name, index: 0, key: "key", value: .string("value1"))
                self.data.answer.updateValue(groupName: self.data.mandatoryIcons[0].name, index: 1, key: "key", value: .string("value2"))

                self.subject = CampaignForm(remoteData: self.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))
                ViewHosting.host(view: self.subject)
            }

            return true
        })

        let exp2 = waitFor(timeout: 5, validation: {
            let fieldLabels = try self.subject.inspect().findAll(CampaignGroupLabel.self)
            return fieldLabels.count == 4
        })

        wait(for: [exp1, exp2], timeout: 5)
    }

    func test_renders_the_two_groups_as_icon_section() async throws {
        let fieldLabels = try subject.inspect().findAll(CampaignGroupLabel.self)
        let text = try fieldLabels.map { try $0.find(ViewType.Text.self).string() }

        expect(text).to(equal(["Location", "Pictures", "1.Some icon list group", "2.Some icon list group"]))
    }

    func test_renders_delete_icons() async throws {
        let buttons = try subject.inspect().findAll(where: { (try? $0.accessibilityLabel().string() == "Delete group") ?? false })
        expect(buttons.count).to(equal(2))
    }

    func test_deletes_a_group_when_a_button_is_pressed() async throws {
        let buttons = try subject.inspect().findAll(where: { (try? $0.accessibilityLabel().string() == "Delete group") ?? false })

        try buttons[0].button().tap()

        expect(self.data.answer.attributes["Some icon list group"]?.asList()?.count).to(equal(1))
        expect(self.data.answer.attributes["Some icon list group"]?.asList()?[0]["key"]).to(equal(.string("value2")))

        expect(self.data.iconGroupList.count).to(equal(2))
        expect(self.data.iconGroupList[0].id).to(equal("2-0-2"))
        expect(self.data.iconGroupList[1].id).to(equal("2--1"))
    }
}

class View_CampaignForm_when_there_is_a_campaign_with_an_icon_with_a_long_text_type_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconLongTextGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign)

        DispatchQueue.main.sync {
            subject = CampaignForm(remoteData: self.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

            let exp = waitFor(validation: {
                let fieldLabels = try self.subject.inspect().findAll(CampaignGroupLabel.self)
                return fieldLabels.count == 3
            })

            ViewHosting.host(view: subject)

            wait(for: [exp], timeout: 2)
        }
    }

    func test_updates_the_answer_when_the_values_are_changed() async throws {
        let field = try subject?.inspect()
            .find(FieldLongText.self)
            .zStack()
            .textEditor(0)

        try field?.callOnChange(newValue: "123")

        let exp1 = waitFor(timeout: 5, validation: {
            if self.data.answer.attributes["Some icon group"] == nil {
                return false
            }

            if self.data.answer.attributes["Some icon group"]!["phone"] == nil {
                return false
            }

            let value = self.data.answer.attributes["Some icon group"]!["phone"]

            expect(value).to(equal(.string("123")))
            return true
        })

        wait(for: [exp1], timeout: 5)
    }
}

class View_CampaignForm_when_there_is_a_campaign_with_an_icon_with_an_integer_type_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconIntegerGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign!)

        DispatchQueue.main.sync {
            subject = CampaignForm(remoteData: self.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

            let exp = waitFor(validation: {
                let fieldLabels = try self.subject.inspect().findAll(CampaignGroupLabel.self)
                return fieldLabels.count == 3
            })

            ViewHosting.host(view: subject)
            wait(for: [exp], timeout: 2)
        }
    }

    func test_updates_the_answer_when_the_values_are_changed() async throws {
        let field = try subject?.inspect()
            .find(FieldInteger.self)
            .textField()

        try field?.callOnChange(newValue: "123")

        let exp1 = waitFor(timeout: 5, validation: {
            if self.data.answer.attributes["Some icon group"] == nil {
                return false
            }

            if self.data.answer.attributes["Some icon group"]!["phone"] == nil {
                return false
            }

            let value = self.data.answer.attributes["Some icon group"]!["phone"]

            expect(value).to(equal(.number(123)))
            return true
        })

        wait(for: [exp1], timeout: 5)
    }
}

class View_CampaignForm_when_there_is_a_campaign_with_an_icon_with_a_decimal_type_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconDecimalGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign)

        DispatchQueue.main.sync {
            subject = CampaignForm(remoteData: self.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

            let exp = waitFor(validation: {
                let fieldLabels = try self.subject.inspect().findAll(CampaignGroupLabel.self)
                return fieldLabels.count == 3
            })

            ViewHosting.host(view: subject)
            self.wait(for: [exp], timeout: 2)
        }
    }

    func test_updates_the_answer_when_the_values_are_changed_with_a_number_with_dot() async throws {
        let field = try subject?.inspect()
            .find(FieldDecimal.self)
            .textField()

        try field?.callOnChange(newValue: "123.4")

        let exp1 = waitFor(timeout: 5, validation: {
            if self.data.answer.attributes["Some icon group"] == nil {
                return false
            }

            if self.data.answer.attributes["Some icon group"]!["phone"] == nil {
                return false
            }

            let value = self.data.answer.attributes["Some icon group"]!["phone"]

            expect(value).to(equal(.double(123.4)))
            return true
        })

        wait(for: [exp1], timeout: 5)
    }

    func test_updates_the_answer_when_the_values_are_changed_with_a_number_with_comma() async throws {
        let field = try subject?.inspect()
            .find(FieldDecimal.self)
            .textField()

        try field?.callOnChange(newValue: "123,4")

        let exp1 = waitFor(timeout: 5, validation: {
            if self.data.answer.attributes["Some icon group"] == nil {
                return false
            }

            if self.data.answer.attributes["Some icon group"]!["phone"] == nil {
                return false
            }

            let value = self.data.answer.attributes["Some icon group"]!["phone"]

            expect(value).to(equal(.double(123.4)))
            return true
        })

        wait(for: [exp1], timeout: 5)
    }
}

class View_CampaignForm_when_there_is_a_campaign_with_an_icon_with_a_boolean_type_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await
            setupTestData()

        campaign = Campaign.getWithIconBooleanGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign!)

        DispatchQueue.main.sync {
            subject = CampaignForm(remoteData: self.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

            let exp = waitFor(validation: {
                let fieldLabels = try self.subject.inspect().findAll(CampaignGroupLabel.self)
                return fieldLabels.count == 3
            })

            ViewHosting.host(view: subject)
            self.wait(for: [exp], timeout: 2)
        }
    }

    func test_updates_the_answer_when_the_values_are_changed() async throws {
        let field = try subject?.inspect()
            .find(FieldBoolean.self)

        try field?.hStack().button(0).tap()

        let exp1 = waitFor(timeout: 5, validation: {
            if self.data.answer.attributes["Some icon group"] == nil {
                return false
            }

            if self.data.answer.attributes["Some icon group"]!["phone"] == nil {
                return false
            }

            let value = self.data.answer.attributes["Some icon group"]!["phone"]

            expect(value).to(equal(.bool(true)))
            return true
        })

        wait(for: [exp1], timeout: 5)
    }
}

class View_CampaignForm_when_there_is_a_campaign_with_an_icon_with_an_options_type_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconOptionsGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign)

        DispatchQueue.main.sync {
            subject = CampaignForm(remoteData: self.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

            let exp = waitFor(validation: {
                let field = try self.subject?.inspect().find(FieldOptions.self)
                return field != nil
            })

            ViewHosting.host(view: subject)
            self.wait(for: [exp], timeout: 2)
        }
    }

    func test_updates_the_answer_when_the_values_are_changed() async throws {
        let field = try subject?.inspect().find(FieldOptions.self).actualView()
        field?.onChange(.string("new value"))

        let value = data?.answer.attributes["Some icon group"]!["phone"]
        expect(value).to(equal(.string("new value")))
    }
}

class View_CampaignForm_when_there_is_a_campaign_with_an_icon_with_an_with_other_type_Test: XCTestCase {
    var subject: CampaignForm!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconOptionsWithOtherGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign!)

        DispatchQueue.main.sync {
            subject = CampaignForm(remoteData: self.data, isValid: Binding<Bool>(wrappedValue: false), pictures: Binding(wrappedValue: []))

            let exp = waitFor(validation: {
                let field = try self.subject?.inspect().find(FieldOptions.self)
                return field != nil
            })

            ViewHosting.host(view: subject)
            self.wait(for: [exp], timeout: 2)
        }
    }

    func test_updates_the_answer_when_the_values_are_changed() async throws {
        let field = try subject?.inspect().find(FieldOptionsWithOther.self).actualView()
        field?.onChange(.string("new other value"))

        let value = data?.answer.attributes["Some icon group"]!["phone"]
        expect(value).to(equal(.string("new other value")))
    }
}
