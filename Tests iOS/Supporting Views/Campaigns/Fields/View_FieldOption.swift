//
//  View_FieldOption.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 18.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_FieldOptionsTests: QuickSpec {
    override func spec() {
        var value: Binding<Attribute>?
        var subject: FieldOptions?

        describe("FieldOptions view") {
            describe("when the value is set") {
                beforeEach {
                    value = Binding(get: { .string("option1") }, set: { _ in })
                    subject = FieldOptions(value: value!, options: "option1,option2,option3") { _ in }

                    ViewHosting.host(view: subject)
                }

                it("renders the field value") {
                    let text = try subject.inspect().find(ViewType.Text.self).string()
                    expect(text).to(equal("option1"))
                }
            }
        }

        describe("when the options are opened") {
            var updatedAttribute: Attribute = .undefined

            beforeEach {
                updatedAttribute = .undefined

                subject = FieldOptions(value: Binding<Attribute>(wrappedValue: .undefined), options: "option1,option2,option3", isOpen: true, onChange: { attr in
                    updatedAttribute = attr
                })
            }

            it("triggers the on change event when an option is selected") {
                let buttons = try subject!.inspect().findAll(ViewType.Button.self)

                try buttons[1].tap()

                expect(updatedAttribute).to(equal(.string("option1")))
            }

            it("sets the is open flag to false") {
                let exp = subject!.inspection.inspect { view in
                    expect(try view.actualView().isOpen).to(equal(true))

                    let buttons = try view.actualView().inspect().findAll(ViewType.Button.self)
                    try buttons[1].tap()

                    expect(try view.actualView().isOpen).to(equal(false))
                }

                ViewHosting.host(view: subject)

                self.wait(for: [exp], timeout: 2)
            }
        }
    }
}
