//
//  View_FieldOption.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 18.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_FieldOptionsWithOtherTests: QuickSpec {
    override func spec() {
        var value: Binding<Attribute>!
        var subject: FieldOptionsWithOther!

        describe("when an option value is set") {
            var updatedAttribute: Attribute = .undefined

            beforeEach {
                updatedAttribute = .undefined

                value = Binding<Attribute>(wrappedValue: .string("option1"))
                subject = FieldOptionsWithOther(value: value, options: "option1,option2,option3", onChange: { attr in
                    updatedAttribute = attr
                })
            }

            it("renders the field value") {
                let text = try subject.inspect().find(ViewType.Text.self).string()
                expect(text).to(equal("option1"))
            }

            it("triggers the on change event when an option is selected") {
                try subject.inspect().find(FieldOptions.self).actualView().onChange(.string("option1"))

                expect(updatedAttribute).to(equal(.string("option1")))
            }

            it("does not trigger the on change event when the selected option is `other`") {
                try subject.inspect().find(FieldOptions.self).actualView().onChange(.string("other"))

                expect(updatedAttribute).to(equal(.undefined))
            }
        }

        describe("when an other option value is set") {
            var updatedAttribute: Attribute = .undefined

            beforeEach {
                updatedAttribute = .undefined

                value = Binding<Attribute>(wrappedValue: .string("some option"))
                subject = FieldOptionsWithOther(value: value, options: "option1,option2,option3", onChange: { attr in
                    updatedAttribute = attr
                })
            }

            it("renders the other option") {
                let text = try subject.inspect().find(FieldOptions.self).find(ViewType.Text.self).string()
                expect(text).to(equal("other"))
            }

            it("renders a hint message") {
                let text = try subject.inspect().find(ViewType.Text.self).string()
                expect(text).to(equal("Please specify if you selected “Other”."))
            }

            it("triggers the on change event when the other value is filled in") {
                let field = try subject?.inspect()
                    .find(FieldText.self)
                    .textField()

                try field?.callOnChange(newValue: "text")

                expect(updatedAttribute).to(equal(.string("text")))
            }
        }
    }
}
