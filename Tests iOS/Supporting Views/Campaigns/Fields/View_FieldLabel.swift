//
//  View_FieldLabel.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 10.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_FieldLabelTests: QuickSpec {
    var subject: FieldLabel!

    override func spec() {
        describe("for a required label") {
            beforeEach {
                self.subject = FieldLabel(label: "some label", isRequired: true)
            }

            it("renders the label string") {
                let text = try self.subject.inspect().findAll(ViewType.Text.self)[0].string()
                expect(text).to(equal("some label"))
            }

            it("renders the required sign") {
                let text = try self.subject.inspect().findAll(ViewType.Text.self)[1].string()
                expect(text).to(equal("*"))
            }
        }

        describe("for an optional label") {
            beforeEach {
                self.subject = FieldLabel(label: "some label", isRequired: false)
            }

            it("renders the label string") {
                let text = try self.subject.inspect().findAll(ViewType.Text.self)[0].string()
                expect(text).to(equal("some label"))
            }

            it("does not render the required sign") {
                let texts = try self.subject.inspect().findAll(ViewType.Text.self)
                expect(texts.count).to(equal(1))
            }
        }
    }
}
