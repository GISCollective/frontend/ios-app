//
//  CampaignField.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 17.08.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_CampaignFieldTests: QuickSpec {
    override func spec() {
        describe("CampaignField view") {
            xdescribe("the field name") {
                it("should render the attribute displayName when is set") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "", options: nil, isPrivate: false, isRequired: false, displayName: "display name", help: "", isInherited: false).withValue) { _, _, _, _ in }
                    let text = try subject.inspect().text(0).string()

                    expect(text).to(equal("display name"))
                }

                it("should render the attribute name when the displayName is not set is set") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "", options: nil, isPrivate: false, isRequired: false, displayName: nil, help: "", isInherited: false).withValue) { _, _, _, _ in }
                    let text = try subject.inspect().text(0).string()

                    expect(text).to(equal("name"))
                }

                it("should render the attribute name when the displayName is an empty string") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "", options: nil, isPrivate: false, isRequired: false, displayName: "", help: "", isInherited: false).withValue) { _, _, _, _ in }
                    let text = try subject.inspect().text(0).string()

                    expect(text).to(equal("name"))
                }
            }

            describe("the help message") {
                it("should not be rendered when is nil") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "", options: nil, isPrivate: false, isRequired: false, displayName: "", help: nil, isInherited: false).withValue) { _, _, _, _ in }

                    var found: Bool = true
                    do {
                        _ = try subject.inspect().text(1)
                    } catch {
                        found = false
                    }

                    expect(found).to(equal(false))
                }

                it("should not be rendered when an empty string") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "", options: nil, isPrivate: false, isRequired: false, displayName: "", help: "", isInherited: false).withValue) { _, _, _, _ in }

                    var found: Bool = true
                    do {
                        _ = try subject.inspect().text(1)
                    } catch {
                        found = false
                    }

                    expect(found).to(equal(false))
                }

                xit("should not be rendered when an empty string") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "", options: nil, isPrivate: false, isRequired: false, displayName: "", help: "help", isInherited: false).withValue) { _, _, _, _ in }

                    let text = try subject.inspect().text(1).string()

                    expect(text).to(equal("help"))
                }
            }

            describe("the editor type") {
                it("should be a text editor by default") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "", options: nil, isPrivate: false, isRequired: false, displayName: "", help: nil, isInherited: false).withValue) { _, _, _, _ in }

                    let found = subject.canFindView(FieldText.self)
                    expect(found).to(equal(true))
                }

                it("should render an options field when the type is options") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "options", options: nil, isPrivate: false, isRequired: false, displayName: "", help: nil, isInherited: false).withValue) { _, _, _, _ in }

                    let hasTextInput = subject.canFindView(FieldText.self)
                    let found = subject.canFindView(FieldOptions.self)
                    expect(found).to(equal(true))
                    expect(hasTextInput).to(equal(false))
                }

                it("should render an options with other field when the type is options with other") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "options with other", options: nil, isPrivate: false, isRequired: false, displayName: "", help: nil, isInherited: false).withValue) { _, _, _, _ in }

                    let hasTextInput = subject.canFindView(FieldText.self)
                    let found = subject.canFindView(FieldOptionsWithOther.self)
                    expect(found).to(equal(true))
                    expect(hasTextInput).to(equal(false))
                }

                it("should render an options with other field when the type is options with other") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "options with other", options: nil, isPrivate: false, isRequired: false, displayName: "", help: nil, isInherited: false).withValue) { _, _, _, _ in }

                    let hasTextInput = subject.canFindView(FieldText.self)
                    let found = subject.canFindView(FieldOptionsWithOther.self)
                    expect(found).to(equal(true))
                    expect(hasTextInput).to(equal(false))
                }

                it("should render an integer field when the type is integer") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "integer", options: nil, isPrivate: false, isRequired: false, displayName: "", help: nil, isInherited: false).withValue) { _, _, _, _ in }

                    let hasTextInput = subject.canFindView(FieldText.self)
                    let found = subject.canFindView(FieldInteger.self)
                    expect(found).to(equal(true))
                    expect(hasTextInput).to(equal(false))
                }

                it("should render a boolean field when the type is boolean") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "boolean", options: nil, isPrivate: false, isRequired: false, displayName: "", help: nil, isInherited: false).withValue) { _, _, _, _ in }

                    let hasTextInput = subject.canFindView(FieldText.self)
                    let found = subject.canFindView(FieldBoolean.self)
                    expect(found).to(equal(true))
                    expect(hasTextInput).to(equal(false))
                }

                it("should render a decimal field when the type is decimal") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "decimal", options: nil, isPrivate: false, isRequired: false, displayName: "", help: nil, isInherited: false).withValue) { _, _, _, _ in }

                    let hasTextInput = subject.canFindView(FieldText.self)
                    let found = subject.canFindView(FieldDecimal.self)
                    expect(found).to(equal(true))
                    expect(hasTextInput).to(equal(false))
                }

                it("should render a long text field when the type is long text") {
                    let subject = CampaignField(name: "", index: 0, attributeWithValue: IconAttribute.withData(name: "name", type: "long text", options: nil, isPrivate: false, isRequired: false, displayName: "", help: nil, isInherited: false).withValue) { _, _, _, _ in }

                    let hasTextInput = subject.canFindView(FieldText.self)
                    let found = subject.canFindView(FieldLongText.self)
                    expect(found).to(equal(true))
                    expect(hasTextInput).to(equal(false))
                }
            }
        }
    }
}

extension View where Self: ViewInspector.Inspectable {
    func canFindView<V>(_ inspectable: V.Type) -> Bool where V: Inspectable {
        var found = true
        do {
            _ = try inspect().find(inspectable)
        } catch {
            found = false
        }

        return found
    }
}
