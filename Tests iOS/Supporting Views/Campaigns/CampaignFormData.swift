//
//  CampaignFormData.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 08.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector
import XCTest

class View_CampaignFormData_when_there_is_a_campaign_with_an_icon_with_a_list_of_two_groups_Test: XCTestCase {
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconListGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign)

        let exp = waitFor(timeout: 5, validation: {
            if self.data.mandatoryIcons.count == 0 {
                return false
            }

            self.data.addIcon(self.data.mandatoryIcons[0])
            self.data.answer.updateValue(groupName: self.data.mandatoryIcons[0].name, index: 0, key: "key", value: .string("value1"))
            self.data.answer.updateValue(groupName: self.data.mandatoryIcons[0].name, index: 1, key: "key", value: .string("value2"))

            return true
        })

        wait(for: [exp], timeout: 5)
    }

    override func tearDownWithError() throws {}

    func test_delete_the_first_group_from_the_list() async throws {
        data.removeGroup(icon: data.mandatoryIcons[0], index: 0)

        expect(self.data.answer.attributes["Some icon list group"]?.asList()?.count).to(equal(1))
        expect(self.data.answer.attributes["Some icon list group"]?.asList()?[0]["key"]).to(equal(.string("value2")))

        expect(self.data.iconGroupList.count).to(equal(2))

        if data.iconGroupList.count != 2 { return }

        expect(self.data.iconGroupList[0].id).to(equal("2-0-2"))
        expect(self.data.iconGroupList[1].id).to(equal("2--1"))
    }
}

class View_CampaignFormData_when_there_is_a_campaign_with_an_icon_with_a_list_of_two_groups_without_values_Test: XCTestCase {
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconListGroup()
        campaign.options!.showNameQuestion = false
        campaign.options!.showDescriptionQuestion = false

        data = CampaignFormData(campaign)

        let exp = waitFor(timeout: 5, validation: {
            if self.data.mandatoryIcons.count == 0 {
                return false
            }

            self.data.addIcon(self.data.mandatoryIcons[0])
            self.data.answer.attributes[self.data.mandatoryIcons[0].name] = .list([])

            return true
        })

        wait(for: [exp], timeout: 5)
    }

    override func tearDownWithError() throws {}

    func test_can_delete_the_first_group_from_the_list() async throws {
        data.removeGroup(icon: data.mandatoryIcons[0], index: 0)

        expect(self.data.answer.attributes["Some icon list group"]?.asList()?.count).to(equal(0))

        expect(self.data.iconGroupList.count).to(equal(2))
        expect(self.data.iconGroupList[0].id).to(equal("2-0-2"))
        expect(self.data.iconGroupList[1].id).to(equal("2--1"))
    }
}

class View_CampaignFormData_when_there_is_a_campaign_with_an_icon_with_values_Test: XCTestCase {
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        try await setupTestData()

        campaign = Campaign.getWithIconTextGroup()
        data = CampaignFormData(campaign)

        let exp = waitFor(timeout: 5, validation: {
            if self.data.mandatoryIcons.count == 0 {
                return false
            }

            self.data.answer.attributes[self.data.mandatoryIcons[0].name] = .group([:])

            return true
        })

        wait(for: [exp], timeout: 5)
    }

    override func tearDownWithError() throws {}

    func test_delete_the_group() async throws {
        data.removeGroup(icon: data.mandatoryIcons[0], index: -1)

        expect(self.data.answer.attributes[self.data.mandatoryIcons[0].name]).to(beNil())
        expect(self.data.iconGroupList.count).to(equal(0))
    }
}
