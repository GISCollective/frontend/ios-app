//
//  View_CampaignDetail.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 14.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Combine
import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector
import XCTest

class View_CampaignDetail_Test: XCTestCase {
    var subject: CampaignDetail!
    var campaign: Campaign!
    var data: CampaignFormData!

    override func setUp() async throws {
        Persistence.instance = Persistence(path: ":memory:")
        try await setupTestData()
        campaign = Campaign.getWithIconTextGroup()
    }

    func test_renders_the_form_when_the_registration_is_not_required() async throws {
        campaign.options!.registrationMandatory = false

        DispatchQueue.main.sync {
            subject = CampaignDetail(campaign: campaign, remoteData: CampaignFormData(campaign), onClose: {})

            let form = try? subject?.inspect().find(CampaignForm.self)
            let message = try? subject?.inspect().find(CampaignRegistrationMessage.self)

            expect(form).notTo(beNil())
            expect(message).to(beNil())
        }
    }

    func test_does_not_render_the_form_when_the_registration_is_required_and_the_user_is_not_logged_in() async throws {
        campaign.options!.registrationMandatory = true

        UserSessionService.instance = UserSessionService()

        DispatchQueue.main.sync {
            UserSessionService.instance.isAuthenticated = false
        }

        DispatchQueue.main.sync {
            subject = CampaignDetail(campaign: campaign, remoteData: CampaignFormData(campaign), onClose: {})

            let form = try? subject?.inspect().find(CampaignForm.self)
            let message = try? subject?.inspect().find(CampaignRegistrationMessage.self)

            expect(form).to(beNil())
            expect(message).notTo(beNil())
        }
    }

    func test_renders_the_form_when_the_registration_is_required_and_the_user_is_logged_in() async throws {
        campaign.options!.registrationMandatory = true

        UserSessionService.instance = UserSessionService()

        DispatchQueue.main.sync {
            UserSessionService.instance.isAuthenticated = true
        }

        DispatchQueue.main.sync {
            subject = CampaignDetail(campaign: campaign, remoteData: CampaignFormData(campaign), onClose: {})

            let form = try? subject?.inspect().find(CampaignForm.self)
            let message = try? subject?.inspect().find(CampaignRegistrationMessage.self)

            expect(message).to(beNil())
            expect(form).notTo(beNil())
        }
    }

//    func test_adds_a_new_record_to_the_campaign_answer_store_on_submit() async throws {
//        try DispatchQueue.main.sync {
//            subject = CampaignDetail(campaign: campaign, remoteData: CampaignFormData(campaign), onClose: {})
//
//            try subject.inspect().find(button: "Submit").tap()
//        }
//
//        let modelStorage = await Persistence.instance?.getModelStorage(name: "CampaignAnswers")
//        let count = try await modelStorage?.count()
//
//        expect(count).to(equal(1))
//    }
}
