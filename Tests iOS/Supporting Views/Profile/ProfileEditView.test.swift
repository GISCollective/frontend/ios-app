//
//  ProfileEditView.test.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 02.10.21.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector
import XCTest

class View_ProfileEditView_Test: XCTestCase {
    var subject: ProfileEditView!

    func test_renders_all_fields_when_the_profile_is_fully_filled() throws {
        let profile = UserProfile(id: "")
        profile.bio = "some bio"
        profile.firstName = "first"
        profile.lastName = "last"
        profile.jobTitle = "job"
        profile.joinedTime = Date(timeIntervalSince1970: 0)
        profile.linkedin = "linkedin"
        profile.location = Location()
        profile.location.simple = "location"
        profile.organization = "organization"
        profile.pictureId = "picture"
        profile.salutation = "mr"
        profile.skype = "skype"
        profile.title = "title"
        profile.twitter = "twitter"
        profile.website = "website"

        subject = ProfileEditView(profile: profile, onUpdate: { _, _ in })

        let exp = subject.inspection.inspect(after: 0.1) { view in
            let strings = view.findAll(ViewType.Text.self).map { try? $0.string() }
            let values = view.findAll(ViewType.TextField.self).map { try? $0.input() }

            expect(strings).to(equal(["Photo Library", "Take Photo", "Change picture", "Salutation:", "None", "Mr.", "Ms.", "Mrs.", "Salutation", "Title:", "", "First name:", "", "Last name:", "", "Location:", "", "Skype:", "", "LinkedIn:", "", "Twitter:", "", "Website URL:", "", "Job title:", "", "Organization:", "", "Bio:", "", "Save"]))

            expect(values).to(equal(["title", "first", "last", "location", "skype", "linkedin", "twitter", "website", "job", "organization", "some bio"]))
        }

        ViewHosting.host(view: subject)
        wait(for: [exp], timeout: 2)
    }

    func test_renders_the_detailed_location_fields_when_the_location_is_detailed() throws {
        let profile = UserProfile(id: "")
        profile.bio = "some bio"
        profile.firstName = "first"
        profile.lastName = "last"
        profile.jobTitle = "job"
        profile.joinedTime = Date(timeIntervalSince1970: 0)
        profile.linkedin = "linkedin"
        profile.location = Location.withData(detailedLocation: DetailedLocation.withData(city: "cluj", country: "ro", postalCode: "10999", province: "cj"), isDetailed: true, simple: "location")
        profile.organization = "organization"
        profile.pictureId = "picture"
        profile.salutation = "mr"
        profile.skype = "skype"
        profile.title = "title"
        profile.twitter = "twitter"
        profile.website = "website"

        subject = ProfileEditView(profile: profile, onUpdate: { _, _ in })

        let exp = subject.inspection.inspect(after: 0.1) { view in
            let strings = view.findAll(ViewType.Text.self).map { try? $0.string() }
            let values = view.findAll(ViewType.TextField.self).map { try? $0.input() }

            expect(strings).to(equal(["Photo Library", "Take Photo", "Change picture", "Salutation:", "None", "Mr.", "Ms.", "Mrs.", "Salutation", "Title:", "", "First name:", "", "Last name:", "", "City:", "", "Province:", "", "Country:", "", "Postal code:", "", "Skype:", "", "LinkedIn:", "", "Twitter:", "", "Website URL:", "", "Job title:", "", "Organization:", "", "Bio:", "", "Save"]))
            expect(values).to(equal(["title", "first", "last", "cluj", "cj", "ro", "10999", "skype", "linkedin", "twitter", "website", "job", "organization", "some bio"]))
        }

        ViewHosting.host(view: subject)
        wait(for: [exp], timeout: 2)
    }

    func test_updates_all_profile_fields_when_the_location_is_simple() throws {
        let profile = UserProfile(id: "")
        profile.location = Location.withData(detailedLocation: DetailedLocation(), isDetailed: false, simple: "")

        subject = ProfileEditView(profile: profile, onUpdate: { _, _ in })

        let exp1 = subject.inspection.inspect(after: 0.1) { view in
            let values = view.findAll(ViewType.TextField.self)

            var index = 0

            values.forEach { view in
                try? view.setInput("value \(index)")
                index += 1
            }
        }

        let exp2 = subject.inspection.inspect(after: 0.2) { view in
            let values = view.findAll(ViewType.TextField.self).map { try? $0.input() }

            expect(values).to(equal(["value 0", "value 1", "value 2", "value 3", "value 4", "value 5", "value 6", "value 7", "value 8", "value 9", "value 10"]))
        }

        ViewHosting.host(view: subject)
        wait(for: [exp1, exp2], timeout: 2)
    }

    func test_updates_all_profile_fields_when_the_location_is_detailed() throws {
        let profile = UserProfile(id: "")
        profile.location = Location.withData(detailedLocation: DetailedLocation(), isDetailed: true, simple: "")

        subject = ProfileEditView(profile: profile, onUpdate: { _, _ in })

        let exp1 = subject.inspection.inspect(after: 0.1) { view in
            let values = view.findAll(ViewType.TextField.self)

            var index = 0

            values.forEach { view in
                try? view.setInput("value \(index)")
                index += 1
            }
        }

        let exp2 = subject.inspection.inspect(after: 0.2) { view in
            let values = view.findAll(ViewType.TextField.self).map { try? $0.input() }

            expect(values).to(equal(["value 0", "value 1", "value 2", "value 3", "value 4", "value 5", "value 6", "value 7", "value 8", "value 9", "value 10", "value 11", "value 12", "value 13"]))
        }

        ViewHosting.host(view: subject)
        wait(for: [exp1, exp2], timeout: 2)
    }
}
