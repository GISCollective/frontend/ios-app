//
//  View_ProfileView.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 14.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector

class View_ProfileView: QuickSpec {
    override func spec() {
        var subject: ProfileView!

        describe("ProfileView") {
            it("renders a placeholder for the name when the fields are not set") {
                let profile = UserProfile(id: "")

                subject = ProfileView(profile: profile)

                let strings = try? subject.inspect().findAll(ViewType.Text.self).map { try? $0.string() }

                expect(strings).to(equal(["---"]))
            }

            it("does not show an edit button when canEdit is false") {
                let profile = UserProfile(id: "")
                profile.canEdit = false

                subject = ProfileView(profile: profile)

                let buttons = try? subject.inspect().findAll(ViewType.Button.self)
                expect(buttons?.count).to(equal(0))
            }

            it("shows an edit button when canEdit is true") {
                let profile = UserProfile(id: "")
                profile.canEdit = true

                subject = ProfileView(profile: profile)

                let buttons = try? subject.inspect().findAll(ViewType.Button.self)
                expect(buttons?.count).to(equal(1))
            }

            it("triggers the onEdit action when the button is pressed") {
                let profile = UserProfile(id: "")
                profile.canEdit = true

                var pressed: Bool?
                subject = ProfileView(profile: profile, onEdit: { pressed = true })

                try? subject.inspect().find(ViewType.Button.self).tap()
                expect(pressed).to(equal(true))
            }

            xit("renders all fields when the profile is fully filled") {
                let profile = UserProfile(id: "")
                profile.bio = "some bio"
                profile.firstName = "first"
                profile.lastName = "last"
                profile.jobTitle = "job"
                profile.joinedTime = Date(timeIntervalSince1970: 0)
                profile.linkedin = "linkedin"
                profile.location = Location.withData(detailedLocation: nil, isDetailed: false, simple: "location")
                profile.organization = "organization"
                profile.pictureId = "picture"
                profile.salutation = "mr"
                profile.skype = "skype"
                profile.title = "title"
                profile.twitter = "twitter"
                profile.website = "website"

                subject = ProfileView(profile: profile)

                let strings = try? subject.inspect().findAll(ViewType.Text.self).map { try? $0.string() }

                expect(strings).to(equal(["title first last", "Member since January 01, 1970", "location", "job at organization", "skype", "linkedin", "twitter", "website", "some bio"]))
            }
        }
    }
}
