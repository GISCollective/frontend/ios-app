//
//  GEOSwiftGeometry.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 30.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import GEOSwift
import UIKit

import Nimble
import Quick

@testable import GISCollective

class GEOSwiftGeometryTests: QuickSpec {
    override func spec() {
        describe("UIColor extension") {
            it("should get the first point") {
                let line = try! Geometry(wkt: "LINESTRING(35 10, 45 45.5, 15 40, 10 20, 35 10)")

                expect(line.firstPoint().longitude).to(equal(35))
                expect(line.firstPoint().latitude).to(equal(10))
            }
        }
    }
}
