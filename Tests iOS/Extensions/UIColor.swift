//
//  UIColor.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 28.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import UIKit

import Nimble
import Quick

@testable import GISCollective

class UIColorTests: QuickSpec {
    override func spec() {
        describe("UIColor extension") {
            it("should return black for an empty string") {
                let color = UIColor.parseColor(value: "")
                expect(color).to(equal(UIColor(red: 0, green: 0, blue: 0, alpha: 1)))
            }

            it("should parse a hex color") {
                let color = UIColor.parseColor(value: "#ffffffff")
                expect(color).to(equal(UIColor(red: 1, green: 1, blue: 1, alpha: 1)))
            }

            it("should parse a rgb color") {
                let color = UIColor.parseColor(value: "rgb(255,255,255)")
                expect(color).to(equal(UIColor(red: 1, green: 1, blue: 1, alpha: 1)))
            }

            it("should parse a rgba color") {
                let color = UIColor.parseColor(value: "rgba(255,255,255,0)")
                expect(color).to(equal(UIColor(red: 1, green: 1, blue: 1, alpha: 0)))
            }
        }
    }
}
