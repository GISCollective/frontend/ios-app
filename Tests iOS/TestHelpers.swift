//
//  TestHelpers.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 01.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick
import SwiftUI
import ViewInspector
import XCTest

extension AttributeGroupListView: Inspectable {}
extension AttributeGroupView: Inspectable {}
extension CampaignAboutGroup: Inspectable {}
extension CampaignDetail: Inspectable {}
extension CampaignField: Inspectable {}
extension CampaignForm: Inspectable {}
extension CampaignGroup: Inspectable {}
extension CampaignGroupAdd: Inspectable {}
extension CampaignGroupIcon: Inspectable {}
extension CampaignGroupLabel: Inspectable {}
extension CampaignLocationEditor: Inspectable {}
extension CampaignLocationGroup: Inspectable {}
extension CampaignOptionalIcons: Inspectable {}
extension CampaignPictures: Inspectable {}
extension CampaignRegistrationMessage: Inspectable {}
extension CircleAnnotationView: Inspectable {}
extension CustomScrollView: Inspectable {}
extension EmailInput: Inspectable {}
extension FeatureAnnotationView: Inspectable {}
extension FeatureAttributes: Inspectable {}
extension FeatureDetail: Inspectable {}
extension FeatureMaskedGeometryMessage: Inspectable {}
extension FieldBoolean: Inspectable {}
extension FieldDecimal: Inspectable {}
extension FieldInteger: Inspectable {}
extension FieldLabel: Inspectable {}
extension FieldLongText: Inspectable {}
extension FieldOptions: Inspectable {}
extension FieldOptionsWithOther: Inspectable {}
extension FieldText: Inspectable {}
extension FieldValue: Inspectable {}
extension ForgotPasswordButton: Inspectable {}
extension ForgotPasswordForm: Inspectable {}
extension GetDirectionsButtonView: Inspectable {}
extension LoginScene: Inspectable {}
extension PrimaryButton: Inspectable {}
extension ProfileEditView: Inspectable {}
extension ProfileLinksView: Inspectable {}
extension ProfilePictureView: Inspectable {}
extension ProfileView: Inspectable {}
extension TaskButton: Inspectable {}
extension TextField: Inspectable {}
extension TextInput: Inspectable {}
extension TextInputWithProps: Inspectable {}

extension Inspection: InspectionEmissary {}

func waitFor(timeout: Double = 2.0, validation: @escaping () throws -> Bool) -> XCTestExpectation {
    let exp = XCTestExpectation(description: "waitFor")
    var remaining = timeout

    let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { timer in
        remaining = remaining - 0.1
        if remaining <= 0 {
            timer.invalidate()
            return
        }

        do {
            if try validation() == true {
                exp.fulfill()
                timer.invalidate()
            }
        } catch {
            print("waitFor error: \(error).")
        }
    }

    timer.fire()

    return exp
}

func waitFor(timeout: Double = 2.0, action: @escaping () throws -> Void) -> XCTestExpectation {
    return waitFor(timeout: timeout, validation: {
        do {
            try action()
            return true
        } catch {
            print("waitFor error: \(error).")
        }

        return false
    })
}
