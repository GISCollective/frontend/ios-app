//
//  PictureUpload.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 25.09.21.
//

import CoreData
import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick
import XCTest

class PictureUploadServiceTest: XCTestCase {
    var service: PictureUploadService!
    var testApi: TestApi!
    var mockPictures: SQLitePushQueuePersistence!
    var testData: Data!

    override func setUp() async throws {
        try await super.setUp()

        testApi = TestApi()

        Persistence.instance = Persistence(path: ":memory:")
        mockPictures = await Persistence.instance?.getModelStorage(name: "test")
        service = PictureUploadService(parentId: "parent-id", storage: mockPictures)

        Api.instance = testApi
        testData = Data(stringLiteral: "test")
    }

    func testLoadWhenThereIsALocalAndASentPicture() async throws {
        let item1 = StoredPushQueueElement(id: 0, slug: "1", remoteId: "1", parentId: "parent-id", data: testData, order: 1)
        let item2 = StoredPushQueueElement(id: 0, slug: "2", remoteId: "", parentId: "parent-id", data: testData, order: 2)

        try await mockPictures.insert(item: item1)
        try await mockPictures.insert(item: item2)

        try await service.load()

        expect(self.service.remoteIds).to(equal(["1"]))
        expect(self.service.localPictures.map { $0.slug }).to(equal(["2"]))
        expect(self.service.progress).to(equal(0.5))
    }

    func testLoadWhenThereAre2LocalPictures() async throws {
        let item1 = StoredPushQueueElement(id: 0, slug: "1", remoteId: "", parentId: "parent-id", data: testData, order: 1)
        let item2 = StoredPushQueueElement(id: 0, slug: "2", remoteId: "", parentId: "parent-id", data: testData, order: 2)

        try await mockPictures.insert(item: item1)
        try await mockPictures.insert(item: item2)

        try await service.load()

        expect(self.service.remoteIds).to(equal([]))
        expect(self.service.localPictures.map { $0.slug }).to(equal(["1", "2"]))
        expect(self.service.progress).to(equal(0))
    }

    func testLoadWhenThereAre2remotepictures() async throws {
        let item1 = StoredPushQueueElement(id: 0, slug: "1", remoteId: "a", parentId: "parent-id", data: testData, order: 1)
        let item2 = StoredPushQueueElement(id: 0, slug: "2", remoteId: "b", parentId: "parent-id", data: testData, order: 2)

        try await mockPictures.insert(item: item1)
        try await mockPictures.insert(item: item2)

        try await service.load()

        expect(self.service.remoteIds).to(equal(["a", "b"]))
        expect(self.service.localPictures.map { $0.id }).to(equal([]))
        expect(self.service.progress).to(equal(1))
    }

    /// send

    func testSendDoesNothingWhenThereAreNoPictures() async throws {
        let service = PictureUploadService(parentId: "parent-id", storage: mockPictures)
        try await service.load()
        let result = try await self.service.send()

        expect(result).to(equal([]))
    }

    func testSendWhenThereIsALocalAndASentPictureSendsTheRemotePicture() async throws {
        let item1 = StoredPushQueueElement(id: 0, slug: "1", remoteId: "a", parentId: "parent-id", data: testData, order: 1)
        let item2 = StoredPushQueueElement(id: 0, slug: "2", remoteId: "", parentId: "parent-id", data: testData, order: 2)

        try await mockPictures.insert(item: item1)
        try await mockPictures.insert(item: item2)

        let service = PictureUploadService(parentId: "parent-id", storage: mockPictures)
        try await service.load()

        let result = try! await service.send()

        expect(result).to(equal(["a", "1"]))

        // expect(self.mockPictures.pictures.map { $0.remoteId }).to(equal(["a", "1"]))
    }

    func testSendWhenThereAreTwoSentPicturesDoesNothing() async throws {
        let item1 = StoredPushQueueElement(id: 0, slug: "1", remoteId: "a", parentId: "parent-id", data: testData, order: 1)
        let item2 = StoredPushQueueElement(id: 0, slug: "2", remoteId: "b", parentId: "parent-id", data: testData, order: 2)

        try await mockPictures.insert(item: item1)
        try await mockPictures.insert(item: item2)

        let service = PictureUploadService(parentId: "parent-id", storage: mockPictures)
        try await service.load()

        let result = try! await service.send()

        expect(result).to(equal(["a", "b"]))

        // expect(self.mockPictures.pictures.map { $0.remoteId }).to(equal(["a", "b"]))
    }
}
