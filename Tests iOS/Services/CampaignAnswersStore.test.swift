//
//  CampaignAnswersStore.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 17.10.21.
//

import _Concurrency
import CoreData
import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick
import UIKit
import XCTest

class CampaignAnswersStoreTest: XCTestCase {
    var testApi: TestApi!
    var service: CampaignAnswerStore!
    var persistence: Persistence!
    var storage: SQLitePushQueuePersistence!

    override func setUp() async throws {
        try await super.setUp()

        testApi = TestApi()

        Persistence.instance = Persistence(path: ":memory:")
        persistence = Persistence.instance

        Api.instance = testApi

        service = CampaignAnswerStore()
        storage = await Persistence.instance?.getModelStorage(name: "CampaignAnswers")
    }

    func testWhenThereIsNoStoredAnswerHasTheStoredItemCount0() {
        expect(self.service.storedItemCount).to(equal(0))
    }

    func testCanStoreAcampaignAnswerWithNoPictures() async throws {
        let answer = CampaignAnswer(id: "")
        let item = AnswerWithPictures(answer: answer, pictures: [])

        try await service.save(item: item)

        let list = try await storage.getAll()

        expect(list.count).to(equal(1))
        expect(self.service.storedItemCount).to(equal(1))

        if list.count > 0 {
            expect(String(data: list[0].data, encoding: .utf8)).to(contain("\"_id\":\"\(answer.id)\""))
        }
    }

    func testCanStoreACampaignAnswerWithAPicture() async throws {
        let answer = CampaignAnswer(id: "1")
        let picture = UIImage(blurHash: "UKO2?V%2Tw=w]~RBVZRi};RPxuwHtLOtxZ%g", size: CGSize(width: 40, height: 40))!
        let item = AnswerWithPictures(answer: answer, pictures: [picture])

        try await service.save(item: item)

        let campaignAnswerList = try await storage.getAll()

        expect(campaignAnswerList.count).to(equal(1))
        if campaignAnswerList.count == 0 {
            return
        }

        let storedAnswer = try JSONDecoder().decode(CampaignAnswer.self, from: campaignAnswerList[0].data)
        let pictureList = try await Persistence.instance?.getModelStorage(name: "CampaignAnswersPictures").get(parentId: storedAnswer.id)

        expect(self.service.storedItemCount).to(equal(1))

        expect(pictureList?.count).to(equal(1))
        if pictureList?.count == 0 {
            return
        }

        expect(pictureList?[0].id).to(equal(1))
        expect(pictureList?[0].remoteId).to(equal(""))
        expect(pictureList?[0].parentId).to(equal(storedAnswer.id))
        expect(pictureList?[0].data.base64EncodedString()).to(contain("/9j/4AAQSkZJRgABAQAASABIAAD/4QBMRXhpZgAATU0AKgAAAAgAAgESAAMAAAABAAEAAIdpAAQAAAABAAAAJgA"))
    }

    func testCanStoreTwoCampaignAnswersWithAPicture() async throws {
        let answer = CampaignAnswer(id: "")
        let picture = UIImage(blurHash: "UKO2?V%2Tw=w]~RBVZRi};RPxuwHtLOtxZ%g", size: CGSize(width: 40, height: 40))!
        let item = AnswerWithPictures(answer: answer, pictures: [picture])

        try await service.save(item: item)
        try await service.save(item: item)

        let campaignAnswerList = try await storage.getAll()
        expect(campaignAnswerList.count).to(equal(2))
        if campaignAnswerList.count == 0 {
            return
        }

        let storedAnswer1 = try JSONDecoder().decode(CampaignAnswer.self, from: campaignAnswerList[0].data)
        let pictureList1 = try await Persistence.instance?.getModelStorage(name: "CampaignAnswersPictures").get(parentId: storedAnswer1.id)

        let storedAnswer2 = try JSONDecoder().decode(CampaignAnswer.self, from: campaignAnswerList[1].data)
        let pictureList2 = try await Persistence.instance?.getModelStorage(name: "CampaignAnswersPictures").get(parentId: storedAnswer2.id)

        expect(storedAnswer1.id).notTo(equal(storedAnswer2.id))

        expect(self.service.storedItemCount).to(equal(2))
        expect(pictureList1?.count).to(equal(1))
        expect(pictureList1?[0].id).to(equal(1))
        expect(pictureList1?[0].parentId).to(equal(storedAnswer1.id))

        expect(pictureList2?.count).to(equal(1))
        expect(pictureList2?[0].id).to(equal(2))
        expect(pictureList2?[0].parentId).to(equal(storedAnswer2.id))
    }

    func testCanStoreACampaignAnswerWithTwoPictures() async throws {
        let answer = CampaignAnswer(id: "1")
        let picture1 = UIImage(blurHash: "UKO2?V%2Tw=w]~RBVZRi};RPxuwHtLOtxZ%g", size: CGSize(width: 40, height: 40))!
        let picture2 = UIImage(blurHash: "UKO2?V%2Tw=w]~RBVZRi};RPxuwHtLOtxZ%g", size: CGSize(width: 20, height: 20))!
        let item = AnswerWithPictures(answer: answer, pictures: [picture1, picture2])

        try await service.save(item: item)

        let campaignAnswerList = try await storage.getAll()
        expect(campaignAnswerList.count).to(equal(1))

        if campaignAnswerList.count == 0 {
            return
        }

        let storedAnswer = try JSONDecoder().decode(CampaignAnswer.self, from: campaignAnswerList[0].data)
        let pictureList = try await Persistence.instance?.getModelStorage(name: "CampaignAnswersPictures").get(parentId: storedAnswer.id)

        expect(self.service.storedItemCount).to(equal(1))
        expect(pictureList?.count).to(equal(2))
        expect(pictureList?[0].id).to(equal(1))
        expect(pictureList?[0].remoteId).to(equal(""))
        expect(pictureList?[0].parentId).to(equal(storedAnswer.id))
        expect(pictureList?[0].data.base64EncodedString()).to(contain("/9j/4AAQSkZJRgABAQAASABIAAD/4QBMRXhpZgAATU0AKgAAAAgAAgESAAMAAAABAAEAAIdpAAQAAAABAAAAJgA"))
        expect(pictureList?[1].data.base64EncodedString()).to(contain("/9j/4AAQSkZJRgABAQAASABIAAD/4QBMRXhpZgAATU0AKgAAAAgAAgESAAMAAAABAAEAAIdpAAQAAAABAAAAJgA"))
    }

    func testCanSendAnAnswerWithoutPictures() async throws {
        let service = CampaignAnswerStore()

        let answer = CampaignAnswer(id: "new")
        answer.campaignId = "1"
        answer.info.createdOn = Date(timeIntervalSince1970: 0)
        answer.info.lastChangeOn = Date(timeIntervalSince1970: 0)

        try await service.save(item: AnswerWithPictures(answer: answer, pictures: []))
        try await service.sendAll()

        expect(self.testApi.created.count).to(equal(1))

        if testApi.created.count == 0 {
            return
        }

        expect(self.testApi.created[0]).to(equal("""
        {
            "campaignAnswer": {
                "_id":"1",
                "info":{
                    "changeIndex":0,"author":"","createdOn":-978307200,"lastChangeOn":-978307200
                },
                "position":null,
                "campaign":"1",
                "pictures":[],
                "attributes":{},
                "icons":null
            }
        }
        """.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "\n", with: "")))
    }

//                beforeEach {
//                    let localService = CampaignAnswerStore()
//                    waitUntil(timeout: .seconds(30)) { done in
//                        Task {
//                            let picture = UIImage(blurHash: "UKO2?V%2Tw=w]~RBVZRi};RPxuwHtLOtxZ%g", size: CGSize(width: 1, height: 1))!
//                            localService.store(picture: picture, answerId: "1", index: 0)
//                            localService.store(picture: picture, answerId: "1", index: 1)
//
//                            _ = try? await localService.sendPicturesFor(answerId: "1")
//                            done()
//                        }
//                    }
//                }
//
//                it("can send pictures of an answer") {
//                    expect(testApi.created.count).to(equal(2))
//
//                    if testApi.created.count == 0 {
//                        return
//                    }
//
//                    let picture1 = try! JSONDecoder().decode(PictureItem.self, from: testApi.created[0].data(using: .utf8)!)
//                    let picture2 = try! JSONDecoder().decode(PictureItem.self, from: testApi.created[1].data(using: .utf8)!)
//
//                    expect(picture1.picture.id).to(equal("1"))
//                    expect(picture1.picture.name).to(equal("image for 1"))
//
//                    expect(picture2.picture.id).to(equal("2"))
//                    expect(picture2.picture.name).to(equal("image for 1"))
//
//                    let pictureStorage = Persistence.instance?.pictures
//                    let pictureList = try? pictureStorage?.getByParent(id: "1")
//                    expect(pictureList?.count).to(equal(2))
//
//                    if pictureList?.count ?? 0 > 0 {
//                        expect(pictureList?[0].remoteId).to(equal("2"))
//                        expect(pictureList?[1].remoteId).to(equal("1"))
//                    }
//                }
}
