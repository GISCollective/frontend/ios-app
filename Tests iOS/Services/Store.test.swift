//
//  Store.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 23.06.22.
//

import _Concurrency
import CoreData
import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick
import UIKit
import XCTest

class Store_Test: XCTestCase {
    var testApi: TestApi!
    var service: Store!
    var persistence: Persistence!
    var storage: SQLitePushQueuePersistence!

    override func setUp() async throws {
        try await super.setUp()

        testApi = TestApi()

        Persistence.instance = Persistence(path: ":memory:")
        persistence = Persistence.instance

        Api.instance = testApi

        service = Store()
        storage = await Persistence.instance?.getModelStorage(name: "CampaignAnswers")

        testApi.data = """
            {"articles": [{
                "_id": "608eb5e2f1b87b0100770eb7",
                "info": {
                  "changeIndex": 35,
                  "createdOn": "0001-01-01T00:00:00Z",
                  "lastChangeOn": "2021-10-26T07:29:19Z",
                  "originalAuthor": "",
                  "author": "5b8eb19697d5681f1c64adc5"
                },
                "visibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "200000000000000000000002"
                },
                "title": "remote article",
                "content": {
                  "time": 1647214481248,
                  "blocks": []
                },
                "canEdit": true,
                "slug": "about"
            }]}
        """.data(using: .utf8)
    }

    func test_query_when_there_is_no_cached_data() async throws {
        let params: [String: String] = [:]
        let result = try await service.articles.query(params: params, collection: "all")

        expect(result.items.count).to(equal(1))
        expect(result.items[0].title).to(equal("remote article"))
    }

    func test_query_when_there_is_a_cached_list() async throws {
        let data = """
            {"articles": [{
                "_id": "608eb5e2f1b87b0100770eb7",
                "info": {
                  "changeIndex": 35,
                  "createdOn": "0001-01-01T00:00:00Z",
                  "lastChangeOn": "2021-10-26T07:29:19Z",
                  "originalAuthor": "",
                  "author": "5b8eb19697d5681f1c64adc5"
                },
                "visibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "200000000000000000000002"
                },
                "title": "local article",
                "content": {
                  "time": 1647214481248,
                  "blocks": []
                },
                "canEdit": true,
                "slug": "about"
            }]}
        """.data(using: .utf8)

        let model = await Persistence.instance?.getModelStorage(name: "articles")
        try await model?.save(item: StoredPushQueueElement(id: 0, slug: "all", remoteId: "", parentId: "", data: data!, order: 0))

        let params: [String: String] = [:]
        let result = try await service.articles.query(params: params, collection: "all")

        expect(result.items.count).to(equal(1))
        expect(result.items[0].title).to(equal("local article"))
    }

    func test_query_when_there_is_a_cached_list_when_online_and_forced() async throws {
        let data = """
            {"articles": [{
                "_id": "608eb5e2f1b87b0100770eb7",
                "info": {
                  "changeIndex": 35,
                  "createdOn": "0001-01-01T00:00:00Z",
                  "lastChangeOn": "2021-10-26T07:29:19Z",
                  "originalAuthor": "",
                  "author": "5b8eb19697d5681f1c64adc5"
                },
                "visibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "200000000000000000000002"
                },
                "title": "local article",
                "content": {
                  "time": 1647214481248,
                  "blocks": []
                },
                "canEdit": true,
                "slug": "about"
            }]}
        """.data(using: .utf8)

        let model = await Persistence.instance?.getModelStorage(name: "articles")
        try await model?.save(item: StoredPushQueueElement(id: 0, slug: "all", remoteId: "", parentId: "", data: data!, order: 0))

        let params: [String: String] = [:]
        let result = try await service.articles.query(params: params, collection: "all", force: true)

        expect(result.items.count).to(equal(1))
        expect(result.items[0].title).to(equal("remote article"))
    }

    func test_query_when_there_is_a_cached_list_when_offline_and_forced() async throws {
        let data = """
            {"articles": [{
                "_id": "608eb5e2f1b87b0100770eb7",
                "info": {
                  "changeIndex": 35,
                  "createdOn": "0001-01-01T00:00:00Z",
                  "lastChangeOn": "2021-10-26T07:29:19Z",
                  "originalAuthor": "",
                  "author": "5b8eb19697d5681f1c64adc5"
                },
                "visibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "200000000000000000000002"
                },
                "title": "local article",
                "content": {
                  "time": 1647214481248,
                  "blocks": []
                },
                "canEdit": true,
                "slug": "about"
            }]}
        """.data(using: .utf8)

        testApi.connectivity = .disconnected

        let model = await Persistence.instance?.getModelStorage(name: "articles")
        try await model?.save(item: StoredPushQueueElement(id: 0, slug: "all", remoteId: "", parentId: "", data: data!, order: 0))

        let params: [String: String] = [:]
        let result = try await service.articles.query(params: params, collection: "all", force: true)

        expect(result.items.count).to(equal(1))
        expect(result.items[0].title).to(equal("local article"))
    }
}
