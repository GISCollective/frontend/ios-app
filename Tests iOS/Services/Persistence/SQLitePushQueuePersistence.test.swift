//
//  PushQueuePersistence.swift
//  Tests macOS
//
//  Created by Bogdan Szabo on 10.06.22.
//

@testable import GISCollective
import Nimble
import Quick
import SQLite3
import XCTest

class SQLitePushQueuePersistenceTest: XCTestCase {
    var service: SQLitePushQueuePersistence!
    var db: OpaquePointer!

    override func setUp() {
        if sqlite3_open_v2(":memory:", &db, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX, nil) != SQLITE_OK {
            print("error opening database")
            sqlite3_close(db)
            db = nil
        }

        service = SQLitePushQueuePersistence(db: db, tableName: "test_push")
    }

    override func tearDownWithError() throws {}

    func testRetrieveStoredItemByRemoteId() async throws {
        let item = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                          order: 2)
        try await service.insert(item: item)
        let retreivedIcon1 = try? await service.get(remoteId: "remote")

        expect(retreivedIcon1?.id).to(equal(1))
        expect(retreivedIcon1?.slug).to(equal("slug"))
        expect(retreivedIcon1?.remoteId).to(equal("remote"))
        expect(retreivedIcon1?.parentId).to(equal("parent"))
        // expect(retreivedIcon1?.order).to(equal(2))
        expect(retreivedIcon1?.time).notTo(equal(0))
        expect(String(decoding: retreivedIcon1!.data, as: UTF8.self)).to(equal("hello!"))
    }

    func testRetrieveStoredItemBySlug() async throws {
        let item = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                          order: 2)
        try await service.insert(item: item)
        let retreivedIcon1 = try? await service.get(slug: "slug")

        expect(retreivedIcon1?.id).to(equal(1))
        expect(retreivedIcon1?.slug).to(equal("slug"))
        expect(retreivedIcon1?.remoteId).to(equal("remote"))
        expect(retreivedIcon1?.parentId).to(equal("parent"))
        // expect(retreivedIcon1?.order).to(equal(2))
        expect(retreivedIcon1?.time).notTo(equal(0))
        expect(String(decoding: retreivedIcon1!.data, as: UTF8.self)).to(equal("hello!"))
    }

    func testRetrieveOneStoredItemByParentId() async throws {
        let item = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                          order: 2)
        try await service.insert(item: item)
        let retreivedIcons = try! await service.get(parentId: "parent")
        let retreivedIcon1 = retreivedIcons[0]

        expect(retreivedIcon1.id).to(equal(1))
        expect(retreivedIcon1.slug).to(equal("slug"))
        expect(retreivedIcon1.remoteId).to(equal("remote"))
        expect(retreivedIcon1.parentId).to(equal("parent"))
        // expect(retreivedIcon1?.order).to(equal(2))
        expect(retreivedIcon1.time).notTo(equal(0))
        expect(String(decoding: retreivedIcon1.data, as: UTF8.self)).to(equal("hello!"))
    }

    func testRetrieveTwoStoredItemByParentId() async throws {
        let item1 = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                           order: 2)
        let item2 = StoredPushQueueElement(id: 1, slug: "slug2", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                           order: 2)
        try await service.insert(item: item1)
        try await service.insert(item: item2)

        let retreivedIcons = try! await service.get(parentId: "parent")
        let retreivedIcon1 = retreivedIcons[0]
        let retreivedIcon2 = retreivedIcons[1]

        expect(retreivedIcon1.id).to(equal(1))
        expect(retreivedIcon1.slug).to(equal("slug"))
        expect(retreivedIcon2.id).to(equal(2))
        expect(retreivedIcon2.slug).to(equal("slug2"))
    }

    func testRetrieveTwoStoredItems() async throws {
        let item1 = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                           order: 2)
        let item2 = StoredPushQueueElement(id: 1, slug: "slug2", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                           order: 2)
        try await service.insert(item: item1)
        try await service.insert(item: item2)

        let retreivedIcons = try! await service.getAll()
        let retreivedIcon1 = retreivedIcons[0]
        let retreivedIcon2 = retreivedIcons[1]

        expect(retreivedIcon1.id).to(equal(1))
        expect(retreivedIcon1.slug).to(equal("slug"))
        expect(retreivedIcon2.id).to(equal(2))
        expect(retreivedIcon2.slug).to(equal("slug2"))
    }

    func testExistsStoredItemByRemoteId() async throws {
        let item = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                          order: 2)
        try await service.insert(item: item)
        let exists = try? await service.exists(remoteId: "remote")
        let missingExists = try? await service.exists(remoteId: "remote-missing")

        expect(exists).to(equal(true))
        expect(missingExists).to(equal(false))
    }

    func testExistsStoredItemBySlug() async throws {
        let item = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                          order: 2)
        try await service.insert(item: item)
        let exists = try? await service.exists(slug: "slug")
        let missingExists = try? await service.exists(slug: "slug-missing")

        expect(exists).to(equal(true))
        expect(missingExists).to(equal(false))
    }

    func testExistsStoredItemByParentId() async throws {
        let item = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                          order: 2)
        try await service.insert(item: item)
        let exists = try? await service.exists(parentId: "parent")
        let missingExists = try? await service.exists(parentId: "parent-missing")

        expect(exists).to(equal(true))
        expect(missingExists).to(equal(false))
    }

    func testExistsStoredItemById() async throws {
        let item = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                          order: 2)
        try await service.insert(item: item)
        let exists = try? await service.exists(id: 1)
        let missingExists = try? await service.exists(id: 2)

        expect(exists).to(equal(true))
        expect(missingExists).to(equal(false))
    }

    func testCountItems() async throws {
        let initial = try await service.count()
        let item = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                          order: 2)
        try await service.insert(item: item)

        let afterInsert = try await service.count()

        expect(initial).to(equal(0))
        expect(afterInsert).to(equal(1))
    }

    func testClearItems() async throws {
        let item = StoredPushQueueElement(id: 1, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                          order: 2)
        try await service.insert(item: item)
        try await service.clear()

        let afterClear = try await service.count()

        expect(afterClear).to(equal(0))
    }

    func testUpdateStoredItem() async throws {
        var item = StoredPushQueueElement(id: 0, slug: "slug", remoteId: "remote", parentId: "parent", data: "hello!".data(using: .utf8)!,
                                          order: 2)

        try await service.insert(item: item)
        item = StoredPushQueueElement(id: 1, slug: "slug1", remoteId: "remote", parentId: "parent1", data: "hello!1".data(using: .utf8)!,
                                      order: 22)
        try await service.update(item: item)

        let retreivedIcon1 = try? await service.get(remoteId: "remote")

        expect(retreivedIcon1?.id).to(equal(1))
        expect(retreivedIcon1?.slug).to(equal("slug1"))
        expect(retreivedIcon1?.remoteId).to(equal("remote"))
        expect(retreivedIcon1?.parentId).to(equal("parent1"))
        // expect(retreivedIcon1?.order).to(equal(22))
        expect(retreivedIcon1?.time).notTo(equal(0))
        expect(String(decoding: retreivedIcon1!.data, as: UTF8.self)).to(equal("hello!1"))
    }

    func testRetreiveMissingItem() async throws {
        var thrown = false
        do {
            _ = try await service.get(remoteId: "remote")
        } catch {
            thrown = true
            expect(error.localizedDescription).to(equal("The operation couldn’t be completed. (GISCollective.PersistenceError error 2.)"))
        }

        expect(thrown).to(equal(true))
    }
}
