//
//  AppTask.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 24.09.21.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick

class AppTaskTests: QuickSpec {
    override func spec() {
        describe("AppTask") {
            it("should be in an empty state on init") {
                let task = AppTask()

                expect(task.state).to(equal(.empty))
            }
        }
    }
}
