//
//  UserProfile.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 13.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick

class UserProfileModelTests: QuickSpec {
    override func spec() {
        describe("Deserialize a list") {
            var testApi: TestApi!

            beforeEach {
                testApi = TestApi()
                Api.instance = testApi
                Persistence.instance = Persistence(path: ":memory:")
            }

            it("should work for an empty list") {
                let store = Store()
                testApi.setData(strData: "{ \"userProfiles\": [] }")

                waitUntil { done in
                    Task {
                        let result = try await store.userProfiles.getList()
                        expect(result.userProfiles.count).to(equal(0))
                        done()
                    }
                }
            }

            describe("when the user profile location is empty") {
                var userProfile = ""

                beforeEach {
                    userProfile = """
                    {
                        "email": "ale.casapu@gmail.com",
                        "organization": "",
                        "location": "",
                        "bio": "",
                        "showCalendarContributions": false,
                        "lastName": "",
                        "statusEmoji": "",
                        "picture": "5fa9b74954b7e201004bcf90",
                        "userName": "ale",
                        "linkedin": "",
                        "_id": "5c7a992e11d31f9e03da58e8",
                        "twitter": "",
                        "skype": "",
                        "canEdit": true,
                        "website": "",
                        "firstName": "Ale",
                        "joinedTime": "0001-01-01T00:00:00Z",
                        "jobTitle": "",
                        "showPrivateContributions": false,
                        "statusMessage": "",
                        "fullName": "Ale"
                    }
                    """
                }

                it("should deserialize the user profile") {
                    let store = Store()
                    testApi.setData(strData: "{ \"userProfiles\": [ \(userProfile) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.userProfiles.getList()
                            expect(result.userProfiles.count).to(equal(1))
                            let userProfile = result.userProfiles[0]

                            expect(userProfile.bio).to(equal(""))
                            expect(userProfile.firstName).to(equal("Ale"))
                            expect(userProfile.jobTitle).to(equal(""))
                            expect(userProfile.joinedTime).to(equal(Date.fromISOString("0001-01-01T00:00:00Z")))
                            expect(userProfile.lastName).to(equal(""))
                            expect(userProfile.linkedin).to(equal(""))
                            expect(userProfile.location.simple).to(equal(""))
                            expect(userProfile.organization).to(equal(""))
                            expect(userProfile.pictureId).to(equal("5fa9b74954b7e201004bcf90"))
                            expect(userProfile.salutation).to(equal(""))
                            expect(userProfile.showCalendarContributions).to(equal(false))
                            expect(userProfile.showPrivateContributions).to(equal(false))
                            expect(userProfile.showWelcomePresentation).to(beNil())
                            expect(userProfile.skype).to(equal(""))
                            expect(userProfile.title).to(equal(""))
                            expect(userProfile.statusEmoji).to(equal(""))
                            expect(userProfile.statusMessage).to(equal(""))
                            expect(userProfile.twitter).to(equal(""))
                            expect(userProfile.website).to(equal(""))
                            expect(userProfile.canEdit).to(equal(true))
                            expect(userProfile.id).to(equal("5c7a992e11d31f9e03da58e8"))

                            done()
                        }
                    }
                }
            }

            describe("when the list is not empty") {
                var userProfile = ""

                beforeEach {
                    userProfile = """
                    {
                    "email":"contact@szabobogdan.com",
                    "organization":"GISCollective",
                    "lastName":"Szabo",
                    "bio":"I'm the GISCollective founder",
                    "showCalendarContributions":true,
                    "showWelcomePresentation":true,
                    "location":{
                        "isDetailed":false,
                        "simple":"Berlin",
                        "detailedLocation": {"postalCode":"1","country":"2","province":"3","city":"4"}
                    },
                    "statusEmoji":"statusEmoji",
                    "picture":"6054983e09891701003ac476",
                    "userName":"bogdan_szabo",
                    "title":"dr",
                    "linkedin":"szabobogdan",
                    "_id":"5b870669796da25424540deb",
                    "twitter":"@szabobogdan1",
                    "skype":"skypeid",
                    "canEdit":true,
                    "firstName":"Bogdan",
                    "salutation":"mr",
                    "website":"https://szabobogdan.com",
                    "jobTitle":"Founder",
                    "canEdit": true,
                    "showPrivateContributions":true,
                    "joinedTime":"2016-07-19T22:53:28Z",
                    "statusMessage":"Developing GISCollective"}
                    """
                }

                it("should work for a list with one element") {
                    let store = Store()
                    testApi.setData(strData: "{ \"userProfiles\": [ \(userProfile) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.userProfiles.getList()
                            expect(result.userProfiles.count).to(equal(1))
                            let userProfile = result.userProfiles[0]

                            expect(userProfile.bio).to(equal("I'm the GISCollective founder"))
                            expect(userProfile.firstName).to(equal("Bogdan"))
                            expect(userProfile.jobTitle).to(equal("Founder"))
                            expect(userProfile.joinedTime).to(equal(Date.fromISOString("2016-07-19T22:53:28Z")))
                            expect(userProfile.lastName).to(equal("Szabo"))
                            expect(userProfile.linkedin).to(equal("szabobogdan"))
                            expect(userProfile.location.isDetailed).to(equal(false))
                            expect(userProfile.location.simple).to(equal("Berlin"))
                            expect(userProfile.location.detailedLocation?.city).to(equal("4"))
                            expect(userProfile.location.detailedLocation?.country).to(equal("2"))
                            expect(userProfile.location.detailedLocation?.postalCode).to(equal("1"))
                            expect(userProfile.location.detailedLocation?.province).to(equal("3"))
                            expect(userProfile.organization).to(equal("GISCollective"))
                            expect(userProfile.pictureId).to(equal("6054983e09891701003ac476"))
                            expect(userProfile.salutation).to(equal("mr"))
                            expect(userProfile.showCalendarContributions).to(equal(true))
                            expect(userProfile.showPrivateContributions).to(equal(true))
                            expect(userProfile.showWelcomePresentation).to(equal(true))
                            expect(userProfile.skype).to(equal("skypeid"))
                            expect(userProfile.title).to(equal("dr"))
                            expect(userProfile.statusEmoji).to(equal("statusEmoji"))
                            expect(userProfile.statusMessage).to(equal("Developing GISCollective"))
                            expect(userProfile.twitter).to(equal("@szabobogdan1"))
                            expect(userProfile.website).to(equal("https://szabobogdan.com"))
                            expect(userProfile.canEdit).to(equal(true))
                            expect(userProfile.id).to(equal("5b870669796da25424540deb"))

                            done()
                        }
                    }
                }

                it("should work for a list with 2 elements") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"userProfiles\": [ \(userProfile), \(userProfile) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.userProfiles.getList()
                            expect(result.userProfiles.count).to(equal(2))
                            done()
                        }
                    }
                }
            }
        }

        describe("Serializing an user profile") {
            var encoder = JSONEncoder()

            beforeEach {
                encoder = JSONEncoder()
                encoder.dateEncodingStrategy = .iso8601
            }

            it("serializes all editable fields") {
                let userProfile = UserProfile(id: "1")
                userProfile.bio = "somebio"
                userProfile.firstName = "firstname"
                userProfile.jobTitle = "job"
                userProfile.joinedTime = Date(timeIntervalSince1970: 20000)
                userProfile.lastName = "lastname"
                userProfile.linkedin = "linkedin"
                userProfile.location = Location.withData(detailedLocation: DetailedLocation.withData(city: "city", country: "country", postalCode: "100", province: "province"), isDetailed: false, simple: "simple")
                userProfile.organization = "organizarion"
                userProfile.pictureId = "picture"
                userProfile.salutation = "dr"
                userProfile.showCalendarContributions = true
                userProfile.showPrivateContributions = true
                userProfile.showWelcomePresentation = true
                userProfile.skype = "skype"
                userProfile.statusEmoji = "emoji"
                userProfile.statusMessage = "message"
                userProfile.title = "title"
                userProfile.twitter = "twitter"
                userProfile.website = "website"
                userProfile.id = "someid"
                userProfile.canEdit = true

                let data = try? encoder.encode(userProfile)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("""
                {
                    "location":{
                        "isDetailed":false,
                        "detailedLocation":{"country":"country","province":"province","city":"city","postalCode":"100"},
                        "simple":"simple"
                    },
                    "title":"title",
                    "showPrivateContributions":true,
                    "statusEmoji":"emoji",
                    "picture":"picture",
                    "_id":"someid",
                    "showWelcomePresentation":true,
                    "joinedTime":"1970-01-01T05:33:20Z",
                    "organization":"organizarion",
                    "bio":"somebio",
                    "statusMessage":"message",
                    "linkedin":"linkedin",
                    "twitter":"twitter",
                    "showCalendarContributions":true,
                    "website":"website",
                    "jobTitle":"job",
                    "firstName":"firstname",
                    "salutation":"dr",
                    "skype":"skype",
                    "lastName":"lastname"
                }
                """.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "\n", with: "")))
            }
        }

        describe("fullName") {
            it("returns --- when no name field is set") {
                let userProfile = UserProfile(id: "")

                expect(userProfile.fullName).to(equal("---"))
            }

            it("returns --- when only the title is set") {
                let userProfile = UserProfile(id: "")
                userProfile.title = "title"

                expect(userProfile.fullName).to(equal("---"))
            }

            it("returns --- when only the salutation is set") {
                let userProfile = UserProfile(id: "")
                userProfile.salutation = "salutation"

                expect(userProfile.fullName).to(equal("---"))
            }

            it("returns only the first name when is set") {
                let userProfile = UserProfile(id: "")
                userProfile.firstName = "first"

                expect(userProfile.fullName).to(equal("first"))
            }

            it("returns only the first name when is set and the last name is empty string") {
                let userProfile = UserProfile(id: "")
                userProfile.firstName = "first"
                userProfile.lastName = ""

                expect(userProfile.fullName).to(equal("first"))
            }

            it("returns only the last name when is set") {
                let userProfile = UserProfile(id: "")
                userProfile.lastName = "last"

                expect(userProfile.fullName).to(equal("last"))
            }

            it("returns the last name when is set and the first name is empty string") {
                let userProfile = UserProfile(id: "")
                userProfile.lastName = "last"
                userProfile.firstName = ""

                expect(userProfile.fullName).to(equal("last"))
            }

            it("returns the full name when all name fields are set") {
                let userProfile = UserProfile(id: "")
                userProfile.lastName = "last"
                userProfile.firstName = "first"
                userProfile.title = "title"
                userProfile.salutation = "salutation"

                expect(userProfile.fullName).to(equal("title first last"))
            }
        }

        describe("prettyJoinedTime") {
            it("returns --- when joined time is not set") {
                let userProfile = UserProfile(id: "")
                expect(userProfile.prettyJoinedTime).to(equal("---"))
            }

            it("returns a nice formated date when joined date is set") {
                let userProfile = UserProfile(id: "")
                userProfile.joinedTime = Date.fromISOString("2020-12-20T12:00:00Z")
                expect(userProfile.prettyJoinedTime).to(equal("December 20, 2020"))
            }
        }

        describe("prettyLocation") {
            it("returns --- when the location is not set") {
                let userProfile = UserProfile(id: "")
                expect(userProfile.prettyLocation).to(equal("---"))
            }

            it("returns --- when the location is simple and not set") {
                let userProfile = UserProfile(id: "")
                userProfile.location = Location()

                expect(userProfile.prettyLocation).to(equal("---"))
            }

            it("returns the simple address when is set") {
                let userProfile = UserProfile(id: "")
                userProfile.location = Location()
                userProfile.location.isDetailed = false
                userProfile.location.simple = "simple"

                expect(userProfile.prettyLocation).to(equal("simple"))
            }

            it("returns the detailed address when is set") {
                let userProfile = UserProfile(id: "")
                userProfile.location = Location()
                userProfile.location.isDetailed = true
                userProfile.location.simple = "simple"
                userProfile.location.detailedLocation = DetailedLocation.withData(city: "city", country: "country", postalCode: "1000", province: "province")

                expect(userProfile.prettyLocation).to(equal("1000, city, province, country"))
            }
        }

        describe("prettyJob") {
            it("returns --- when the job and company are not set") {
                let userProfile = UserProfile(id: "")
                expect(userProfile.prettyJob).to(equal("---"))
            }

            it("returns the jobTitle when is set") {
                let userProfile = UserProfile(id: "")
                userProfile.jobTitle = "jobTitle"
                expect(userProfile.prettyJob).to(equal("jobTitle"))
            }

            it("returns the organization when is set") {
                let userProfile = UserProfile(id: "")
                userProfile.organization = "organization"
                expect(userProfile.prettyJob).to(equal("organization"))
            }

            it("returns a nice string when jobTitle and organization are set") {
                let userProfile = UserProfile(id: "")
                userProfile.organization = "organization"
                userProfile.jobTitle = "jobTitle"
                expect(userProfile.prettyJob).to(equal("jobTitle at organization"))
            }
        }
    }
}
