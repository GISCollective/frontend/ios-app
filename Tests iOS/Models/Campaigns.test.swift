//
//  Campaigns.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 28.05.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick

class CampaignsTest: QuickSpec {
    override func spec() {
        describe("Serializing a campaign") {
            var encoder = JSONEncoder()

            beforeEach {
                encoder = JSONEncoder()
            }

            it("serializes all fields") {
                let campaign = Campaign(id: "1")
                campaign.article = ArticleBody(paragraph: "description")
                campaign.name = "name"
                campaign.coverId = "cover"
                campaign.endDate = Date.fromISOString("2020-02-02T00:00:00Z")
                campaign.startDate = Date.fromISOString("2019-02-02T00:00:00Z")
                campaign.info = ModelInfo()
                campaign.info.lastChangeOn = Date.fromISOString("2020-02-02T00:00:00Z")!
                campaign.info.createdOn = Date.fromISOString("2019-02-02T00:00:00Z")!
                campaign.map = OptionalMap()
                campaign.visibility = Visibility()
                campaign.iconsId = ["icon"]
                campaign.optionalIconsId = ["optionalicon"]
                campaign.options = CampaignOptions()

                let data = try? encoder.encode(campaign)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("""
                {
                    "options":{
                        "iconsLabel":"",
                        "featureNamePrefix":"",
                        "showDescriptionQuestion":false,
                        "descriptionLabel":"",
                        "showNameQuestion":false,
                        "registrationMandatory":false,
                        "nameLabel":""
                    },
                    "map":{"map":"","isEnabled":false},
                    "_id":"1",
                    "article":{"blocks":[{"type":"paragraph","data":{"text":"description"}}]},
                    "endDate":602294400,
                    "optionalIcons":["optionalicon"],
                    "cover":"cover",
                    "visibility":{"isPublic":false,"team":"","isDefault":null},
                    "startDate":570758400,
                    "info":{"changeIndex":0,"author":"","createdOn":570758400,"lastChangeOn":602294400},
                    "name":"name",
                    "icons":["icon"]
                }
                """.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "\n", with: "")))
            }
        }

        describe("Deserialize a list") {
            var testApi: TestApi!

            beforeEach {
                testApi = TestApi()
                Api.instance = testApi
                Persistence.instance = Persistence(path: ":memory:")
            }

            it("should work for an empty list") {
                let store = Store()
                testApi?.setData(strData: "{ \"campaigns\": [] }")

                waitUntil { done in
                    Task {
                        let result = try await store.campaigns.getList()
                        expect(result.campaigns.count).to(equal(0))
                        done()
                    }
                }
            }

            describe("when the list is not empty") {
                var campaign = ""
                beforeEach {
                    campaign = """
                    {
                        "info":{"changeIndex":14,"createdOn":"2021-02-26T18:53:39Z","lastChangeOn":"2021-03-13T13:17:01Z","originalAuthor":"5f817757c9d9520100b18d72","author":"5f817757c9d9520100b18d72"},
                        "map":{"map":"6037a73af85a750100c7228e","isEnabled":true},
                        "visibility":{"isDefault":false,"isPublic":true,"team":"5f81a513dbe2780100156172"},
                        "article":{"time":1615641403189,"blocks":[{"type":"header","data":{"level":1,"text":"Îlots de fraicheur à Mons"}},{"type":"paragraph","data":{"text":"Quels sont ces endroits de la ville de Mons et ses alentours où vous vous sentez bien en été, quand le thermomètre monte?&nbsp;"}},{"type":"paragraph","data":{"text":"N' oubliez pas d' activer la fonction de localisation de votre téléphone ou ordinateur!&nbsp;"}},{"type":"paragraph","data":{"text":"Partagez! Participez!&nbsp;"}},{"type":"paragraph","data":{"text":"Nathalie, habitante de Mons.&nbsp;"}}],"version":"2.19.1"},
                        "name":"Îlots de fraicheur à Mons",
                        "_id":"603943b3395e980100843d03",
                        "endDate":"2020-02-26T19:36:09Z",
                        "startDate":"2021-02-26T18:53:39Z",
                        "cover":"603943d109977601001a960f",
                        "canEdit":true,
                        "icons":["5ebdb0c6304c6b01005c518b"],
                        "optionalIcons":["5ca7bfeaecd8490100cab9f3","5ca7bfe8ecd8490100cab9ef"],
                        "options":{"showNameQuestion":true,"descriptionLabel":"descriptionLabel","registrationMandatory":false,"nameLabel":"nameLabel","iconsLabel":"iconsLabel","featureNamePrefix":"featureNamePrefix","showDescriptionQuestion":true}
                    }
                    """
                }

                it("should work for a list with one element") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"campaigns\": [ \(campaign) ] }")

                    waitUntil(timeout: .seconds(5)) { done in
                        Task {
                            let result = try await store.campaigns.getList()
                            expect(result).notTo(beNil())
                            expect(result.campaigns.count).to(equal(1))
                            expect(result.campaigns[0].id).to(equal("603943b3395e980100843d03"))
                            expect(result.campaigns[0].name).to(equal("Îlots de fraicheur à Mons"))
                            expect(result.campaigns[0].endDate?.toISOString()).to(equal("2020-02-26T19:36:09Z"))
                            expect(result.campaigns[0].startDate?.toISOString()).to(equal("2021-02-26T18:53:39Z"))
                            expect(result.campaigns[0].iconsId).to(equal(["5ebdb0c6304c6b01005c518b"]))
                            expect(result.campaigns[0].optionalIconsId).to(equal(["5ca7bfeaecd8490100cab9f3", "5ca7bfe8ecd8490100cab9ef"]))

                            expect(result.campaigns[0].info.changeIndex).to(equal(14))
                            expect(result.campaigns[0].info.createdOn.toISOString()).to(equal("2021-02-26T18:53:39Z"))
                            expect(result.campaigns[0].info.lastChangeOn.toISOString()).to(equal("2021-03-13T13:17:01Z"))
                            expect(result.campaigns[0].info.originalAuthor).to(equal("5f817757c9d9520100b18d72"))
                            expect(result.campaigns[0].info.author).to(equal("5f817757c9d9520100b18d72"))

                            expect(result.campaigns[0].map?.map).to(equal("6037a73af85a750100c7228e"))

                            expect(result.campaigns[0].visibility.isDefault).to(equal(false))
                            expect(result.campaigns[0].visibility.isPublic).to(equal(true))
                            expect(result.campaigns[0].visibility.teamId).to(equal("5f81a513dbe2780100156172"))

                            expect(result.campaigns[0].options?.showNameQuestion).to(equal(true))
                            expect(result.campaigns[0].options?.descriptionLabel).to(equal("descriptionLabel"))
                            expect(result.campaigns[0].options?.registrationMandatory).to(equal(false))

                            done()
                        }
                    }
                }

                it("should include the campaign name in the description block as header") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"campaigns\": [ \(campaign) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.campaigns.getList()
                            expect(result).notTo(beNil())
                            expect(result.campaigns[0].article.blockContainer.blocks.count).to(equal(5))
                            expect(result.campaigns[0].article.blockContainer.blocks[0].asHeader().text).to(equal("Îlots de fraicheur à Mons"))
                            done()
                        }
                    }
                }

                it("should work for a list with 2 elements") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"campaigns\": [ \(campaign), \(campaign) ] }")

                    waitUntil(timeout: .seconds(5)) { done in
                        Task {
                            let result = try await store.campaigns.getList()
                            expect(result).notTo(beNil())
                            expect(result.campaigns.count).to(equal(2))
                            done()
                        }
                    }
                }
            }
        }
    }
}
