//
//  Campaigns.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 28.05.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick

class PageColTest: QuickSpec {
    override func spec() {
        describe("Deserializing") {
            it("can deserialize an element with all fields") {
                let JSON = """
                {
                    "container": 1,
                    "col": 2,
                    "row": 3,
                    "type": "type",
                    "data": {
                      "query": { "ids": "1,2,3" },
                      "model": "campaign"
                    }
                }
                """

                let jsonData = JSON.data(using: .utf8)!

                let result: PageCol = try! JSONDecoder().decode(PageCol.self, from: jsonData)

                expect(result.container).to(equal(1))
                expect(result.col).to(equal(2))
                expect(result.row).to(equal(3))
                expect(result.type).to(equal("type"))
                expect(result.data["query"]?.toJSON()).to(equal("{\"ids\":\"1,2,3\"}"))
                expect(result.data["model"]?.toJSON()).to(equal("\"campaign\""))
            }
        }
    }
}
