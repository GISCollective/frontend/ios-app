//
//  Icons.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 01.06.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick

class Icons: QuickSpec {
    override func spec() {
        describe("Deserialize a list") {
            var testApi: TestApi!

            beforeEach {
                testApi = TestApi()
                Api.instance = testApi
                Persistence.instance = Persistence(path: ":memory:")
            }

            it("should work for an empty list") {
                let store = Store()
                testApi?.setData(strData: "{ \"icons\": [] }")

                waitUntil { done in
                    Task {
                        let result = try await store.icons.getList()
                        expect(result.icons.count).to(equal(0))
                        done()
                    }
                }
            }

            describe("when the list is not empty") {
                var icon = ""
                beforeEach {
                    icon = """
                    {
                        "parent":"parent",
                        "otherNames": ["name1", "name2"],
                        "image":{"useParent":false,"value":"https://new.opengreenmap.org/api-v1/icons/5ca7bfb6ecd8490100cab971/image/value"},
                        "allowMany":false,
                        "localName":"Green Store",
                        "maxRelevanceLevel":4,
                        "subcategory":"Green Economy",
                        "verticalIndex":0,
                        "_id":"5ca7bfb6ecd8490100cab971",
                        "category":"Sustainable Living - Green Map Icons",
                        "name":"Green Store",
                        "description":"Sells ecologically-conscious products (eco-products). 100% of all products may not truly be major improvements over conventional products, but the intention, method of production, materials, reductions of impacts in use, store policies, etc. address sustainability principles.",
                        "attributes":[{}],
                        "iconSet":"5ca7b702ecd8490100cab96f",
                        "styles":{
                            "hasCustomStyle":false,
                            "types":{
                            "lineMarker":{"backgroundColor":"rgba(255,255,255,0.6)","isVisible":true,"size":18,"shape":"circle","borderWidth":1,"borderColor":"#1ac6ff"},
                            "line":{"backgroundColor":"transparent","lineDash":[],"borderWidth":2,"borderColor":"#1ac6ff"},
                            "polygon":{"backgroundColor":"transparent","hideBackgroundOnZoom":true,"lineDash":[10,10,5,10],"borderWidth":2,"borderColor":"#1ac6ff"},
                            "site":{"backgroundColor":"rgba(255,255,255,0.6)","isVisible":true,"size":18,"shape":"circle","borderWidth":1,"borderColor":"rgba(255,255,255,0.9)"},
                            "lineLabel":{"align_":"center","offsetX":0,"isVisible":false,"text":"wrap","size":10,"lineHeight":1,"color":"#000","borderColor":"#fff","baseline":"bottom","offsetY":0,"weight":"bold","borderWidth":2},
                            "polygonMarker":{"backgroundColor":"rgba(255,255,255,0.6)","isVisible":true,"size":18,"shape":"circle","borderWidth":1,"borderColor":"#1ac6ff"},
                            "siteLabel":{"align_":"center","offsetX":0,"isVisible":false,"text":"wrap","size":10,"lineHeight":1,"color":"#000","borderColor":"#fff","baseline":"bottom","offsetY":0,"weight":"bold","borderWidth":2},
                            "polygonLabel":{"align_":"center","offsetX":0,"isVisible":false,"text":"wrap","size":10,"lineHeight":1,"color":"#000","borderColor":"#fff","baseline":"bottom","offsetY":0,"weight":"bold","borderWidth":2}}}}
                    """
                }

                it("should work for a list with one element") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"icons\": [ \(icon) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.icons.getList()
                            expect(result).notTo(beNil())
                            expect(result.icons.count).to(equal(1))
                            expect(result.icons[0].id).to(equal("5ca7bfb6ecd8490100cab971"))
                            expect(result.icons[0].name).to(equal("Green Store"))
                            expect(result.icons[0].localName).to(equal("Green Store"))
                            expect(result.icons[0].parent).to(equal("parent"))
                            expect(result.icons[0].allowMany).to(equal(false))
                            expect(result.icons[0].maxRelevanceLevel).to(equal(4))
                            expect(result.icons[0].subcategory).to(equal("Green Economy"))
                            expect(result.icons[0].category).to(equal("Sustainable Living - Green Map Icons"))
                            expect(result.icons[0].iconSetId).to(equal("5ca7b702ecd8490100cab96f"))
                            expect(result.icons[0].otherNames).to(equal(["name1", "name2"]))
                            expect(result.icons[0].image).to(equal(OptionalIconImage.withData(useParent: false, value: "https://new.opengreenmap.org/api-v1/icons/5ca7bfb6ecd8490100cab971/image/value")))
                            expect(result.icons[0].styles?.hasCustomStyle).to(equal(false))
                            expect(result.icons[0].styles?.types?.line).to(equal(LineStyleProperties.withData(borderColor: "#1ac6ff", backgroundColor: "transparent", borderWidth: 2, lineDash: [])))
                            expect(result.icons[0].styles?.types?.polygon).to(equal(PolygonStyleProperties.withData(borderColor: "#1ac6ff", backgroundColor: "transparent", borderWidth: 2, lineDash: [10, 10, 5, 10], hideBackgroundOnZoom: true)))

                            done()
                        }
                    }
                }

                it("should include the icon name in the description block as header") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"icons\": [ \(icon) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.icons.getList()
                            expect(result).notTo(beNil())
                            expect(result.icons[0].processedDescription?.blockContainer.blocks.count).to(equal(2))
                            expect(result.icons[0].processedDescription?.blockContainer.blocks[0].asHeader().text).to(equal("Green Store"))
                            done()
                        }
                    }
                }

                it("should work for a list with 2 elements") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"icons\": [ \(icon), \(icon) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.icons.getList()
                            expect(result).notTo(beNil())
                            expect(result.icons.count).to(equal(2))
                            done()
                        }
                    }
                }
            }
        }
    }
}
