//
//  Pages.swift
//  GISCollective (iOS)
//
//  Created by Bogdan Szabo on 25.02.22.
//

import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick

class Pages: QuickSpec {
    override func spec() {
        describe("Deserialize a list") {
            var testApi: TestApi!

            beforeEach {
                testApi = TestApi()
                Api.instance = testApi
                Persistence.instance = Persistence(path: ":memory:")
            }

            it("should work for an empty list") {
                let store = Store()
                testApi?.setData(strData: "{ \"pages\": [] }")

                waitUntil { done in
                    Task {
                        let result = try await store.pages.getList()
                        expect(result.pages.count).to(equal(0))
                        done()
                    }
                }
            }

            describe("when the list is not empty") {
                var page = ""
                beforeEach {
                    page = """
                    {
                        "info": {
                          "changeIndex": 12,
                          "createdOn": "2021-01-01T00:00:00Z",
                          "lastChangeOn": "2021-10-25T22:33:11Z",
                          "originalAuthor": "@unknown",
                          "author": "5b870669796da25424540deb"
                        },
                        "cols": [
                          {
                            "row": 0,
                            "type": "blocks",
                            "col": 0,
                            "container": 0,
                            "data": {
                              "buttonLabel": "View Campaigns",
                              "lineHeight": "base",
                              "buttonLink": "/events/cop26",
                              "font": "default",
                              "description": {
                                "blocks": [
                                  {
                                    "type": "paragraph",
                                    "data": {
                                      "text": "We’re asking the Glasgow community to share their favourite, local places to the Glasgow Green Map. These could be cafes, zero waste shops, bike shops or community gardens. Post-COP26, this will create a legacy that can be built on by the community for an ever-greener Glasgow. "
                                    }
                                  }
                                ]
                              },
                              "alignment": "start",
                              "heading": "COP26 Campaigns"
                            }
                          },
                          {
                            "row": 1,
                            "type": "article",
                            "col": 0,
                            "container": 0,
                            "data": {
                              "id": "61292c4c7bdf9301008fd7b7",
                              "model": "article"
                            }
                          },
                          {
                            "row": 2,
                            "type": "campaign-card-list",
                            "col": 0,
                            "container": 0,
                            "data": {
                              "query": {},
                              "model": "campaign"
                            }
                          }
                        ],
                        "visibility": {
                          "isDefault": false,
                          "isPublic": true,
                          "team": "61292c4c7bdf9301008fd7b6"
                        },
                        "name": "Campaigns",
                        "_id": "61292c4c7bdf9301008fd7bd",
                        "layout": "613f8349d6bb4d6a6f67a074",
                        "containers": [],
                        "slug": "campaigns"
                      }
                    """
                }

                it("should work for a list with one element") {
                    let store = Store()
                    testApi!.setData(strData: "{ \"pages\": [ \(page) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.pages.getList()
                            expect(result.pages.count).to(equal(1))
                            expect(result.pages[0].cols.count).to(equal(3))
                            expect(result.pages[0].name).to(equal("Campaigns"))

                            done()
                        }
                    }
                }

                it("should work for a list with 2 elements") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"pages\": [ \(page), \(page) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.pages.getList()
                            expect(result.pages.count).to(equal(2))
                            done()
                        }
                    }
                }
            }
        }
    }
}
