//
//  Campaigns.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 28.05.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick

class CampaignAnswers: QuickSpec {
    override func spec() {
        describe("Serializing a CampaignAnswer") {
            var encoder = JSONEncoder()

            beforeEach {
                encoder = JSONEncoder()
            }

            it("should serialize an item with all fields") {
                let answer = CampaignAnswer(id: "1")
                answer.attributes["group"] = AttributeGroup.group([:])
                answer.attributes["group"]!["key"] = Attribute.bool(true)
                answer.campaignId = "2"
                answer.iconsId = ["3"]
                answer.info = ModelInfo.withData(changeIndex: 1, createdOn: "2020-01-02T00:00:00Z", lastChangeOn: "2020-02-02T00:00:00Z", originalAuthor: "me", author: "them")
                answer.picturesId = ["4"]

                let point = try! Point(wkt: "Point(1 2)")
                answer.position = .point(point)

                let data = try? encoder.encode(answer)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("""
                {
                    "_id":"1",
                    "info":{"changeIndex":1,"author":"them","originalAuthor":"me","createdOn":599616000,"lastChangeOn":602294400},
                    "position":{"type":"Point","coordinates":[1,2]},
                    "campaign":"2",
                    "pictures":["4"],
                    "attributes":{"group":{"key":true}},
                    "icons":["3"]}
                """.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "\n", with: "")))
            }
        }

        describe("Deserialize a list") {
            var testApi: TestApi?

            beforeEach {
                testApi = TestApi()
                Api.instance = testApi!
                Persistence.instance = Persistence(path: ":memory:")
            }

            it("should work for an empty list") {
                let store = Store()
                testApi?.setData(strData: "{ \"campaignAnswers\": [] }")

                waitUntil(timeout: .seconds(50)) { done in
                    Task {
                        let result = try await store.campaignAnswers.getList()
                        expect(result.campaignAnswers.count).to(equal(0))
                        done()
                    }
                }
            }

            describe("when the list is not empty") {
                var campaignAnswer = ""
                beforeEach {
                    campaignAnswer = """
                    {
                        "_id": "603943b3395e980100843d03",
                        "info":{"changeIndex":14,"createdOn":"2021-02-26T18:53:39Z","lastChangeOn":"2021-03-13T13:17:01Z","originalAuthor":"5f817757c9d9520100b18d72","author":"5f817757c9d9520100b18d72"},
                        "icons": [ "1", "2", "3" ],
                        "pictures": [ "11", "22", "33" ],
                        "campaign": "campaign-id",
                        "position": { "type":"Point", "coordinates":[-77.896557,34.224495] },
                        "attributes": { "group1": [ { "key": "value1" } ],  "group2": { "key": "value2" } },
                    }
                    """
                }

                it("should work for a list with one element") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"campaignAnswers\": [ \(campaignAnswer) ] }")

                    waitUntil(timeout: .seconds(50)) { done in
                        Task {
                            let result = try await store.campaignAnswers.getList()
                            expect(result).notTo(beNil())
                            expect(result.campaignAnswers.count).to(equal(1))

                            if result.campaignAnswers.count == 0 {
                                done()
                                return
                            }

                            expect(result.campaignAnswers[0].id).to(equal("603943b3395e980100843d03"))
                            expect(result.campaignAnswers[0].campaignId).to(equal("campaign-id"))
                            expect(result.campaignAnswers[0].iconsId).to(equal(["1", "2", "3"]))
                            expect(result.campaignAnswers[0].info).to(equal(ModelInfo.withData(changeIndex: 14, createdOn: "2021-02-26T18:53:39Z", lastChangeOn: "2021-03-13T13:17:01Z", originalAuthor: "5f817757c9d9520100b18d72", author: "5f817757c9d9520100b18d72")))

                            let firstGroup: [String: Attribute] = (result.campaignAnswers[0].attributes["group1"]![0])!
                            let secondGroupValue: Attribute = (result.campaignAnswers[0].attributes["group2"]!["key"])!
                            var value = ""

                            if case let .string(strValue) = firstGroup["key"] {
                                value = strValue
                            }

                            expect(value).to(equal("value1"))

                            if case let .string(strValue) = secondGroupValue {
                                value = strValue
                            }
                            expect(value).to(equal("value2"))

                            done()
                        }
                    }
                }

                it("should work for a list with 2 elements") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"campaignAnswers\": [ \(campaignAnswer), \(campaignAnswer) ] }")

                    waitUntil(timeout: .seconds(50)) { done in
                        Task {
                            let result = try await store.campaignAnswers.getList()
                            expect(result).notTo(beNil())
                            expect(result.campaignAnswers.count).to(equal(2))
                            done()
                        }
                    }
                }
            }
        }

        describe("Updating group values") {
            var answer = CampaignAnswer(id: "")

            beforeEach {
                answer = CampaignAnswer(id: "1")
            }

            it("should create a new group when is not set") {
                answer.updateValue(groupName: "other", index: 0, key: "key", value: .string("value"))

                expect(answer.attributes.count).to(equal(1))
                var group: [String: Attribute]?

                if case let .group(value) = answer.attributes["other"] {
                    group = value
                }

                expect(group!["key"]).to(equal(.string("value")))
            }

            it("should set a value in the existing group when it exists") {
                answer.attributes["other"] = .group(["existing": .number(1)])
                answer.updateValue(groupName: "other", index: 0, key: "key", value: .string("value"))

                expect(answer.attributes.count).to(equal(1))
                var group: [String: Attribute]?

                if case let .group(value) = answer.attributes["other"] {
                    group = value
                }

                expect(group!["key"]).to(equal(.string("value")))
                expect(group!["existing"]).to(equal(.number(1)))
            }

            it("should create a list when there is an icon with the allowMany flag") {
                let icon = Icon(id: "1")
                icon.allowMany = true
                icon.name = "icon"

                answer.icons = [icon]
                answer.updateValue(groupName: "icon", index: 1, key: "key", value: .string("value"))

                expect(answer.attributes.count).to(equal(1))

                var secondGroup: [String: Attribute]?
                var firstGroup: [String: Attribute]?

                if case let .list(value) = answer.attributes["icon"] {
                    firstGroup = value[0]
                    secondGroup = value[1]
                }

                expect(firstGroup?.count).to(equal(0))
                expect(secondGroup!["key"]).to(equal(.string("value")))
            }
        }
    }
}
