//
//  Features.swift
//  GISCollectiveTests
//
//  Created by Alexandra Casapu on 26.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick

class Features: QuickSpec {
    override func spec() {
        describe("Deserialize a list") {
            var testApi: TestApi!

            beforeEach {
                testApi = TestApi()
                Api.instance = testApi
                Persistence.instance = Persistence(path: ":memory:")
            }

            it("should work for an empty list") {
                let store = Store()
                testApi?.setData(strData: "{ \"features\": [] }")

                waitUntil { done in
                    Task {
                        let result = try await store.features.getList()
                        expect(result.features.count).to(equal(0))
                        done()
                    }
                }
            }

            describe("when the list is not empty") {
                var feature = ""

                beforeEach {
                    feature = """
                    {
                    "info": {
                        "changeIndex":5,
                        "createdOn":"2011-07-27T00:30:00Z",
                        "lastChangeOn":"2012-03-10T00:30:00Z",
                        "originalAuthor":"5ca90cbf82ff92d26a7ee7cf",
                        "author":"5ca90cbf82ff92d26a7ee7cf"
                    },
                    "contributors": [ "5b86efef796da25424540dea", "5b870669796da25424540deb"],
                    "maps":["5ca89e27ef1f7e010007f4b2"],
                    "canEdit": true,
                    "isMasked": true,
                    "name":"St. Joan of Arc Church St. Joan of Arc Church",
                    "_id":"5ca8b62cef1f7e0100081f1c",
                    "visibility":1,
                    "icons":
                    [
                    "5ca7bfc2ecd8490100cab987",
                    "5ca7bfddecd8490100cab9c8"
                    ],
                    "description":"AeonSolar installed a 16.56 kW PV generator on the roof in 2009.\\n\\n#CatholicChurch",
                    "position":
                    {
                    "type":"Point",
                    "coordinates":[-77.896557,34.224495]
                    },
                    "pictures":[]
                    }
                    """
                }

                it("should work for a list with one element") {
                    let store = Store()
                    testApi!.setData(strData: "{ \"features\": [ \(feature) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.features.getList()
                            expect(result.features.count).to(equal(1))
                            expect(result.features[0].info).to(equal(ModelInfo.withData(changeIndex: 5, createdOn: "2011-07-27T00:30:00Z", lastChangeOn: "2012-03-10T00:30:00Z", originalAuthor: "5ca90cbf82ff92d26a7ee7cf", author: "5ca90cbf82ff92d26a7ee7cf")))
                            expect(result.features[0].contributors).to(equal(["5b86efef796da25424540dea", "5b870669796da25424540deb"]))
                            expect(result.features[0].mapsId).to(equal(["5ca89e27ef1f7e010007f4b2"]))
                            expect(result.features[0].name).to(equal("St. Joan of Arc Church St. Joan of Arc Church"))
                            expect(result.features[0].id).to(equal("5ca8b62cef1f7e0100081f1c"))
                            expect(result.features[0].visibility).to(equal(1))
                            expect(result.features[0].iconsId).to(equal(["5ca7bfc2ecd8490100cab987", "5ca7bfddecd8490100cab9c8"]))
                            expect(result.features[0].processedDescription.blockContainer.blocks.count).to(equal(2))
                            expect(result.features[0].picturesId).to(equal([]))
                            // expect(result.features[0].canEdit).to(equal(true))
                            expect(result.features[0].isMasked).to(equal(true))
                            done()
                        }
                    }
                }

                it("should include the feature name in the description block as header") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"features\": [ \(feature) ] }")

                    waitUntil { done in
                        Task {
                            defer { done() }
                            let result = try await store.features.getList()
                            expect(result.features.count).to(equal(1))

                            if result.features.count == 0 {
                                return
                            }

                            expect(result.features[0].processedDescription.blockContainer.blocks.count).to(equal(2))
                            expect(result.features[0].processedDescription.blockContainer.blocks[0].asHeader().text).to(equal("St. Joan of Arc Church St. Joan of Arc Church"))
                        }
                    }
                }

                it("should work for a list with 2 elements") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"features\": [ \(feature), \(feature) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.features.getList()
                            expect(result.features.count).to(equal(2))
                            done()
                        }
                    }
                }
            }
        }

        describe("Deserialize nearby list") {
            var testApi: TestApi!

            beforeEach {
                testApi = TestApi()
                Api.instance = testApi
                Persistence.instance = Persistence(path: ":memory:")
            }

            it("should return the model name and params") {
                let store = Store()
                testApi?.setData(strData: "{ \"features\": [] }")

                waitUntil { done in
                    Task {
                        _ = try await store.features.query(params: ["lat": "52", "lon": "13", "skip": "0", "limit": "10"], collection: "test")
                        done()
                    }
                }

                expect(testApi?.lastModelName).to(equal("features"))
                expect(testApi?.lastParams!["skip"]).to(equal("0"))
                expect(testApi?.lastParams!["limit"]).to(equal("10"))
                expect(testApi?.lastParams!["lat"]).to(equal("52"))
                expect(testApi?.lastParams!["lon"]).to(equal("13"))
            }
        }

        describe("Serializing a feature") {
            var encoder = JSONEncoder()

            beforeEach {
                encoder = JSONEncoder()
            }

            it("serializes all fields") {
                let feature = Feature(id: "1")
                feature.attributes = [:]
                feature.attributes!["group"] = AttributeGroup.group([:])
                feature.attributes!["group"]!["key"] = Attribute.bool(true)
                feature.iconsId = ["3"]
                feature.soundsId = ["sound"]
                feature.info = ModelInfo.withData(changeIndex: 1, createdOn: "2020-02-02T00:00:00Z", lastChangeOn: "2020-01-02T00:00:00Z", originalAuthor: "me", author: "them")
                feature.picturesId = ["picture"]
                feature.contributors = ["contributor"]
                feature.mapsId = ["map"]
                feature.description = ArticleBody(paragraph: "description")
                feature.name = "name"
                feature.visibility = 1

                let point = try! Point(wkt: "Point(1 2)")
                feature.position = .point(point)

                let data = try? encoder.encode(feature)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("""
                {
                    "description":{
                        "blocks":[{
                            "type":"paragraph",
                            "data":{"text":"description"}
                        }]
                    },
                    "position":{"type":"Point","coordinates":[1,2]},
                    "_id":"1",
                    "contributors":["contributor"],
                    "sounds":["sound"],
                    "pictures":["picture"],
                    "attributes":{"group":{"key":true}},
                    "visibility":1,
                    "info":{"changeIndex":1,"author":"them","originalAuthor":"me","createdOn":602294400,"lastChangeOn":599616000},
                    "maps":["map"],
                    "name":"name",
                    "icons":["3"]
                }
                """.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "\n", with: "")))
            }
        }
    }
}
