//
//  User.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 13.09.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick

class UserModelTests: QuickSpec {
    override func spec() {
        describe("Deserialize a list") {
            var testApi: TestApi!

            beforeEach {
                testApi = TestApi()
                Api.instance = testApi
                Persistence.instance = Persistence(path: ":memory:")
            }

            it("should work for an empty list") {
                let store = Store()
                testApi.setData(strData: "{ \"users\": [] }")
                waitUntil { done in
                    Task {
                        let result = try await store.users.getList()
                        expect(result.users.count).to(equal(0))
                        done()
                    }
                }
            }

            describe("when the list is not empty") {
                var user = ""

                beforeEach {
                    user = """
                    {
                    "email":"contact@szabobogdan.com",
                    "username":"bogdan_szabo",
                    "lastActivity":1631557162,
                    "isAdmin":true,
                    "isActive":true,
                    "name":"Bogdan Szabo",
                    "_id":"5b870669796da25424540deb"
                    }
                    """
                }

                it("should work for a list with one element") {
                    let store = Store()
                    testApi.setData(strData: "{ \"users\": [ \(user) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.users.getList()
                            expect(result.users.count).to(equal(1))
                            let user = result.users[0]

                            expect(user.email).to(equal("contact@szabobogdan.com"))
                            expect(user.username).to(equal("bogdan_szabo"))
                            expect(user.lastActivity).to(equal(1_631_557_162))
                            // expect(user.isAdmin).to(equal(true))
                            expect(user.isActive).to(equal(true))
                            // expect(user.name).to(equal("Bogdan Szabo"))
                            expect(user.id).to(equal("5b870669796da25424540deb"))

                            done()
                        }
                    }
                }

                it("should work for a list with 2 elements") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"users\": [ \(user), \(user) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.users.getList()
                            expect(result.users.count).to(equal(2))
                            done()
                        }
                    }
                }
            }
        }

        describe("Serializing an user") {
            var encoder = JSONEncoder()

            beforeEach {
                encoder = JSONEncoder()
                encoder.dateEncodingStrategy = .iso8601
            }

            it("serializes all fields") {
                let user = User(id: "1")
                user.email = "a@b.c"
                user.username = "username"
                user.lastActivity = 1_631_557_162
                // user.isAdmin = true
                user.isActive = true
                // user.name = "name"
                user.id = "id"

                let data = try? encoder.encode(user)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("""
                {
                    "_id":"id",
                    "isActive":true,
                    "firstName":null,
                    "title":null,
                    "lastActivity": 1631557162,
                    "username":"username",
                    "email":"a@b.c",
                    "salutation":null,
                    "createdAt":null,
                    "lastName":null
                }
                """.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "\n", with: "")))
            }
        }
    }
}
