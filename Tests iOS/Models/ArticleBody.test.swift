//
//  ArticleBody.swift
//  GISCollectiveTests
//
//  Created by Alexandra Casapu on 23.01.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick

class ArticleBodyTests: QuickSpec {
    override func spec() {
        describe("Article Body description") {
            it("should be deserialized when it's a string") {
                let data: Data? = "\"string description of a site\"".data(using: .utf8)
                let result = try? JSONDecoder().decode(ArticleBody.self, from: data!)

                expect(result?.blockContainer.blocks.count).to(equal(Int(1)))
                expect(result?.blockContainer.blocks[0].type).to(equal("paragraph"))
            }

            it("should be deserialized when it's a json") {
                let data: Data? = """
                {
                "time": 1609696379092,
                "blocks": [
                    {
                        "type": "header",
                        "data": {
                            "level": 1,
                            "text": "Bench by the Landwehr Canal"
                        }
                    },
                    {
                        "type": "paragraph",
                        "data": {
                            "text": "A spot next to the walking path along the Canal with opportunities for spotting and listening to birds."
                        }
                    }
                ],
                "version": "2.19.0"
                }
                """.data(using: .utf8)

                let result = try? JSONDecoder().decode(ArticleBody.self, from: data!)
                expect(result?.blockContainer.blocks.count).to(equal(Int(2)))
                expect(result?.blockContainer.blocks[0].type).to(equal("header"))
                expect(result?.blockContainer.blocks[1].type).to(equal("paragraph"))
            }
        }

        describe("Article Body blocks") {
            it("should be able to deserialize a header of level 1") {
                let data: Data? = """
                {
                    "blocks": [{
                        "type": "header",
                        "data": {
                            "level": 1,
                            "text": "Bench by the Landwehr Canal"
                        }
                    }
                ]}
                """.data(using: .utf8)

                let result = try? JSONDecoder().decode(ArticleBody.self, from: data!)
                let header = result?.blockContainer.blocks[0].asHeader()

                expect(header!.level).to(equal(1))
                expect(header!.text).to(equal("Bench by the Landwehr Canal"))
            }

            it("should remove html tabs from headers") {
                let data: Data? = """
                {
                    "blocks": [{
                        "type": "header",
                        "data": {
                            "level": 1,
                            "text": "Bench by the Landwehr Canal<br>"
                        }
                    }
                ]}
                """.data(using: .utf8)

                let result = try? JSONDecoder().decode(ArticleBody.self, from: data!)
                let header = result?.blockContainer.blocks[0].asHeader()

                expect(header!.level).to(equal(1))
                expect(header!.text).to(equal("Bench by the Landwehr Canal"))
            }

            it("should be able to deserialize a paragraph") {
                let data: Data? = """
                {
                    "blocks": [{
                        "type": "paragraph",
                        "data": {
                            "text": "some text"
                        }
                    }
                ]}
                """.data(using: .utf8)

                let result = try? JSONDecoder().decode(ArticleBody.self, from: data!)
                let paragraph = result?.blockContainer.blocks[0].asParagraph()

                expect(paragraph!.text.pieces.count).to(equal(Int(1)))
                expect(paragraph!.text.pieces[0].text).to(equal("some text"))
            }

            it("should be able to deserialize a list") {
                let data: Data? = """
                {
                    "blocks": [{
                        "type": "list",
                        "data": {
                            "style": "unordered",
                            "items": [
                                "item1", "item2"
                            ]
                        }
                    }
                ]}
                """.data(using: .utf8)

                let result = try? JSONDecoder().decode(ArticleBody.self, from: data!)
                let list = result?.blockContainer.blocks[0].asList()

                expect(list!.items.count).to(equal(Int(2)))
                expect(list!.items[0]).to(equal("item1"))
            }

            it("should be able to deserialize a paragraph with html chars") {
                let data: Data? = """
                {
                    "blocks": [{
                        "type": "paragraph",
                        "data": {
                            "text": "some&nbsp;text"
                        }
                    }
                ]}
                """.data(using: .utf8)

                let result = try? JSONDecoder().decode(ArticleBody.self, from: data!)
                let paragraph = result?.blockContainer.blocks[0].asParagraph()

                expect(paragraph!.text.pieces.count).to(equal(Int(3)))
                expect(paragraph!.text.pieces[0].text).to(equal("some"))
                expect(paragraph!.text.pieces[1].text).to(equal(" "))
                expect(paragraph!.text.pieces[2].text).to(equal("text"))
            }

            it("should be able to deserialize string as paragraph") {
                let data: Data? = "\"string description of a site\"".data(using: .utf8)

                let result = try? JSONDecoder().decode(ArticleBody.self, from: data!)
                let paragraph = result?.blockContainer.blocks[0].asParagraph()

                expect(paragraph!.text.pieces.count).to(equal(Int(1)))
                expect(paragraph!.text.pieces[0].text).to(equal("string description of a site"))
            }
        }
    }
}
