import Foundation
import GEOSwift
@testable import GISCollective
import Nimble
import Quick

class Layouts: QuickSpec {
    override func spec() {
        describe("Deserialize a list") {
            var testApi: TestApi!

            beforeEach {
                testApi = TestApi()
                Api.instance = testApi
                Persistence.instance = Persistence(path: ":memory:")
            }

            it("should work for an empty list") {
                let store = Store()
                testApi?.setData(strData: "{ \"layouts\": [] }")

                waitUntil { done in
                    Task {
                        let result = try await store.layouts.getList()
                        expect(result.layouts.count).to(equal(0))
                        done()
                    }
                }
            }

            describe("when the list is not empty") {
                var layout = ""
                beforeEach {
                    layout = """
                    {
                        "_id": "613f8349d6bb4d6a6f67a074",
                        "rows": [],
                        "info": {
                          "changeIndex": 1,
                          "createdOn": "2021-08-27T16:17:48.529Z",
                          "lastChangeOn": "2021-08-27T16:17:48.529Z",
                          "originalAuthor": "",
                          "author": "5b870669796da25424540deb"
                        },
                        "canEdit": true,
                        "containers": [
                          {
                            "rows": [
                              {
                                "options": [],
                                "cols": [
                                  {
                                    "type": "",
                                    "options": []
                                  }
                                ]
                              },
                              {
                                "options": [],
                                "cols": [
                                  {
                                    "type": "",
                                    "options": []
                                  }
                                ]
                              },
                              {
                                "options": [],
                                "cols": [
                                  {
                                    "type": "",
                                    "options": []
                                  }
                                ]
                              }
                            ],
                            "options": [
                              "pt-5"
                            ]
                          }
                        ],
                        "name": "3-rows"
                      }
                    """
                }

                it("should work for a list with one element") {
                    let store = Store()
                    testApi!.setData(strData: "{ \"layouts\": [ \(layout) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.layouts.getList()
                            expect(result.layouts.count).to(equal(1))
                            expect(result.layouts[0].containers?.count).to(equal(1))
                            expect(result.layouts[0].name).to(equal("3-rows"))

                            done()
                        }
                    }
                }

                it("should work for a list with 2 elements") {
                    let store = Store()
                    testApi?.setData(strData: "{ \"layouts\": [ \(layout), \(layout) ] }")

                    waitUntil { done in
                        Task {
                            let result = try await store.layouts.getList()
                            expect(result.layouts.count).to(equal(2))
                            done()
                        }
                    }
                }
            }
        }
    }
}
