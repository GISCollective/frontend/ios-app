//
//  Api.swift
//  GISCollectiveTests
//
//  Created by Alexandra Casapu on 27.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

@testable import GISCollective
import Nimble
import Quick

class ApiTests: QuickSpec {
    override func spec() {
        describe("composeURL") {
            let testApi = Api(baseUrl: "www.blabla.org/api-v1/")

            it("should append lat and lon query params") {
                let url = testApi.composeUrl(modelName: "features", params: ["lat": "52.988505964292614", "lon": "13.378412134267341"])?.absoluteString
                expect(url).to(beginWith("www.blabla.org/api-v1/features?"))
                expect(url).to(contain("lat=52.988505964292614"))
                expect(url).to(contain("lon=13.378412134267341"))
            }
        }
    }
}
