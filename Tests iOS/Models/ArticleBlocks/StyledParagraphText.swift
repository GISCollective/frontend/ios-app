//
//  StyledParagraphText.swift
//  GISCollectiveTests
//
//  Created by Alexandra Casapu on 07.02.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation

@testable import GISCollective
import Nimble
import Quick

class StyledParagraphTextTests: QuickSpec {
    override func spec() {
        describe("StyledParagraphText") {
            it("parses an empty string to an empty list") {
                let text = StyledParagraphText(text: "")
                expect(text.pieces.count).to(equal(0))
            }

            it("parses plain string to a list with one element") {
                let text = StyledParagraphText(text: "text")
                expect(text.pieces.count).to(equal(1))
                expect(text.pieces[0].text).to(equal("text"))
                expect(text.pieces[0].isBold).to(equal(false))
                expect(text.pieces[0].isItalic).to(equal(false))
            }

            it("parses a bold string") {
                let text = StyledParagraphText(text: "<b>text</b>")
                expect(text.pieces.count).to(equal(1))
                expect(text.pieces[0].text).to(equal("text"))
                expect(text.pieces[0].isBold).to(equal(true))
            }

            it("parses nested bold tags") {
                let text = StyledParagraphText(text: "<b>t<b>ex</b>t</b>")
                expect(text.pieces.count).to(equal(3))
                expect(text.pieces[0].text).to(equal("t"))
                expect(text.pieces[1].text).to(equal("ex"))
                expect(text.pieces[2].text).to(equal("t"))

                expect(text.pieces[0].isBold).to(equal(true))
                expect(text.pieces[1].isBold).to(equal(true))
                expect(text.pieces[2].isBold).to(equal(true))
            }

            it("parses regular string followed by a bold string") {
                let text = StyledParagraphText(text: "some <b>text</b>")
                expect(text.pieces.count).to(equal(2))
                expect(text.pieces[0].text).to(equal("some "))
                expect(text.pieces[1].text).to(equal("text"))
                expect(text.pieces[1].isBold).to(equal(true))
            }

            it("parses regular string with a bold text in the middle") {
                let text = StyledParagraphText(text: "some <b>text</b> some")
                expect(text.pieces.count).to(equal(3))
                expect(text.pieces[0].text).to(equal("some "))

                expect(text.pieces[1].text).to(equal("text"))
                expect(text.pieces[1].isBold).to(equal(true))

                expect(text.pieces[2].text).to(equal(" some"))
                expect(text.pieces[2].isBold).to(equal(false))
            }

            it("parses an italic string") {
                let text = StyledParagraphText(text: "<i>text</i>")
                expect(text.pieces.count).to(equal(1))
                expect(text.pieces[0].text).to(equal("text"))
                expect(text.pieces[0].isItalic).to(equal(true))
            }

            it("parses a string that is both bold and italic") {
                let text = StyledParagraphText(text: "<b><i>text</i></b>")
                expect(text.pieces.count).to(equal(1))
                expect(text.pieces[0].text).to(equal("text"))
                expect(text.pieces[0].isItalic).to(equal(true))
                expect(text.pieces[0].isBold).to(equal(true))
            }

            it("parses a link") {
                let text = StyledParagraphText(text: "<a href='bla'>text</a>")
                expect(text.pieces.count).to(equal(1))
                expect(text.pieces[0].text).to(equal("text"))
                expect(text.pieces[0].isLink).to(equal(true))
                expect(text.pieces[0].href).to(equal("bla"))
            }

            it("ignores an invalid link") {
                let text = StyledParagraphText(text: "<a>text</a>")
                expect(text.pieces.count).to(equal(1))
                expect(text.pieces[0].text).to(equal("text"))
                expect(text.pieces[0].isLink).to(equal(false))
            }

            it("parses a misformatted string") {
                let text = StyledParagraphText(text: "<b><i>text</i> test")
                expect(text.pieces.count).to(equal(2))
                expect(text.pieces[0].text).to(equal("text"))
                expect(text.pieces[1].text).to(equal(" test"))

                expect(text.pieces[0].isItalic).to(equal(true))
                expect(text.pieces[0].isBold).to(equal(true))
            }

            it("parses interlaced tags") {
                let text = StyledParagraphText(text: "<i>te<b>xt</i> test</b>")
                expect(text.pieces.count).to(equal(2))
                expect(text.pieces[0].text).to(equal("te"))
                expect(text.pieces[1].text).to(equal("xt"))
            }
        }
    }
}
