//
//  Attributes.swift
//  GISCollectiveTests
//
//  Created by Bogdan Szabo on 14.07.21.
//  Copyright © 2021 GISCollective. All rights reserved.
//

import Foundation
@testable import GISCollective
import Nimble
import Quick

class Attributes: QuickSpec {
    override func spec() {
        describe("Serializing Attribute") {
            var encoder = JSONEncoder()

            beforeEach {
                encoder = JSONEncoder()
            }

            it("should serialize a string") {
                let attribute = Attribute.string("String value")
                let data = try? encoder.encode(attribute)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("\"String value\""))
            }

            it("should serialize a number") {
                let attribute = Attribute.number(4)
                let data = try? encoder.encode(attribute)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("4"))
            }

            it("should serialize a double") {
                let attribute = Attribute.double(6)
                let data = try? encoder.encode(attribute)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("6"))
            }

            it("should serialize a boolean") {
                let attribute = Attribute.bool(true)
                let data = try? encoder.encode(attribute)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("true"))
            }

            it("should serialize a null") {
                let attribute = Attribute.undefined
                let data = try? encoder.encode(attribute)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("null"))
            }
        }

        describe("Deserializing Attribute") {
            it("should deserialize a string") {
                let attribute = try? JSONDecoder().decode(Attribute.self, from: "\"String value\"".data(using: .utf8)!)
                var value = ""

                if case let .string(strValue) = attribute {
                    value = strValue
                }
                expect(value).to(equal("String value"))
            }

            it("should deserialize an int") {
                let attribute = try? JSONDecoder().decode(Attribute.self, from: "2".data(using: .utf8)!)
                var value = 0

                if case let .number(strValue) = attribute {
                    value = strValue
                }
                expect(value).to(equal(2))
            }

            it("should deserialize a double") {
                let attribute = try? JSONDecoder().decode(Attribute.self, from: "2.1".data(using: .utf8)!)
                var value = 0.0

                if case let .double(v) = attribute {
                    value = v
                }
                expect(value).to(equal(2.1))
            }

            it("should deserialize a bool") {
                let attribute = try? JSONDecoder().decode(Attribute.self, from: "true".data(using: .utf8)!)
                var value = false

                if case let .bool(v) = attribute {
                    value = v
                }

                expect(value).to(equal(true))
            }

            it("should not deserialize an undefined") {
                let attribute = try? JSONDecoder().decode(Attribute.self, from: "undefined".data(using: .utf8)!)

                expect(attribute).to(beNil())
            }

            it("should deserialize a null as undefined") {
                let attribute = try? JSONDecoder().decode(Attribute.self, from: "null".data(using: .utf8)!)
                var isSet = false

                if case .undefined = attribute {
                    isSet = true
                }

                expect(isSet).to(beTrue())
            }
        }

        describe("Serializing AttributeGroup") {
            var encoder = JSONEncoder()

            beforeEach {
                encoder = JSONEncoder()
            }

            it("should serialize an object") {
                let group = AttributeGroup.group(["key1": Attribute.number(1), "key2": Attribute.number(2)])

                let data = try? encoder.encode(group)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("{\"key1\":1,\"key2\":2}"))
            }

            it("should serialize a list") {
                let group = AttributeGroup.list([["key1": Attribute.number(1)], ["key2": Attribute.number(2)]])

                let data = try? encoder.encode(group)
                let value = String(data: data!, encoding: .utf8)!

                expect(value).to(equal("[{\"key1\":1},{\"key2\":2}]"))
            }
        }

        describe("Deserializing AttributeGroup") {
            it("should deserialize a group") {
                let group = try? JSONDecoder().decode(AttributeGroup.self, from: "{ \"key\": 1 }".data(using: .utf8)!)
                var value = 0

                if case let .group(g) = group, case let .number(v) = g["key"] {
                    value = v
                }
                expect(value).to(equal(1))
            }

            it("should deserialize a group list") {
                let list = try? JSONDecoder().decode(AttributeGroup.self, from: "[{ \"key\": 1 }]".data(using: .utf8)!)
                var value = 0

                if case let .list(l) = list, case let .number(v) = l[0]["key"] {
                    value = v
                }
                expect(value).to(equal(1))
            }
        }
    }
}
