//
//  GISCollectiveTests.swift
//  GISCollectiveTests
//
//  Created by Alexandra Casapu on 04.09.20.
//  Copyright © 2020 GISCollective. All rights reserved.
//

import CoreGraphics
import Foundation
@testable import GISCollective
import Nimble
import Quick

class NextPosition: QuickSpec {
    override func spec() {
        describe("nextPosition function") {
            it("should return the position of the first point as half of the size for both x and y") {
                let rect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(100), height: CGFloat(50))
                let point = nextPosition(index: 0, size: 40, distance: 10, bucketRect: rect)
                expect(point.x).to(equal(CGFloat(20)))
                expect(point.y).to(equal(CGFloat(20)))
            }

            it("should return the position of the second point applying the padding") {
                let rect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(100), height: CGFloat(50))
                let point = nextPosition(index: 1, size: 40, distance: 5, bucketRect: rect)
                expect(point.x).to(equal(CGFloat(65)))
                expect(point.y).to(equal(CGFloat(20)))
            }

            it("should return the position of the third point in a row applying the padding") {
                let rect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(200), height: CGFloat(50))
                let point = nextPosition(index: 2, size: 40, distance: 5, bucketRect: rect)
                expect(point.x).to(equal(CGFloat(110)))
                expect(point.y).to(equal(CGFloat(20)))
            }

            it("should do row wrapping when the next point fits on the next row") {
                let rect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(100), height: CGFloat(100))
                let point = nextPosition(index: 2, size: 40, distance: 5, bucketRect: rect)
                expect(point.x).to(equal(CGFloat(20)))
                expect(point.y).to(equal(CGFloat(65)))
            }

            it("should do row wrapping for a width of 382") {
                let rect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(382), height: CGFloat(164))
                let point = nextPosition(index: 6, size: 40, distance: 15, bucketRect: rect)
                expect(point.x).to(equal(CGFloat(20)))
                expect(point.y).to(equal(CGFloat(75)))
            }
        }
    }
}
